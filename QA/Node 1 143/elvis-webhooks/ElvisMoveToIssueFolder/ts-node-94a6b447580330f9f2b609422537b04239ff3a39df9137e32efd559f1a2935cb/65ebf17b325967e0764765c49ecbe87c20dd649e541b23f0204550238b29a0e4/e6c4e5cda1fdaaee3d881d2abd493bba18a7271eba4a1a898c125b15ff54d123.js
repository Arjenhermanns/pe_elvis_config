"use strict";
/**
 * Start web server and entry point for API requests.
 */
Object.defineProperty(exports, "__esModule", { value: true });
require("console-stamp")(console, { pattern: "dd-mm-yyyy HH:MM:ss.l" });
const express = require("express");
const bodyParser = require("body-parser");
const config_1 = require("./config");
const webhook_endpoint_1 = require("./app/webhook-endpoint");
/**
 * Singleton server class
 */
class Server {
    static getInstance() {
        return this.instance || (this.instance = new this());
    }
    constructor() {
        this.app = express();
        this.webhookEndPoint = new webhook_endpoint_1.WebhookEndpoint(this.app);
    }
    /**
     * Start the server
     *
     * @param port Server HTTP port.
     */
    start(port) {
        // Configure bodyParser
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        // Start server
        this.app.listen(port);
        console.info('Move to issue folder server started at port: ' + port);
        // Start listening for webhook events
        this.webhookEndPoint.addRoutes();
    }
}
let server = Server.getInstance();
server.start(config_1.Config.port);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL3NlcnZlci50cyIsInNvdXJjZXMiOlsiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL3NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBRUgsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxDQUFDLENBQUM7QUFDeEUsbUNBQW9DO0FBRXBDLDBDQUEyQztBQUMzQyxxQ0FBa0M7QUFDbEMsNkRBQXlEO0FBRXpEOztHQUVHO0FBQ0g7SUFJUyxNQUFNLENBQUMsV0FBVztRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFLRDtRQUNFLElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksS0FBSyxDQUFDLElBQVk7UUFDdkIsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRWhDLGVBQWU7UUFDZixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixPQUFPLENBQUMsSUFBSSxDQUFDLCtDQUErQyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBRXJFLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25DLENBQUM7Q0FDRjtBQUVELElBQUksTUFBTSxHQUFXLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUMxQyxNQUFNLENBQUMsS0FBSyxDQUFDLGVBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogU3RhcnQgd2ViIHNlcnZlciBhbmQgZW50cnkgcG9pbnQgZm9yIEFQSSByZXF1ZXN0cy5cbiAqL1xuXG5yZXF1aXJlKFwiY29uc29sZS1zdGFtcFwiKShjb25zb2xlLCB7IHBhdHRlcm46IFwiZGQtbW0teXl5eSBISDpNTTpzcy5sXCIgfSk7XG5pbXBvcnQgZXhwcmVzcyA9IHJlcXVpcmUoJ2V4cHJlc3MnKTtcbmltcG9ydCB7IEFwcGxpY2F0aW9uIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgYm9keVBhcnNlciA9IHJlcXVpcmUoJ2JvZHktcGFyc2VyJyk7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5pbXBvcnQgeyBXZWJob29rRW5kcG9pbnQgfSBmcm9tICcuL2FwcC93ZWJob29rLWVuZHBvaW50JztcblxuLyoqXG4gKiBTaW5nbGV0b24gc2VydmVyIGNsYXNzXG4gKi9cbmNsYXNzIFNlcnZlciB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IFNlcnZlcjtcblxuICBwdWJsaWMgc3RhdGljIGdldEluc3RhbmNlKCk6IFNlcnZlciB7XG4gICAgcmV0dXJuIHRoaXMuaW5zdGFuY2UgfHwgKHRoaXMuaW5zdGFuY2UgPSBuZXcgdGhpcygpKTtcbiAgfVxuXG4gIHByaXZhdGUgYXBwOiBBcHBsaWNhdGlvbjtcbiAgcHJpdmF0ZSB3ZWJob29rRW5kUG9pbnQ6IFdlYmhvb2tFbmRwb2ludDtcblxuICBwcml2YXRlIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuYXBwID0gZXhwcmVzcygpO1xuICAgIHRoaXMud2ViaG9va0VuZFBvaW50ID0gbmV3IFdlYmhvb2tFbmRwb2ludCh0aGlzLmFwcCk7XG4gIH1cblxuICAvKipcbiAgICogU3RhcnQgdGhlIHNlcnZlclxuICAgKiBcbiAgICogQHBhcmFtIHBvcnQgU2VydmVyIEhUVFAgcG9ydC5cbiAgICovXG4gIHB1YmxpYyBzdGFydChwb3J0OiBzdHJpbmcpOiB2b2lkIHtcbiAgICAvLyBDb25maWd1cmUgYm9keVBhcnNlclxuICAgIHRoaXMuYXBwLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoeyBleHRlbmRlZDogdHJ1ZSB9KSk7XG4gICAgdGhpcy5hcHAudXNlKGJvZHlQYXJzZXIuanNvbigpKTtcblxuICAgIC8vIFN0YXJ0IHNlcnZlclxuICAgIHRoaXMuYXBwLmxpc3Rlbihwb3J0KTtcbiAgICBjb25zb2xlLmluZm8oJ01vdmUgdG8gaXNzdWUgZm9sZGVyIHNlcnZlciBzdGFydGVkIGF0IHBvcnQ6ICcgKyBwb3J0KTtcblxuICAgIC8vIFN0YXJ0IGxpc3RlbmluZyBmb3Igd2ViaG9vayBldmVudHNcbiAgICB0aGlzLndlYmhvb2tFbmRQb2ludC5hZGRSb3V0ZXMoKTtcbiAgfVxufVxuXG5sZXQgc2VydmVyOiBTZXJ2ZXIgPSBTZXJ2ZXIuZ2V0SW5zdGFuY2UoKTtcbnNlcnZlci5zdGFydChDb25maWcucG9ydCk7Il19