import jayson = require('jayson');
import Promise = require('bluebird');

export class EnterpriseApi {

  private rpcClient: any;
  private ticket: string;
  private auth: Promise<any>;

  constructor(public url: string, public username: string, public password: string) {
    let endPoint: string = url += '?protocol=JSON';
    this.rpcClient = url.startsWith('https') ? jayson.client.https(endPoint) : jayson.client.http(endPoint);
    this.logOn();
  }

  public getObjects(params: any): Promise<any> {
    return this.runRequest('GetObjects', params);
  }

  public runRequest(operation: string, params: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      // Insert ticket
      params.Ticket = this.ticket;

      // Run request
      this.rpcClient.request(operation, [params], (err, response) => {
        let error = this.getError(err, response);
        if (error) {
          // Request failed
          if (error.message && error.message.search('S1043')) {
            if (!this.auth) {
              // Ticket is expired, log on again
              this.auth = this.logOn().then(() => {
                // Re-run original request
                this.auth = null;
                return this.runRequest(operation, params);
              });
              resolve(this.auth);
            }
            else {
              console.log('Already logging on to Enterprise, waiting for log-on to finish...');
              resolve(this.auth.then(() => {
                // Retry initial call
                return this.runRequest(operation, params);
              }));
            }
          }
          else {
            reject(new Error('Operation ' + operation + ' to Enterprise failed, with error:\n' + this.getErrorString(error)));
          }
        }
        else {
          // Request was successful, return results
          resolve(response.result);
        }
      });
    });
  }

  private logOn(): Promise<string> {
    let logOnRequest: any = {
      User: this.username,
      Password: this.password,
      ClientName: 'localhost',
      ClientAppName: 'web',
      ClientAppVersion: '10.0.0 build 0',
      RequestTicket: true
    }

    return new Promise<any>((resolve, reject) => {
      this.rpcClient.request('LogOn', [logOnRequest], (err, response) => {
        let error = this.getError(err, response);
        if (error) {
          return reject(new Error('LogOn to Enterprise failed:\n' + this.getErrorString(error)));
        }
        console.info('Logged in to Enterprise with ticket: ' + response.result.Ticket);
        this.ticket = response.result.Ticket;
        resolve();
      });
    })
  }

  private getError(error?, response?): any {
    if (error) {
      return error;
    }
    else if (response && response.error) {
      return response.error;
    }
    else {
      return null;
    }
  }

  private getErrorString(error?, response?): string {
    let err: any = this.getError(error, response);
    if (err.message) {
      return err.message;
    }
    else if (this.isObject(err)) {
      return JSON.stringify(err, null, 2);
    }
    else {
      return err;
    }
  }

  private isObject = function (a): boolean {
    return (!!a) && (a.constructor === Object);
  };
}