# 1. Introduction

Simple webhooks based integration that moves copied production Enterprise assets to an issue folder.

# 2. Prerequisites

- Fully installed and licensed Elvis server (5.26 or higher). You can obtain Elvis via: https://www.woodwing.com/en/digital-asset-management-system.
- Fully installed and licensed Enterprise Server (9.XXX or higher)
- Fully configured Elvis - Enterprise integration that needs to be configured to copy assets to a production zone, more info: https://helpcenter.woodwing.com/hc/en-us/articles/214453783
- Server where this integration can run (can be on the same machine where Elvis runs).
- Elvis API user license.

# 3. Installation steps

## 3.1 Configure the Elvis Webhook

An Elvis webhook needs to be configured to detect incoming archived images

- Log-in to the Elvis web client as admin user.
- Go to the management console, webhooks section and add a new webhook.
- Name: For example, "Move to issue folder".
- URL: Point it to the URL where this integration is running, if it's running on the same machine as Elvis, this will be: http://localhost:9092/
- Event type: `asset_update_metadata`.
- Metadata to include: `assetPath`.
- Save the webhook.
- The generated secret token needs to be specified in the configuration later on.

Detailed information on setting up and using webhooks can be found on [Help Center](https://helpcenter.woodwing.com/hc/en-us/articles/115001884346).

## 3.2 Install the server

- Clone or download this package.
- Open src/config.ts and configure the settings (Port where this server runs, Elvis Server settings, Enterprise Server settings, etc). You can either configure the settings in this config file or by setting environment variables. Note: the configured Elvis user needs to be an API user that can process all incoming webhook events.
- Install nodejs (6.9 or higher) from https://nodejs.org
- Open a terminal and go to the package folder.
- Install TypeScript (globally) via npm: `npm install -g typescript`
- Install node modules: `npm install`
- Start the server: `npm start`

# 4. Usage

- Drag an Elvis image to a dossier in Content Station; or
- Use the layout restore function in Enterprise - Smart Connection.
- Browse the production folder structure in Elvis which should show the assets in an issue folder (instead of the default root of the production folder)

# 5. Version history

## v1.1.0
- Add support for Elvis images added to Enterprise dossiers

## v1.0.0
- Initial version