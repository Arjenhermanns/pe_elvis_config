"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const compare = require("secure-compare");
const path = require("path");
const JSONPath = require("JSONPath");
const config_1 = require("../config");
const api_manager_1 = require("../elvis-api/api-manager");
const enterprise_api_1 = require("../enterprise-api/enterprise-api");
/**
 * Handle Elvis webhook events
 */
class WebhookEndpoint {
    constructor(app) {
        this.app = app;
        this.productionFolders = config_1.Config.productionFolders.split('|');
        this.enterpriseApi = new enterprise_api_1.EnterpriseApi(config_1.Config.enterpriseServerUrl, config_1.Config.enterpriseUsername, config_1.Config.enterprisePassword);
    }
    /**
     * Register HTTP Post route on '/' and listen for Elvis webhook events
     */
    addRoutes() {
        // Recognize API
        this.app.post('/', (req, res) => {
            // Send a response back to Elvis before handling the event, so the app won't keep 
            // the connection open while it's handling the event. This results in better performance.
            res.status(200).send();
            // Validate the webhook signature
            let signature = req.header('x-hook-signature');
            if (!this.validateSignature(signature, JSON.stringify(req.body))) {
                return console.error('Invalid WebHook signature: ' + signature + '. Make sure the elvisToken is configured correctly.');
            }
            // Handle the event. 
            this.handle(req.body);
        });
    }
    /**
     * Validate Elvis webhook signature
     *
     * @param signature Request header signature
     * @param data Request data to validate
     */
    validateSignature(signature, data) {
        try {
            let hmac = crypto.createHmac('sha256', config_1.Config.elvisToken);
            hmac.update(data);
            let digest = hmac.digest('hex');
            return compare(digest, signature);
        }
        catch (error) {
            console.warn('Signature validation failed for signature: ' + signature + ' and data: ' + data + '; details: ' + error);
        }
        return false;
    }
    /**
     * Handle Elvis webhook events.
     *
     * @param event The Elvis webhook event to handle
     */
    handle(event) {
        let isMetadataUpdateEvent = (event.type === 'asset_update_metadata');
        if (!isMetadataUpdateEvent) {
            console.warn('IGNORING EVENT, expecting "asset_update_metadata", received: "' + event.type + '"');
            return;
        }
        if (!event.metadata || !event.metadata.assetPath) {
            console.warn('IGNORING EVENT, expecting assetPath metadata field for: "' + event.assetId + '"');
            return;
        }
        let cm = event.changedMetadata;
        let sourceFolder = path.dirname(event.metadata.assetPath);
        if (!this.productionFolders.includes(sourceFolder) || !cm.sceId || !cm.sceId.newValue) {
            // We can ignore this event, since either:
            // A. The file is not in the root of a production zone; Or
            // B. The sceId isn't set to a new value
            return;
        }
        if (cm.sceIssue && !cm.sceIssue.oldValue && cm.sceIssue.newValue) {
            // sceIssue is set for the first time, let's use that as folder value
            this.moveAsset(event.assetId, event.metadata.assetPath, cm.sceIssue.newValue[0]);
        }
        else {
            // Wait here in order to make sure the object relations are also created in Enterprise (link to dossier)
            setTimeout(() => {
                // Get issue from Enterprise
                let request = {
                    IDs: [cm.sceId.newValue],
                    Lock: false,
                    Rendition: 'none'
                };
                this.enterpriseApi.getObjects(request).then((response) => {
                    // Get issue from dossier relation target
                    let issue = this.selectSingleString({ json: response, path: "$.Objects[0].Relations[?(@.ParentInfo.Type === 'Dossier')].Targets[0].Issue.Name" });
                    if (!issue) {
                        // Try to get the issue from the Object target (fallback)
                        issue = this.selectSingleString({ json: response, path: "$.Objects[0].Targets[0].Issue.Name" });
                    }
                    if (issue) {
                        // Search assetPath in Elvis, it might have changed since this call is delayed
                        api_manager_1.ApiManager.getApi().searchGet("id:" + event.assetId, 0, 1, "assetPath").then((sr) => {
                            if (!sr.hits || sr.hits.length == 0) {
                                console.error('MOVE ASSET FAILED: "' + event.assetId + '". The asset is no longer available in Elvis.');
                            }
                            else {
                                // Move asset, issue name was found in Enterprise Target info
                                this.moveAsset(event.assetId, sr.hits[0].metadata.assetPath, issue);
                            }
                        });
                    }
                    else {
                        console.error('MOVE ASSET FAILED: "' + event.assetId + '". No issue name found in Enterprise for sceId: ' + cm.sceId.newValue + '. GetObjects response:\n' + JSON.stringify(response, null, 2));
                    }
                }).catch((error) => {
                    console.error('MOVE ASSET FAILED: "' + event.assetId + '". Unable to retrieve issue information from Enterprise for sceId: ' + cm.sceId.newValue + '. Cause: ' + error.message);
                });
            }, config_1.Config.enterpriseGetObjectTimeout);
        }
    }
    selectSingleString(params) {
        let result = JSONPath(params);
        if (result.length == 1) {
            return result[0];
        }
        return null;
    }
    moveAsset(assetId, sourceAssetPath, issueName) {
        let sourceFolder = path.dirname(sourceAssetPath);
        let filename = path.basename(sourceAssetPath);
        let destinationAssetPath = path.join(sourceFolder, issueName, filename);
        api_manager_1.ApiManager.getApi().move(sourceAssetPath, destinationAssetPath).then((response) => {
            if (response.errorCount == 0) {
                console.info('MOVED ASSET: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '"');
            }
            else {
                console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Check the Elvis Server log files for details.');
            }
        }).catch((error) => {
            console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Cause: ' + error.message);
        });
    }
}
exports.WebhookEndpoint = WebhookEndpoint;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL2FwcC93ZWJob29rLWVuZHBvaW50LnRzIiwic291cmNlcyI6WyIvc3J2L0VsdmlzV2ViSG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvYXBwL3dlYmhvb2stZW5kcG9pbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQ0FBa0M7QUFDbEMsMENBQTJDO0FBQzNDLDZCQUE4QjtBQUM5QixxQ0FBc0M7QUFFdEMsc0NBQW1DO0FBQ25DLDBEQUFzRDtBQUN0RCxxRUFBZ0U7QUFJaEU7O0dBRUc7QUFDSDtJQUtFLFlBQW1CLEdBQWdCO1FBQWhCLFFBQUcsR0FBSCxHQUFHLENBQWE7UUFDakMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGVBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLDhCQUFhLENBQUMsZUFBTSxDQUFDLG1CQUFtQixFQUFFLGVBQU0sQ0FBQyxrQkFBa0IsRUFBRSxlQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUMzSCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxTQUFTO1FBQ2QsZ0JBQWdCO1FBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQVksRUFBRSxHQUFhO1lBRTdDLGtGQUFrRjtZQUNsRix5RkFBeUY7WUFDekYsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV2QixpQ0FBaUM7WUFDakMsSUFBSSxTQUFTLEdBQVcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEdBQUcsU0FBUyxHQUFHLHFEQUFxRCxDQUFDLENBQUM7WUFDMUgsQ0FBQztZQUVELHFCQUFxQjtZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLGlCQUFpQixDQUFDLFNBQWlCLEVBQUUsSUFBWTtRQUN2RCxJQUFJLENBQUM7WUFDSCxJQUFJLElBQUksR0FBZ0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsZUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBQ0QsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNiLE9BQU8sQ0FBQyxJQUFJLENBQUMsNkNBQTZDLEdBQUcsU0FBUyxHQUFHLGFBQWEsR0FBRyxJQUFJLEdBQUcsYUFBYSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ3pILENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVEOzs7O09BSUc7SUFDSyxNQUFNLENBQUMsS0FBVTtRQUN2QixJQUFJLHFCQUFxQixHQUFZLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyx1QkFBdUIsQ0FBQyxDQUFDO1FBRTlFLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0VBQWdFLEdBQUcsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNsRyxNQUFNLENBQUM7UUFDVCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pELE9BQU8sQ0FBQyxJQUFJLENBQUMsMkRBQTJELEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNoRyxNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQztRQUMvQixJQUFJLFlBQVksR0FBVyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN0RiwwQ0FBMEM7WUFDMUMsMERBQTBEO1lBQzFELHdDQUF3QztZQUN4QyxNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNqRSxxRUFBcUU7WUFDckUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkYsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osd0dBQXdHO1lBQ3hHLFVBQVUsQ0FBQztnQkFDVCw0QkFBNEI7Z0JBQzVCLElBQUksT0FBTyxHQUFRO29CQUNqQixHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztvQkFDeEIsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsU0FBUyxFQUFFLE1BQU07aUJBQ2xCLENBQUE7Z0JBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBYTtvQkFDeEQseUNBQXlDO29CQUN6QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxrRkFBa0YsRUFBRSxDQUFDLENBQUM7b0JBQ2xKLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDWCx5REFBeUQ7d0JBQ3pELEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxvQ0FBb0MsRUFBRSxDQUFDLENBQUM7b0JBQ2xHLENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDViw4RUFBOEU7d0JBQzlFLHdCQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBaUI7NEJBQzdGLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNwQyxPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsK0NBQStDLENBQUMsQ0FBQzs0QkFDMUcsQ0FBQzs0QkFDRCxJQUFJLENBQUEsQ0FBQztnQ0FDSCw2REFBNkQ7Z0NBQzdELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7NEJBQ3RFLENBQUM7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0wsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDSixPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsa0RBQWtELEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xNLENBQUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSztvQkFDYixPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcscUVBQXFFLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsV0FBVyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEwsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUUsZUFBTSxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDeEMsQ0FBQztJQUNILENBQUM7SUFFTyxrQkFBa0IsQ0FBQyxNQUFXO1FBQ3BDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyxTQUFTLENBQUMsT0FBZSxFQUFFLGVBQXVCLEVBQUUsU0FBaUI7UUFDM0UsSUFBSSxZQUFZLEdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6RCxJQUFJLFFBQVEsR0FBVyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RELElBQUksb0JBQW9CLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRWhGLHdCQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVE7WUFDNUUsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsZUFBZSxHQUFHLFNBQVMsR0FBRyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNwSCxDQUFDO1lBQ0QsSUFBSSxDQUFDLENBQUM7Z0JBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLGVBQWUsR0FBRyxTQUFTLEdBQUcsb0JBQW9CLEdBQUcsa0RBQWtELENBQUMsQ0FBQztZQUMxSyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBVTtZQUNsQixPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsZUFBZSxHQUFHLFNBQVMsR0FBRyxvQkFBb0IsR0FBRyxZQUFZLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BKLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBaEpELDBDQWdKQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjcnlwdG8gPSByZXF1aXJlKCdjcnlwdG8nKTtcbmltcG9ydCBjb21wYXJlID0gcmVxdWlyZSgnc2VjdXJlLWNvbXBhcmUnKTtcbmltcG9ydCBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xuaW1wb3J0IEpTT05QYXRoID0gcmVxdWlyZSgnSlNPTlBhdGgnKTtcblxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vY29uZmlnJztcbmltcG9ydCB7IEFwaU1hbmFnZXIgfSBmcm9tICcuLi9lbHZpcy1hcGkvYXBpLW1hbmFnZXInO1xuaW1wb3J0IHsgRW50ZXJwcmlzZUFwaSB9IGZyb20gJy4uL2VudGVycHJpc2UtYXBpL2VudGVycHJpc2UtYXBpJ1xuaW1wb3J0IHsgQXBwbGljYXRpb24sIFJlcXVlc3QsIFJlc3BvbnNlIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQge1NlYXJjaFJlc3BvbnNlfSBmcm9tICcuLi9lbHZpcy1hcGkvYXBpJztcblxuLyoqXG4gKiBIYW5kbGUgRWx2aXMgd2ViaG9vayBldmVudHNcbiAqL1xuZXhwb3J0IGNsYXNzIFdlYmhvb2tFbmRwb2ludCB7XG5cbiAgcHJpdmF0ZSBwcm9kdWN0aW9uRm9sZGVyczogc3RyaW5nW107XG4gIHByaXZhdGUgZW50ZXJwcmlzZUFwaTogRW50ZXJwcmlzZUFwaTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgYXBwOiBBcHBsaWNhdGlvbikge1xuICAgIHRoaXMucHJvZHVjdGlvbkZvbGRlcnMgPSBDb25maWcucHJvZHVjdGlvbkZvbGRlcnMuc3BsaXQoJ3wnKTtcbiAgICB0aGlzLmVudGVycHJpc2VBcGkgPSBuZXcgRW50ZXJwcmlzZUFwaShDb25maWcuZW50ZXJwcmlzZVNlcnZlclVybCwgQ29uZmlnLmVudGVycHJpc2VVc2VybmFtZSwgQ29uZmlnLmVudGVycHJpc2VQYXNzd29yZCk7XG4gIH1cblxuICAvKipcbiAgICogUmVnaXN0ZXIgSFRUUCBQb3N0IHJvdXRlIG9uICcvJyBhbmQgbGlzdGVuIGZvciBFbHZpcyB3ZWJob29rIGV2ZW50c1xuICAgKi9cbiAgcHVibGljIGFkZFJvdXRlcygpOiB2b2lkIHtcbiAgICAvLyBSZWNvZ25pemUgQVBJXG4gICAgdGhpcy5hcHAucG9zdCgnLycsIChyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpID0+IHtcblxuICAgICAgLy8gU2VuZCBhIHJlc3BvbnNlIGJhY2sgdG8gRWx2aXMgYmVmb3JlIGhhbmRsaW5nIHRoZSBldmVudCwgc28gdGhlIGFwcCB3b24ndCBrZWVwIFxuICAgICAgLy8gdGhlIGNvbm5lY3Rpb24gb3BlbiB3aGlsZSBpdCdzIGhhbmRsaW5nIHRoZSBldmVudC4gVGhpcyByZXN1bHRzIGluIGJldHRlciBwZXJmb3JtYW5jZS5cbiAgICAgIHJlcy5zdGF0dXMoMjAwKS5zZW5kKCk7XG5cbiAgICAgIC8vIFZhbGlkYXRlIHRoZSB3ZWJob29rIHNpZ25hdHVyZVxuICAgICAgbGV0IHNpZ25hdHVyZTogc3RyaW5nID0gcmVxLmhlYWRlcigneC1ob29rLXNpZ25hdHVyZScpO1xuICAgICAgaWYgKCF0aGlzLnZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZSwgSlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpKSkge1xuICAgICAgICByZXR1cm4gY29uc29sZS5lcnJvcignSW52YWxpZCBXZWJIb29rIHNpZ25hdHVyZTogJyArIHNpZ25hdHVyZSArICcuIE1ha2Ugc3VyZSB0aGUgZWx2aXNUb2tlbiBpcyBjb25maWd1cmVkIGNvcnJlY3RseS4nKTtcbiAgICAgIH1cblxuICAgICAgLy8gSGFuZGxlIHRoZSBldmVudC4gXG4gICAgICB0aGlzLmhhbmRsZShyZXEuYm9keSk7XG4gICAgfSk7XG5cbiAgfVxuXG4gIC8qKlxuICAgKiBWYWxpZGF0ZSBFbHZpcyB3ZWJob29rIHNpZ25hdHVyZVxuICAgKiBcbiAgICogQHBhcmFtIHNpZ25hdHVyZSBSZXF1ZXN0IGhlYWRlciBzaWduYXR1cmVcbiAgICogQHBhcmFtIGRhdGEgUmVxdWVzdCBkYXRhIHRvIHZhbGlkYXRlXG4gICAqL1xuICBwcml2YXRlIHZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZTogc3RyaW5nLCBkYXRhOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICB0cnkge1xuICAgICAgbGV0IGhtYWM6IGNyeXB0by5IbWFjID0gY3J5cHRvLmNyZWF0ZUhtYWMoJ3NoYTI1NicsIENvbmZpZy5lbHZpc1Rva2VuKTtcbiAgICAgIGhtYWMudXBkYXRlKGRhdGEpO1xuICAgICAgbGV0IGRpZ2VzdDogc3RyaW5nID0gaG1hYy5kaWdlc3QoJ2hleCcpO1xuICAgICAgcmV0dXJuIGNvbXBhcmUoZGlnZXN0LCBzaWduYXR1cmUpO1xuICAgIH1cbiAgICBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUud2FybignU2lnbmF0dXJlIHZhbGlkYXRpb24gZmFpbGVkIGZvciBzaWduYXR1cmU6ICcgKyBzaWduYXR1cmUgKyAnIGFuZCBkYXRhOiAnICsgZGF0YSArICc7IGRldGFpbHM6ICcgKyBlcnJvcik7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIYW5kbGUgRWx2aXMgd2ViaG9vayBldmVudHMuXG4gICAqIFxuICAgKiBAcGFyYW0gZXZlbnQgVGhlIEVsdmlzIHdlYmhvb2sgZXZlbnQgdG8gaGFuZGxlXG4gICAqL1xuICBwcml2YXRlIGhhbmRsZShldmVudDogYW55KTogdm9pZCB7XG4gICAgbGV0IGlzTWV0YWRhdGFVcGRhdGVFdmVudDogYm9vbGVhbiA9IChldmVudC50eXBlID09PSAnYXNzZXRfdXBkYXRlX21ldGFkYXRhJyk7XG5cbiAgICBpZiAoIWlzTWV0YWRhdGFVcGRhdGVFdmVudCkge1xuICAgICAgY29uc29sZS53YXJuKCdJR05PUklORyBFVkVOVCwgZXhwZWN0aW5nIFwiYXNzZXRfdXBkYXRlX21ldGFkYXRhXCIsIHJlY2VpdmVkOiBcIicgKyBldmVudC50eXBlICsgJ1wiJyk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICghZXZlbnQubWV0YWRhdGEgfHwgIWV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCkge1xuICAgICAgY29uc29sZS53YXJuKCdJR05PUklORyBFVkVOVCwgZXhwZWN0aW5nIGFzc2V0UGF0aCBtZXRhZGF0YSBmaWVsZCBmb3I6IFwiJyArIGV2ZW50LmFzc2V0SWQgKyAnXCInKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsZXQgY20gPSBldmVudC5jaGFuZ2VkTWV0YWRhdGE7XG4gICAgbGV0IHNvdXJjZUZvbGRlcjogc3RyaW5nID0gcGF0aC5kaXJuYW1lKGV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCk7XG4gICAgaWYgKCF0aGlzLnByb2R1Y3Rpb25Gb2xkZXJzLmluY2x1ZGVzKHNvdXJjZUZvbGRlcikgfHwgIWNtLnNjZUlkIHx8ICFjbS5zY2VJZC5uZXdWYWx1ZSkge1xuICAgICAgLy8gV2UgY2FuIGlnbm9yZSB0aGlzIGV2ZW50LCBzaW5jZSBlaXRoZXI6XG4gICAgICAvLyBBLiBUaGUgZmlsZSBpcyBub3QgaW4gdGhlIHJvb3Qgb2YgYSBwcm9kdWN0aW9uIHpvbmU7IE9yXG4gICAgICAvLyBCLiBUaGUgc2NlSWQgaXNuJ3Qgc2V0IHRvIGEgbmV3IHZhbHVlXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGNtLnNjZUlzc3VlICYmICFjbS5zY2VJc3N1ZS5vbGRWYWx1ZSAmJiBjbS5zY2VJc3N1ZS5uZXdWYWx1ZSkge1xuICAgICAgLy8gc2NlSXNzdWUgaXMgc2V0IGZvciB0aGUgZmlyc3QgdGltZSwgbGV0J3MgdXNlIHRoYXQgYXMgZm9sZGVyIHZhbHVlXG4gICAgICB0aGlzLm1vdmVBc3NldChldmVudC5hc3NldElkLCBldmVudC5tZXRhZGF0YS5hc3NldFBhdGgsIGNtLnNjZUlzc3VlLm5ld1ZhbHVlWzBdKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAvLyBXYWl0IGhlcmUgaW4gb3JkZXIgdG8gbWFrZSBzdXJlIHRoZSBvYmplY3QgcmVsYXRpb25zIGFyZSBhbHNvIGNyZWF0ZWQgaW4gRW50ZXJwcmlzZSAobGluayB0byBkb3NzaWVyKVxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIC8vIEdldCBpc3N1ZSBmcm9tIEVudGVycHJpc2VcbiAgICAgICAgbGV0IHJlcXVlc3Q6IGFueSA9IHtcbiAgICAgICAgICBJRHM6IFtjbS5zY2VJZC5uZXdWYWx1ZV0sXG4gICAgICAgICAgTG9jazogZmFsc2UsXG4gICAgICAgICAgUmVuZGl0aW9uOiAnbm9uZSdcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmVudGVycHJpc2VBcGkuZ2V0T2JqZWN0cyhyZXF1ZXN0KS50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgLy8gR2V0IGlzc3VlIGZyb20gZG9zc2llciByZWxhdGlvbiB0YXJnZXRcbiAgICAgICAgICBsZXQgaXNzdWUgPSB0aGlzLnNlbGVjdFNpbmdsZVN0cmluZyh7IGpzb246IHJlc3BvbnNlLCBwYXRoOiBcIiQuT2JqZWN0c1swXS5SZWxhdGlvbnNbPyhALlBhcmVudEluZm8uVHlwZSA9PT0gJ0Rvc3NpZXInKV0uVGFyZ2V0c1swXS5Jc3N1ZS5OYW1lXCIgfSk7XG4gICAgICAgICAgaWYgKCFpc3N1ZSkge1xuICAgICAgICAgICAgLy8gVHJ5IHRvIGdldCB0aGUgaXNzdWUgZnJvbSB0aGUgT2JqZWN0IHRhcmdldCAoZmFsbGJhY2spXG4gICAgICAgICAgICBpc3N1ZSA9IHRoaXMuc2VsZWN0U2luZ2xlU3RyaW5nKHsganNvbjogcmVzcG9uc2UsIHBhdGg6IFwiJC5PYmplY3RzWzBdLlRhcmdldHNbMF0uSXNzdWUuTmFtZVwiIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoaXNzdWUpIHtcbiAgICAgICAgICAgIC8vIFNlYXJjaCBhc3NldFBhdGggaW4gRWx2aXMsIGl0IG1pZ2h0IGhhdmUgY2hhbmdlZCBzaW5jZSB0aGlzIGNhbGwgaXMgZGVsYXllZFxuICAgICAgICAgICAgQXBpTWFuYWdlci5nZXRBcGkoKS5zZWFyY2hHZXQoXCJpZDpcIiArIGV2ZW50LmFzc2V0SWQsIDAsIDEsIFwiYXNzZXRQYXRoXCIpLnRoZW4oKHNyOlNlYXJjaFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgIGlmICghc3IuaGl0cyB8fCBzci5oaXRzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignTU9WRSBBU1NFVCBGQUlMRUQ6IFwiJyArIGV2ZW50LmFzc2V0SWQgKyAnXCIuIFRoZSBhc3NldCBpcyBubyBsb25nZXIgYXZhaWxhYmxlIGluIEVsdmlzLicpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2V7XG4gICAgICAgICAgICAgICAgLy8gTW92ZSBhc3NldCwgaXNzdWUgbmFtZSB3YXMgZm91bmQgaW4gRW50ZXJwcmlzZSBUYXJnZXQgaW5mb1xuICAgICAgICAgICAgICAgIHRoaXMubW92ZUFzc2V0KGV2ZW50LmFzc2V0SWQsIHNyLmhpdHNbMF0ubWV0YWRhdGEuYXNzZXRQYXRoLCBpc3N1ZSk7XG4gICAgICAgICAgICAgIH0gXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdNT1ZFIEFTU0VUIEZBSUxFRDogXCInICsgZXZlbnQuYXNzZXRJZCArICdcIi4gTm8gaXNzdWUgbmFtZSBmb3VuZCBpbiBFbnRlcnByaXNlIGZvciBzY2VJZDogJyArIGNtLnNjZUlkLm5ld1ZhbHVlICsgJy4gR2V0T2JqZWN0cyByZXNwb25zZTpcXG4nICsgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UsIG51bGwsIDIpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ01PVkUgQVNTRVQgRkFJTEVEOiBcIicgKyBldmVudC5hc3NldElkICsgJ1wiLiBVbmFibGUgdG8gcmV0cmlldmUgaXNzdWUgaW5mb3JtYXRpb24gZnJvbSBFbnRlcnByaXNlIGZvciBzY2VJZDogJyArIGNtLnNjZUlkLm5ld1ZhbHVlICsgJy4gQ2F1c2U6ICcgKyBlcnJvci5tZXNzYWdlKTtcbiAgICAgICAgfSk7XG4gICAgICB9LCBDb25maWcuZW50ZXJwcmlzZUdldE9iamVjdFRpbWVvdXQpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2VsZWN0U2luZ2xlU3RyaW5nKHBhcmFtczogYW55KTogc3RyaW5nIHtcbiAgICBsZXQgcmVzdWx0ID0gSlNPTlBhdGgocGFyYW1zKTtcbiAgICBpZiAocmVzdWx0Lmxlbmd0aCA9PSAxKSB7XG4gICAgICByZXR1cm4gcmVzdWx0WzBdO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByaXZhdGUgbW92ZUFzc2V0KGFzc2V0SWQ6IHN0cmluZywgc291cmNlQXNzZXRQYXRoOiBzdHJpbmcsIGlzc3VlTmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgbGV0IHNvdXJjZUZvbGRlcjogc3RyaW5nID0gcGF0aC5kaXJuYW1lKHNvdXJjZUFzc2V0UGF0aCk7XG4gICAgbGV0IGZpbGVuYW1lOiBzdHJpbmcgPSBwYXRoLmJhc2VuYW1lKHNvdXJjZUFzc2V0UGF0aCk7XG4gICAgbGV0IGRlc3RpbmF0aW9uQXNzZXRQYXRoOiBzdHJpbmcgPSBwYXRoLmpvaW4oc291cmNlRm9sZGVyLCBpc3N1ZU5hbWUsIGZpbGVuYW1lKTtcblxuICAgIEFwaU1hbmFnZXIuZ2V0QXBpKCkubW92ZShzb3VyY2VBc3NldFBhdGgsIGRlc3RpbmF0aW9uQXNzZXRQYXRoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgaWYgKHJlc3BvbnNlLmVycm9yQ291bnQgPT0gMCkge1xuICAgICAgICBjb25zb2xlLmluZm8oJ01PVkVEIEFTU0VUOiBcIicgKyBhc3NldElkICsgJ1wiIEZST006IFwiJyArIHNvdXJjZUFzc2V0UGF0aCArICdcIiBUTzogXCInICsgZGVzdGluYXRpb25Bc3NldFBhdGggKyAnXCInKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdNT1ZFIEFTU0VUIEZBSUxFRDogXCInICsgYXNzZXRJZCArICdcIiBGUk9NOiBcIicgKyBzb3VyY2VBc3NldFBhdGggKyAnXCIgVE86IFwiJyArIGRlc3RpbmF0aW9uQXNzZXRQYXRoICsgJ1wiLiBDaGVjayB0aGUgRWx2aXMgU2VydmVyIGxvZyBmaWxlcyBmb3IgZGV0YWlscy4nKTtcbiAgICAgIH1cbiAgICB9KS5jYXRjaCgoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgY29uc29sZS5lcnJvcignTU9WRSBBU1NFVCBGQUlMRUQ6IFwiJyArIGFzc2V0SWQgKyAnXCIgRlJPTTogXCInICsgc291cmNlQXNzZXRQYXRoICsgJ1wiIFRPOiBcIicgKyBkZXN0aW5hdGlvbkFzc2V0UGF0aCArICdcIi4gQ2F1c2U6ICcgKyBlcnJvci5tZXNzYWdlKTtcbiAgICB9KTtcbiAgfVxufVxuIl19