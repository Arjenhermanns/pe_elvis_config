"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jayson = require("jayson");
const Promise = require("bluebird");
class EnterpriseApi {
    constructor(url, username, password) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.isObject = function (a) {
            return (!!a) && (a.constructor === Object);
        };
        let endPoint = url += '?protocol=JSON';
        this.rpcClient = url.startsWith('https') ? jayson.client.https(endPoint) : jayson.client.http(endPoint);
        this.logOn();
    }
    getObjects(params) {
        return this.runRequest('GetObjects', params);
    }
    runRequest(operation, params) {
        return new Promise((resolve, reject) => {
            // Insert ticket
            params.Ticket = this.ticket;
            // Run request
            this.rpcClient.request(operation, [params], (err, response) => {
                let error = this.getError(err, response);
                if (error) {
                    // Request failed
                    if (error.message && error.message.search('S1043')) {
                        if (!this.auth) {
                            // Ticket is expired, log on again
                            this.auth = this.logOn().then(() => {
                                // Re-run original request
                                this.auth = null;
                                return this.runRequest(operation, params);
                            });
                            resolve(this.auth);
                        }
                        else {
                            console.log('Already logging on to Enterprise, waiting for log-on to finish...');
                            resolve(this.auth.then(() => {
                                // Retry initial call
                                return this.runRequest(operation, params);
                            }));
                        }
                    }
                    else {
                        reject(new Error('Operation ' + operation + ' to Enterprise failed, with error:\n' + this.getErrorString(error)));
                    }
                }
                else {
                    // Request was successful, return results
                    resolve(response.result);
                }
            });
        });
    }
    logOn() {
        let logOnRequest = {
            User: this.username,
            Password: this.password,
            ClientName: 'localhost',
            ClientAppName: 'web',
            ClientAppVersion: '10.0.0 build 0',
            RequestTicket: true
        };
        return new Promise((resolve, reject) => {
            this.rpcClient.request('LogOn', [logOnRequest], (err, response) => {
                let error = this.getError(err, response);
                if (error) {
                    return reject(new Error('LogOn to Enterprise failed:\n' + this.getErrorString(error)));
                }
                console.info('Logged in to Enterprise with ticket: ' + response.result.Ticket);
                this.ticket = response.result.Ticket;
                resolve();
            });
        });
    }
    getError(error, response) {
        if (error) {
            return error;
        }
        else if (response && response.error) {
            return response.error;
        }
        else {
            return null;
        }
    }
    getErrorString(error, response) {
        let err = this.getError(error, response);
        if (err.message) {
            return err.message;
        }
        else if (this.isObject(err)) {
            return JSON.stringify(err, null, 2);
        }
        else {
            return err;
        }
    }
}
exports.EnterpriseApi = EnterpriseApi;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL2VudGVycHJpc2UtYXBpL2VudGVycHJpc2UtYXBpLnRzIiwic291cmNlcyI6WyIvc3J2L0VsdmlzV2ViSG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvZW50ZXJwcmlzZS1hcGkvZW50ZXJwcmlzZS1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQ0FBa0M7QUFDbEMsb0NBQXFDO0FBRXJDO0lBTUUsWUFBbUIsR0FBVyxFQUFTLFFBQWdCLEVBQVMsUUFBZ0I7UUFBN0QsUUFBRyxHQUFILEdBQUcsQ0FBUTtRQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7UUFBUyxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBa0d4RSxhQUFRLEdBQUcsVUFBVSxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxDQUFDO1FBbkdBLElBQUksUUFBUSxHQUFXLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVNLFVBQVUsQ0FBQyxNQUFXO1FBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU0sVUFBVSxDQUFDLFNBQWlCLEVBQUUsTUFBVztRQUM5QyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUN0QyxnQkFBZ0I7WUFDaEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBRTVCLGNBQWM7WUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxRQUFRO2dCQUN4RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDekMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixpQkFBaUI7b0JBQ2pCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzRCQUNmLGtDQUFrQzs0QkFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDO2dDQUM1QiwwQkFBMEI7Z0NBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dDQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7NEJBQzVDLENBQUMsQ0FBQyxDQUFDOzRCQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3JCLENBQUM7d0JBQ0QsSUFBSSxDQUFDLENBQUM7NEJBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxtRUFBbUUsQ0FBQyxDQUFDOzRCQUNqRixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0NBQ3JCLHFCQUFxQjtnQ0FDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDOzRCQUM1QyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNOLENBQUM7b0JBQ0gsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDSixNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsWUFBWSxHQUFHLFNBQVMsR0FBRyxzQ0FBc0MsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEgsQ0FBQztnQkFDSCxDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLHlDQUF5QztvQkFDekMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDM0IsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sS0FBSztRQUNYLElBQUksWUFBWSxHQUFRO1lBQ3RCLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUTtZQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsVUFBVSxFQUFFLFdBQVc7WUFDdkIsYUFBYSxFQUFFLEtBQUs7WUFDcEIsZ0JBQWdCLEVBQUUsZ0JBQWdCO1lBQ2xDLGFBQWEsRUFBRSxJQUFJO1NBQ3BCLENBQUE7UUFFRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxRQUFRO2dCQUM1RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDekMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLCtCQUErQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6RixDQUFDO2dCQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsdUNBQXVDLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDckMsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVPLFFBQVEsQ0FBQyxLQUFNLEVBQUUsUUFBUztRQUNoQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1YsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNmLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQ3hCLENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO0lBQ0gsQ0FBQztJQUVPLGNBQWMsQ0FBQyxLQUFNLEVBQUUsUUFBUztRQUN0QyxJQUFJLEdBQUcsR0FBUSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM5QyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNoQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUNyQixDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNiLENBQUM7SUFDSCxDQUFDO0NBS0Y7QUEzR0Qsc0NBMkdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGpheXNvbiA9IHJlcXVpcmUoJ2pheXNvbicpO1xuaW1wb3J0IFByb21pc2UgPSByZXF1aXJlKCdibHVlYmlyZCcpO1xuXG5leHBvcnQgY2xhc3MgRW50ZXJwcmlzZUFwaSB7XG5cbiAgcHJpdmF0ZSBycGNDbGllbnQ6IGFueTtcbiAgcHJpdmF0ZSB0aWNrZXQ6IHN0cmluZztcbiAgcHJpdmF0ZSBhdXRoOiBQcm9taXNlPGFueT47XG5cbiAgY29uc3RydWN0b3IocHVibGljIHVybDogc3RyaW5nLCBwdWJsaWMgdXNlcm5hbWU6IHN0cmluZywgcHVibGljIHBhc3N3b3JkOiBzdHJpbmcpIHtcbiAgICBsZXQgZW5kUG9pbnQ6IHN0cmluZyA9IHVybCArPSAnP3Byb3RvY29sPUpTT04nO1xuICAgIHRoaXMucnBjQ2xpZW50ID0gdXJsLnN0YXJ0c1dpdGgoJ2h0dHBzJykgPyBqYXlzb24uY2xpZW50Lmh0dHBzKGVuZFBvaW50KSA6IGpheXNvbi5jbGllbnQuaHR0cChlbmRQb2ludCk7XG4gICAgdGhpcy5sb2dPbigpO1xuICB9XG5cbiAgcHVibGljIGdldE9iamVjdHMocGFyYW1zOiBhbnkpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLnJ1blJlcXVlc3QoJ0dldE9iamVjdHMnLCBwYXJhbXMpO1xuICB9XG5cbiAgcHVibGljIHJ1blJlcXVlc3Qob3BlcmF0aW9uOiBzdHJpbmcsIHBhcmFtczogYW55KTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAvLyBJbnNlcnQgdGlja2V0XG4gICAgICBwYXJhbXMuVGlja2V0ID0gdGhpcy50aWNrZXQ7XG5cbiAgICAgIC8vIFJ1biByZXF1ZXN0XG4gICAgICB0aGlzLnJwY0NsaWVudC5yZXF1ZXN0KG9wZXJhdGlvbiwgW3BhcmFtc10sIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGxldCBlcnJvciA9IHRoaXMuZ2V0RXJyb3IoZXJyLCByZXNwb25zZSk7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIC8vIFJlcXVlc3QgZmFpbGVkXG4gICAgICAgICAgaWYgKGVycm9yLm1lc3NhZ2UgJiYgZXJyb3IubWVzc2FnZS5zZWFyY2goJ1MxMDQzJykpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5hdXRoKSB7XG4gICAgICAgICAgICAgIC8vIFRpY2tldCBpcyBleHBpcmVkLCBsb2cgb24gYWdhaW5cbiAgICAgICAgICAgICAgdGhpcy5hdXRoID0gdGhpcy5sb2dPbigpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIC8vIFJlLXJ1biBvcmlnaW5hbCByZXF1ZXN0XG4gICAgICAgICAgICAgICAgdGhpcy5hdXRoID0gbnVsbDtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ydW5SZXF1ZXN0KG9wZXJhdGlvbiwgcGFyYW1zKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHJlc29sdmUodGhpcy5hdXRoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZygnQWxyZWFkeSBsb2dnaW5nIG9uIHRvIEVudGVycHJpc2UsIHdhaXRpbmcgZm9yIGxvZy1vbiB0byBmaW5pc2guLi4nKTtcbiAgICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLmF1dGgudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucnVuUmVxdWVzdChvcGVyYXRpb24sIHBhcmFtcyk7XG4gICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZWplY3QobmV3IEVycm9yKCdPcGVyYXRpb24gJyArIG9wZXJhdGlvbiArICcgdG8gRW50ZXJwcmlzZSBmYWlsZWQsIHdpdGggZXJyb3I6XFxuJyArIHRoaXMuZ2V0RXJyb3JTdHJpbmcoZXJyb3IpKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIC8vIFJlcXVlc3Qgd2FzIHN1Y2Nlc3NmdWwsIHJldHVybiByZXN1bHRzXG4gICAgICAgICAgcmVzb2x2ZShyZXNwb25zZS5yZXN1bHQpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgbG9nT24oKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgbG9nT25SZXF1ZXN0OiBhbnkgPSB7XG4gICAgICBVc2VyOiB0aGlzLnVzZXJuYW1lLFxuICAgICAgUGFzc3dvcmQ6IHRoaXMucGFzc3dvcmQsXG4gICAgICBDbGllbnROYW1lOiAnbG9jYWxob3N0JyxcbiAgICAgIENsaWVudEFwcE5hbWU6ICd3ZWInLFxuICAgICAgQ2xpZW50QXBwVmVyc2lvbjogJzEwLjAuMCBidWlsZCAwJyxcbiAgICAgIFJlcXVlc3RUaWNrZXQ6IHRydWVcbiAgICB9XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLnJwY0NsaWVudC5yZXF1ZXN0KCdMb2dPbicsIFtsb2dPblJlcXVlc3RdLCAoZXJyLCByZXNwb25zZSkgPT4ge1xuICAgICAgICBsZXQgZXJyb3IgPSB0aGlzLmdldEVycm9yKGVyciwgcmVzcG9uc2UpO1xuICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KG5ldyBFcnJvcignTG9nT24gdG8gRW50ZXJwcmlzZSBmYWlsZWQ6XFxuJyArIHRoaXMuZ2V0RXJyb3JTdHJpbmcoZXJyb3IpKSk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc29sZS5pbmZvKCdMb2dnZWQgaW4gdG8gRW50ZXJwcmlzZSB3aXRoIHRpY2tldDogJyArIHJlc3BvbnNlLnJlc3VsdC5UaWNrZXQpO1xuICAgICAgICB0aGlzLnRpY2tldCA9IHJlc3BvbnNlLnJlc3VsdC5UaWNrZXQ7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0pO1xuICAgIH0pXG4gIH1cblxuICBwcml2YXRlIGdldEVycm9yKGVycm9yPywgcmVzcG9uc2U/KTogYW55IHtcbiAgICBpZiAoZXJyb3IpIHtcbiAgICAgIHJldHVybiBlcnJvcjtcbiAgICB9XG4gICAgZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuZXJyb3IpIHtcbiAgICAgIHJldHVybiByZXNwb25zZS5lcnJvcjtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGdldEVycm9yU3RyaW5nKGVycm9yPywgcmVzcG9uc2U/KTogc3RyaW5nIHtcbiAgICBsZXQgZXJyOiBhbnkgPSB0aGlzLmdldEVycm9yKGVycm9yLCByZXNwb25zZSk7XG4gICAgaWYgKGVyci5tZXNzYWdlKSB7XG4gICAgICByZXR1cm4gZXJyLm1lc3NhZ2U7XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoaXMuaXNPYmplY3QoZXJyKSkge1xuICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGVyciwgbnVsbCwgMik7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcmV0dXJuIGVycjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGlzT2JqZWN0ID0gZnVuY3Rpb24gKGEpOiBib29sZWFuIHtcbiAgICByZXR1cm4gKCEhYSkgJiYgKGEuY29uc3RydWN0b3IgPT09IE9iamVjdCk7XG4gIH07XG59Il19