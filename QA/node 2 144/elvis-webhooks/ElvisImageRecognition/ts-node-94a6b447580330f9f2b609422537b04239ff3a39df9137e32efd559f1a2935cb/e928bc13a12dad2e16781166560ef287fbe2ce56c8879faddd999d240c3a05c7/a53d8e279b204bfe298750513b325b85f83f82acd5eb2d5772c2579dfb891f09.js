"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const api_1 = require("../elvis-api/api");
/**
 * Singleton class that ensures only one Elvis API session is used.
 */
class ApiManager {
    /**
     * Create an Elvis API instance
     */
    static getApi() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance.api;
    }
    constructor() {
        // Log into Elvis
        this.api = new api_1.ElvisApi(config_1.Config.elvisUsername, config_1.Config.elvisPassword, config_1.Config.elvisUrl);
    }
}
exports.ApiManager = ApiManager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvZWx2aXMtYXBpL2FwaS1tYW5hZ2VyLnRzIiwic291cmNlcyI6WyIvc3J2L0VsdmlzV2ViSG9va3MvRWx2aXNJbWFnZVJlY29nbml0aW9uL3NyYy9lbHZpcy1hcGkvYXBpLW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBbUM7QUFDbkMsMENBQTRDO0FBRTVDOztHQUVHO0FBQ0g7SUFNRTs7T0FFRztJQUNJLE1BQU0sQ0FBQyxNQUFNO1FBQ2xCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQzdCLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7SUFDM0IsQ0FBQztJQUVEO1FBQ0UsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxjQUFRLENBQUMsZUFBTSxDQUFDLGFBQWEsRUFBRSxlQUFNLENBQUMsYUFBYSxFQUFFLGVBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN2RixDQUFDO0NBRUY7QUFyQkQsZ0NBcUJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vY29uZmlnJztcbmltcG9ydCB7IEVsdmlzQXBpIH0gZnJvbSAnLi4vZWx2aXMtYXBpL2FwaSc7XG5cbi8qKlxuICogU2luZ2xldG9uIGNsYXNzIHRoYXQgZW5zdXJlcyBvbmx5IG9uZSBFbHZpcyBBUEkgc2Vzc2lvbiBpcyB1c2VkLlxuICovXG5leHBvcnQgY2xhc3MgQXBpTWFuYWdlciB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IEFwaU1hbmFnZXI7XG5cbiAgcHVibGljIGFwaTogRWx2aXNBcGk7XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhbiBFbHZpcyBBUEkgaW5zdGFuY2VcbiAgICovXG4gIHB1YmxpYyBzdGF0aWMgZ2V0QXBpKCk6IEVsdmlzQXBpIHtcbiAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHtcbiAgICAgIHRoaXMuaW5zdGFuY2UgPSBuZXcgdGhpcygpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5pbnN0YW5jZS5hcGk7XG4gIH1cblxuICBwcml2YXRlIGNvbnN0cnVjdG9yKCkge1xuICAgIC8vIExvZyBpbnRvIEVsdmlzXG4gICAgdGhpcy5hcGkgPSBuZXcgRWx2aXNBcGkoQ29uZmlnLmVsdmlzVXNlcm5hbWUsIENvbmZpZy5lbHZpc1Bhc3N3b3JkLCBDb25maWcuZWx2aXNVcmwpO1xuICB9XG5cbn0iXX0=