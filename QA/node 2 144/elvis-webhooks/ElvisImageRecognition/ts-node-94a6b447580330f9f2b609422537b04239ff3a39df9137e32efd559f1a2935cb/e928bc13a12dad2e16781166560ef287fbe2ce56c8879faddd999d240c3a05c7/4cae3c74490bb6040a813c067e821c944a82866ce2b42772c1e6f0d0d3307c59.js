"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const ClarifaiAPI = require("clarifai");
const config_1 = require("../../config");
const utils_1 = require("../utils");
const service_response_1 = require("./service-response");
/**
 * Uses the Clarifai API to detect labels in the given file.
 */
class Clarifai {
    constructor() {
        this.readFile = Promise.promisify(require("fs").readFile);
        this.detectSettings = { maxConcepts: 20, minValue: 0.85 };
        this.clarifai = new ClarifaiAPI.App({ apiKey: config_1.Config.clarifaiAPIKey });
    }
    /**
     * Detect tags
     *
     * @param inputFile Full path to the image to analyze
     */
    detect(inputFile, models = null, assetPath = null) {
        if (!models || models.length == 0) {
            models = assetPath ? this.findModelForPath(assetPath) : [ClarifaiAPI.GENERAL_MODEL];
        }
        return this.readFile(inputFile).then((data) => {
            let base64data = new Buffer(data).toString('base64');
            let promises = [];
            let sr = new service_response_1.ServiceResponse();
            models.forEach((model) => {
                switch (model) {
                    case ClarifaiAPI.GENERAL_MODEL:
                    case ClarifaiAPI.FOOD_MODEL:
                    case ClarifaiAPI.TRAVEL_MODEL:
                    case ClarifaiAPI.WEDDING_MODEL:
                    case ClarifaiAPI.APPAREL_MODEL:
                        promises.push(this.detectTags(base64data, model));
                        break;
                    case 'e466caa0619f444ab97497640cefc4dc':// Celebrity model
                        promises.push(this.detectCelebrities(sr, base64data, model));
                        break;
                    default:
                        throw new Error('Unsupported Clarifai model: ' + model);
                }
            });
            return Promise.all(promises).then((responses) => {
                // Consolidate tags into one service response
                responses.forEach((tags) => {
                    sr.tags = utils_1.Utils.mergeArrays(sr.tags, tags);
                });
                if (config_1.Config.clarifaiTagsField && sr.tags.length > 0) {
                    sr.metadata[config_1.Config.clarifaiTagsField] = sr.tags.join(',');
                }
                return Promise.resolve(sr);
            });
        });
    }
    findModelForPath(assetPath) {
        let modelsMapping = config_1.Config.clarifaiFolderToModelMapping.find((mapping) => {
            return assetPath.startsWith(mapping.folder);
        });
        return modelsMapping ? modelsMapping.models : [ClarifaiAPI.GENERAL_MODEL];
    }
    detectTags(data, model) {
        return new Promise((resolve, reject) => {
            this.clarifai.models.predict(model, { base64: data }, this.detectSettings).then((response) => {
                let resData = this.getResponseData(response);
                if (resData && resData.concepts) {
                    resolve(this.getTagsFromConcepts(resData.concepts));
                }
                else {
                    resolve([]);
                }
            }).catch((error) => {
                reject(new Error('An error occurred while getting labels from Clarifai: ' + JSON.stringify(error.data.status, null, 2)));
            });
        });
    }
    detectCelebrities(sr, data, model) {
        return new Promise((resolve, reject) => {
            this.clarifai.models.predict(model, { base64: data }, this.detectSettings).then((response) => {
                let resData = this.getResponseData(response);
                if (!resData || !resData.regions) {
                    return resolve();
                }
                let regions = resData.regions;
                let celebs = [];
                regions.forEach(region => {
                    if (region.data && region.data.face && region.data.face.identity && region.data.face.identity.concepts) {
                        let concepts = region.data.face.identity.concepts;
                        celebs = utils_1.Utils.mergeArrays(celebs, this.getTagsFromConcepts(concepts, false));
                    }
                });
                if (celebs.length > 0) {
                    sr.metadata['subjectPerson'] = celebs.join(', ');
                }
                resolve();
            }).catch((error) => {
                reject(new Error('An error occurred while getting celebrity info from Clarifai: ' + JSON.stringify(error.data.status, null, 2)));
            });
        });
    }
    getTagsFromConcepts(concepts, lowercase = true) {
        let tags = [];
        concepts.forEach(concept => {
            let tag = lowercase ? concept.name.toLowerCase() : concept.name;
            tags.push(tag);
        });
        return tags;
    }
    getResponseData(response) {
        if (response.outputs && response.outputs.length > 0 && response.outputs[0].data) {
            return response.outputs[0].data;
        }
        return null;
    }
}
exports.Clarifai = Clarifai;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvY2xhcmlmYWkudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9zZXJ2aWNlL2NsYXJpZmFpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0NBQXFDO0FBQ3JDLHdDQUF5QztBQUN6Qyx5Q0FBc0M7QUFDdEMsb0NBQWlDO0FBQ2pDLHlEQUFxRDtBQUVyRDs7R0FFRztBQUNIO0lBTUU7UUFIUSxhQUFRLEdBQWEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0QsbUJBQWMsR0FBUSxFQUFFLFdBQVcsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO1FBR2hFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUUsTUFBTSxFQUFFLGVBQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLFNBQWlCLEVBQUUsU0FBbUIsSUFBSSxFQUFFLFlBQW9CLElBQUk7UUFDaEYsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEYsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQ3BELElBQUksVUFBVSxHQUFXLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3RCxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxrQ0FBZSxFQUFFLENBQUM7WUFDL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQWEsRUFBRSxFQUFFO2dCQUMvQixNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNkLEtBQUssV0FBVyxDQUFDLGFBQWEsQ0FBQztvQkFDL0IsS0FBSyxXQUFXLENBQUMsVUFBVSxDQUFDO29CQUM1QixLQUFLLFdBQVcsQ0FBQyxZQUFZLENBQUM7b0JBQzlCLEtBQUssV0FBVyxDQUFDLGFBQWEsQ0FBQztvQkFDL0IsS0FBSyxXQUFXLENBQUMsYUFBYTt3QkFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNsRCxLQUFLLENBQUM7b0JBQ1IsS0FBSyxrQ0FBa0MsQ0FBRSxrQkFBa0I7d0JBQ3pELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDN0QsS0FBSyxDQUFDO29CQUNSO3dCQUNFLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLEdBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQzVELENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQW1CLEVBQUUsRUFBRTtnQkFDeEQsNkNBQTZDO2dCQUM3QyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7b0JBQ3pCLEVBQUUsQ0FBQyxJQUFJLEdBQUcsYUFBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUM3QyxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsaUJBQWlCLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbkQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxlQUFNLENBQUMsaUJBQWlCLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUQsQ0FBQztnQkFDRCxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGdCQUFnQixDQUFDLFNBQWlCO1FBQ3hDLElBQUksYUFBYSxHQUFHLGVBQU0sQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUN2RSxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRU8sVUFBVSxDQUFDLElBQVksRUFBRSxLQUFVO1FBQ3pDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBVyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtnQkFDaEcsSUFBSSxPQUFPLEdBQVEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEQsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDZCxDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7Z0JBQ3RCLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyx3REFBd0QsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxpQkFBaUIsQ0FBQyxFQUFtQixFQUFFLElBQVksRUFBRSxLQUFVO1FBQ3JFLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBTyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtnQkFDaEcsSUFBSSxPQUFPLEdBQVEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDakMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNuQixDQUFDO2dCQUNELElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0JBQzlCLElBQUksTUFBTSxHQUFhLEVBQUUsQ0FBQztnQkFDMUIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDdkIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBQ3ZHLElBQUksUUFBUSxHQUFRLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7d0JBQ3ZELE1BQU0sR0FBRyxhQUFLLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ2hGLENBQUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN0QixFQUFFLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25ELENBQUM7Z0JBQ0QsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFVLEVBQUUsRUFBRTtnQkFDdEIsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLGdFQUFnRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuSSxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLG1CQUFtQixDQUFDLFFBQWEsRUFBRSxZQUFxQixJQUFJO1FBQ2xFLElBQUksSUFBSSxHQUFhLEVBQUUsQ0FBQztRQUN4QixRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3pCLElBQUksR0FBRyxHQUFXLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUN4RSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyxlQUFlLENBQUMsUUFBYTtRQUNuQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDaEYsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2xDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztDQUNGO0FBcEhELDRCQW9IQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9taXNlID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcbmltcG9ydCBDbGFyaWZhaUFQSSA9IHJlcXVpcmUoJ2NsYXJpZmFpJyk7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuaW1wb3J0IHsgVXRpbHMgfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQgeyBTZXJ2aWNlUmVzcG9uc2UgfSBmcm9tICcuL3NlcnZpY2UtcmVzcG9uc2UnO1xuXG4vKipcbiAqIFVzZXMgdGhlIENsYXJpZmFpIEFQSSB0byBkZXRlY3QgbGFiZWxzIGluIHRoZSBnaXZlbiBmaWxlLlxuICovXG5leHBvcnQgY2xhc3MgQ2xhcmlmYWkge1xuXG4gIHByaXZhdGUgY2xhcmlmYWk6IENsYXJpZmFpQVBJLkFwcDtcbiAgcHJpdmF0ZSByZWFkRmlsZTogRnVuY3Rpb24gPSBQcm9taXNlLnByb21pc2lmeShyZXF1aXJlKFwiZnNcIikucmVhZEZpbGUpO1xuICBwcml2YXRlIGRldGVjdFNldHRpbmdzOiBhbnkgPSB7IG1heENvbmNlcHRzOiAyMCwgbWluVmFsdWU6IDAuODUgfTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNsYXJpZmFpID0gbmV3IENsYXJpZmFpQVBJLkFwcCh7IGFwaUtleTogQ29uZmlnLmNsYXJpZmFpQVBJS2V5IH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVjdCB0YWdzXG4gICAqIFxuICAgKiBAcGFyYW0gaW5wdXRGaWxlIEZ1bGwgcGF0aCB0byB0aGUgaW1hZ2UgdG8gYW5hbHl6ZVxuICAgKi9cbiAgcHVibGljIGRldGVjdChpbnB1dEZpbGU6IHN0cmluZywgbW9kZWxzOiBzdHJpbmdbXSA9IG51bGwsIGFzc2V0UGF0aDogc3RyaW5nID0gbnVsbCk6IFByb21pc2U8U2VydmljZVJlc3BvbnNlPiB7XG4gICAgaWYgKCFtb2RlbHMgfHwgbW9kZWxzLmxlbmd0aCA9PSAwKSB7XG4gICAgICBtb2RlbHMgPSBhc3NldFBhdGggPyB0aGlzLmZpbmRNb2RlbEZvclBhdGgoYXNzZXRQYXRoKSA6IFtDbGFyaWZhaUFQSS5HRU5FUkFMX01PREVMXTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMucmVhZEZpbGUoaW5wdXRGaWxlKS50aGVuKChkYXRhOiBCdWZmZXIpID0+IHtcbiAgICAgIGxldCBiYXNlNjRkYXRhOiBzdHJpbmcgPSBuZXcgQnVmZmVyKGRhdGEpLnRvU3RyaW5nKCdiYXNlNjQnKTtcbiAgICAgIGxldCBwcm9taXNlcyA9IFtdO1xuICAgICAgbGV0IHNyID0gbmV3IFNlcnZpY2VSZXNwb25zZSgpO1xuICAgICAgbW9kZWxzLmZvckVhY2goKG1vZGVsOiBzdHJpbmcpID0+IHtcbiAgICAgICAgc3dpdGNoIChtb2RlbCkge1xuICAgICAgICAgIGNhc2UgQ2xhcmlmYWlBUEkuR0VORVJBTF9NT0RFTDpcbiAgICAgICAgICBjYXNlIENsYXJpZmFpQVBJLkZPT0RfTU9ERUw6XG4gICAgICAgICAgY2FzZSBDbGFyaWZhaUFQSS5UUkFWRUxfTU9ERUw6XG4gICAgICAgICAgY2FzZSBDbGFyaWZhaUFQSS5XRURESU5HX01PREVMOlxuICAgICAgICAgIGNhc2UgQ2xhcmlmYWlBUEkuQVBQQVJFTF9NT0RFTDpcbiAgICAgICAgICAgIHByb21pc2VzLnB1c2godGhpcy5kZXRlY3RUYWdzKGJhc2U2NGRhdGEsIG1vZGVsKSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlICdlNDY2Y2FhMDYxOWY0NDRhYjk3NDk3NjQwY2VmYzRkYyc6IC8vIENlbGVicml0eSBtb2RlbFxuICAgICAgICAgICAgcHJvbWlzZXMucHVzaCh0aGlzLmRldGVjdENlbGVicml0aWVzKHNyLCBiYXNlNjRkYXRhLCBtb2RlbCkpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQgQ2xhcmlmYWkgbW9kZWw6ICcgKyBtb2RlbCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIFByb21pc2UuYWxsKHByb21pc2VzKS50aGVuKChyZXNwb25zZXM6IHN0cmluZ1tdKSA9PiB7XG4gICAgICAgIC8vIENvbnNvbGlkYXRlIHRhZ3MgaW50byBvbmUgc2VydmljZSByZXNwb25zZVxuICAgICAgICByZXNwb25zZXMuZm9yRWFjaCgodGFncykgPT4ge1xuICAgICAgICAgIHNyLnRhZ3MgPSBVdGlscy5tZXJnZUFycmF5cyhzci50YWdzLCB0YWdzKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKENvbmZpZy5jbGFyaWZhaVRhZ3NGaWVsZCAmJiBzci50YWdzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBzci5tZXRhZGF0YVtDb25maWcuY2xhcmlmYWlUYWdzRmllbGRdID0gc3IudGFncy5qb2luKCcsJyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShzcik7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZmluZE1vZGVsRm9yUGF0aChhc3NldFBhdGg6IHN0cmluZyk6IHN0cmluZ1tdIHtcbiAgICBsZXQgbW9kZWxzTWFwcGluZyA9IENvbmZpZy5jbGFyaWZhaUZvbGRlclRvTW9kZWxNYXBwaW5nLmZpbmQoKG1hcHBpbmcpID0+IHtcbiAgICAgIHJldHVybiBhc3NldFBhdGguc3RhcnRzV2l0aChtYXBwaW5nLmZvbGRlcik7XG4gICAgfSk7XG4gICAgcmV0dXJuIG1vZGVsc01hcHBpbmcgPyBtb2RlbHNNYXBwaW5nLm1vZGVscyA6IFtDbGFyaWZhaUFQSS5HRU5FUkFMX01PREVMXTtcbiAgfVxuXG4gIHByaXZhdGUgZGV0ZWN0VGFncyhkYXRhOiBzdHJpbmcsIG1vZGVsOiBhbnkpOiBQcm9taXNlPHN0cmluZ1tdPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPHN0cmluZ1tdPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLmNsYXJpZmFpLm1vZGVscy5wcmVkaWN0KG1vZGVsLCB7IGJhc2U2NDogZGF0YSB9LCB0aGlzLmRldGVjdFNldHRpbmdzKS50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgIGxldCByZXNEYXRhOiBhbnkgPSB0aGlzLmdldFJlc3BvbnNlRGF0YShyZXNwb25zZSk7XG4gICAgICAgIGlmIChyZXNEYXRhICYmIHJlc0RhdGEuY29uY2VwdHMpIHtcbiAgICAgICAgICByZXNvbHZlKHRoaXMuZ2V0VGFnc0Zyb21Db25jZXB0cyhyZXNEYXRhLmNvbmNlcHRzKSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgcmVzb2x2ZShbXSk7XG4gICAgICAgIH1cbiAgICAgIH0pLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG4gICAgICAgIHJlamVjdChuZXcgRXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIGdldHRpbmcgbGFiZWxzIGZyb20gQ2xhcmlmYWk6ICcgKyBKU09OLnN0cmluZ2lmeShlcnJvci5kYXRhLnN0YXR1cywgbnVsbCwgMikpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkZXRlY3RDZWxlYnJpdGllcyhzcjogU2VydmljZVJlc3BvbnNlLCBkYXRhOiBzdHJpbmcsIG1vZGVsOiBhbnkpOiBQcm9taXNlPHZvaWQ+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8dm9pZD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgdGhpcy5jbGFyaWZhaS5tb2RlbHMucHJlZGljdChtb2RlbCwgeyBiYXNlNjQ6IGRhdGEgfSwgdGhpcy5kZXRlY3RTZXR0aW5ncykudGhlbigocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICBsZXQgcmVzRGF0YTogYW55ID0gdGhpcy5nZXRSZXNwb25zZURhdGEocmVzcG9uc2UpO1xuICAgICAgICBpZiAoIXJlc0RhdGEgfHwgIXJlc0RhdGEucmVnaW9ucykge1xuICAgICAgICAgIHJldHVybiByZXNvbHZlKCk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHJlZ2lvbnMgPSByZXNEYXRhLnJlZ2lvbnM7XG4gICAgICAgIGxldCBjZWxlYnM6IHN0cmluZ1tdID0gW107XG4gICAgICAgIHJlZ2lvbnMuZm9yRWFjaChyZWdpb24gPT4ge1xuICAgICAgICAgIGlmIChyZWdpb24uZGF0YSAmJiByZWdpb24uZGF0YS5mYWNlICYmIHJlZ2lvbi5kYXRhLmZhY2UuaWRlbnRpdHkgJiYgcmVnaW9uLmRhdGEuZmFjZS5pZGVudGl0eS5jb25jZXB0cykge1xuICAgICAgICAgICAgbGV0IGNvbmNlcHRzOiBhbnkgPSByZWdpb24uZGF0YS5mYWNlLmlkZW50aXR5LmNvbmNlcHRzO1xuICAgICAgICAgICAgY2VsZWJzID0gVXRpbHMubWVyZ2VBcnJheXMoY2VsZWJzLCB0aGlzLmdldFRhZ3NGcm9tQ29uY2VwdHMoY29uY2VwdHMsIGZhbHNlKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKGNlbGVicy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgc3IubWV0YWRhdGFbJ3N1YmplY3RQZXJzb24nXSA9IGNlbGVicy5qb2luKCcsICcpO1xuICAgICAgICB9XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0pLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG4gICAgICAgIHJlamVjdChuZXcgRXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIGdldHRpbmcgY2VsZWJyaXR5IGluZm8gZnJvbSBDbGFyaWZhaTogJyArIEpTT04uc3RyaW5naWZ5KGVycm9yLmRhdGEuc3RhdHVzLCBudWxsLCAyKSkpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGdldFRhZ3NGcm9tQ29uY2VwdHMoY29uY2VwdHM6IGFueSwgbG93ZXJjYXNlOiBib29sZWFuID0gdHJ1ZSk6IHN0cmluZ1tdIHtcbiAgICBsZXQgdGFnczogc3RyaW5nW10gPSBbXTtcbiAgICBjb25jZXB0cy5mb3JFYWNoKGNvbmNlcHQgPT4ge1xuICAgICAgbGV0IHRhZzogc3RyaW5nID0gbG93ZXJjYXNlID8gY29uY2VwdC5uYW1lLnRvTG93ZXJDYXNlKCkgOiBjb25jZXB0Lm5hbWU7XG4gICAgICB0YWdzLnB1c2godGFnKTtcbiAgICB9KTtcbiAgICByZXR1cm4gdGFncztcbiAgfVxuXG4gIHByaXZhdGUgZ2V0UmVzcG9uc2VEYXRhKHJlc3BvbnNlOiBhbnkpOiBhbnkge1xuICAgIGlmIChyZXNwb25zZS5vdXRwdXRzICYmIHJlc3BvbnNlLm91dHB1dHMubGVuZ3RoID4gMCAmJiByZXNwb25zZS5vdXRwdXRzWzBdLmRhdGEpIHtcbiAgICAgIHJldHVybiByZXNwb25zZS5vdXRwdXRzWzBdLmRhdGE7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG59XG4iXX0=