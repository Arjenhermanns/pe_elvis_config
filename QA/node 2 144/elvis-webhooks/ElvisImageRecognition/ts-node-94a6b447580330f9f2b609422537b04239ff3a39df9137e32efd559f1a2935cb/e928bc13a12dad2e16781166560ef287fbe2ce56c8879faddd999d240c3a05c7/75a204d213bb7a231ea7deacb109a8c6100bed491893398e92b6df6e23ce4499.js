"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const config_1 = require("../../config");
class Google {
    validateConfig() {
        return new Promise((resolve, reject) => {
            fs.exists(config_1.Config.googleKeyFilename, exists => {
                // We have to check ourselves as Google throws weird errors when there's no valid path specified.
                // For example when people configure an API key instead of a path to a key file.
                if (!exists) {
                    return reject(Error('The file specified in googleKeyFilename doesn\'t exists: "' + config_1.Config.googleKeyFilename + '". Please configure the correct full file path to the Google Service account keyfile.'));
                }
                resolve('');
            });
        });
    }
}
exports.Google = Google;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLWJhc2UudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9zZXJ2aWNlL2dvb2dsZS1iYXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEseUJBQTBCO0FBQzFCLHlDQUFzQztBQUV0QztJQUVZLGNBQWM7UUFDdEIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLEVBQUUsQ0FBQyxNQUFNLENBQUMsZUFBTSxDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxFQUFFO2dCQUMzQyxpR0FBaUc7Z0JBQ2pHLGdGQUFnRjtnQkFDaEYsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNaLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDREQUE0RCxHQUFHLGVBQU0sQ0FBQyxpQkFBaUIsR0FBRyx1RkFBdUYsQ0FBQyxDQUFDLENBQUM7Z0JBQzFNLENBQUM7Z0JBQ0QsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FFRjtBQWZELHdCQWVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGZzID0gcmVxdWlyZSgnZnMnKTtcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBHb29nbGUge1xuXG4gIHByb3RlY3RlZCB2YWxpZGF0ZUNvbmZpZygpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBmcy5leGlzdHMoQ29uZmlnLmdvb2dsZUtleUZpbGVuYW1lLCBleGlzdHMgPT4ge1xuICAgICAgICAvLyBXZSBoYXZlIHRvIGNoZWNrIG91cnNlbHZlcyBhcyBHb29nbGUgdGhyb3dzIHdlaXJkIGVycm9ycyB3aGVuIHRoZXJlJ3Mgbm8gdmFsaWQgcGF0aCBzcGVjaWZpZWQuXG4gICAgICAgIC8vIEZvciBleGFtcGxlIHdoZW4gcGVvcGxlIGNvbmZpZ3VyZSBhbiBBUEkga2V5IGluc3RlYWQgb2YgYSBwYXRoIHRvIGEga2V5IGZpbGUuXG4gICAgICAgIGlmICghZXhpc3RzKSB7XG4gICAgICAgICAgcmV0dXJuIHJlamVjdChFcnJvcignVGhlIGZpbGUgc3BlY2lmaWVkIGluIGdvb2dsZUtleUZpbGVuYW1lIGRvZXNuXFwndCBleGlzdHM6IFwiJyArIENvbmZpZy5nb29nbGVLZXlGaWxlbmFtZSArICdcIi4gUGxlYXNlIGNvbmZpZ3VyZSB0aGUgY29ycmVjdCBmdWxsIGZpbGUgcGF0aCB0byB0aGUgR29vZ2xlIFNlcnZpY2UgYWNjb3VudCBrZXlmaWxlLicpKTtcbiAgICAgICAgfVxuICAgICAgICByZXNvbHZlKCcnKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbn1cblxuIl19