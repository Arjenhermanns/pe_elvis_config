"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vision = require("@google-cloud/vision");
const google_base_1 = require("./google-base");
const config_1 = require("../../config");
const service_response_1 = require("./service-response");
/**
 * Uses the Google Vision API to detect tags and locations in the given image.
 */
class GoogleVision extends google_base_1.Google {
    constructor() {
        super();
        this.validateConfig().then(() => {
            this.gv = vision({
                keyFilename: config_1.Config.googleKeyFilename
            });
        });
    }
    /**
     * Detect tags & locations
     *
     * @param inputFile Full path to the image to analyze
     */
    detect(inputFile) {
        return new Promise((resolve, reject) => {
            let params = {
                types: ['labels', 'landmarks'],
                verbose: true
            };
            this.gv.detect(inputFile, params).then((response) => {
                var sr = new service_response_1.ServiceResponse();
                response[0].labels.forEach(label => {
                    if (label.score > 0.85) {
                        sr.tags.push(label.desc.toLowerCase());
                    }
                });
                if (response[0].landmarks !== undefined && response[0].landmarks.length > 0) {
                    let landmarks = response[0].landmarks;
                    sr.metadata['gpsLatitude'] = landmarks[0].locations[0].latitude;
                    sr.metadata['gpsLongitude'] = landmarks[0].locations[0].longitude;
                    let locations = [];
                    landmarks.forEach(landmark => {
                        // Google sometimes gives us duplicate landmarks...
                        if (!locations.includes(landmark.desc))
                            locations.push(landmark.desc);
                    });
                    sr.metadata['shownSublocation'] = locations.join(', ');
                }
                if (config_1.Config.googleTagsField && sr.tags.length > 0) {
                    sr.metadata[config_1.Config.googleTagsField] = sr.tags.join(',');
                }
                resolve(sr);
            }).catch((error) => {
                reject(this.getErrorObj(inputFile, error));
            });
        });
    }
    getErrorObj(inputFile, error) {
        let errorVal = (typeof error === 'string') ? error : JSON.stringify(error, Object.getOwnPropertyNames(error), 2);
        return new Error('An error occurred while getting labels for "' + inputFile + '" from Google Vision: ' + errorVal);
    }
}
exports.GoogleVision = GoogleVision;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLXZpc2lvbi50cyIsInNvdXJjZXMiOlsiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLXZpc2lvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLCtDQUFnRDtBQUNoRCwrQ0FBdUM7QUFDdkMseUNBQXNDO0FBQ3RDLHlEQUFxRDtBQUVyRDs7R0FFRztBQUNILGtCQUEwQixTQUFRLG9CQUFNO0lBSXRDO1FBQ0UsS0FBSyxFQUFFLENBQUM7UUFDUixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUM5QixJQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDZixXQUFXLEVBQUUsZUFBTSxDQUFDLGlCQUFpQjthQUN0QyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLFNBQVM7UUFDckIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBRXJDLElBQUksTUFBTSxHQUFHO2dCQUNYLEtBQUssRUFBRSxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUM7Z0JBQzlCLE9BQU8sRUFBRSxJQUFJO2FBQ2QsQ0FBQztZQUVGLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDbEQsSUFBSSxFQUFFLEdBQUcsSUFBSSxrQ0FBZSxFQUFFLENBQUM7Z0JBRS9CLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNqQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3ZCLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFDekMsQ0FBQztnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLFNBQVMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO29CQUN0QyxFQUFFLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO29CQUNoRSxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO29CQUNsRSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7b0JBQ25CLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQzNCLG1EQUFtRDt3QkFDbkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDckMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO29CQUNILEVBQUUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6RCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxlQUFlLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxlQUFNLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzFELENBQUM7Z0JBRUQsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sV0FBVyxDQUFDLFNBQVMsRUFBRSxLQUFLO1FBQ2xDLElBQUksUUFBUSxHQUFXLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3pILE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsR0FBRyxTQUFTLEdBQUcsd0JBQXdCLEdBQUcsUUFBUSxDQUFDLENBQUM7SUFDckgsQ0FBQztDQUNGO0FBL0RELG9DQStEQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB2aXNpb24gPSByZXF1aXJlKCdAZ29vZ2xlLWNsb3VkL3Zpc2lvbicpO1xuaW1wb3J0IHsgR29vZ2xlIH0gZnJvbSAnLi9nb29nbGUtYmFzZSc7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuaW1wb3J0IHsgU2VydmljZVJlc3BvbnNlIH0gZnJvbSAnLi9zZXJ2aWNlLXJlc3BvbnNlJztcblxuLyoqXG4gKiBVc2VzIHRoZSBHb29nbGUgVmlzaW9uIEFQSSB0byBkZXRlY3QgdGFncyBhbmQgbG9jYXRpb25zIGluIHRoZSBnaXZlbiBpbWFnZS5cbiAqL1xuZXhwb3J0IGNsYXNzIEdvb2dsZVZpc2lvbiBleHRlbmRzIEdvb2dsZSB7XG5cbiAgcHJpdmF0ZSBndjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMudmFsaWRhdGVDb25maWcoKS50aGVuKCgpID0+IHtcbiAgICAgIHRoaXMuZ3YgPSB2aXNpb24oe1xuICAgICAgICBrZXlGaWxlbmFtZTogQ29uZmlnLmdvb2dsZUtleUZpbGVuYW1lXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlY3QgdGFncyAmIGxvY2F0aW9uc1xuICAgKiBcbiAgICogQHBhcmFtIGlucHV0RmlsZSBGdWxsIHBhdGggdG8gdGhlIGltYWdlIHRvIGFuYWx5emVcbiAgICovXG4gIHB1YmxpYyBkZXRlY3QoaW5wdXRGaWxlKTogUHJvbWlzZTxTZXJ2aWNlUmVzcG9uc2U+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICBsZXQgcGFyYW1zID0ge1xuICAgICAgICB0eXBlczogWydsYWJlbHMnLCAnbGFuZG1hcmtzJ10sXG4gICAgICAgIHZlcmJvc2U6IHRydWVcbiAgICAgIH07XG5cbiAgICAgIHRoaXMuZ3YuZGV0ZWN0KGlucHV0RmlsZSwgcGFyYW1zKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICB2YXIgc3IgPSBuZXcgU2VydmljZVJlc3BvbnNlKCk7XG5cbiAgICAgICAgcmVzcG9uc2VbMF0ubGFiZWxzLmZvckVhY2gobGFiZWwgPT4ge1xuICAgICAgICAgIGlmIChsYWJlbC5zY29yZSA+IDAuODUpIHtcbiAgICAgICAgICAgIHNyLnRhZ3MucHVzaChsYWJlbC5kZXNjLnRvTG93ZXJDYXNlKCkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHJlc3BvbnNlWzBdLmxhbmRtYXJrcyAhPT0gdW5kZWZpbmVkICYmIHJlc3BvbnNlWzBdLmxhbmRtYXJrcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgbGV0IGxhbmRtYXJrcyA9IHJlc3BvbnNlWzBdLmxhbmRtYXJrcztcbiAgICAgICAgICBzci5tZXRhZGF0YVsnZ3BzTGF0aXR1ZGUnXSA9IGxhbmRtYXJrc1swXS5sb2NhdGlvbnNbMF0ubGF0aXR1ZGU7XG4gICAgICAgICAgc3IubWV0YWRhdGFbJ2dwc0xvbmdpdHVkZSddID0gbGFuZG1hcmtzWzBdLmxvY2F0aW9uc1swXS5sb25naXR1ZGU7XG4gICAgICAgICAgbGV0IGxvY2F0aW9ucyA9IFtdO1xuICAgICAgICAgIGxhbmRtYXJrcy5mb3JFYWNoKGxhbmRtYXJrID0+IHtcbiAgICAgICAgICAgIC8vIEdvb2dsZSBzb21ldGltZXMgZ2l2ZXMgdXMgZHVwbGljYXRlIGxhbmRtYXJrcy4uLlxuICAgICAgICAgICAgaWYgKCFsb2NhdGlvbnMuaW5jbHVkZXMobGFuZG1hcmsuZGVzYykpXG4gICAgICAgICAgICAgIGxvY2F0aW9ucy5wdXNoKGxhbmRtYXJrLmRlc2MpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHNyLm1ldGFkYXRhWydzaG93blN1YmxvY2F0aW9uJ10gPSBsb2NhdGlvbnMuam9pbignLCAnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChDb25maWcuZ29vZ2xlVGFnc0ZpZWxkICYmIHNyLnRhZ3MubGVuZ3RoID4gMCkge1xuICAgICAgICAgIHNyLm1ldGFkYXRhW0NvbmZpZy5nb29nbGVUYWdzRmllbGRdID0gc3IudGFncy5qb2luKCcsJyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXNvbHZlKHNyKTtcbiAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICByZWplY3QodGhpcy5nZXRFcnJvck9iaihpbnB1dEZpbGUsIGVycm9yKSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0RXJyb3JPYmooaW5wdXRGaWxlLCBlcnJvcik6IEVycm9yIHtcbiAgICBsZXQgZXJyb3JWYWw6IHN0cmluZyA9ICh0eXBlb2YgZXJyb3IgPT09ICdzdHJpbmcnKSA/IGVycm9yIDogSlNPTi5zdHJpbmdpZnkoZXJyb3IsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKGVycm9yKSwgMik7XG4gICAgcmV0dXJuIG5ldyBFcnJvcignQW4gZXJyb3Igb2NjdXJyZWQgd2hpbGUgZ2V0dGluZyBsYWJlbHMgZm9yIFwiJyArIGlucHV0RmlsZSArICdcIiBmcm9tIEdvb2dsZSBWaXNpb246ICcgKyBlcnJvclZhbCk7XG4gIH1cbn1cblxuIl19