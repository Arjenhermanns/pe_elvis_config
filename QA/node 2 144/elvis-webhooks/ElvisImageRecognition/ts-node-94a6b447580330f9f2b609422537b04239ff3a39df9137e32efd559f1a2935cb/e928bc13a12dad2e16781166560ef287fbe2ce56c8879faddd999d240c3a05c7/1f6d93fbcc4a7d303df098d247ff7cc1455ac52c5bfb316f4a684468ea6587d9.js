"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const fs = require("fs");
var request = require('request').defaults({ jar: true });
class HttpError extends Error {
    constructor(message, statusCode, options) {
        super(message);
        this.message = message;
        this.statusCode = statusCode;
        this.options = options;
    }
}
exports.HttpError = HttpError;
class ElvisRequest {
    constructor(serverUrl, username, password) {
        this.serverUrl = serverUrl;
        this.username = username;
        this.password = password;
    }
    // TODO: The request and requestFile code is long and redundant. not sure yet how to minimize code without losing functionality
    request(options) {
        return this.apiRequest(options).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.apiRequest(options);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.apiRequest(options);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    requestFile(url, destination) {
        return this.fileRequest(url, destination).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.fileRequest(url, destination);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.fileRequest(url, destination);
                    });
                }
            }
            else if (error.statusCode == 409) {
                let delay = 5;
                console.warn('Download failed with 409 error (file not available), retrying once more in ' + delay + ' seconds. URL: ' + url);
                return Promise.delay(delay * 1000).then(() => {
                    return this.fileRequest(url, destination);
                });
            }
            else {
                throw error;
            }
        });
    }
    authenticate() {
        var options = {
            method: 'POST',
            url: this.serverUrl + '/services/login?username=' + this.username + '&password=' + this.password
        };
        console.info('Not logged in, logging in...');
        return this.apiRequest(options).then(response => {
            if (!response.loginSuccess) {
                throw new HttpError(response.loginFaultMessage, 401, options);
            }
            else {
                console.info('Login successful!');
                if (response.csrfToken) {
                    // Elvis 6+ login
                    this.csrfToken = response.csrfToken;
                }
                return response;
            }
        });
    }
    addCsrfToken(options) {
        if (this.csrfToken) {
            // Elvis 6+
            if (!options.headers) {
                options.headers = {};
            }
            options.headers['X-CSRF-TOKEN'] = this.csrfToken;
        }
    }
    apiRequest(options) {
        return new Promise((resolve, reject) => {
            options.json = true;
            this.addCsrfToken(options);
            request(options, (error, response, body) => {
                if (error) {
                    // Handle generic errors, for example unknown host
                    reject(new HttpError('Elvis request failed: ' + error, 0, options));
                }
                if (body.errorcode) {
                    response.statusCode = body.errorcode;
                    response.statusMessage = body.message;
                }
                if (response.statusCode < 200 || response.statusCode > 299) {
                    // Handle Elvis HTTP errors: 404, 401, 500, etc
                    reject(new HttpError('Elvis request failed: ' + response.statusMessage, response.statusCode, options));
                }
                else {
                    // All good, return API response
                    resolve(body);
                }
            });
        });
    }
    fileRequest(url, destination) {
        return new Promise((resolve, reject) => {
            let errorMsg = 'Download of ' + url + ' to: ' + destination + ' failed';
            let file = fs.createWriteStream(destination);
            let options = {
                method: 'GET',
                url: url
            };
            this.addCsrfToken(options);
            let req = request(options)
                .on('error', (error) => {
                // Handle generic errors when getting the file, for example unknown host
                reject(new Error(errorMsg + ': ' + error));
            })
                .on('response', response => {
                // Handle Elvis HTTP errors: 404, 401, 500, etc
                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new HttpError(errorMsg, response.statusCode, options));
                }
                // Request went well, let's start piping the data...
                req.pipe(file)
                    .on('error', (error) => {
                    // Handle piping errors: unable to write file, stream closed, ...
                    reject(new Error(errorMsg + ': ' + error));
                })
                    .on('finish', () => {
                    // Piping complete, we've got the file!
                    resolve(destination);
                });
            });
        });
    }
}
exports.ElvisRequest = ElvisRequest;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvZWx2aXMtYXBpL2VsdmlzLXJlcXVlc3QudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2VsdmlzLWFwaS9lbHZpcy1yZXF1ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0NBQXFDO0FBRXJDLHlCQUEwQjtBQUUxQixJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7QUFFekQsZUFBdUIsU0FBUSxLQUFLO0lBQ2xDLFlBQW1CLE9BQWUsRUFBUyxVQUFrQixFQUFTLE9BQWdFO1FBQ3BJLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQURFLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFBUyxlQUFVLEdBQVYsVUFBVSxDQUFRO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBeUQ7SUFFdEksQ0FBQztDQUNGO0FBSkQsOEJBSUM7QUFFRDtJQUtFLFlBQW9CLFNBQWlCLEVBQVUsUUFBZ0IsRUFBVSxRQUFnQjtRQUFyRSxjQUFTLEdBQVQsU0FBUyxDQUFRO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVE7SUFDekYsQ0FBQztJQUVELCtIQUErSDtJQUV4SCxPQUFPLENBQUMsT0FBZ0U7UUFDN0UsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzVDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDZiw2QkFBNkI7b0JBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ3hDLHFCQUFxQjt3QkFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7d0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNsQyxDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7b0JBQ2xFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ3pCLHFCQUFxQjt3QkFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUM7WUFDSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sTUFBTSxLQUFLLENBQUM7WUFDZCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sV0FBVyxDQUFDLEdBQVcsRUFBRSxXQUFtQjtRQUNqRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3RELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDZiw2QkFBNkI7b0JBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ3hDLHFCQUFxQjt3QkFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7d0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO29CQUNsRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO3dCQUN6QixxQkFBcUI7d0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLEtBQUssR0FBVyxDQUFDLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsNkVBQTZFLEdBQUcsS0FBSyxHQUFHLGlCQUFpQixHQUFHLEdBQUcsQ0FBQyxDQUFDO2dCQUM5SCxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtvQkFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLEtBQUssQ0FBQztZQUNkLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxZQUFZO1FBQ2xCLElBQUksT0FBTyxHQUFHO1lBQ1osTUFBTSxFQUFFLE1BQU07WUFDZCxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRywyQkFBMkIsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUTtTQUNqRyxDQUFBO1FBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBQzdDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM5QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDaEUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbEMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLGlCQUFpQjtvQkFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUN0QyxDQUFDO2dCQUNELE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDbEIsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFlBQVksQ0FBQyxPQUFPO1FBQzFCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ25CLFdBQVc7WUFDWCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixPQUFPLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUN2QixDQUFDO1lBQ0QsT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ25ELENBQUM7SUFDSCxDQUFDO0lBRU8sVUFBVSxDQUFDLE9BQWdFO1FBQ2pGLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNyQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFO2dCQUN6QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNWLGtEQUFrRDtvQkFDbEQsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDLHdCQUF3QixHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDdEUsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDbkIsUUFBUSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNyQyxRQUFRLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ3hDLENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMzRCwrQ0FBK0M7b0JBQy9DLE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBQyx3QkFBd0IsR0FBRyxRQUFRLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDekcsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixnQ0FBZ0M7b0JBQ2hDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDaEIsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sV0FBVyxDQUFDLEdBQVcsRUFBRSxXQUFtQjtRQUNsRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQVMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDN0MsSUFBSSxRQUFRLEdBQVcsY0FBYyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLFNBQVMsQ0FBQztZQUNoRixJQUFJLElBQUksR0FBbUIsRUFBRSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzdELElBQUksT0FBTyxHQUFRO2dCQUNqQixNQUFNLEVBQUUsS0FBSztnQkFDYixHQUFHLEVBQUUsR0FBRzthQUNULENBQUM7WUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRTNCLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7aUJBQ3ZCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDckIsd0VBQXdFO2dCQUN4RSxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQztpQkFDRCxFQUFFLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxFQUFFO2dCQUV6QiwrQ0FBK0M7Z0JBQy9DLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxJQUFJLFFBQVEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDM0QsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hFLENBQUM7Z0JBRUQsb0RBQW9EO2dCQUNwRCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztxQkFDWCxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBQ3JCLGlFQUFpRTtvQkFDakUsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQyxDQUFDO3FCQUNELEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO29CQUNqQix1Q0FBdUM7b0JBQ3ZDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBOUpELG9DQThKQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9taXNlID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcbmltcG9ydCB7IFVyaU9wdGlvbnMsIFVybE9wdGlvbnMsIENvcmVPcHRpb25zIH0gZnJvbSAncmVxdWVzdCc7XG5pbXBvcnQgZnMgPSByZXF1aXJlKCdmcycpO1xuXG52YXIgcmVxdWVzdCA9IHJlcXVpcmUoJ3JlcXVlc3QnKS5kZWZhdWx0cyh7IGphcjogdHJ1ZSB9KTtcblxuZXhwb3J0IGNsYXNzIEh0dHBFcnJvciBleHRlbmRzIEVycm9yIHtcbiAgY29uc3RydWN0b3IocHVibGljIG1lc3NhZ2U6IHN0cmluZywgcHVibGljIHN0YXR1c0NvZGU6IG51bWJlciwgcHVibGljIG9wdGlvbnM6IChVcmlPcHRpb25zICYgQ29yZU9wdGlvbnMpIHwgKFVybE9wdGlvbnMgJiBDb3JlT3B0aW9ucykpIHtcbiAgICBzdXBlcihtZXNzYWdlKTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgRWx2aXNSZXF1ZXN0IHtcblxuICBwcml2YXRlIGNzcmZUb2tlbjogc3RyaW5nO1xuICBwcml2YXRlIGF1dGg6IFByb21pc2U8YW55PlxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2VydmVyVXJsOiBzdHJpbmcsIHByaXZhdGUgdXNlcm5hbWU6IHN0cmluZywgcHJpdmF0ZSBwYXNzd29yZDogc3RyaW5nKSB7XG4gIH1cblxuICAvLyBUT0RPOiBUaGUgcmVxdWVzdCBhbmQgcmVxdWVzdEZpbGUgY29kZSBpcyBsb25nIGFuZCByZWR1bmRhbnQuIG5vdCBzdXJlIHlldCBob3cgdG8gbWluaW1pemUgY29kZSB3aXRob3V0IGxvc2luZyBmdW5jdGlvbmFsaXR5XG5cbiAgcHVibGljIHJlcXVlc3Qob3B0aW9uczogKFVyaU9wdGlvbnMgJiBDb3JlT3B0aW9ucykgfCAoVXJsT3B0aW9ucyAmIENvcmVPcHRpb25zKSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuYXBpUmVxdWVzdChvcHRpb25zKS5jYXRjaChlcnJvciA9PiB7XG4gICAgICBpZiAoZXJyb3Iuc3RhdHVzQ29kZSA9PSA0MDEpIHtcbiAgICAgICAgaWYgKCF0aGlzLmF1dGgpIHtcbiAgICAgICAgICAvLyBOb3QgbG9nZ2VkIGluLCBsb2dpbiBmaXJzdFxuICAgICAgICAgIHRoaXMuYXV0aCA9IHRoaXMuYXV0aGVudGljYXRlKCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAvLyBSZXRyeSBpbml0aWFsIGNhbGxcbiAgICAgICAgICAgIHRoaXMuYXV0aCA9IG51bGw7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiB0aGlzLmF1dGg7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ0FscmVhZHkgbG9nZ2luZyBpbiwgd2FpdGluZyBmb3IgbG9naW4gdG8gZmluaXNoLi4uJyk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIC8vIFJldHJ5IGluaXRpYWwgY2FsbFxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXBpUmVxdWVzdChvcHRpb25zKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgcmVxdWVzdEZpbGUodXJsOiBzdHJpbmcsIGRlc3RpbmF0aW9uOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmZpbGVSZXF1ZXN0KHVybCwgZGVzdGluYXRpb24pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIGlmIChlcnJvci5zdGF0dXNDb2RlID09IDQwMSkge1xuICAgICAgICBpZiAoIXRoaXMuYXV0aCkge1xuICAgICAgICAgIC8vIE5vdCBsb2dnZWQgaW4sIGxvZ2luIGZpcnN0XG4gICAgICAgICAgdGhpcy5hdXRoID0gdGhpcy5hdXRoZW50aWNhdGUoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIC8vIFJldHJ5IGluaXRpYWwgY2FsbFxuICAgICAgICAgICAgdGhpcy5hdXRoID0gbnVsbDtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbGVSZXF1ZXN0KHVybCwgZGVzdGluYXRpb24pO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiB0aGlzLmF1dGg7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ0FscmVhZHkgbG9nZ2luZyBpbiwgd2FpdGluZyBmb3IgbG9naW4gdG8gZmluaXNoLi4uJyk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIC8vIFJldHJ5IGluaXRpYWwgY2FsbFxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmlsZVJlcXVlc3QodXJsLCBkZXN0aW5hdGlvbik7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoZXJyb3Iuc3RhdHVzQ29kZSA9PSA0MDkpIHtcbiAgICAgICAgbGV0IGRlbGF5OiBudW1iZXIgPSA1O1xuICAgICAgICBjb25zb2xlLndhcm4oJ0Rvd25sb2FkIGZhaWxlZCB3aXRoIDQwOSBlcnJvciAoZmlsZSBub3QgYXZhaWxhYmxlKSwgcmV0cnlpbmcgb25jZSBtb3JlIGluICcgKyBkZWxheSArICcgc2Vjb25kcy4gVVJMOiAnICsgdXJsKTtcbiAgICAgICAgcmV0dXJuIFByb21pc2UuZGVsYXkoZGVsYXkgKiAxMDAwKS50aGVuKCgpID0+IHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5maWxlUmVxdWVzdCh1cmwsIGRlc3RpbmF0aW9uKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYXV0aGVudGljYXRlKCk6IFByb21pc2U8YW55PiB7XG4gICAgdmFyIG9wdGlvbnMgPSB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIHVybDogdGhpcy5zZXJ2ZXJVcmwgKyAnL3NlcnZpY2VzL2xvZ2luP3VzZXJuYW1lPScgKyB0aGlzLnVzZXJuYW1lICsgJyZwYXNzd29yZD0nICsgdGhpcy5wYXNzd29yZFxuICAgIH1cbiAgICBjb25zb2xlLmluZm8oJ05vdCBsb2dnZWQgaW4sIGxvZ2dpbmcgaW4uLi4nKTtcbiAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgaWYgKCFyZXNwb25zZS5sb2dpblN1Y2Nlc3MpIHtcbiAgICAgICAgdGhyb3cgbmV3IEh0dHBFcnJvcihyZXNwb25zZS5sb2dpbkZhdWx0TWVzc2FnZSwgNDAxLCBvcHRpb25zKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnTG9naW4gc3VjY2Vzc2Z1bCEnKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmNzcmZUb2tlbikge1xuICAgICAgICAgIC8vIEVsdmlzIDYrIGxvZ2luXG4gICAgICAgICAgdGhpcy5jc3JmVG9rZW4gPSByZXNwb25zZS5jc3JmVG9rZW47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRDc3JmVG9rZW4ob3B0aW9ucyk6IHZvaWQge1xuICAgIGlmICh0aGlzLmNzcmZUb2tlbikge1xuICAgICAgLy8gRWx2aXMgNitcbiAgICAgIGlmICghb3B0aW9ucy5oZWFkZXJzKSB7XG4gICAgICAgIG9wdGlvbnMuaGVhZGVycyA9IHt9O1xuICAgICAgfVxuICAgICAgb3B0aW9ucy5oZWFkZXJzWydYLUNTUkYtVE9LRU4nXSA9IHRoaXMuY3NyZlRva2VuO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYXBpUmVxdWVzdChvcHRpb25zOiAoVXJpT3B0aW9ucyAmIENvcmVPcHRpb25zKSB8IChVcmxPcHRpb25zICYgQ29yZU9wdGlvbnMpKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgb3B0aW9ucy5qc29uID0gdHJ1ZTtcbiAgICAgIHRoaXMuYWRkQ3NyZlRva2VuKG9wdGlvbnMpO1xuICAgICAgcmVxdWVzdChvcHRpb25zLCAoZXJyb3IsIHJlc3BvbnNlLCBib2R5KSA9PiB7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIC8vIEhhbmRsZSBnZW5lcmljIGVycm9ycywgZm9yIGV4YW1wbGUgdW5rbm93biBob3N0XG4gICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoJ0VsdmlzIHJlcXVlc3QgZmFpbGVkOiAnICsgZXJyb3IsIDAsIG9wdGlvbnMpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChib2R5LmVycm9yY29kZSkge1xuICAgICAgICAgIHJlc3BvbnNlLnN0YXR1c0NvZGUgPSBib2R5LmVycm9yY29kZTtcbiAgICAgICAgICByZXNwb25zZS5zdGF0dXNNZXNzYWdlID0gYm9keS5tZXNzYWdlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1c0NvZGUgPCAyMDAgfHwgcmVzcG9uc2Uuc3RhdHVzQ29kZSA+IDI5OSkge1xuICAgICAgICAgIC8vIEhhbmRsZSBFbHZpcyBIVFRQIGVycm9yczogNDA0LCA0MDEsIDUwMCwgZXRjXG4gICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoJ0VsdmlzIHJlcXVlc3QgZmFpbGVkOiAnICsgcmVzcG9uc2Uuc3RhdHVzTWVzc2FnZSwgcmVzcG9uc2Uuc3RhdHVzQ29kZSwgb3B0aW9ucykpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIC8vIEFsbCBnb29kLCByZXR1cm4gQVBJIHJlc3BvbnNlXG4gICAgICAgICAgcmVzb2x2ZShib2R5KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGZpbGVSZXF1ZXN0KHVybDogc3RyaW5nLCBkZXN0aW5hdGlvbjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgZXJyb3JNc2c6IHN0cmluZyA9ICdEb3dubG9hZCBvZiAnICsgdXJsICsgJyB0bzogJyArIGRlc3RpbmF0aW9uICsgJyBmYWlsZWQnO1xuICAgICAgbGV0IGZpbGU6IGZzLldyaXRlU3RyZWFtID0gZnMuY3JlYXRlV3JpdGVTdHJlYW0oZGVzdGluYXRpb24pO1xuICAgICAgbGV0IG9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgdXJsOiB1cmxcbiAgICAgIH07XG4gICAgICB0aGlzLmFkZENzcmZUb2tlbihvcHRpb25zKTtcblxuICAgICAgbGV0IHJlcSA9IHJlcXVlc3Qob3B0aW9ucylcbiAgICAgICAgLm9uKCdlcnJvcicsIChlcnJvcikgPT4ge1xuICAgICAgICAgIC8vIEhhbmRsZSBnZW5lcmljIGVycm9ycyB3aGVuIGdldHRpbmcgdGhlIGZpbGUsIGZvciBleGFtcGxlIHVua25vd24gaG9zdFxuICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoZXJyb3JNc2cgKyAnOiAnICsgZXJyb3IpKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm9uKCdyZXNwb25zZScsIHJlc3BvbnNlID0+IHtcblxuICAgICAgICAgIC8vIEhhbmRsZSBFbHZpcyBIVFRQIGVycm9yczogNDA0LCA0MDEsIDUwMCwgZXRjXG4gICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1c0NvZGUgPCAyMDAgfHwgcmVzcG9uc2Uuc3RhdHVzQ29kZSA+IDI5OSkge1xuICAgICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoZXJyb3JNc2csIHJlc3BvbnNlLnN0YXR1c0NvZGUsIG9wdGlvbnMpKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBSZXF1ZXN0IHdlbnQgd2VsbCwgbGV0J3Mgc3RhcnQgcGlwaW5nIHRoZSBkYXRhLi4uXG4gICAgICAgICAgcmVxLnBpcGUoZmlsZSlcbiAgICAgICAgICAgIC5vbignZXJyb3InLCAoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgLy8gSGFuZGxlIHBpcGluZyBlcnJvcnM6IHVuYWJsZSB0byB3cml0ZSBmaWxlLCBzdHJlYW0gY2xvc2VkLCAuLi5cbiAgICAgICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihlcnJvck1zZyArICc6ICcgKyBlcnJvcikpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vbignZmluaXNoJywgKCkgPT4ge1xuICAgICAgICAgICAgICAvLyBQaXBpbmcgY29tcGxldGUsIHdlJ3ZlIGdvdCB0aGUgZmlsZSFcbiAgICAgICAgICAgICAgcmVzb2x2ZShkZXN0aW5hdGlvbik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn0iXX0=