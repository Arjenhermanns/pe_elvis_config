"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const compare = require("secure-compare");
const recognizer_1 = require("./recognizer");
const config_1 = require("../config");
/**
 * Handle Elvis webhook events
 */
class WebhookEndpoint {
    constructor(app) {
        this.app = app;
        let translateEnabled = config_1.Config.languages !== '';
        this.recognizer = new recognizer_1.Recognizer(config_1.Config.clarifaiEnabled, config_1.Config.googleEnabled, config_1.Config.awsEnabled, translateEnabled);
    }
    /**
     * Register HTTP Post route on '/' and listen for Elvis webhook events
     */
    addRoutes() {
        // Recognize API
        this.app.post('/', (req, res) => {
            // Send a response back to Elvis before handling the event, so the app won't keep 
            // the connection open while it's handling the event. This results in better performance.
            res.status(200).send();
            // Validate the webhook signature
            let signature = req.header('x-hook-signature');
            if (!this.validateSignature(signature, JSON.stringify(req.body))) {
                return console.error('Invalid WebHook signature: ' + signature + '. Make sure the elvisToken is configured correctly.');
            }
            // Handle the event. 
            this.handle(req.body);
        });
    }
    /**
     * Validate Elvis webhook signature
     *
     * @param signature Request header signature
     * @param data Request data to validate
     */
    validateSignature(signature, data) {
        try {
            let hmac = crypto.createHmac('sha256', config_1.Config.elvisToken);
            hmac.update(data);
            let digest = hmac.digest('hex');
            return compare(digest, signature);
        }
        catch (error) {
            console.warn('Signature validation failed for signature: ' + signature + ' and data: ' + data + '; details: ' + error);
        }
        return false;
    }
    /**
     * Handles the asset_update_metadata Elvis Server event and
     * starts the recognition process if the correct metadata is specified
     *
     * @param event The Elvis webhook event to handle
     */
    handle(event) {
        let isUpdateMetadataEvent = (event.type === 'asset_update_metadata');
        let hasAssetDomainMetadata = (event.metadata && event.metadata.assetDomain);
        let hasAssetPathMetadata = (event.metadata && event.metadata.assetPath);
        if (!isUpdateMetadataEvent || !hasAssetDomainMetadata || !hasAssetPathMetadata) {
            console.warn('Received useless event, we\'re only interested in metadata_update events where the assetDomain and assetPath metadata fields are specified. ' +
                'Make sure to configure the Elvis webhook correctly. Event: '
                + JSON.stringify(event));
            return;
        }
        let previewIsReady = (event.changedMetadata && event.changedMetadata.previewState && event.changedMetadata.previewState.newValue === 'yes');
        if (event.metadata.assetPath.startsWith('/Users/') || !previewIsReady || event.metadata.assetDomain !== 'image') {
            // Simply ignore any metadata update that:
            // - Is in the Users folder, the configured API user doesn't have access here
            // - When we don't have a preview
            // - When it's not an image
            return;
        }
        this.recognizer.recognize(event.assetId, null, event.metadata.assetPath);
    }
}
exports.WebhookEndpoint = WebhookEndpoint;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3dlYmhvb2stZW5kcG9pbnQudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC93ZWJob29rLWVuZHBvaW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUNBQWtDO0FBQ2xDLDBDQUEyQztBQUUzQyw2Q0FBMEM7QUFDMUMsc0NBQW1DO0FBR25DOztHQUVHO0FBQ0g7SUFJRSxZQUFtQixHQUFnQjtRQUFoQixRQUFHLEdBQUgsR0FBRyxDQUFhO1FBQ2pDLElBQUksZ0JBQWdCLEdBQVksZUFBTSxDQUFDLFNBQVMsS0FBSyxFQUFFLENBQUM7UUFDeEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLHVCQUFVLENBQUMsZUFBTSxDQUFDLGVBQWUsRUFBRSxlQUFNLENBQUMsYUFBYSxFQUFFLGVBQU0sQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUN0SCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxTQUFTO1FBQ2QsZ0JBQWdCO1FBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsRUFBRTtZQUVqRCxrRkFBa0Y7WUFDbEYseUZBQXlGO1lBQ3pGLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFdkIsaUNBQWlDO1lBQ2pDLElBQUksU0FBUyxHQUFXLEdBQUcsQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUN2RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pFLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLDZCQUE2QixHQUFHLFNBQVMsR0FBRyxxREFBcUQsQ0FBQyxDQUFDO1lBQzFILENBQUM7WUFFRCxxQkFBcUI7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSyxpQkFBaUIsQ0FBQyxTQUFpQixFQUFFLElBQVk7UUFDdkQsSUFBSSxDQUFDO1lBQ0gsSUFBSSxJQUFJLEdBQWdCLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLGVBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN2RSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xCLElBQUksTUFBTSxHQUFXLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDYixPQUFPLENBQUMsSUFBSSxDQUFDLDZDQUE2QyxHQUFHLFNBQVMsR0FBRyxhQUFhLEdBQUcsSUFBSSxHQUFHLGFBQWEsR0FBRyxLQUFLLENBQUMsQ0FBQztRQUN6SCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLE1BQU0sQ0FBQyxLQUFVO1FBQ3ZCLElBQUkscUJBQXFCLEdBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHVCQUF1QixDQUFDLENBQUM7UUFDOUUsSUFBSSxzQkFBc0IsR0FBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNyRixJQUFJLG9CQUFvQixHQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2pGLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUMvRSxPQUFPLENBQUMsSUFBSSxDQUFDLDhJQUE4STtnQkFDekosNkRBQTZEO2tCQUMzRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUNELElBQUksY0FBYyxHQUFZLENBQUMsS0FBSyxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLENBQUM7UUFDckosRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDaEgsMENBQTBDO1lBQzFDLDZFQUE2RTtZQUM3RSxpQ0FBaUM7WUFDakMsMkJBQTJCO1lBQzNCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7Q0FDRjtBQTdFRCwwQ0E2RUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY3J5cHRvID0gcmVxdWlyZSgnY3J5cHRvJyk7XG5pbXBvcnQgY29tcGFyZSA9IHJlcXVpcmUoJ3NlY3VyZS1jb21wYXJlJyk7XG5cbmltcG9ydCB7IFJlY29nbml6ZXIgfSBmcm9tICcuL3JlY29nbml6ZXInO1xuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vY29uZmlnJztcbmltcG9ydCB7IEFwcGxpY2F0aW9uLCBSZXF1ZXN0LCBSZXNwb25zZSB9IGZyb20gJ2V4cHJlc3MnO1xuXG4vKipcbiAqIEhhbmRsZSBFbHZpcyB3ZWJob29rIGV2ZW50c1xuICovXG5leHBvcnQgY2xhc3MgV2ViaG9va0VuZHBvaW50IHtcblxuICBwcml2YXRlIHJlY29nbml6ZXI6IFJlY29nbml6ZXI7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGFwcDogQXBwbGljYXRpb24pIHtcbiAgICBsZXQgdHJhbnNsYXRlRW5hYmxlZDogYm9vbGVhbiA9IENvbmZpZy5sYW5ndWFnZXMgIT09ICcnO1xuICAgIHRoaXMucmVjb2duaXplciA9IG5ldyBSZWNvZ25pemVyKENvbmZpZy5jbGFyaWZhaUVuYWJsZWQsIENvbmZpZy5nb29nbGVFbmFibGVkLCBDb25maWcuYXdzRW5hYmxlZCwgdHJhbnNsYXRlRW5hYmxlZCk7XG4gIH1cblxuICAvKipcbiAgICogUmVnaXN0ZXIgSFRUUCBQb3N0IHJvdXRlIG9uICcvJyBhbmQgbGlzdGVuIGZvciBFbHZpcyB3ZWJob29rIGV2ZW50c1xuICAgKi9cbiAgcHVibGljIGFkZFJvdXRlcygpOiB2b2lkIHtcbiAgICAvLyBSZWNvZ25pemUgQVBJXG4gICAgdGhpcy5hcHAucG9zdCgnLycsIChyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpID0+IHtcblxuICAgICAgLy8gU2VuZCBhIHJlc3BvbnNlIGJhY2sgdG8gRWx2aXMgYmVmb3JlIGhhbmRsaW5nIHRoZSBldmVudCwgc28gdGhlIGFwcCB3b24ndCBrZWVwIFxuICAgICAgLy8gdGhlIGNvbm5lY3Rpb24gb3BlbiB3aGlsZSBpdCdzIGhhbmRsaW5nIHRoZSBldmVudC4gVGhpcyByZXN1bHRzIGluIGJldHRlciBwZXJmb3JtYW5jZS5cbiAgICAgIHJlcy5zdGF0dXMoMjAwKS5zZW5kKCk7XG5cbiAgICAgIC8vIFZhbGlkYXRlIHRoZSB3ZWJob29rIHNpZ25hdHVyZVxuICAgICAgbGV0IHNpZ25hdHVyZTogc3RyaW5nID0gcmVxLmhlYWRlcigneC1ob29rLXNpZ25hdHVyZScpO1xuICAgICAgaWYgKCF0aGlzLnZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZSwgSlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpKSkge1xuICAgICAgICByZXR1cm4gY29uc29sZS5lcnJvcignSW52YWxpZCBXZWJIb29rIHNpZ25hdHVyZTogJyArIHNpZ25hdHVyZSArICcuIE1ha2Ugc3VyZSB0aGUgZWx2aXNUb2tlbiBpcyBjb25maWd1cmVkIGNvcnJlY3RseS4nKTtcbiAgICAgIH1cblxuICAgICAgLy8gSGFuZGxlIHRoZSBldmVudC4gXG4gICAgICB0aGlzLmhhbmRsZShyZXEuYm9keSk7XG4gICAgfSk7XG5cbiAgfVxuXG4gIC8qKlxuICAgKiBWYWxpZGF0ZSBFbHZpcyB3ZWJob29rIHNpZ25hdHVyZVxuICAgKiBcbiAgICogQHBhcmFtIHNpZ25hdHVyZSBSZXF1ZXN0IGhlYWRlciBzaWduYXR1cmVcbiAgICogQHBhcmFtIGRhdGEgUmVxdWVzdCBkYXRhIHRvIHZhbGlkYXRlXG4gICAqL1xuICBwcml2YXRlIHZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZTogc3RyaW5nLCBkYXRhOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICB0cnkge1xuICAgICAgbGV0IGhtYWM6IGNyeXB0by5IbWFjID0gY3J5cHRvLmNyZWF0ZUhtYWMoJ3NoYTI1NicsIENvbmZpZy5lbHZpc1Rva2VuKTtcbiAgICAgIGhtYWMudXBkYXRlKGRhdGEpO1xuICAgICAgbGV0IGRpZ2VzdDogc3RyaW5nID0gaG1hYy5kaWdlc3QoJ2hleCcpO1xuICAgICAgcmV0dXJuIGNvbXBhcmUoZGlnZXN0LCBzaWduYXR1cmUpO1xuICAgIH1cbiAgICBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUud2FybignU2lnbmF0dXJlIHZhbGlkYXRpb24gZmFpbGVkIGZvciBzaWduYXR1cmU6ICcgKyBzaWduYXR1cmUgKyAnIGFuZCBkYXRhOiAnICsgZGF0YSArICc7IGRldGFpbHM6ICcgKyBlcnJvcik7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIYW5kbGVzIHRoZSBhc3NldF91cGRhdGVfbWV0YWRhdGEgRWx2aXMgU2VydmVyIGV2ZW50IGFuZCBcbiAgICogc3RhcnRzIHRoZSByZWNvZ25pdGlvbiBwcm9jZXNzIGlmIHRoZSBjb3JyZWN0IG1ldGFkYXRhIGlzIHNwZWNpZmllZFxuICAgKiBcbiAgICogQHBhcmFtIGV2ZW50IFRoZSBFbHZpcyB3ZWJob29rIGV2ZW50IHRvIGhhbmRsZVxuICAgKi9cbiAgcHJpdmF0ZSBoYW5kbGUoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIGxldCBpc1VwZGF0ZU1ldGFkYXRhRXZlbnQ6IGJvb2xlYW4gPSAoZXZlbnQudHlwZSA9PT0gJ2Fzc2V0X3VwZGF0ZV9tZXRhZGF0YScpO1xuICAgIGxldCBoYXNBc3NldERvbWFpbk1ldGFkYXRhOiBib29sZWFuID0gKGV2ZW50Lm1ldGFkYXRhICYmIGV2ZW50Lm1ldGFkYXRhLmFzc2V0RG9tYWluKTtcbiAgICBsZXQgaGFzQXNzZXRQYXRoTWV0YWRhdGE6IGJvb2xlYW4gPSAoZXZlbnQubWV0YWRhdGEgJiYgZXZlbnQubWV0YWRhdGEuYXNzZXRQYXRoKTtcbiAgICBpZiAoIWlzVXBkYXRlTWV0YWRhdGFFdmVudCB8fCAhaGFzQXNzZXREb21haW5NZXRhZGF0YSB8fCAhaGFzQXNzZXRQYXRoTWV0YWRhdGEpIHtcbiAgICAgIGNvbnNvbGUud2FybignUmVjZWl2ZWQgdXNlbGVzcyBldmVudCwgd2VcXCdyZSBvbmx5IGludGVyZXN0ZWQgaW4gbWV0YWRhdGFfdXBkYXRlIGV2ZW50cyB3aGVyZSB0aGUgYXNzZXREb21haW4gYW5kIGFzc2V0UGF0aCBtZXRhZGF0YSBmaWVsZHMgYXJlIHNwZWNpZmllZC4gJyArXG4gICAgICAgICdNYWtlIHN1cmUgdG8gY29uZmlndXJlIHRoZSBFbHZpcyB3ZWJob29rIGNvcnJlY3RseS4gRXZlbnQ6ICdcbiAgICAgICAgKyBKU09OLnN0cmluZ2lmeShldmVudCkpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBsZXQgcHJldmlld0lzUmVhZHk6IGJvb2xlYW4gPSAoZXZlbnQuY2hhbmdlZE1ldGFkYXRhICYmIGV2ZW50LmNoYW5nZWRNZXRhZGF0YS5wcmV2aWV3U3RhdGUgJiYgZXZlbnQuY2hhbmdlZE1ldGFkYXRhLnByZXZpZXdTdGF0ZS5uZXdWYWx1ZSA9PT0gJ3llcycpO1xuICAgIGlmIChldmVudC5tZXRhZGF0YS5hc3NldFBhdGguc3RhcnRzV2l0aCgnL1VzZXJzLycpIHx8ICFwcmV2aWV3SXNSZWFkeSB8fCBldmVudC5tZXRhZGF0YS5hc3NldERvbWFpbiAhPT0gJ2ltYWdlJykge1xuICAgICAgLy8gU2ltcGx5IGlnbm9yZSBhbnkgbWV0YWRhdGEgdXBkYXRlIHRoYXQ6XG4gICAgICAvLyAtIElzIGluIHRoZSBVc2VycyBmb2xkZXIsIHRoZSBjb25maWd1cmVkIEFQSSB1c2VyIGRvZXNuJ3QgaGF2ZSBhY2Nlc3MgaGVyZVxuICAgICAgLy8gLSBXaGVuIHdlIGRvbid0IGhhdmUgYSBwcmV2aWV3XG4gICAgICAvLyAtIFdoZW4gaXQncyBub3QgYW4gaW1hZ2VcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5yZWNvZ25pemVyLnJlY29nbml6ZShldmVudC5hc3NldElkLCBudWxsLCBldmVudC5tZXRhZGF0YS5hc3NldFBhdGgpO1xuICB9XG59XG4iXX0=