"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const AWS = require("aws-sdk");
const config_1 = require("../../config");
const aws_sdk_1 = require("aws-sdk");
const service_response_1 = require("./service-response");
/**
 * Uses the AWS Rekognition API to detect tags and facial expressions in the given image.
 */
class AwsRekognition {
    constructor() {
        this.readFile = Promise.promisify(require("fs").readFile);
        AWS.config.update({
            accessKeyId: config_1.Config.awsAccessKeyId,
            secretAccessKey: config_1.Config.awsSecretAccessKey,
            region: config_1.Config.awsRegion
        });
        this.rekognition = new aws_sdk_1.Rekognition();
    }
    /**
     * Detect tags & facial expressions.
     *
     * @param inputFile Full path to the image to analyze
     */
    detect(inputFile) {
        // Read the file
        return this.readFile(inputFile).then((data) => {
            // Detect faces
            let sr = new service_response_1.ServiceResponse();
            return this.detectFaces(data, sr);
        }).then((response) => {
            // Detect tags
            return this.detectTags(response.data, response.sr);
        }).then((sr) => {
            // Add detected tags to metadata and resolve
            if (config_1.Config.awsTagsField && sr.tags.length > 0) {
                sr.metadata[config_1.Config.awsTagsField] = sr.tags.join(',');
            }
            return sr;
        });
    }
    detectFaces(data, sr) {
        let params = {
            Image: {
                Bytes: data
            },
            Attributes: [
                'ALL'
            ]
        };
        return new Promise((resolve, reject) => {
            this.rekognition.detectFaces(params, (error, response) => {
                if (error) {
                    return reject(new Error('An error occurred while getting faces from AWS Rekognition: ' + error));
                }
                if (response.FaceDetails.length == 0) {
                    // Nothing found, return
                    return resolve({ data, sr });
                }
                // Just work with the first result
                let fd = response.FaceDetails[0];
                let properties = this.getFaceProps(fd, ['Beard', 'Eyeglasses', 'EyesOpen', 'MouthOpen', 'Mustache', 'Smile', 'Sunglasses']);
                if (properties.length > 0) {
                    sr.metadata.cf_personProperties = properties.join(',');
                }
                let emotions = [];
                fd.Emotions.forEach(emotion => {
                    if (emotion.Confidence >= 80) {
                        emotions.push(emotion.Type.toLowerCase());
                    }
                });
                if (emotions.length > 0) {
                    sr.metadata.cf_personEmotions = emotions.join(',');
                }
                if (fd.Gender.Confidence >= 95) {
                    sr.metadata.cf_personGender = fd.Gender.Value;
                }
                sr.metadata.cf_personAgeMin = fd.AgeRange.Low;
                sr.metadata.cf_personAgeMax = fd.AgeRange.High;
                resolve({ data, sr });
            });
        });
    }
    getFaceProps(faceDetails, properties) {
        var props = [];
        properties.forEach(propName => {
            var property = faceDetails[propName];
            if (property.Value && property.Confidence >= 95) {
                props.push(propName);
            }
        });
        return props;
    }
    detectTags(data, sr) {
        // return new Promise((resolve, reject) => {
        let params = {
            Image: {
                Bytes: data
            },
            MaxLabels: 20,
            MinConfidence: 80
        };
        return new Promise((resolve, reject) => {
            // Get the labels
            this.rekognition.detectLabels(params, (error, response) => {
                if (error) {
                    return reject(new Error('An error occurred while getting labels from AWS Rekognition: ' + error));
                }
                response.Labels.forEach((label) => {
                    sr.tags.push(label.Name.toLowerCase());
                });
                resolve(sr);
            });
        });
    }
}
exports.AwsRekognition = AwsRekognition;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvYXdzLXJla29nbml0aW9uLnRzIiwic291cmNlcyI6WyIvc3J2L0VsdmlzV2ViSG9va3MvRWx2aXNJbWFnZVJlY29nbml0aW9uL3NyYy9hcHAvc2VydmljZS9hd3MtcmVrb2duaXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvQ0FBcUM7QUFDckMsK0JBQWdDO0FBQ2hDLHlDQUFzQztBQUN0QyxxQ0FBc0M7QUFFdEMseURBQXFEO0FBRXJEOztHQUVHO0FBQ0g7SUFLRTtRQUZRLGFBQVEsR0FBYSxPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUdyRSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUNoQixXQUFXLEVBQUUsZUFBTSxDQUFDLGNBQWM7WUFDbEMsZUFBZSxFQUFFLGVBQU0sQ0FBQyxrQkFBa0I7WUFDMUMsTUFBTSxFQUFFLGVBQU0sQ0FBQyxTQUFTO1NBQ3pCLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxxQkFBVyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsU0FBaUI7UUFDN0IsZ0JBQWdCO1FBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQ3BELGVBQWU7WUFDZixJQUFJLEVBQUUsR0FBb0IsSUFBSSxrQ0FBZSxFQUFFLENBQUM7WUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQStDLEVBQUUsRUFBRTtZQUMxRCxjQUFjO1lBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckQsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBbUIsRUFBRSxFQUFFO1lBQzlCLDRDQUE0QztZQUM1QyxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsWUFBWSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZELENBQUM7WUFDRCxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sV0FBVyxDQUFDLElBQVksRUFBRSxFQUFtQjtRQUNuRCxJQUFJLE1BQU0sR0FBbUM7WUFDM0MsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxJQUFJO2FBQ1o7WUFDRCxVQUFVLEVBQUU7Z0JBQ1YsS0FBSzthQUNOO1NBQ0YsQ0FBQztRQUVGLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBd0MsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBZSxFQUFFLFFBQXlDLEVBQUUsRUFBRTtnQkFDbEcsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLDhEQUE4RCxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ25HLENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckMsd0JBQXdCO29CQUN4QixNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQy9CLENBQUM7Z0JBRUQsa0NBQWtDO2dCQUNsQyxJQUFJLEVBQUUsR0FBMkIsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFekQsSUFBSSxVQUFVLEdBQWEsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUN0SSxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzFCLEVBQUUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekQsQ0FBQztnQkFFRCxJQUFJLFFBQVEsR0FBYSxFQUFFLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUM1QixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUM1QyxDQUFDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQy9CLEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNoRCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO2dCQUM5QyxFQUFFLENBQUMsUUFBUSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFFL0MsT0FBTyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVU7UUFDMUMsSUFBSSxLQUFLLEdBQWEsRUFBRSxDQUFDO1FBQ3pCLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDNUIsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3JDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRU8sVUFBVSxDQUFDLElBQVksRUFBRSxFQUFtQjtRQUNsRCw0Q0FBNEM7UUFDNUMsSUFBSSxNQUFNLEdBQW9DO1lBQzVDLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsSUFBSTthQUNaO1lBQ0QsU0FBUyxFQUFFLEVBQUU7WUFDYixhQUFhLEVBQUUsRUFBRTtTQUNsQixDQUFDO1FBRUYsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFrQixDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUN0RCxpQkFBaUI7WUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBZSxFQUFFLFFBQTBDLEVBQUUsRUFBRTtnQkFDcEcsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLCtEQUErRCxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3BHLENBQUM7Z0JBQ0QsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUF3QixFQUFFLEVBQUU7b0JBQ25ELEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFDekMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2QsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FDRjtBQTNIRCx3Q0EySEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvbWlzZSA9IHJlcXVpcmUoJ2JsdWViaXJkJyk7XG5pbXBvcnQgQVdTID0gcmVxdWlyZSgnYXdzLXNkaycpO1xuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcbmltcG9ydCB7IFJla29nbml0aW9uIH0gZnJvbSAnYXdzLXNkayc7XG5pbXBvcnQgeyBBV1NFcnJvciB9IGZyb20gJ2F3cy1zZGsnO1xuaW1wb3J0IHsgU2VydmljZVJlc3BvbnNlIH0gZnJvbSAnLi9zZXJ2aWNlLXJlc3BvbnNlJztcblxuLyoqXG4gKiBVc2VzIHRoZSBBV1MgUmVrb2duaXRpb24gQVBJIHRvIGRldGVjdCB0YWdzIGFuZCBmYWNpYWwgZXhwcmVzc2lvbnMgaW4gdGhlIGdpdmVuIGltYWdlLlxuICovXG5leHBvcnQgY2xhc3MgQXdzUmVrb2duaXRpb24ge1xuXG4gIHByaXZhdGUgcmVrb2duaXRpb246IFJla29nbml0aW9uO1xuICBwcml2YXRlIHJlYWRGaWxlOiBGdW5jdGlvbiA9IFByb21pc2UucHJvbWlzaWZ5KHJlcXVpcmUoXCJmc1wiKS5yZWFkRmlsZSk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgQVdTLmNvbmZpZy51cGRhdGUoe1xuICAgICAgYWNjZXNzS2V5SWQ6IENvbmZpZy5hd3NBY2Nlc3NLZXlJZCxcbiAgICAgIHNlY3JldEFjY2Vzc0tleTogQ29uZmlnLmF3c1NlY3JldEFjY2Vzc0tleSxcbiAgICAgIHJlZ2lvbjogQ29uZmlnLmF3c1JlZ2lvblxuICAgIH0pO1xuXG4gICAgdGhpcy5yZWtvZ25pdGlvbiA9IG5ldyBSZWtvZ25pdGlvbigpO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVjdCB0YWdzICYgZmFjaWFsIGV4cHJlc3Npb25zLlxuICAgKiBcbiAgICogQHBhcmFtIGlucHV0RmlsZSBGdWxsIHBhdGggdG8gdGhlIGltYWdlIHRvIGFuYWx5emVcbiAgICovXG4gIHB1YmxpYyBkZXRlY3QoaW5wdXRGaWxlOiBzdHJpbmcpOiBQcm9taXNlPFNlcnZpY2VSZXNwb25zZT4ge1xuICAgIC8vIFJlYWQgdGhlIGZpbGVcbiAgICByZXR1cm4gdGhpcy5yZWFkRmlsZShpbnB1dEZpbGUpLnRoZW4oKGRhdGE6IEJ1ZmZlcikgPT4ge1xuICAgICAgLy8gRGV0ZWN0IGZhY2VzXG4gICAgICBsZXQgc3I6IFNlcnZpY2VSZXNwb25zZSA9IG5ldyBTZXJ2aWNlUmVzcG9uc2UoKTtcbiAgICAgIHJldHVybiB0aGlzLmRldGVjdEZhY2VzKGRhdGEsIHNyKTtcbiAgICB9KS50aGVuKChyZXNwb25zZTogeyBkYXRhOiBCdWZmZXIsIHNyOiBTZXJ2aWNlUmVzcG9uc2UgfSkgPT4ge1xuICAgICAgLy8gRGV0ZWN0IHRhZ3NcbiAgICAgIHJldHVybiB0aGlzLmRldGVjdFRhZ3MocmVzcG9uc2UuZGF0YSwgcmVzcG9uc2Uuc3IpO1xuICAgIH0pLnRoZW4oKHNyOiBTZXJ2aWNlUmVzcG9uc2UpID0+IHtcbiAgICAgIC8vIEFkZCBkZXRlY3RlZCB0YWdzIHRvIG1ldGFkYXRhIGFuZCByZXNvbHZlXG4gICAgICBpZiAoQ29uZmlnLmF3c1RhZ3NGaWVsZCAmJiBzci50YWdzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgc3IubWV0YWRhdGFbQ29uZmlnLmF3c1RhZ3NGaWVsZF0gPSBzci50YWdzLmpvaW4oJywnKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzcjtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZGV0ZWN0RmFjZXMoZGF0YTogQnVmZmVyLCBzcjogU2VydmljZVJlc3BvbnNlKTogUHJvbWlzZTx7IGRhdGE6IEJ1ZmZlciwgc3I6IFNlcnZpY2VSZXNwb25zZSB9PiB7XG4gICAgbGV0IHBhcmFtczogUmVrb2duaXRpb24uRGV0ZWN0RmFjZXNSZXF1ZXN0ID0ge1xuICAgICAgSW1hZ2U6IHtcbiAgICAgICAgQnl0ZXM6IGRhdGFcbiAgICAgIH0sXG4gICAgICBBdHRyaWJ1dGVzOiBbXG4gICAgICAgICdBTEwnXG4gICAgICBdXG4gICAgfTtcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZTx7IGRhdGE6IEJ1ZmZlciwgc3I6IFNlcnZpY2VSZXNwb25zZSB9PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLnJla29nbml0aW9uLmRldGVjdEZhY2VzKHBhcmFtcywgKGVycm9yOiBBV1NFcnJvciwgcmVzcG9uc2U6IFJla29nbml0aW9uLkRldGVjdEZhY2VzUmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgcmV0dXJuIHJlamVjdChuZXcgRXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIGdldHRpbmcgZmFjZXMgZnJvbSBBV1MgUmVrb2duaXRpb246ICcgKyBlcnJvcikpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChyZXNwb25zZS5GYWNlRGV0YWlscy5sZW5ndGggPT0gMCkge1xuICAgICAgICAgIC8vIE5vdGhpbmcgZm91bmQsIHJldHVyblxuICAgICAgICAgIHJldHVybiByZXNvbHZlKHsgZGF0YSwgc3IgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBKdXN0IHdvcmsgd2l0aCB0aGUgZmlyc3QgcmVzdWx0XG4gICAgICAgIGxldCBmZDogUmVrb2duaXRpb24uRmFjZURldGFpbCA9IHJlc3BvbnNlLkZhY2VEZXRhaWxzWzBdO1xuXG4gICAgICAgIGxldCBwcm9wZXJ0aWVzOiBzdHJpbmdbXSA9IHRoaXMuZ2V0RmFjZVByb3BzKGZkLCBbJ0JlYXJkJywgJ0V5ZWdsYXNzZXMnLCAnRXllc09wZW4nLCAnTW91dGhPcGVuJywgJ011c3RhY2hlJywgJ1NtaWxlJywgJ1N1bmdsYXNzZXMnXSk7XG4gICAgICAgIGlmIChwcm9wZXJ0aWVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBzci5tZXRhZGF0YS5jZl9wZXJzb25Qcm9wZXJ0aWVzID0gcHJvcGVydGllcy5qb2luKCcsJyk7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgZW1vdGlvbnM6IHN0cmluZ1tdID0gW107XG4gICAgICAgIGZkLkVtb3Rpb25zLmZvckVhY2goZW1vdGlvbiA9PiB7XG4gICAgICAgICAgaWYgKGVtb3Rpb24uQ29uZmlkZW5jZSA+PSA4MCkge1xuICAgICAgICAgICAgZW1vdGlvbnMucHVzaChlbW90aW9uLlR5cGUudG9Mb3dlckNhc2UoKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoZW1vdGlvbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgIHNyLm1ldGFkYXRhLmNmX3BlcnNvbkVtb3Rpb25zID0gZW1vdGlvbnMuam9pbignLCcpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGZkLkdlbmRlci5Db25maWRlbmNlID49IDk1KSB7XG4gICAgICAgICAgc3IubWV0YWRhdGEuY2ZfcGVyc29uR2VuZGVyID0gZmQuR2VuZGVyLlZhbHVlO1xuICAgICAgICB9XG5cbiAgICAgICAgc3IubWV0YWRhdGEuY2ZfcGVyc29uQWdlTWluID0gZmQuQWdlUmFuZ2UuTG93O1xuICAgICAgICBzci5tZXRhZGF0YS5jZl9wZXJzb25BZ2VNYXggPSBmZC5BZ2VSYW5nZS5IaWdoO1xuXG4gICAgICAgIHJlc29sdmUoeyBkYXRhLCBzciB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRGYWNlUHJvcHMoZmFjZURldGFpbHMsIHByb3BlcnRpZXMpOiBzdHJpbmdbXSB7XG4gICAgdmFyIHByb3BzOiBzdHJpbmdbXSA9IFtdO1xuICAgIHByb3BlcnRpZXMuZm9yRWFjaChwcm9wTmFtZSA9PiB7XG4gICAgICB2YXIgcHJvcGVydHkgPSBmYWNlRGV0YWlsc1twcm9wTmFtZV07XG4gICAgICBpZiAocHJvcGVydHkuVmFsdWUgJiYgcHJvcGVydHkuQ29uZmlkZW5jZSA+PSA5NSkge1xuICAgICAgICBwcm9wcy5wdXNoKHByb3BOYW1lKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gcHJvcHM7XG4gIH1cblxuICBwcml2YXRlIGRldGVjdFRhZ3MoZGF0YTogQnVmZmVyLCBzcjogU2VydmljZVJlc3BvbnNlKTogUHJvbWlzZTxTZXJ2aWNlUmVzcG9uc2U+IHtcbiAgICAvLyByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIGxldCBwYXJhbXM6IFJla29nbml0aW9uLkRldGVjdExhYmVsc1JlcXVlc3QgPSB7XG4gICAgICBJbWFnZToge1xuICAgICAgICBCeXRlczogZGF0YVxuICAgICAgfSxcbiAgICAgIE1heExhYmVsczogMjAsXG4gICAgICBNaW5Db25maWRlbmNlOiA4MFxuICAgIH07XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2U8U2VydmljZVJlc3BvbnNlPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAvLyBHZXQgdGhlIGxhYmVsc1xuICAgICAgdGhpcy5yZWtvZ25pdGlvbi5kZXRlY3RMYWJlbHMocGFyYW1zLCAoZXJyb3I6IEFXU0Vycm9yLCByZXNwb25zZTogUmVrb2duaXRpb24uRGV0ZWN0TGFiZWxzUmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgcmV0dXJuIHJlamVjdChuZXcgRXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIGdldHRpbmcgbGFiZWxzIGZyb20gQVdTIFJla29nbml0aW9uOiAnICsgZXJyb3IpKTtcbiAgICAgICAgfVxuICAgICAgICByZXNwb25zZS5MYWJlbHMuZm9yRWFjaCgobGFiZWw6IFJla29nbml0aW9uLkxhYmVsKSA9PiB7XG4gICAgICAgICAgc3IudGFncy5wdXNoKGxhYmVsLk5hbWUudG9Mb3dlckNhc2UoKSk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXNvbHZlKHNyKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59Il19