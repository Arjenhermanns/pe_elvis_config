"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const url = require("url");
const fs = require("fs");
const path = require("path");
const api_manager_1 = require("../elvis-api/api-manager");
const Promise = require("bluebird");
const uuidV4 = require("uuid/v4");
class FileUtils {
    /**
     * Download a thumbnail from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadThumbnail(hit, destinationFolder) {
        return this.download(DownloadKind.Thumbnail, hit, destinationFolder);
    }
    /**
     * Download a preview from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadPreview(hit, destinationFolder) {
        return this.download(DownloadKind.Preview, hit, destinationFolder);
    }
    /**
     * Download the original file from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadOriginal(hit, destinationFolder) {
        return this.download(DownloadKind.Original, hit, destinationFolder);
    }
    /**
     * Download a thumbnail, preview or original file from Elvis to a specified destination folder.
     * On success, returns a Promise with the destination file path.
     *
     * @param kind What to download: thumbnail, preview or original
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static download(kind, hit, destinationFolder) {
        if (!hit)
            throw new Error('hit is a required parameter');
        if (!destinationFolder)
            throw new Error('destinationFolder is a required parameter');
        let fileUrl;
        switch (kind) {
            case DownloadKind.Thumbnail:
                fileUrl = hit.thumbnailUrl;
                break;
            case DownloadKind.Preview:
                fileUrl = hit.previewUrl;
                break;
            case DownloadKind.Original:
                fileUrl = hit.originalUrl;
                break;
            default:
                throw new Error('Unsupported kind specified: ' + kind);
        }
        if (!fileUrl) {
            throw new Error('No ' + DownloadKind[kind] + ' URL available for hit: ' + hit.id);
        }
        let filename = path.basename(url.parse(fileUrl).pathname);
        let ext = path.extname(filename);
        let baseFilename = path.basename(filename, ext);
        let tempFilename = baseFilename + '_' + uuidV4() + ext;
        let destination = path.join(destinationFolder, tempFilename);
        return this.downloadFile(hit.previewUrl, destination);
    }
    /**
     * Download an Elvis URL.
     * On success, returns a Promise with the destination file path.
     *
     *  @param url URL to download
     * @param destination Destination folder
     */
    static downloadFile(url, destination) {
        return this.createDestinationDirectory(destination).then(() => {
            return api_manager_1.ApiManager.getApi().elvisRequest.requestFile(url, destination);
        });
    }
    /**
     * Create a folder for a given file if it doesn't exists
     *
     * @param file
     */
    static createDestinationDirectory(file) {
        let dir = require('path').dirname(file);
        return new Promise((resolve, reject) => {
            fs.mkdir(dir, error => {
                if (!error || (error && error.code === 'EEXIST')) {
                    resolve(dir);
                }
                else {
                    reject(error);
                }
            });
        });
    }
}
exports.FileUtils = FileUtils;
var DownloadKind;
(function (DownloadKind) {
    DownloadKind[DownloadKind["Thumbnail"] = 0] = "Thumbnail";
    DownloadKind[DownloadKind["Preview"] = 1] = "Preview";
    DownloadKind[DownloadKind["Original"] = 2] = "Original";
})(DownloadKind = exports.DownloadKind || (exports.DownloadKind = {}));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL2ZpbGUtdXRpbHMudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9maWxlLXV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkJBQTRCO0FBQzVCLHlCQUEwQjtBQUMxQiw2QkFBOEI7QUFDOUIsMERBQXNEO0FBQ3RELG9DQUFxQztBQUVyQyxrQ0FBbUM7QUFFbkM7SUFFRTs7Ozs7O09BTUc7SUFDSSxNQUFNLENBQUMsaUJBQWlCLENBQUMsR0FBbUIsRUFBRSxpQkFBeUI7UUFDNUUsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0ksTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFtQixFQUFFLGlCQUF5QjtRQUMxRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBbUIsRUFBRSxpQkFBeUI7UUFDM0UsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBa0IsRUFBRSxHQUFtQixFQUFFLGlCQUF5QjtRQUN2RixFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQztRQUN6RCxFQUFFLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO1lBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO1FBRXJGLElBQUksT0FBZSxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQyxTQUFTO2dCQUN6QixPQUFPLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQztnQkFDM0IsS0FBSyxDQUFDO1lBQ1IsS0FBSyxZQUFZLENBQUMsT0FBTztnQkFDdkIsT0FBTyxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUM7Z0JBQ3pCLEtBQUssQ0FBQztZQUNSLEtBQUssWUFBWSxDQUFDLFFBQVE7Z0JBQ3hCLE9BQU8sR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDO2dCQUMxQixLQUFLLENBQUM7WUFDUjtnQkFDRSxNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzNELENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDYixNQUFNLElBQUksS0FBSyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsMEJBQTBCLEdBQUcsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7UUFFRCxJQUFJLFFBQVEsR0FBVyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEUsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxJQUFJLFlBQVksR0FBVyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN4RCxJQUFJLFlBQVksR0FBVyxZQUFZLEdBQUcsR0FBRyxHQUFHLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQztRQUMvRCxJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxDQUFDO1FBRXJFLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNLLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBVyxFQUFFLFdBQW1CO1FBQzFELE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUM1RCxNQUFNLENBQUMsd0JBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQVk7UUFDcEQsSUFBSSxHQUFHLEdBQVcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQVMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDN0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ3BCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2YsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hCLENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUVGO0FBMUdELDhCQTBHQztBQUVELElBQVksWUFJWDtBQUpELFdBQVksWUFBWTtJQUN0Qix5REFBUyxDQUFBO0lBQ1QscURBQU8sQ0FBQTtJQUNQLHVEQUFRLENBQUE7QUFDVixDQUFDLEVBSlcsWUFBWSxHQUFaLG9CQUFZLEtBQVosb0JBQVksUUFJdkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgdXJsID0gcmVxdWlyZSgndXJsJyk7XG5pbXBvcnQgZnMgPSByZXF1aXJlKCdmcycpO1xuaW1wb3J0IHBhdGggPSByZXF1aXJlKCdwYXRoJyk7XG5pbXBvcnQgeyBBcGlNYW5hZ2VyIH0gZnJvbSAnLi4vZWx2aXMtYXBpL2FwaS1tYW5hZ2VyJztcbmltcG9ydCBQcm9taXNlID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcbmltcG9ydCBsdnMgPSByZXF1aXJlKCcuLi9lbHZpcy1hcGkvYXBpJyk7XG5pbXBvcnQgdXVpZFY0ID0gcmVxdWlyZSgndXVpZC92NCcpO1xuXG5leHBvcnQgY2xhc3MgRmlsZVV0aWxzIHtcblxuICAvKipcbiAgICogRG93bmxvYWQgYSB0aHVtYm5haWwgZnJvbSBFbHZpcy5cbiAgICogT24gc3VjY2VzcywgcmV0dXJucyBhIFByb21pc2Ugd2l0aCB0aGUgZGVzdGluYXRpb24gZmlsZSBwYXRoLlxuICAgKiBcbiAgICogQHBhcmFtIGhpdCBIaXQgdG8gZG93bmxvYWRcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uRm9sZGVyIFRhcmdldCBmb2xkZXIgd2hlcmUgdGhlIGZpbGUgaXMgZG93bmxvYWRlZFxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBkb3dubG9hZFRodW1ibmFpbChoaXQ6IGx2cy5IaXRFbGVtZW50LCBkZXN0aW5hdGlvbkZvbGRlcjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5kb3dubG9hZChEb3dubG9hZEtpbmQuVGh1bWJuYWlsLCBoaXQsIGRlc3RpbmF0aW9uRm9sZGVyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEb3dubG9hZCBhIHByZXZpZXcgZnJvbSBFbHZpcy5cbiAgICogT24gc3VjY2VzcywgcmV0dXJucyBhIFByb21pc2Ugd2l0aCB0aGUgZGVzdGluYXRpb24gZmlsZSBwYXRoLlxuICAgKiBcbiAgICogQHBhcmFtIGhpdCBIaXQgdG8gZG93bmxvYWRcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uRm9sZGVyIFRhcmdldCBmb2xkZXIgd2hlcmUgdGhlIGZpbGUgaXMgZG93bmxvYWRlZFxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBkb3dubG9hZFByZXZpZXcoaGl0OiBsdnMuSGl0RWxlbWVudCwgZGVzdGluYXRpb25Gb2xkZXI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHRoaXMuZG93bmxvYWQoRG93bmxvYWRLaW5kLlByZXZpZXcsIGhpdCwgZGVzdGluYXRpb25Gb2xkZXIpO1xuICB9XG5cbiAgLyoqXG4gICAqIERvd25sb2FkIHRoZSBvcmlnaW5hbCBmaWxlIGZyb20gRWx2aXMuXG4gICAqIE9uIHN1Y2Nlc3MsIHJldHVybnMgYSBQcm9taXNlIHdpdGggdGhlIGRlc3RpbmF0aW9uIGZpbGUgcGF0aC5cbiAgICogXG4gICAqIEBwYXJhbSBoaXQgSGl0IHRvIGRvd25sb2FkXG4gICAqIEBwYXJhbSBkZXN0aW5hdGlvbkZvbGRlciBUYXJnZXQgZm9sZGVyIHdoZXJlIHRoZSBmaWxlIGlzIGRvd25sb2FkZWRcbiAgICovXG4gIHB1YmxpYyBzdGF0aWMgZG93bmxvYWRPcmlnaW5hbChoaXQ6IGx2cy5IaXRFbGVtZW50LCBkZXN0aW5hdGlvbkZvbGRlcjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5kb3dubG9hZChEb3dubG9hZEtpbmQuT3JpZ2luYWwsIGhpdCwgZGVzdGluYXRpb25Gb2xkZXIpO1xuICB9XG5cbiAgLyoqXG4gICAqIERvd25sb2FkIGEgdGh1bWJuYWlsLCBwcmV2aWV3IG9yIG9yaWdpbmFsIGZpbGUgZnJvbSBFbHZpcyB0byBhIHNwZWNpZmllZCBkZXN0aW5hdGlvbiBmb2xkZXIuIFxuICAgKiBPbiBzdWNjZXNzLCByZXR1cm5zIGEgUHJvbWlzZSB3aXRoIHRoZSBkZXN0aW5hdGlvbiBmaWxlIHBhdGguXG4gICAqIFxuICAgKiBAcGFyYW0ga2luZCBXaGF0IHRvIGRvd25sb2FkOiB0aHVtYm5haWwsIHByZXZpZXcgb3Igb3JpZ2luYWxcbiAgICogQHBhcmFtIGhpdCBIaXQgdG8gZG93bmxvYWRcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uRm9sZGVyIFRhcmdldCBmb2xkZXIgd2hlcmUgdGhlIGZpbGUgaXMgZG93bmxvYWRlZFxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBkb3dubG9hZChraW5kOiBEb3dubG9hZEtpbmQsIGhpdDogbHZzLkhpdEVsZW1lbnQsIGRlc3RpbmF0aW9uRm9sZGVyOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGlmICghaGl0KSB0aHJvdyBuZXcgRXJyb3IoJ2hpdCBpcyBhIHJlcXVpcmVkIHBhcmFtZXRlcicpO1xuICAgIGlmICghZGVzdGluYXRpb25Gb2xkZXIpIHRocm93IG5ldyBFcnJvcignZGVzdGluYXRpb25Gb2xkZXIgaXMgYSByZXF1aXJlZCBwYXJhbWV0ZXInKTtcblxuICAgIGxldCBmaWxlVXJsOiBzdHJpbmc7XG4gICAgc3dpdGNoIChraW5kKSB7XG4gICAgICBjYXNlIERvd25sb2FkS2luZC5UaHVtYm5haWw6XG4gICAgICAgIGZpbGVVcmwgPSBoaXQudGh1bWJuYWlsVXJsO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRG93bmxvYWRLaW5kLlByZXZpZXc6XG4gICAgICAgIGZpbGVVcmwgPSBoaXQucHJldmlld1VybDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIERvd25sb2FkS2luZC5PcmlnaW5hbDpcbiAgICAgICAgZmlsZVVybCA9IGhpdC5vcmlnaW5hbFVybDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1Vuc3VwcG9ydGVkIGtpbmQgc3BlY2lmaWVkOiAnICsga2luZCk7XG4gICAgfVxuICAgIGlmICghZmlsZVVybCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdObyAnICsgRG93bmxvYWRLaW5kW2tpbmRdICsgJyBVUkwgYXZhaWxhYmxlIGZvciBoaXQ6ICcgKyBoaXQuaWQpO1xuICAgIH1cblxuICAgIGxldCBmaWxlbmFtZTogc3RyaW5nID0gcGF0aC5iYXNlbmFtZSh1cmwucGFyc2UoZmlsZVVybCkucGF0aG5hbWUpO1xuICAgIGxldCBleHQ6IHN0cmluZyA9IHBhdGguZXh0bmFtZShmaWxlbmFtZSk7XG4gICAgbGV0IGJhc2VGaWxlbmFtZTogc3RyaW5nID0gcGF0aC5iYXNlbmFtZShmaWxlbmFtZSwgZXh0KTtcbiAgICBsZXQgdGVtcEZpbGVuYW1lOiBzdHJpbmcgPSBiYXNlRmlsZW5hbWUgKyAnXycgKyB1dWlkVjQoKSArIGV4dDtcbiAgICBsZXQgZGVzdGluYXRpb246IHN0cmluZyA9IHBhdGguam9pbihkZXN0aW5hdGlvbkZvbGRlciwgdGVtcEZpbGVuYW1lKTtcblxuICAgIHJldHVybiB0aGlzLmRvd25sb2FkRmlsZShoaXQucHJldmlld1VybCwgZGVzdGluYXRpb24pO1xuICB9XG5cbiAgLyoqXG4gICAqIERvd25sb2FkIGFuIEVsdmlzIFVSTC5cbiAgICogT24gc3VjY2VzcywgcmV0dXJucyBhIFByb21pc2Ugd2l0aCB0aGUgZGVzdGluYXRpb24gZmlsZSBwYXRoLlxuICAgKiBcbiAgICogIEBwYXJhbSB1cmwgVVJMIHRvIGRvd25sb2FkXG4gICAqIEBwYXJhbSBkZXN0aW5hdGlvbiBEZXN0aW5hdGlvbiBmb2xkZXJcbiAgICovXG4gIHByaXZhdGUgc3RhdGljIGRvd25sb2FkRmlsZSh1cmw6IHN0cmluZywgZGVzdGluYXRpb246IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHRoaXMuY3JlYXRlRGVzdGluYXRpb25EaXJlY3RvcnkoZGVzdGluYXRpb24pLnRoZW4oKCkgPT4ge1xuICAgICAgcmV0dXJuIEFwaU1hbmFnZXIuZ2V0QXBpKCkuZWx2aXNSZXF1ZXN0LnJlcXVlc3RGaWxlKHVybCwgZGVzdGluYXRpb24pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIGZvbGRlciBmb3IgYSBnaXZlbiBmaWxlIGlmIGl0IGRvZXNuJ3QgZXhpc3RzXG4gICAqIFxuICAgKiBAcGFyYW0gZmlsZSBcbiAgICovXG4gIHByaXZhdGUgc3RhdGljIGNyZWF0ZURlc3RpbmF0aW9uRGlyZWN0b3J5KGZpbGU6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IGRpcjogc3RyaW5nID0gcmVxdWlyZSgncGF0aCcpLmRpcm5hbWUoZmlsZSk7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPHN0cmluZz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgZnMubWtkaXIoZGlyLCBlcnJvciA9PiB7XG4gICAgICAgIGlmICghZXJyb3IgfHwgKGVycm9yICYmIGVycm9yLmNvZGUgPT09ICdFRVhJU1QnKSkge1xuICAgICAgICAgIHJlc29sdmUoZGlyKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG59XG5cbmV4cG9ydCBlbnVtIERvd25sb2FkS2luZCB7XG4gIFRodW1ibmFpbCxcbiAgUHJldmlldyxcbiAgT3JpZ2luYWxcbn0iXX0=