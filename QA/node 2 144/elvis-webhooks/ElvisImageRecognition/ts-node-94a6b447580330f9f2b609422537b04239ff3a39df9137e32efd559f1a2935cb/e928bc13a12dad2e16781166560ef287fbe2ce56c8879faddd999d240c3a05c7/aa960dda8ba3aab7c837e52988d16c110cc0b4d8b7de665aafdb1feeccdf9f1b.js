"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const config_1 = require("../config");
const api_manager_1 = require("../elvis-api/api-manager");
const file_utils_1 = require("./file-utils");
const utils_1 = require("./utils");
const google_vision_1 = require("./service/google-vision");
const aws_rekognition_1 = require("./service/aws-rekognition");
const clarifai_1 = require("./service/clarifai");
const google_translate_1 = require("./service/google-translate");
class Recognizer {
    constructor(useClarifai = false, useGoogle = false, useAws = false, translate = false) {
        this.useClarifai = useClarifai;
        this.useGoogle = useGoogle;
        this.useAws = useAws;
        this.translate = translate;
        this.api = api_manager_1.ApiManager.getApi();
        this.deleteFile = Promise.promisify(require('fs').unlink);
        if (!useClarifai && !useGoogle && !useAws) {
            throw new Error('Specify at least one recognition service');
        }
        if (useClarifai) {
            this.clarifai = new clarifai_1.Clarifai();
        }
        if (useGoogle) {
            this.googleVision = new google_vision_1.GoogleVision();
        }
        if (useAws) {
            this.aws = new aws_rekognition_1.AwsRekognition();
        }
        if (translate) {
            this.googleTranslate = new google_translate_1.GoogleTranslate();
        }
    }
    recognize(assetId, models = null, assetPath = null) {
        console.info('Image recognition started for asset: ' + assetId);
        let filePath;
        let metadata = {};
        // 1. Download the asset preview
        return this.downloadAsset(assetId).then((path) => {
            // 2. Send image to all configured AI image recognition services
            filePath = path;
            let services = [];
            if (this.useClarifai) {
                services.push(this.clarifai.detect(filePath, models, assetPath));
            }
            if (this.useGoogle) {
                services.push(this.googleVision.detect(filePath));
            }
            if (this.useAws) {
                services.push(this.aws.detect(filePath));
            }
            return Promise.all(services);
        }).then((serviceResponses) => {
            // 3. Delete the image, we no longer need it 
            this.deleteFile(filePath).catch((error) => {
                console.error('Unable to remove temporary file: ' + error);
            });
            // 4. Combine results from the services
            let tags = [];
            serviceResponses.forEach((serviceResponse) => {
                // Merge metadata values, note: this is quite a blunt merge, 
                // this only works because the cloud services don't use identical metadata fields
                metadata = (Object.assign(metadata, serviceResponse.metadata));
                tags = utils_1.Utils.mergeArrays(tags, serviceResponse.tags);
            });
            let tagString = tags.join(',');
            metadata[config_1.Config.elvisTagsField] = tagString;
            // 5. Translate (if configured)
            if (this.translate) {
                return this.googleTranslate.translate(tagString).then((translatedMetadata) => {
                    metadata = (Object.assign(metadata, translatedMetadata));
                });
            }
        }).then(() => {
            // 6. Update metadata
            metadata[config_1.Config.aiMetadataModifiedField] = new Date().getTime();
            return this.api.update(assetId, JSON.stringify(metadata), undefined, 'filename');
        }).then((hit) => {
            // 7. We're done!
            console.info('Image recognition finshed for asset: ' + assetId + ' (' + hit.metadata['filename'] + ')');
            return hit;
        }).catch((error) => {
            if (error instanceof NoPreviewError) {
                // We're not logging this error as it's triggered by desktop client uploads
                // console.info(error.message);
            }
            else {
                console.error('Image recognition failed for asset: ' + assetId + '. Error details:\n' + error.stack);
            }
        });
    }
    downloadAsset(assetId) {
        let query = 'id:' + assetId;
        let search = {
            query: {
                QueryStringQuery: {
                    queryString: query
                }
            },
            returnPendingImports: true
        };
        return this.api.searchPost(search).then((sr) => {
            if (sr.totalHits !== 1) {
                // Should only happen when the asset is not available any more for some reason (deleted / incorrect permission setup)
                throw new Error('Unexpected number of assets retrieved (' + sr.totalHits + '). This query should return 1 asset: ' + query
                    + '\nThis error can occur when the asset is no longer available in Elvis or when the configured Elvis user does not have permission to access the given asset.');
            }
            let hit = sr.hits[0];
            if (!hit.previewUrl) {
                throw new NoPreviewError('Asset ' + assetId + ' doesn\'t have a preview, unable to extract labels.', assetId);
            }
            return file_utils_1.FileUtils.downloadPreview(hit, config_1.Config.tempDir);
        });
    }
}
exports.Recognizer = Recognizer;
class NoPreviewError extends Error {
    constructor(message = '', assetId = '') {
        super(message);
        this.message = message;
        this.assetId = assetId;
    }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3JlY29nbml6ZXIudHMiLCJzb3VyY2VzIjpbIi9zcnYvRWx2aXNXZWJIb29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9yZWNvZ25pemVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0NBQXFDO0FBQ3JDLHNDQUFtQztBQUNuQywwREFBc0Q7QUFFdEQsNkNBQXlDO0FBQ3pDLG1DQUFnQztBQUNoQywyREFBdUQ7QUFDdkQsK0RBQTJEO0FBQzNELGlEQUE4QztBQUU5QyxpRUFBNkQ7QUFFN0Q7SUFVRSxZQUFtQixjQUF1QixLQUFLLEVBQVMsWUFBcUIsS0FBSyxFQUFTLFNBQWtCLEtBQUssRUFBUyxZQUFZLEtBQUs7UUFBekgsZ0JBQVcsR0FBWCxXQUFXLENBQWlCO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFpQjtRQUFTLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFScEksUUFBRyxHQUFhLHdCQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDcEMsZUFBVSxHQUFhLE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBUXJFLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUMxQyxNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7UUFDOUQsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztRQUNqQyxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSw0QkFBWSxFQUFFLENBQUM7UUFDekMsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDWCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksZ0NBQWMsRUFBRSxDQUFDO1FBQ2xDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLGtDQUFlLEVBQUUsQ0FBQztRQUMvQyxDQUFDO0lBQ0gsQ0FBQztJQUVNLFNBQVMsQ0FBQyxPQUFlLEVBQUUsU0FBbUIsSUFBSSxFQUFFLFlBQW9CLElBQUk7UUFFakYsT0FBTyxDQUFDLElBQUksQ0FBQyx1Q0FBdUMsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUVoRSxJQUFJLFFBQWdCLENBQUM7UUFDckIsSUFBSSxRQUFRLEdBQVEsRUFBRSxDQUFDO1FBRXZCLGdDQUFnQztRQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFZLEVBQUUsRUFBRTtZQUN2RCxnRUFBZ0U7WUFDaEUsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ25FLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDbkIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3BELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDaEIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzNDLENBQUM7WUFDRCxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxnQkFBbUMsRUFBRSxFQUFFO1lBQzlDLDZDQUE2QztZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQTRCLEVBQUUsRUFBRTtnQkFDL0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUM3RCxDQUFDLENBQUMsQ0FBQztZQUVILHVDQUF1QztZQUN2QyxJQUFJLElBQUksR0FBYSxFQUFFLENBQUM7WUFDeEIsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsZUFBZ0MsRUFBRSxFQUFFO2dCQUM1RCw2REFBNkQ7Z0JBQzdELGlGQUFpRjtnQkFDakYsUUFBUSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQy9ELElBQUksR0FBRyxhQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkQsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLFNBQVMsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLFFBQVEsQ0FBQyxlQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsU0FBUyxDQUFDO1lBRTVDLCtCQUErQjtZQUMvQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLGtCQUF1QixFQUFFLEVBQUU7b0JBQ2hGLFFBQVEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLENBQUMsQ0FBQztnQkFDM0QsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNYLHFCQUFxQjtZQUNyQixRQUFRLENBQUMsZUFBTSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNoRSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQWUsRUFBRSxFQUFFO1lBQzFCLGlCQUFpQjtZQUNqQixPQUFPLENBQUMsSUFBSSxDQUFDLHVDQUF1QyxHQUFHLE9BQU8sR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUN4RyxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ2IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDdEIsRUFBRSxDQUFDLENBQUMsS0FBSyxZQUFZLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLDJFQUEyRTtnQkFDM0UsK0JBQStCO1lBQ2pDLENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxHQUFHLE9BQU8sR0FBRyxvQkFBb0IsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkcsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGFBQWEsQ0FBQyxPQUFlO1FBQ25DLElBQUksS0FBSyxHQUFXLEtBQUssR0FBRyxPQUFPLENBQUM7UUFDcEMsSUFBSSxNQUFNLEdBQWdCO1lBQ3hCLEtBQUssRUFBRTtnQkFDTCxnQkFBZ0IsRUFBRTtvQkFDaEIsV0FBVyxFQUFFLEtBQUs7aUJBQ25CO2FBQ0Y7WUFDRCxvQkFBb0IsRUFBRSxJQUFJO1NBQzNCLENBQUM7UUFDRixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBa0IsRUFBRSxFQUFFO1lBQzdELEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkIscUhBQXFIO2dCQUNySCxNQUFNLElBQUksS0FBSyxDQUFDLHlDQUF5QyxHQUFHLEVBQUUsQ0FBQyxTQUFTLEdBQUcsdUNBQXVDLEdBQUcsS0FBSztzQkFDdEgsNkpBQTZKLENBQUMsQ0FBQztZQUNySyxDQUFDO1lBQ0QsSUFBSSxHQUFHLEdBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixNQUFNLElBQUksY0FBYyxDQUFDLFFBQVEsR0FBRyxPQUFPLEdBQUcscURBQXFELEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDaEgsQ0FBQztZQUNELE1BQU0sQ0FBQyxzQkFBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsZUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3hELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBbkhELGdDQW1IQztBQUVELG9CQUFxQixTQUFRLEtBQUs7SUFDaEMsWUFBbUIsVUFBVSxFQUFFLEVBQVMsVUFBa0IsRUFBRTtRQUMxRCxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFERSxZQUFPLEdBQVAsT0FBTyxDQUFLO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBYTtJQUU1RCxDQUFDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvbWlzZSA9IHJlcXVpcmUoJ2JsdWViaXJkJyk7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi9jb25maWcnO1xuaW1wb3J0IHsgQXBpTWFuYWdlciB9IGZyb20gJy4uL2VsdmlzLWFwaS9hcGktbWFuYWdlcic7XG5pbXBvcnQgeyBFbHZpc0FwaSwgQXNzZXRTZWFyY2gsIFNlYXJjaFJlc3BvbnNlLCBIaXRFbGVtZW50IH0gZnJvbSAnLi4vZWx2aXMtYXBpL2FwaSc7XG5pbXBvcnQgeyBGaWxlVXRpbHMgfSBmcm9tICcuL2ZpbGUtdXRpbHMnO1xuaW1wb3J0IHsgVXRpbHMgfSBmcm9tICcuL3V0aWxzJztcbmltcG9ydCB7IEdvb2dsZVZpc2lvbiB9IGZyb20gJy4vc2VydmljZS9nb29nbGUtdmlzaW9uJztcbmltcG9ydCB7IEF3c1Jla29nbml0aW9uIH0gZnJvbSAnLi9zZXJ2aWNlL2F3cy1yZWtvZ25pdGlvbic7XG5pbXBvcnQgeyBDbGFyaWZhaSB9IGZyb20gJy4vc2VydmljZS9jbGFyaWZhaSc7XG5pbXBvcnQgeyBTZXJ2aWNlUmVzcG9uc2UgfSBmcm9tICcuL3NlcnZpY2Uvc2VydmljZS1yZXNwb25zZSc7XG5pbXBvcnQgeyBHb29nbGVUcmFuc2xhdGUgfSBmcm9tICcuL3NlcnZpY2UvZ29vZ2xlLXRyYW5zbGF0ZSc7XG5cbmV4cG9ydCBjbGFzcyBSZWNvZ25pemVyIHtcblxuICBwcml2YXRlIGFwaTogRWx2aXNBcGkgPSBBcGlNYW5hZ2VyLmdldEFwaSgpO1xuICBwcml2YXRlIGRlbGV0ZUZpbGU6IEZ1bmN0aW9uID0gUHJvbWlzZS5wcm9taXNpZnkocmVxdWlyZSgnZnMnKS51bmxpbmspO1xuXG4gIHByaXZhdGUgY2xhcmlmYWk6IENsYXJpZmFpO1xuICBwcml2YXRlIGdvb2dsZVZpc2lvbjogR29vZ2xlVmlzaW9uO1xuICBwcml2YXRlIGF3czogQXdzUmVrb2duaXRpb247XG4gIHByaXZhdGUgZ29vZ2xlVHJhbnNsYXRlOiBHb29nbGVUcmFuc2xhdGU7XG5cbiAgY29uc3RydWN0b3IocHVibGljIHVzZUNsYXJpZmFpOiBib29sZWFuID0gZmFsc2UsIHB1YmxpYyB1c2VHb29nbGU6IGJvb2xlYW4gPSBmYWxzZSwgcHVibGljIHVzZUF3czogYm9vbGVhbiA9IGZhbHNlLCBwdWJsaWMgdHJhbnNsYXRlID0gZmFsc2UpIHtcbiAgICBpZiAoIXVzZUNsYXJpZmFpICYmICF1c2VHb29nbGUgJiYgIXVzZUF3cykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdTcGVjaWZ5IGF0IGxlYXN0IG9uZSByZWNvZ25pdGlvbiBzZXJ2aWNlJyk7XG4gICAgfVxuICAgIGlmICh1c2VDbGFyaWZhaSkge1xuICAgICAgdGhpcy5jbGFyaWZhaSA9IG5ldyBDbGFyaWZhaSgpO1xuICAgIH1cbiAgICBpZiAodXNlR29vZ2xlKSB7XG4gICAgICB0aGlzLmdvb2dsZVZpc2lvbiA9IG5ldyBHb29nbGVWaXNpb24oKTtcbiAgICB9XG4gICAgaWYgKHVzZUF3cykge1xuICAgICAgdGhpcy5hd3MgPSBuZXcgQXdzUmVrb2duaXRpb24oKTtcbiAgICB9XG4gICAgaWYgKHRyYW5zbGF0ZSkge1xuICAgICAgdGhpcy5nb29nbGVUcmFuc2xhdGUgPSBuZXcgR29vZ2xlVHJhbnNsYXRlKCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHJlY29nbml6ZShhc3NldElkOiBzdHJpbmcsIG1vZGVsczogc3RyaW5nW10gPSBudWxsLCBhc3NldFBhdGg6IHN0cmluZyA9IG51bGwpOiBQcm9taXNlPEhpdEVsZW1lbnQ+IHtcblxuICAgIGNvbnNvbGUuaW5mbygnSW1hZ2UgcmVjb2duaXRpb24gc3RhcnRlZCBmb3IgYXNzZXQ6ICcgKyBhc3NldElkKTtcblxuICAgIGxldCBmaWxlUGF0aDogc3RyaW5nO1xuICAgIGxldCBtZXRhZGF0YTogYW55ID0ge307XG5cbiAgICAvLyAxLiBEb3dubG9hZCB0aGUgYXNzZXQgcHJldmlld1xuICAgIHJldHVybiB0aGlzLmRvd25sb2FkQXNzZXQoYXNzZXRJZCkudGhlbigocGF0aDogc3RyaW5nKSA9PiB7XG4gICAgICAvLyAyLiBTZW5kIGltYWdlIHRvIGFsbCBjb25maWd1cmVkIEFJIGltYWdlIHJlY29nbml0aW9uIHNlcnZpY2VzXG4gICAgICBmaWxlUGF0aCA9IHBhdGg7XG4gICAgICBsZXQgc2VydmljZXMgPSBbXTtcbiAgICAgIGlmICh0aGlzLnVzZUNsYXJpZmFpKSB7XG4gICAgICAgIHNlcnZpY2VzLnB1c2godGhpcy5jbGFyaWZhaS5kZXRlY3QoZmlsZVBhdGgsIG1vZGVscywgYXNzZXRQYXRoKSk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy51c2VHb29nbGUpIHtcbiAgICAgICAgc2VydmljZXMucHVzaCh0aGlzLmdvb2dsZVZpc2lvbi5kZXRlY3QoZmlsZVBhdGgpKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnVzZUF3cykge1xuICAgICAgICBzZXJ2aWNlcy5wdXNoKHRoaXMuYXdzLmRldGVjdChmaWxlUGF0aCkpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIFByb21pc2UuYWxsKHNlcnZpY2VzKTtcbiAgICB9KS50aGVuKChzZXJ2aWNlUmVzcG9uc2VzOiBTZXJ2aWNlUmVzcG9uc2VbXSkgPT4ge1xuICAgICAgLy8gMy4gRGVsZXRlIHRoZSBpbWFnZSwgd2Ugbm8gbG9uZ2VyIG5lZWQgaXQgXG4gICAgICB0aGlzLmRlbGV0ZUZpbGUoZmlsZVBhdGgpLmNhdGNoKChlcnJvcjogTm9kZUpTLkVycm5vRXhjZXB0aW9uKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byByZW1vdmUgdGVtcG9yYXJ5IGZpbGU6ICcgKyBlcnJvcik7XG4gICAgICB9KTtcblxuICAgICAgLy8gNC4gQ29tYmluZSByZXN1bHRzIGZyb20gdGhlIHNlcnZpY2VzXG4gICAgICBsZXQgdGFnczogc3RyaW5nW10gPSBbXTtcbiAgICAgIHNlcnZpY2VSZXNwb25zZXMuZm9yRWFjaCgoc2VydmljZVJlc3BvbnNlOiBTZXJ2aWNlUmVzcG9uc2UpID0+IHtcbiAgICAgICAgLy8gTWVyZ2UgbWV0YWRhdGEgdmFsdWVzLCBub3RlOiB0aGlzIGlzIHF1aXRlIGEgYmx1bnQgbWVyZ2UsIFxuICAgICAgICAvLyB0aGlzIG9ubHkgd29ya3MgYmVjYXVzZSB0aGUgY2xvdWQgc2VydmljZXMgZG9uJ3QgdXNlIGlkZW50aWNhbCBtZXRhZGF0YSBmaWVsZHNcbiAgICAgICAgbWV0YWRhdGEgPSAoT2JqZWN0LmFzc2lnbihtZXRhZGF0YSwgc2VydmljZVJlc3BvbnNlLm1ldGFkYXRhKSk7XG4gICAgICAgIHRhZ3MgPSBVdGlscy5tZXJnZUFycmF5cyh0YWdzLCBzZXJ2aWNlUmVzcG9uc2UudGFncyk7XG4gICAgICB9KTtcbiAgICAgIGxldCB0YWdTdHJpbmc6IHN0cmluZyA9IHRhZ3Muam9pbignLCcpO1xuICAgICAgbWV0YWRhdGFbQ29uZmlnLmVsdmlzVGFnc0ZpZWxkXSA9IHRhZ1N0cmluZztcblxuICAgICAgLy8gNS4gVHJhbnNsYXRlIChpZiBjb25maWd1cmVkKVxuICAgICAgaWYgKHRoaXMudHJhbnNsYXRlKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdvb2dsZVRyYW5zbGF0ZS50cmFuc2xhdGUodGFnU3RyaW5nKS50aGVuKCh0cmFuc2xhdGVkTWV0YWRhdGE6IGFueSkgPT4ge1xuICAgICAgICAgIG1ldGFkYXRhID0gKE9iamVjdC5hc3NpZ24obWV0YWRhdGEsIHRyYW5zbGF0ZWRNZXRhZGF0YSkpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KS50aGVuKCgpID0+IHtcbiAgICAgIC8vIDYuIFVwZGF0ZSBtZXRhZGF0YVxuICAgICAgbWV0YWRhdGFbQ29uZmlnLmFpTWV0YWRhdGFNb2RpZmllZEZpZWxkXSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgcmV0dXJuIHRoaXMuYXBpLnVwZGF0ZShhc3NldElkLCBKU09OLnN0cmluZ2lmeShtZXRhZGF0YSksIHVuZGVmaW5lZCwgJ2ZpbGVuYW1lJyk7XG4gICAgfSkudGhlbigoaGl0OiBIaXRFbGVtZW50KSA9PiB7XG4gICAgICAvLyA3LiBXZSdyZSBkb25lIVxuICAgICAgY29uc29sZS5pbmZvKCdJbWFnZSByZWNvZ25pdGlvbiBmaW5zaGVkIGZvciBhc3NldDogJyArIGFzc2V0SWQgKyAnICgnICsgaGl0Lm1ldGFkYXRhWydmaWxlbmFtZSddICsgJyknKTtcbiAgICAgIHJldHVybiBoaXQ7XG4gICAgfSkuY2F0Y2goKGVycm9yOiBhbnkpID0+IHtcbiAgICAgIGlmIChlcnJvciBpbnN0YW5jZW9mIE5vUHJldmlld0Vycm9yKSB7XG4gICAgICAgIC8vIFdlJ3JlIG5vdCBsb2dnaW5nIHRoaXMgZXJyb3IgYXMgaXQncyB0cmlnZ2VyZWQgYnkgZGVza3RvcCBjbGllbnQgdXBsb2Fkc1xuICAgICAgICAvLyBjb25zb2xlLmluZm8oZXJyb3IubWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgY29uc29sZS5lcnJvcignSW1hZ2UgcmVjb2duaXRpb24gZmFpbGVkIGZvciBhc3NldDogJyArIGFzc2V0SWQgKyAnLiBFcnJvciBkZXRhaWxzOlxcbicgKyBlcnJvci5zdGFjayk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRvd25sb2FkQXNzZXQoYXNzZXRJZDogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgcXVlcnk6IHN0cmluZyA9ICdpZDonICsgYXNzZXRJZDtcbiAgICBsZXQgc2VhcmNoOiBBc3NldFNlYXJjaCA9IHtcbiAgICAgIHF1ZXJ5OiB7XG4gICAgICAgIFF1ZXJ5U3RyaW5nUXVlcnk6IHtcbiAgICAgICAgICBxdWVyeVN0cmluZzogcXVlcnlcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHJldHVyblBlbmRpbmdJbXBvcnRzOiB0cnVlXG4gICAgfTtcbiAgICByZXR1cm4gdGhpcy5hcGkuc2VhcmNoUG9zdChzZWFyY2gpLnRoZW4oKHNyOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xuICAgICAgaWYgKHNyLnRvdGFsSGl0cyAhPT0gMSkge1xuICAgICAgICAvLyBTaG91bGQgb25seSBoYXBwZW4gd2hlbiB0aGUgYXNzZXQgaXMgbm90IGF2YWlsYWJsZSBhbnkgbW9yZSBmb3Igc29tZSByZWFzb24gKGRlbGV0ZWQgLyBpbmNvcnJlY3QgcGVybWlzc2lvbiBzZXR1cClcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbmV4cGVjdGVkIG51bWJlciBvZiBhc3NldHMgcmV0cmlldmVkICgnICsgc3IudG90YWxIaXRzICsgJykuIFRoaXMgcXVlcnkgc2hvdWxkIHJldHVybiAxIGFzc2V0OiAnICsgcXVlcnlcbiAgICAgICAgICArICdcXG5UaGlzIGVycm9yIGNhbiBvY2N1ciB3aGVuIHRoZSBhc3NldCBpcyBubyBsb25nZXIgYXZhaWxhYmxlIGluIEVsdmlzIG9yIHdoZW4gdGhlIGNvbmZpZ3VyZWQgRWx2aXMgdXNlciBkb2VzIG5vdCBoYXZlIHBlcm1pc3Npb24gdG8gYWNjZXNzIHRoZSBnaXZlbiBhc3NldC4nKTtcbiAgICAgIH1cbiAgICAgIGxldCBoaXQ6IEhpdEVsZW1lbnQgPSBzci5oaXRzWzBdO1xuICAgICAgaWYgKCFoaXQucHJldmlld1VybCkge1xuICAgICAgICB0aHJvdyBuZXcgTm9QcmV2aWV3RXJyb3IoJ0Fzc2V0ICcgKyBhc3NldElkICsgJyBkb2VzblxcJ3QgaGF2ZSBhIHByZXZpZXcsIHVuYWJsZSB0byBleHRyYWN0IGxhYmVscy4nLCBhc3NldElkKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBGaWxlVXRpbHMuZG93bmxvYWRQcmV2aWV3KGhpdCwgQ29uZmlnLnRlbXBEaXIpO1xuICAgIH0pO1xuICB9XG59XG5cbmNsYXNzIE5vUHJldmlld0Vycm9yIGV4dGVuZHMgRXJyb3Ige1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgbWVzc2FnZSA9ICcnLCBwdWJsaWMgYXNzZXRJZDogc3RyaW5nID0gJycpIHtcbiAgICBzdXBlcihtZXNzYWdlKTtcbiAgfVxufVxuIl19