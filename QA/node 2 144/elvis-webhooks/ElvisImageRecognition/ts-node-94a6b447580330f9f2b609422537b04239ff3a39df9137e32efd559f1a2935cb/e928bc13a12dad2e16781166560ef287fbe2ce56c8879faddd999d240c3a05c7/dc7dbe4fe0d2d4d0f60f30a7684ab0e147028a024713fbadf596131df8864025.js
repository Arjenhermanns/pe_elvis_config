"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const translate = require("@google-cloud/translate");
const google_base_1 = require("./google-base");
const config_1 = require("../../config");
/**
 * Uses the Google Translate API to translate tags.
 */
class GoogleTranslate extends google_base_1.Google {
    constructor() {
        super();
        this.validateConfig().then(() => {
            if (!config_1.Config.languages) {
                throw new Error('The languages config property cannot be empty.');
            }
            if (!config_1.Config.sourceLanguage) {
                throw new Error('The sourceLanguage property cannot be empty.');
            }
            if (!config_1.Config.languageTagFields) {
                throw new Error('The languageTagFields property cannot be empty.');
            }
            this.languages = config_1.Config.languages.split(',');
            this.languageTagFields = config_1.Config.languageTagFields.split(',');
            if (this.languages.length != this.languageTagFields.length) {
                throw new Error('Invalid language translation configuration, the number of languages (' + this.languages.length + ') must be equal to the number of languageTagFields (' + this.languageTagFields.length + ').');
            }
            this.gt = translate({
                keyFilename: config_1.Config.googleKeyFilename
            });
        });
    }
    /**
     * Translate text into the configured languages
     *
     * @param text Text string to translate
     */
    translate(text) {
        let translations = [];
        let metadata = {};
        for (let i = 0; i < this.languages.length; i++) {
            if (this.languages[i] !== config_1.Config.sourceLanguage) {
                translations.push(this.translateTo(text, config_1.Config.sourceLanguage, this.languages[i]));
            }
        }
        return Promise.all(translations).then((responses) => {
            responses.forEach(response => {
                let metadataField = this.languageTagFields[this.languages.indexOf(response.language)];
                metadata[metadataField] = response.translation;
            });
            return metadata;
        });
    }
    translateTo(text, sourceLanguage, destinationLanguage) {
        let options = {
            from: sourceLanguage,
            to: destinationLanguage
        };
        return this.gt.translate(text, options).then((response) => {
            return {
                language: destinationLanguage,
                translation: response[0]
            };
        }).catch((error) => {
            throw new Error('An error occurred while translating tags: ' + error.message);
        });
    }
}
exports.GoogleTranslate = GoogleTranslate;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLXRyYW5zbGF0ZS50cyIsInNvdXJjZXMiOlsiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLXRyYW5zbGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHFEQUFzRDtBQUN0RCwrQ0FBdUM7QUFDdkMseUNBQXNDO0FBRXRDOztHQUVHO0FBQ0gscUJBQTZCLFNBQVEsb0JBQU07SUFPekM7UUFDRSxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLE1BQU0sSUFBSSxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztZQUNwRSxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ2xFLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztZQUNyRSxDQUFDO1lBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxlQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZUFBTSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUU3RCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDM0QsTUFBTSxJQUFJLEtBQUssQ0FBQyx1RUFBdUUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxzREFBc0QsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQ25OLENBQUM7WUFFRCxJQUFJLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDbEIsV0FBVyxFQUFFLGVBQU0sQ0FBQyxpQkFBaUI7YUFDdEMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLFNBQVMsQ0FBQyxJQUFZO1FBQzNCLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLFFBQVEsR0FBUSxFQUFFLENBQUM7UUFFdkIsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssZUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsZUFBTSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RixDQUFDO1FBQ0gsQ0FBQztRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQWdCLEVBQUUsRUFBRTtZQUN6RCxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUMzQixJQUFJLGFBQWEsR0FBVyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzlGLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ2pELENBQUMsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxXQUFXLENBQUMsSUFBWSxFQUFFLGNBQWMsRUFBRSxtQkFBMkI7UUFDM0UsSUFBSSxPQUFPLEdBQVE7WUFDakIsSUFBSSxFQUFFLGNBQWM7WUFDcEIsRUFBRSxFQUFFLG1CQUFtQjtTQUN4QixDQUFDO1FBRUYsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUM3RCxNQUFNLENBQUM7Z0JBQ0wsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDekIsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQUMsNENBQTRDLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBdkVELDBDQXVFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0cmFuc2xhdGUgPSByZXF1aXJlKCdAZ29vZ2xlLWNsb3VkL3RyYW5zbGF0ZScpO1xuaW1wb3J0IHsgR29vZ2xlIH0gZnJvbSAnLi9nb29nbGUtYmFzZSc7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIFVzZXMgdGhlIEdvb2dsZSBUcmFuc2xhdGUgQVBJIHRvIHRyYW5zbGF0ZSB0YWdzLlxuICovXG5leHBvcnQgY2xhc3MgR29vZ2xlVHJhbnNsYXRlIGV4dGVuZHMgR29vZ2xlIHtcblxuICBwcml2YXRlIGd0O1xuXG4gIHByaXZhdGUgbGFuZ3VhZ2VzOiBzdHJpbmdbXTtcbiAgcHJpdmF0ZSBsYW5ndWFnZVRhZ0ZpZWxkczogc3RyaW5nW107XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLnZhbGlkYXRlQ29uZmlnKCkudGhlbigoKSA9PiB7XG4gICAgICBpZiAoIUNvbmZpZy5sYW5ndWFnZXMpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgbGFuZ3VhZ2VzIGNvbmZpZyBwcm9wZXJ0eSBjYW5ub3QgYmUgZW1wdHkuJyk7XG4gICAgICB9XG4gICAgICBpZiAoIUNvbmZpZy5zb3VyY2VMYW5ndWFnZSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBzb3VyY2VMYW5ndWFnZSBwcm9wZXJ0eSBjYW5ub3QgYmUgZW1wdHkuJyk7XG4gICAgICB9XG4gICAgICBpZiAoIUNvbmZpZy5sYW5ndWFnZVRhZ0ZpZWxkcykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBsYW5ndWFnZVRhZ0ZpZWxkcyBwcm9wZXJ0eSBjYW5ub3QgYmUgZW1wdHkuJyk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMubGFuZ3VhZ2VzID0gQ29uZmlnLmxhbmd1YWdlcy5zcGxpdCgnLCcpO1xuICAgICAgdGhpcy5sYW5ndWFnZVRhZ0ZpZWxkcyA9IENvbmZpZy5sYW5ndWFnZVRhZ0ZpZWxkcy5zcGxpdCgnLCcpO1xuXG4gICAgICBpZiAodGhpcy5sYW5ndWFnZXMubGVuZ3RoICE9IHRoaXMubGFuZ3VhZ2VUYWdGaWVsZHMubGVuZ3RoKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBsYW5ndWFnZSB0cmFuc2xhdGlvbiBjb25maWd1cmF0aW9uLCB0aGUgbnVtYmVyIG9mIGxhbmd1YWdlcyAoJyArIHRoaXMubGFuZ3VhZ2VzLmxlbmd0aCArICcpIG11c3QgYmUgZXF1YWwgdG8gdGhlIG51bWJlciBvZiBsYW5ndWFnZVRhZ0ZpZWxkcyAoJyArIHRoaXMubGFuZ3VhZ2VUYWdGaWVsZHMubGVuZ3RoICsgJykuJyk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZ3QgPSB0cmFuc2xhdGUoe1xuICAgICAgICBrZXlGaWxlbmFtZTogQ29uZmlnLmdvb2dsZUtleUZpbGVuYW1lXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUcmFuc2xhdGUgdGV4dCBpbnRvIHRoZSBjb25maWd1cmVkIGxhbmd1YWdlc1xuICAgKiBcbiAgICogQHBhcmFtIHRleHQgVGV4dCBzdHJpbmcgdG8gdHJhbnNsYXRlXG4gICAqL1xuICBwdWJsaWMgdHJhbnNsYXRlKHRleHQ6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHRyYW5zbGF0aW9ucyA9IFtdO1xuICAgIGxldCBtZXRhZGF0YTogYW55ID0ge307XG5cbiAgICBmb3IgKGxldCBpOiBudW1iZXIgPSAwOyBpIDwgdGhpcy5sYW5ndWFnZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmICh0aGlzLmxhbmd1YWdlc1tpXSAhPT0gQ29uZmlnLnNvdXJjZUxhbmd1YWdlKSB7XG4gICAgICAgIHRyYW5zbGF0aW9ucy5wdXNoKHRoaXMudHJhbnNsYXRlVG8odGV4dCwgQ29uZmlnLnNvdXJjZUxhbmd1YWdlLCB0aGlzLmxhbmd1YWdlc1tpXSkpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gUHJvbWlzZS5hbGwodHJhbnNsYXRpb25zKS50aGVuKChyZXNwb25zZXM6IGFueVtdKSA9PiB7XG4gICAgICByZXNwb25zZXMuZm9yRWFjaChyZXNwb25zZSA9PiB7XG4gICAgICAgIGxldCBtZXRhZGF0YUZpZWxkOiBzdHJpbmcgPSB0aGlzLmxhbmd1YWdlVGFnRmllbGRzW3RoaXMubGFuZ3VhZ2VzLmluZGV4T2YocmVzcG9uc2UubGFuZ3VhZ2UpXTtcbiAgICAgICAgbWV0YWRhdGFbbWV0YWRhdGFGaWVsZF0gPSByZXNwb25zZS50cmFuc2xhdGlvbjtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIG1ldGFkYXRhO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSB0cmFuc2xhdGVUbyh0ZXh0OiBzdHJpbmcsIHNvdXJjZUxhbmd1YWdlLCBkZXN0aW5hdGlvbkxhbmd1YWdlOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCBvcHRpb25zOiBhbnkgPSB7XG4gICAgICBmcm9tOiBzb3VyY2VMYW5ndWFnZSxcbiAgICAgIHRvOiBkZXN0aW5hdGlvbkxhbmd1YWdlXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmd0LnRyYW5zbGF0ZSh0ZXh0LCBvcHRpb25zKS50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsYW5ndWFnZTogZGVzdGluYXRpb25MYW5ndWFnZSxcbiAgICAgICAgdHJhbnNsYXRpb246IHJlc3BvbnNlWzBdXG4gICAgICB9O1xuICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdBbiBlcnJvciBvY2N1cnJlZCB3aGlsZSB0cmFuc2xhdGluZyB0YWdzOiAnICsgZXJyb3IubWVzc2FnZSk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==