This is a special folder with internal Elvis plugins.

The folder will be updated at server startup and created the first time the server starts. 
It will not overwrite your modifications to files inside this folder or any of it's subfolders.

DO NOT MOVE OR RENAME this folder!

Detailed documentation about internal plugins:
https://helpcenter.woodwing.com/hc/articles/202249509-Plugins-introduction-internal-plugins