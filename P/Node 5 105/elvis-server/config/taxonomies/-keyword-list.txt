Photo specs
	Image type
		Action
		Aerial
		Background
		Close-up
		Conceptual
		Full body
		Group
		Headshot
		Indoor
		Landscape
		Outdoor
		Portrait
		Profile
		Silhouette
		Still life
		Time exposure
	Scans
		35mm print
		35mm transparency
		Large format
		Medium format
		Panoramic
	Time
		Day
		Night
		Sunrise
		Sunset		
Stock categories
	Abstract
	Agriculture
	Animals
	Architecture
	Backgrounds
	Business
	Celebrations
	City
	Culture
	Documentary
	Education
	Entertainment
	Fantasy
	Fine art
	Flowers
	Food
	Glamour
	Government
	Health
	Historic
	Holiday
	Homes
	Industry
	International
	Landscapes
	Leisure
	Lifestyles
	Manufacturing
	Medical
	Military
	Models
	Nature
	Nautical
	Nostalgia
	Outdoors
	People
		Adults
		Baby
		Boy
		Children
		Couple
		Girl
		Man
		Woman
	Political
	Recreation
	Religion
	Science
	Signs
	Sports
	Still life
	Symbols
	Technology
	Tourism
	Traditional
	Transportation
	Travel
	Underwater
	Vintage
	Weather
	Wildlife
Photojournalism
	Arts
	Conceptual
	Environment
	Feature
	Magazine
	Nature
	News
		Domestic
		Election
		General
		International
	Newspaper
	Personality
	Photo essay
	Portrait
	Sequence
	Sports
		Action
		Feature
		Individual
		Team