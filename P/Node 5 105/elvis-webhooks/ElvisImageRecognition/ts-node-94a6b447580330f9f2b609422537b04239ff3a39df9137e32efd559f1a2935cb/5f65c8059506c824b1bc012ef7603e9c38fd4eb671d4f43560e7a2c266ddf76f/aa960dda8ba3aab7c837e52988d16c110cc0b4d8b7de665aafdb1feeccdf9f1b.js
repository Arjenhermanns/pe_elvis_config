"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const config_1 = require("../config");
const api_manager_1 = require("../elvis-api/api-manager");
const file_utils_1 = require("./file-utils");
const utils_1 = require("./utils");
const google_vision_1 = require("./service/google-vision");
const aws_rekognition_1 = require("./service/aws-rekognition");
const clarifai_1 = require("./service/clarifai");
const google_translate_1 = require("./service/google-translate");
class Recognizer {
    constructor(useClarifai = false, useGoogle = false, useAws = false, translate = false) {
        this.useClarifai = useClarifai;
        this.useGoogle = useGoogle;
        this.useAws = useAws;
        this.translate = translate;
        this.api = api_manager_1.ApiManager.getApi();
        this.deleteFile = Promise.promisify(require('fs').unlink);
        if (!useClarifai && !useGoogle && !useAws) {
            throw new Error('Specify at least one recognition service');
        }
        if (useClarifai) {
            this.clarifai = new clarifai_1.Clarifai();
        }
        if (useGoogle) {
            this.googleVision = new google_vision_1.GoogleVision();
        }
        if (useAws) {
            this.aws = new aws_rekognition_1.AwsRekognition();
        }
        if (translate) {
            this.googleTranslate = new google_translate_1.GoogleTranslate();
        }
    }
    recognize(assetId, models = null, assetPath = null) {
        console.info('Image recognition started for asset: ' + assetId);
        let filePath;
        let metadata = {};
        // 1. Download the asset preview
        return this.downloadAsset(assetId).then((path) => {
            // 2. Send image to all configured AI image recognition services
            filePath = path;
            let services = [];
            if (this.useClarifai) {
                services.push(this.clarifai.detect(filePath, models, assetPath));
            }
            if (this.useGoogle) {
                services.push(this.googleVision.detect(filePath));
            }
            if (this.useAws) {
                services.push(this.aws.detect(filePath));
            }
            return Promise.all(services);
        }).then((serviceResponses) => {
            // 3. Delete the image, we no longer need it 
            this.deleteFile(filePath).catch((error) => {
                console.error('Unable to remove temporary file: ' + error);
            });
            // 4. Combine results from the services
            let tags = [];
            serviceResponses.forEach((serviceResponse) => {
                // Merge metadata values, note: this is quite a blunt merge, 
                // this only works because the cloud services don't use identical metadata fields
                metadata = (Object.assign(metadata, serviceResponse.metadata));
                tags = utils_1.Utils.mergeArrays(tags, serviceResponse.tags);
            });
            let tagString = tags.join(',');
            metadata[config_1.Config.elvisTagsField] = tagString;
            // 5. Translate (if configured)
            if (this.translate) {
                return this.googleTranslate.translate(tagString).then((translatedMetadata) => {
                    metadata = (Object.assign(metadata, translatedMetadata));
                });
            }
        }).then(() => {
            // 6. Update metadata
            metadata[config_1.Config.aiMetadataModifiedField] = new Date().getTime();
            return this.api.update(assetId, JSON.stringify(metadata), undefined, 'filename');
        }).then((hit) => {
            // 7. We're done!
            console.info('Image recognition finshed for asset: ' + assetId + ' (' + hit.metadata['filename'] + ')');
            return hit;
        }).catch((error) => {
            if (error instanceof NoPreviewError) {
                // We're not logging this error as it's triggered by desktop client uploads
                // console.info(error.message);
            }
            else {
                console.error('Image recognition failed for asset: ' + assetId + '. Error details:\n' + error.stack);
            }
        });
    }
    downloadAsset(assetId) {
        let query = 'id:' + assetId;
        let search = {
            query: {
                QueryStringQuery: {
                    queryString: query
                }
            },
            returnPendingImports: true
        };
        return this.api.searchPost(search).then((sr) => {
            if (sr.totalHits !== 1) {
                // Should only happen when the asset is not available any more for some reason (deleted / incorrect permission setup)
                throw new Error('Unexpected number of assets retrieved (' + sr.totalHits + '). This query should return 1 asset: ' + query
                    + '\nThis error can occur when the asset is no longer available in Elvis or when the configured Elvis user does not have permission to access the given asset.');
            }
            let hit = sr.hits[0];
            if (!hit.previewUrl) {
                throw new NoPreviewError('Asset ' + assetId + ' doesn\'t have a preview, unable to extract labels.', assetId);
            }
            return file_utils_1.FileUtils.downloadPreview(hit, config_1.Config.tempDir);
        });
    }
}
exports.Recognizer = Recognizer;
class NoPreviewError extends Error {
    constructor(message = '', assetId = '') {
        super(message);
        this.message = message;
        this.assetId = assetId;
    }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9yZWNvZ25pemVyLnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3JlY29nbml6ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvQ0FBcUM7QUFDckMsc0NBQW1DO0FBQ25DLDBEQUFzRDtBQUV0RCw2Q0FBeUM7QUFDekMsbUNBQWdDO0FBQ2hDLDJEQUF1RDtBQUN2RCwrREFBMkQ7QUFDM0QsaURBQThDO0FBRTlDLGlFQUE2RDtBQUU3RDtJQVVFLFlBQW1CLGNBQXVCLEtBQUssRUFBUyxZQUFxQixLQUFLLEVBQVMsU0FBa0IsS0FBSyxFQUFTLFlBQVksS0FBSztRQUF6SCxnQkFBVyxHQUFYLFdBQVcsQ0FBaUI7UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUFTLFdBQU0sR0FBTixNQUFNLENBQWlCO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQVJwSSxRQUFHLEdBQWEsd0JBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQyxlQUFVLEdBQWEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7UUFRckUsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztRQUM5RCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO1FBQ2pDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLDRCQUFZLEVBQUUsQ0FBQztRQUN6QyxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxnQ0FBYyxFQUFFLENBQUM7UUFDbEMsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDZCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksa0NBQWUsRUFBRSxDQUFDO1FBQy9DLENBQUM7SUFDSCxDQUFDO0lBRU0sU0FBUyxDQUFDLE9BQWUsRUFBRSxTQUFtQixJQUFJLEVBQUUsWUFBb0IsSUFBSTtRQUVqRixPQUFPLENBQUMsSUFBSSxDQUFDLHVDQUF1QyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBRWhFLElBQUksUUFBZ0IsQ0FBQztRQUNyQixJQUFJLFFBQVEsR0FBUSxFQUFFLENBQUM7UUFFdkIsZ0NBQWdDO1FBQ2hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQ3ZELGdFQUFnRTtZQUNoRSxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDbkUsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDcEQsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQztZQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLGdCQUFtQyxFQUFFLEVBQUU7WUFDOUMsNkNBQTZDO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBNEIsRUFBRSxFQUFFO2dCQUMvRCxPQUFPLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzdELENBQUMsQ0FBQyxDQUFDO1lBRUgsdUNBQXVDO1lBQ3ZDLElBQUksSUFBSSxHQUFhLEVBQUUsQ0FBQztZQUN4QixnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxlQUFnQyxFQUFFLEVBQUU7Z0JBQzVELDZEQUE2RDtnQkFDN0QsaUZBQWlGO2dCQUNqRixRQUFRLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxHQUFHLGFBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2RCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksU0FBUyxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkMsUUFBUSxDQUFDLGVBQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxTQUFTLENBQUM7WUFFNUMsK0JBQStCO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsa0JBQXVCLEVBQUUsRUFBRTtvQkFDaEYsUUFBUSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ1gscUJBQXFCO1lBQ3JCLFFBQVEsQ0FBQyxlQUFNLENBQUMsdUJBQXVCLENBQUMsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2hFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBZSxFQUFFLEVBQUU7WUFDMUIsaUJBQWlCO1lBQ2pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUNBQXVDLEdBQUcsT0FBTyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ3hHLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDYixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFVLEVBQUUsRUFBRTtZQUN0QixFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVksY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsMkVBQTJFO2dCQUMzRSwrQkFBK0I7WUFDakMsQ0FBQztZQUNELElBQUksQ0FBQyxDQUFDO2dCQUNKLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0NBQXNDLEdBQUcsT0FBTyxHQUFHLG9CQUFvQixHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2RyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sYUFBYSxDQUFDLE9BQWU7UUFDbkMsSUFBSSxLQUFLLEdBQVcsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUNwQyxJQUFJLE1BQU0sR0FBZ0I7WUFDeEIsS0FBSyxFQUFFO2dCQUNMLGdCQUFnQixFQUFFO29CQUNoQixXQUFXLEVBQUUsS0FBSztpQkFDbkI7YUFDRjtZQUNELG9CQUFvQixFQUFFLElBQUk7U0FDM0IsQ0FBQztRQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFrQixFQUFFLEVBQUU7WUFDN0QsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixxSEFBcUg7Z0JBQ3JILE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLEdBQUcsRUFBRSxDQUFDLFNBQVMsR0FBRyx1Q0FBdUMsR0FBRyxLQUFLO3NCQUN0SCw2SkFBNkosQ0FBQyxDQUFDO1lBQ3JLLENBQUM7WUFDRCxJQUFJLEdBQUcsR0FBZSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLE1BQU0sSUFBSSxjQUFjLENBQUMsUUFBUSxHQUFHLE9BQU8sR0FBRyxxREFBcUQsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNoSCxDQUFDO1lBQ0QsTUFBTSxDQUFDLHNCQUFTLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxlQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUFuSEQsZ0NBbUhDO0FBRUQsb0JBQXFCLFNBQVEsS0FBSztJQUNoQyxZQUFtQixVQUFVLEVBQUUsRUFBUyxVQUFrQixFQUFFO1FBQzFELEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQURFLFlBQU8sR0FBUCxPQUFPLENBQUs7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUFhO0lBRTVELENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9taXNlID0gcmVxdWlyZSgnYmx1ZWJpcmQnKTtcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uL2NvbmZpZyc7XG5pbXBvcnQgeyBBcGlNYW5hZ2VyIH0gZnJvbSAnLi4vZWx2aXMtYXBpL2FwaS1tYW5hZ2VyJztcbmltcG9ydCB7IEVsdmlzQXBpLCBBc3NldFNlYXJjaCwgU2VhcmNoUmVzcG9uc2UsIEhpdEVsZW1lbnQgfSBmcm9tICcuLi9lbHZpcy1hcGkvYXBpJztcbmltcG9ydCB7IEZpbGVVdGlscyB9IGZyb20gJy4vZmlsZS11dGlscyc7XG5pbXBvcnQgeyBVdGlscyB9IGZyb20gJy4vdXRpbHMnO1xuaW1wb3J0IHsgR29vZ2xlVmlzaW9uIH0gZnJvbSAnLi9zZXJ2aWNlL2dvb2dsZS12aXNpb24nO1xuaW1wb3J0IHsgQXdzUmVrb2duaXRpb24gfSBmcm9tICcuL3NlcnZpY2UvYXdzLXJla29nbml0aW9uJztcbmltcG9ydCB7IENsYXJpZmFpIH0gZnJvbSAnLi9zZXJ2aWNlL2NsYXJpZmFpJztcbmltcG9ydCB7IFNlcnZpY2VSZXNwb25zZSB9IGZyb20gJy4vc2VydmljZS9zZXJ2aWNlLXJlc3BvbnNlJztcbmltcG9ydCB7IEdvb2dsZVRyYW5zbGF0ZSB9IGZyb20gJy4vc2VydmljZS9nb29nbGUtdHJhbnNsYXRlJztcblxuZXhwb3J0IGNsYXNzIFJlY29nbml6ZXIge1xuXG4gIHByaXZhdGUgYXBpOiBFbHZpc0FwaSA9IEFwaU1hbmFnZXIuZ2V0QXBpKCk7XG4gIHByaXZhdGUgZGVsZXRlRmlsZTogRnVuY3Rpb24gPSBQcm9taXNlLnByb21pc2lmeShyZXF1aXJlKCdmcycpLnVubGluayk7XG5cbiAgcHJpdmF0ZSBjbGFyaWZhaTogQ2xhcmlmYWk7XG4gIHByaXZhdGUgZ29vZ2xlVmlzaW9uOiBHb29nbGVWaXNpb247XG4gIHByaXZhdGUgYXdzOiBBd3NSZWtvZ25pdGlvbjtcbiAgcHJpdmF0ZSBnb29nbGVUcmFuc2xhdGU6IEdvb2dsZVRyYW5zbGF0ZTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgdXNlQ2xhcmlmYWk6IGJvb2xlYW4gPSBmYWxzZSwgcHVibGljIHVzZUdvb2dsZTogYm9vbGVhbiA9IGZhbHNlLCBwdWJsaWMgdXNlQXdzOiBib29sZWFuID0gZmFsc2UsIHB1YmxpYyB0cmFuc2xhdGUgPSBmYWxzZSkge1xuICAgIGlmICghdXNlQ2xhcmlmYWkgJiYgIXVzZUdvb2dsZSAmJiAhdXNlQXdzKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1NwZWNpZnkgYXQgbGVhc3Qgb25lIHJlY29nbml0aW9uIHNlcnZpY2UnKTtcbiAgICB9XG4gICAgaWYgKHVzZUNsYXJpZmFpKSB7XG4gICAgICB0aGlzLmNsYXJpZmFpID0gbmV3IENsYXJpZmFpKCk7XG4gICAgfVxuICAgIGlmICh1c2VHb29nbGUpIHtcbiAgICAgIHRoaXMuZ29vZ2xlVmlzaW9uID0gbmV3IEdvb2dsZVZpc2lvbigpO1xuICAgIH1cbiAgICBpZiAodXNlQXdzKSB7XG4gICAgICB0aGlzLmF3cyA9IG5ldyBBd3NSZWtvZ25pdGlvbigpO1xuICAgIH1cbiAgICBpZiAodHJhbnNsYXRlKSB7XG4gICAgICB0aGlzLmdvb2dsZVRyYW5zbGF0ZSA9IG5ldyBHb29nbGVUcmFuc2xhdGUoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgcmVjb2duaXplKGFzc2V0SWQ6IHN0cmluZywgbW9kZWxzOiBzdHJpbmdbXSA9IG51bGwsIGFzc2V0UGF0aDogc3RyaW5nID0gbnVsbCk6IFByb21pc2U8SGl0RWxlbWVudD4ge1xuXG4gICAgY29uc29sZS5pbmZvKCdJbWFnZSByZWNvZ25pdGlvbiBzdGFydGVkIGZvciBhc3NldDogJyArIGFzc2V0SWQpO1xuXG4gICAgbGV0IGZpbGVQYXRoOiBzdHJpbmc7XG4gICAgbGV0IG1ldGFkYXRhOiBhbnkgPSB7fTtcblxuICAgIC8vIDEuIERvd25sb2FkIHRoZSBhc3NldCBwcmV2aWV3XG4gICAgcmV0dXJuIHRoaXMuZG93bmxvYWRBc3NldChhc3NldElkKS50aGVuKChwYXRoOiBzdHJpbmcpID0+IHtcbiAgICAgIC8vIDIuIFNlbmQgaW1hZ2UgdG8gYWxsIGNvbmZpZ3VyZWQgQUkgaW1hZ2UgcmVjb2duaXRpb24gc2VydmljZXNcbiAgICAgIGZpbGVQYXRoID0gcGF0aDtcbiAgICAgIGxldCBzZXJ2aWNlcyA9IFtdO1xuICAgICAgaWYgKHRoaXMudXNlQ2xhcmlmYWkpIHtcbiAgICAgICAgc2VydmljZXMucHVzaCh0aGlzLmNsYXJpZmFpLmRldGVjdChmaWxlUGF0aCwgbW9kZWxzLCBhc3NldFBhdGgpKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnVzZUdvb2dsZSkge1xuICAgICAgICBzZXJ2aWNlcy5wdXNoKHRoaXMuZ29vZ2xlVmlzaW9uLmRldGVjdChmaWxlUGF0aCkpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMudXNlQXdzKSB7XG4gICAgICAgIHNlcnZpY2VzLnB1c2godGhpcy5hd3MuZGV0ZWN0KGZpbGVQYXRoKSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5hbGwoc2VydmljZXMpO1xuICAgIH0pLnRoZW4oKHNlcnZpY2VSZXNwb25zZXM6IFNlcnZpY2VSZXNwb25zZVtdKSA9PiB7XG4gICAgICAvLyAzLiBEZWxldGUgdGhlIGltYWdlLCB3ZSBubyBsb25nZXIgbmVlZCBpdCBcbiAgICAgIHRoaXMuZGVsZXRlRmlsZShmaWxlUGF0aCkuY2F0Y2goKGVycm9yOiBOb2RlSlMuRXJybm9FeGNlcHRpb24pID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHJlbW92ZSB0ZW1wb3JhcnkgZmlsZTogJyArIGVycm9yKTtcbiAgICAgIH0pO1xuXG4gICAgICAvLyA0LiBDb21iaW5lIHJlc3VsdHMgZnJvbSB0aGUgc2VydmljZXNcbiAgICAgIGxldCB0YWdzOiBzdHJpbmdbXSA9IFtdO1xuICAgICAgc2VydmljZVJlc3BvbnNlcy5mb3JFYWNoKChzZXJ2aWNlUmVzcG9uc2U6IFNlcnZpY2VSZXNwb25zZSkgPT4ge1xuICAgICAgICAvLyBNZXJnZSBtZXRhZGF0YSB2YWx1ZXMsIG5vdGU6IHRoaXMgaXMgcXVpdGUgYSBibHVudCBtZXJnZSwgXG4gICAgICAgIC8vIHRoaXMgb25seSB3b3JrcyBiZWNhdXNlIHRoZSBjbG91ZCBzZXJ2aWNlcyBkb24ndCB1c2UgaWRlbnRpY2FsIG1ldGFkYXRhIGZpZWxkc1xuICAgICAgICBtZXRhZGF0YSA9IChPYmplY3QuYXNzaWduKG1ldGFkYXRhLCBzZXJ2aWNlUmVzcG9uc2UubWV0YWRhdGEpKTtcbiAgICAgICAgdGFncyA9IFV0aWxzLm1lcmdlQXJyYXlzKHRhZ3MsIHNlcnZpY2VSZXNwb25zZS50YWdzKTtcbiAgICAgIH0pO1xuICAgICAgbGV0IHRhZ1N0cmluZzogc3RyaW5nID0gdGFncy5qb2luKCcsJyk7XG4gICAgICBtZXRhZGF0YVtDb25maWcuZWx2aXNUYWdzRmllbGRdID0gdGFnU3RyaW5nO1xuXG4gICAgICAvLyA1LiBUcmFuc2xhdGUgKGlmIGNvbmZpZ3VyZWQpXG4gICAgICBpZiAodGhpcy50cmFuc2xhdGUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ29vZ2xlVHJhbnNsYXRlLnRyYW5zbGF0ZSh0YWdTdHJpbmcpLnRoZW4oKHRyYW5zbGF0ZWRNZXRhZGF0YTogYW55KSA9PiB7XG4gICAgICAgICAgbWV0YWRhdGEgPSAoT2JqZWN0LmFzc2lnbihtZXRhZGF0YSwgdHJhbnNsYXRlZE1ldGFkYXRhKSk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pLnRoZW4oKCkgPT4ge1xuICAgICAgLy8gNi4gVXBkYXRlIG1ldGFkYXRhXG4gICAgICBtZXRhZGF0YVtDb25maWcuYWlNZXRhZGF0YU1vZGlmaWVkRmllbGRdID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICByZXR1cm4gdGhpcy5hcGkudXBkYXRlKGFzc2V0SWQsIEpTT04uc3RyaW5naWZ5KG1ldGFkYXRhKSwgdW5kZWZpbmVkLCAnZmlsZW5hbWUnKTtcbiAgICB9KS50aGVuKChoaXQ6IEhpdEVsZW1lbnQpID0+IHtcbiAgICAgIC8vIDcuIFdlJ3JlIGRvbmUhXG4gICAgICBjb25zb2xlLmluZm8oJ0ltYWdlIHJlY29nbml0aW9uIGZpbnNoZWQgZm9yIGFzc2V0OiAnICsgYXNzZXRJZCArICcgKCcgKyBoaXQubWV0YWRhdGFbJ2ZpbGVuYW1lJ10gKyAnKScpO1xuICAgICAgcmV0dXJuIGhpdDtcbiAgICB9KS5jYXRjaCgoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycm9yIGluc3RhbmNlb2YgTm9QcmV2aWV3RXJyb3IpIHtcbiAgICAgICAgLy8gV2UncmUgbm90IGxvZ2dpbmcgdGhpcyBlcnJvciBhcyBpdCdzIHRyaWdnZXJlZCBieSBkZXNrdG9wIGNsaWVudCB1cGxvYWRzXG4gICAgICAgIC8vIGNvbnNvbGUuaW5mbyhlcnJvci5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdJbWFnZSByZWNvZ25pdGlvbiBmYWlsZWQgZm9yIGFzc2V0OiAnICsgYXNzZXRJZCArICcuIEVycm9yIGRldGFpbHM6XFxuJyArIGVycm9yLnN0YWNrKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZG93bmxvYWRBc3NldChhc3NldElkOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGxldCBxdWVyeTogc3RyaW5nID0gJ2lkOicgKyBhc3NldElkO1xuICAgIGxldCBzZWFyY2g6IEFzc2V0U2VhcmNoID0ge1xuICAgICAgcXVlcnk6IHtcbiAgICAgICAgUXVlcnlTdHJpbmdRdWVyeToge1xuICAgICAgICAgIHF1ZXJ5U3RyaW5nOiBxdWVyeVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgcmV0dXJuUGVuZGluZ0ltcG9ydHM6IHRydWVcbiAgICB9O1xuICAgIHJldHVybiB0aGlzLmFwaS5zZWFyY2hQb3N0KHNlYXJjaCkudGhlbigoc3I6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XG4gICAgICBpZiAoc3IudG90YWxIaXRzICE9PSAxKSB7XG4gICAgICAgIC8vIFNob3VsZCBvbmx5IGhhcHBlbiB3aGVuIHRoZSBhc3NldCBpcyBub3QgYXZhaWxhYmxlIGFueSBtb3JlIGZvciBzb21lIHJlYXNvbiAoZGVsZXRlZCAvIGluY29ycmVjdCBwZXJtaXNzaW9uIHNldHVwKVxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1VuZXhwZWN0ZWQgbnVtYmVyIG9mIGFzc2V0cyByZXRyaWV2ZWQgKCcgKyBzci50b3RhbEhpdHMgKyAnKS4gVGhpcyBxdWVyeSBzaG91bGQgcmV0dXJuIDEgYXNzZXQ6ICcgKyBxdWVyeVxuICAgICAgICAgICsgJ1xcblRoaXMgZXJyb3IgY2FuIG9jY3VyIHdoZW4gdGhlIGFzc2V0IGlzIG5vIGxvbmdlciBhdmFpbGFibGUgaW4gRWx2aXMgb3Igd2hlbiB0aGUgY29uZmlndXJlZCBFbHZpcyB1c2VyIGRvZXMgbm90IGhhdmUgcGVybWlzc2lvbiB0byBhY2Nlc3MgdGhlIGdpdmVuIGFzc2V0LicpO1xuICAgICAgfVxuICAgICAgbGV0IGhpdDogSGl0RWxlbWVudCA9IHNyLmhpdHNbMF07XG4gICAgICBpZiAoIWhpdC5wcmV2aWV3VXJsKSB7XG4gICAgICAgIHRocm93IG5ldyBOb1ByZXZpZXdFcnJvcignQXNzZXQgJyArIGFzc2V0SWQgKyAnIGRvZXNuXFwndCBoYXZlIGEgcHJldmlldywgdW5hYmxlIHRvIGV4dHJhY3QgbGFiZWxzLicsIGFzc2V0SWQpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIEZpbGVVdGlscy5kb3dubG9hZFByZXZpZXcoaGl0LCBDb25maWcudGVtcERpcik7XG4gICAgfSk7XG4gIH1cbn1cblxuY2xhc3MgTm9QcmV2aWV3RXJyb3IgZXh0ZW5kcyBFcnJvciB7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBtZXNzYWdlID0gJycsIHB1YmxpYyBhc3NldElkOiBzdHJpbmcgPSAnJykge1xuICAgIHN1cGVyKG1lc3NhZ2UpO1xuICB9XG59XG4iXX0=