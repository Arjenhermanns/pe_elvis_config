"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vision = require("@google-cloud/vision");
const google_base_1 = require("./google-base");
const config_1 = require("../../config");
const service_response_1 = require("./service-response");
/**
 * Uses the Google Vision API to detect tags and locations in the given image.
 */
class GoogleVision extends google_base_1.Google {
    constructor() {
        super();
        this.validateConfig().then(() => {
            this.gv = vision({
                keyFilename: config_1.Config.googleKeyFilename
            });
        });
    }
    /**
     * Detect tags & locations
     *
     * @param inputFile Full path to the image to analyze
     */
    detect(inputFile) {
        return new Promise((resolve, reject) => {
            let params = {
                types: ['labels', 'landmarks'],
                verbose: true
            };
            this.gv.detect(inputFile, params).then((response) => {
                var sr = new service_response_1.ServiceResponse();
                response[0].labels.forEach(label => {
                    if (label.score > 0.85) {
                        sr.tags.push(label.desc.toLowerCase());
                    }
                });
                if (response[0].landmarks !== undefined && response[0].landmarks.length > 0) {
                    let landmarks = response[0].landmarks;
                    sr.metadata['gpsLatitude'] = landmarks[0].locations[0].latitude;
                    sr.metadata['gpsLongitude'] = landmarks[0].locations[0].longitude;
                    let locations = [];
                    landmarks.forEach(landmark => {
                        // Google sometimes gives us duplicate landmarks...
                        if (!locations.includes(landmark.desc))
                            locations.push(landmark.desc);
                    });
                    sr.metadata['shownSublocation'] = locations.join(', ');
                }
                if (config_1.Config.googleTagsField && sr.tags.length > 0) {
                    sr.metadata[config_1.Config.googleTagsField] = sr.tags.join(',');
                }
                resolve(sr);
            }).catch((error) => {
                reject(this.getErrorObj(inputFile, error));
            });
        });
    }
    getErrorObj(inputFile, error) {
        let errorVal = (typeof error === 'string') ? error : JSON.stringify(error, Object.getOwnPropertyNames(error), 2);
        return new Error('An error occurred while getting labels for "' + inputFile + '" from Google Vision: ' + errorVal);
    }
}
exports.GoogleVision = GoogleVision;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9zZXJ2aWNlL2dvb2dsZS12aXNpb24udHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNJbWFnZVJlY29nbml0aW9uL3NyYy9hcHAvc2VydmljZS9nb29nbGUtdmlzaW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsK0NBQWdEO0FBQ2hELCtDQUF1QztBQUN2Qyx5Q0FBc0M7QUFDdEMseURBQXFEO0FBRXJEOztHQUVHO0FBQ0gsa0JBQTBCLFNBQVEsb0JBQU07SUFJdEM7UUFDRSxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQzlCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNmLFdBQVcsRUFBRSxlQUFNLENBQUMsaUJBQWlCO2FBQ3RDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsU0FBUztRQUNyQixNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFckMsSUFBSSxNQUFNLEdBQUc7Z0JBQ1gsS0FBSyxFQUFFLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQztnQkFDOUIsT0FBTyxFQUFFLElBQUk7YUFDZCxDQUFDO1lBRUYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUNsRCxJQUFJLEVBQUUsR0FBRyxJQUFJLGtDQUFlLEVBQUUsQ0FBQztnQkFFL0IsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ2pDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDdkIsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUN6QyxDQUFDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzVFLElBQUksU0FBUyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQ3RDLEVBQUUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7b0JBQ2hFLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQ2xFLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztvQkFDbkIsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDM0IsbURBQW1EO3dCQUNuRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNyQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsRUFBRSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3pELENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsZUFBTSxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRCxFQUFFLENBQUMsUUFBUSxDQUFDLGVBQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUQsQ0FBQztnQkFFRCxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxXQUFXLENBQUMsU0FBUyxFQUFFLEtBQUs7UUFDbEMsSUFBSSxRQUFRLEdBQVcsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDekgsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLDhDQUE4QyxHQUFHLFNBQVMsR0FBRyx3QkFBd0IsR0FBRyxRQUFRLENBQUMsQ0FBQztJQUNySCxDQUFDO0NBQ0Y7QUEvREQsb0NBK0RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHZpc2lvbiA9IHJlcXVpcmUoJ0Bnb29nbGUtY2xvdWQvdmlzaW9uJyk7XG5pbXBvcnQgeyBHb29nbGUgfSBmcm9tICcuL2dvb2dsZS1iYXNlJztcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5pbXBvcnQgeyBTZXJ2aWNlUmVzcG9uc2UgfSBmcm9tICcuL3NlcnZpY2UtcmVzcG9uc2UnO1xuXG4vKipcbiAqIFVzZXMgdGhlIEdvb2dsZSBWaXNpb24gQVBJIHRvIGRldGVjdCB0YWdzIGFuZCBsb2NhdGlvbnMgaW4gdGhlIGdpdmVuIGltYWdlLlxuICovXG5leHBvcnQgY2xhc3MgR29vZ2xlVmlzaW9uIGV4dGVuZHMgR29vZ2xlIHtcblxuICBwcml2YXRlIGd2O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgdGhpcy52YWxpZGF0ZUNvbmZpZygpLnRoZW4oKCkgPT4ge1xuICAgICAgdGhpcy5ndiA9IHZpc2lvbih7XG4gICAgICAgIGtleUZpbGVuYW1lOiBDb25maWcuZ29vZ2xlS2V5RmlsZW5hbWVcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVjdCB0YWdzICYgbG9jYXRpb25zXG4gICAqIFxuICAgKiBAcGFyYW0gaW5wdXRGaWxlIEZ1bGwgcGF0aCB0byB0aGUgaW1hZ2UgdG8gYW5hbHl6ZVxuICAgKi9cbiAgcHVibGljIGRldGVjdChpbnB1dEZpbGUpOiBQcm9taXNlPFNlcnZpY2VSZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIGxldCBwYXJhbXMgPSB7XG4gICAgICAgIHR5cGVzOiBbJ2xhYmVscycsICdsYW5kbWFya3MnXSxcbiAgICAgICAgdmVyYm9zZTogdHJ1ZVxuICAgICAgfTtcblxuICAgICAgdGhpcy5ndi5kZXRlY3QoaW5wdXRGaWxlLCBwYXJhbXMpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIHZhciBzciA9IG5ldyBTZXJ2aWNlUmVzcG9uc2UoKTtcblxuICAgICAgICByZXNwb25zZVswXS5sYWJlbHMuZm9yRWFjaChsYWJlbCA9PiB7XG4gICAgICAgICAgaWYgKGxhYmVsLnNjb3JlID4gMC44NSkge1xuICAgICAgICAgICAgc3IudGFncy5wdXNoKGxhYmVsLmRlc2MudG9Mb3dlckNhc2UoKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAocmVzcG9uc2VbMF0ubGFuZG1hcmtzICE9PSB1bmRlZmluZWQgJiYgcmVzcG9uc2VbMF0ubGFuZG1hcmtzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBsZXQgbGFuZG1hcmtzID0gcmVzcG9uc2VbMF0ubGFuZG1hcmtzO1xuICAgICAgICAgIHNyLm1ldGFkYXRhWydncHNMYXRpdHVkZSddID0gbGFuZG1hcmtzWzBdLmxvY2F0aW9uc1swXS5sYXRpdHVkZTtcbiAgICAgICAgICBzci5tZXRhZGF0YVsnZ3BzTG9uZ2l0dWRlJ10gPSBsYW5kbWFya3NbMF0ubG9jYXRpb25zWzBdLmxvbmdpdHVkZTtcbiAgICAgICAgICBsZXQgbG9jYXRpb25zID0gW107XG4gICAgICAgICAgbGFuZG1hcmtzLmZvckVhY2gobGFuZG1hcmsgPT4ge1xuICAgICAgICAgICAgLy8gR29vZ2xlIHNvbWV0aW1lcyBnaXZlcyB1cyBkdXBsaWNhdGUgbGFuZG1hcmtzLi4uXG4gICAgICAgICAgICBpZiAoIWxvY2F0aW9ucy5pbmNsdWRlcyhsYW5kbWFyay5kZXNjKSlcbiAgICAgICAgICAgICAgbG9jYXRpb25zLnB1c2gobGFuZG1hcmsuZGVzYyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgc3IubWV0YWRhdGFbJ3Nob3duU3VibG9jYXRpb24nXSA9IGxvY2F0aW9ucy5qb2luKCcsICcpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKENvbmZpZy5nb29nbGVUYWdzRmllbGQgJiYgc3IudGFncy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgc3IubWV0YWRhdGFbQ29uZmlnLmdvb2dsZVRhZ3NGaWVsZF0gPSBzci50YWdzLmpvaW4oJywnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlc29sdmUoc3IpO1xuICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIHJlamVjdCh0aGlzLmdldEVycm9yT2JqKGlucHV0RmlsZSwgZXJyb3IpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRFcnJvck9iaihpbnB1dEZpbGUsIGVycm9yKTogRXJyb3Ige1xuICAgIGxldCBlcnJvclZhbDogc3RyaW5nID0gKHR5cGVvZiBlcnJvciA9PT0gJ3N0cmluZycpID8gZXJyb3IgOiBKU09OLnN0cmluZ2lmeShlcnJvciwgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoZXJyb3IpLCAyKTtcbiAgICByZXR1cm4gbmV3IEVycm9yKCdBbiBlcnJvciBvY2N1cnJlZCB3aGlsZSBnZXR0aW5nIGxhYmVscyBmb3IgXCInICsgaW5wdXRGaWxlICsgJ1wiIGZyb20gR29vZ2xlIFZpc2lvbjogJyArIGVycm9yVmFsKTtcbiAgfVxufVxuXG4iXX0=