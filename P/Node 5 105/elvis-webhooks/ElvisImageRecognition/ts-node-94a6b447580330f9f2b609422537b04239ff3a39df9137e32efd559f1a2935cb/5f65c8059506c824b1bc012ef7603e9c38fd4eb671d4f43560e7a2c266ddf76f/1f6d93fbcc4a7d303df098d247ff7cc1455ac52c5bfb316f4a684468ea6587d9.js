"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const fs = require("fs");
var request = require('request').defaults({ jar: true });
class HttpError extends Error {
    constructor(message, statusCode, options) {
        super(message);
        this.message = message;
        this.statusCode = statusCode;
        this.options = options;
    }
}
exports.HttpError = HttpError;
class ElvisRequest {
    constructor(serverUrl, username, password) {
        this.serverUrl = serverUrl;
        this.username = username;
        this.password = password;
    }
    // TODO: The request and requestFile code is long and redundant. not sure yet how to minimize code without losing functionality
    request(options) {
        return this.apiRequest(options).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.apiRequest(options);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.apiRequest(options);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    requestFile(url, destination) {
        return this.fileRequest(url, destination).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.fileRequest(url, destination);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.fileRequest(url, destination);
                    });
                }
            }
            else if (error.statusCode == 409) {
                let delay = 5;
                console.warn('Download failed with 409 error (file not available), retrying once more in ' + delay + ' seconds. URL: ' + url);
                return Promise.delay(delay * 1000).then(() => {
                    return this.fileRequest(url, destination);
                });
            }
            else {
                throw error;
            }
        });
    }
    authenticate() {
        var options = {
            method: 'POST',
            url: this.serverUrl + '/services/login?username=' + this.username + '&password=' + this.password
        };
        console.info('Not logged in, logging in...');
        return this.apiRequest(options).then(response => {
            if (!response.loginSuccess) {
                throw new HttpError(response.loginFaultMessage, 401, options);
            }
            else {
                console.info('Login successful!');
                if (response.csrfToken) {
                    // Elvis 6+ login
                    this.csrfToken = response.csrfToken;
                }
                return response;
            }
        });
    }
    addCsrfToken(options) {
        if (this.csrfToken) {
            // Elvis 6+
            if (!options.headers) {
                options.headers = {};
            }
            options.headers['X-CSRF-TOKEN'] = this.csrfToken;
        }
    }
    apiRequest(options) {
        return new Promise((resolve, reject) => {
            options.json = true;
            this.addCsrfToken(options);
            request(options, (error, response, body) => {
                if (error) {
                    // Handle generic errors, for example unknown host
                    reject(new HttpError('Elvis request failed: ' + error, 0, options));
                }
                if (body.errorcode) {
                    response.statusCode = body.errorcode;
                    response.statusMessage = body.message;
                }
                if (response.statusCode < 200 || response.statusCode > 299) {
                    // Handle Elvis HTTP errors: 404, 401, 500, etc
                    reject(new HttpError('Elvis request failed: ' + response.statusMessage, response.statusCode, options));
                }
                else {
                    // All good, return API response
                    resolve(body);
                }
            });
        });
    }
    fileRequest(url, destination) {
        return new Promise((resolve, reject) => {
            let errorMsg = 'Download of ' + url + ' to: ' + destination + ' failed';
            let file = fs.createWriteStream(destination);
            let options = {
                method: 'GET',
                url: url
            };
            this.addCsrfToken(options);
            let req = request(options)
                .on('error', (error) => {
                // Handle generic errors when getting the file, for example unknown host
                reject(new Error(errorMsg + ': ' + error));
            })
                .on('response', response => {
                // Handle Elvis HTTP errors: 404, 401, 500, etc
                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new HttpError(errorMsg, response.statusCode, options));
                }
                // Request went well, let's start piping the data...
                req.pipe(file)
                    .on('error', (error) => {
                    // Handle piping errors: unable to write file, stream closed, ...
                    reject(new Error(errorMsg + ': ' + error));
                })
                    .on('finish', () => {
                    // Piping complete, we've got the file!
                    resolve(destination);
                });
            });
        });
    }
}
exports.ElvisRequest = ElvisRequest;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2VsdmlzLWFwaS9lbHZpcy1yZXF1ZXN0LnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvZWx2aXMtYXBpL2VsdmlzLXJlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvQ0FBcUM7QUFFckMseUJBQTBCO0FBRTFCLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztBQUV6RCxlQUF1QixTQUFRLEtBQUs7SUFDbEMsWUFBbUIsT0FBZSxFQUFTLFVBQWtCLEVBQVMsT0FBZ0U7UUFDcEksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBREUsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUFTLGVBQVUsR0FBVixVQUFVLENBQVE7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUF5RDtJQUV0SSxDQUFDO0NBQ0Y7QUFKRCw4QkFJQztBQUVEO0lBS0UsWUFBb0IsU0FBaUIsRUFBVSxRQUFnQixFQUFVLFFBQWdCO1FBQXJFLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBUTtJQUN6RixDQUFDO0lBRUQsK0hBQStIO0lBRXhILE9BQU8sQ0FBQyxPQUFnRTtRQUM3RSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDNUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNmLDZCQUE2QjtvQkFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDeEMscUJBQXFCO3dCQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzt3QkFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO29CQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuQixDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0RBQW9ELENBQUMsQ0FBQztvQkFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDekIscUJBQXFCO3dCQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLEtBQUssQ0FBQztZQUNkLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxXQUFXLENBQUMsR0FBVyxFQUFFLFdBQW1CO1FBQ2pELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdEQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNmLDZCQUE2QjtvQkFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDeEMscUJBQXFCO3dCQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzt3QkFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7b0JBQ2xFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQ3pCLHFCQUFxQjt3QkFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLElBQUksS0FBSyxHQUFXLENBQUMsQ0FBQztnQkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyw2RUFBNkUsR0FBRyxLQUFLLEdBQUcsaUJBQWlCLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQzlILE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO29CQUMzQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE1BQU0sS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFlBQVk7UUFDbEIsSUFBSSxPQUFPLEdBQUc7WUFDWixNQUFNLEVBQUUsTUFBTTtZQUNkLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLDJCQUEyQixHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRO1NBQ2pHLENBQUE7UUFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzlDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLE1BQU0sSUFBSSxTQUFTLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNoRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sT0FBTyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNsQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDdkIsaUJBQWlCO29CQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3RDLENBQUM7Z0JBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUNsQixDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sWUFBWSxDQUFDLE9BQU87UUFDMUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDbkIsV0FBVztZQUNYLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLENBQUM7WUFDRCxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDbkQsQ0FBQztJQUNILENBQUM7SUFFTyxVQUFVLENBQUMsT0FBZ0U7UUFDakYsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0IsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ3pDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ1Ysa0RBQWtEO29CQUNsRCxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsd0JBQXdCLEdBQUcsS0FBSyxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN0RSxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNuQixRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3JDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDeEMsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzNELCtDQUErQztvQkFDL0MsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDLHdCQUF3QixHQUFHLFFBQVEsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN6RyxDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLGdDQUFnQztvQkFDaEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxXQUFXLENBQUMsR0FBVyxFQUFFLFdBQW1CO1FBQ2xELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUM3QyxJQUFJLFFBQVEsR0FBVyxjQUFjLEdBQUcsR0FBRyxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsU0FBUyxDQUFDO1lBQ2hGLElBQUksSUFBSSxHQUFtQixFQUFFLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDN0QsSUFBSSxPQUFPLEdBQVE7Z0JBQ2pCLE1BQU0sRUFBRSxLQUFLO2dCQUNiLEdBQUcsRUFBRSxHQUFHO2FBQ1QsQ0FBQztZQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFM0IsSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztpQkFDdkIsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNyQix3RUFBd0U7Z0JBQ3hFLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDO2lCQUNELEVBQUUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLEVBQUU7Z0JBRXpCLCtDQUErQztnQkFDL0MsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEUsQ0FBQztnQkFFRCxvREFBb0Q7Z0JBQ3BELEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3FCQUNYLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDckIsaUVBQWlFO29CQUNqRSxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDLENBQUM7cUJBQ0QsRUFBRSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUU7b0JBQ2pCLHVDQUF1QztvQkFDdkMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN2QixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUE5SkQsb0NBOEpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb21pc2UgPSByZXF1aXJlKCdibHVlYmlyZCcpO1xuaW1wb3J0IHsgVXJpT3B0aW9ucywgVXJsT3B0aW9ucywgQ29yZU9wdGlvbnMgfSBmcm9tICdyZXF1ZXN0JztcbmltcG9ydCBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG5cbnZhciByZXF1ZXN0ID0gcmVxdWlyZSgncmVxdWVzdCcpLmRlZmF1bHRzKHsgamFyOiB0cnVlIH0pO1xuXG5leHBvcnQgY2xhc3MgSHR0cEVycm9yIGV4dGVuZHMgRXJyb3Ige1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgbWVzc2FnZTogc3RyaW5nLCBwdWJsaWMgc3RhdHVzQ29kZTogbnVtYmVyLCBwdWJsaWMgb3B0aW9uczogKFVyaU9wdGlvbnMgJiBDb3JlT3B0aW9ucykgfCAoVXJsT3B0aW9ucyAmIENvcmVPcHRpb25zKSkge1xuICAgIHN1cGVyKG1lc3NhZ2UpO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBFbHZpc1JlcXVlc3Qge1xuXG4gIHByaXZhdGUgY3NyZlRva2VuOiBzdHJpbmc7XG4gIHByaXZhdGUgYXV0aDogUHJvbWlzZTxhbnk+XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXJ2ZXJVcmw6IHN0cmluZywgcHJpdmF0ZSB1c2VybmFtZTogc3RyaW5nLCBwcml2YXRlIHBhc3N3b3JkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIC8vIFRPRE86IFRoZSByZXF1ZXN0IGFuZCByZXF1ZXN0RmlsZSBjb2RlIGlzIGxvbmcgYW5kIHJlZHVuZGFudC4gbm90IHN1cmUgeWV0IGhvdyB0byBtaW5pbWl6ZSBjb2RlIHdpdGhvdXQgbG9zaW5nIGZ1bmN0aW9uYWxpdHlcblxuICBwdWJsaWMgcmVxdWVzdChvcHRpb25zOiAoVXJpT3B0aW9ucyAmIENvcmVPcHRpb25zKSB8IChVcmxPcHRpb25zICYgQ29yZU9wdGlvbnMpKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIGlmIChlcnJvci5zdGF0dXNDb2RlID09IDQwMSkge1xuICAgICAgICBpZiAoIXRoaXMuYXV0aCkge1xuICAgICAgICAgIC8vIE5vdCBsb2dnZWQgaW4sIGxvZ2luIGZpcnN0XG4gICAgICAgICAgdGhpcy5hdXRoID0gdGhpcy5hdXRoZW50aWNhdGUoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIC8vIFJldHJ5IGluaXRpYWwgY2FsbFxuICAgICAgICAgICAgdGhpcy5hdXRoID0gbnVsbDtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaVJlcXVlc3Qob3B0aW9ucyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnQWxyZWFkeSBsb2dnaW5nIGluLCB3YWl0aW5nIGZvciBsb2dpbiB0byBmaW5pc2guLi4nKTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyByZXF1ZXN0RmlsZSh1cmw6IHN0cmluZywgZGVzdGluYXRpb246IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZmlsZVJlcXVlc3QodXJsLCBkZXN0aW5hdGlvbikuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgaWYgKGVycm9yLnN0YXR1c0NvZGUgPT0gNDAxKSB7XG4gICAgICAgIGlmICghdGhpcy5hdXRoKSB7XG4gICAgICAgICAgLy8gTm90IGxvZ2dlZCBpbiwgbG9naW4gZmlyc3RcbiAgICAgICAgICB0aGlzLmF1dGggPSB0aGlzLmF1dGhlbnRpY2F0ZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICB0aGlzLmF1dGggPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmlsZVJlcXVlc3QodXJsLCBkZXN0aW5hdGlvbik7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnQWxyZWFkeSBsb2dnaW5nIGluLCB3YWl0aW5nIGZvciBsb2dpbiB0byBmaW5pc2guLi4nKTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5maWxlUmVxdWVzdCh1cmwsIGRlc3RpbmF0aW9uKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChlcnJvci5zdGF0dXNDb2RlID09IDQwOSkge1xuICAgICAgICBsZXQgZGVsYXk6IG51bWJlciA9IDU7XG4gICAgICAgIGNvbnNvbGUud2FybignRG93bmxvYWQgZmFpbGVkIHdpdGggNDA5IGVycm9yIChmaWxlIG5vdCBhdmFpbGFibGUpLCByZXRyeWluZyBvbmNlIG1vcmUgaW4gJyArIGRlbGF5ICsgJyBzZWNvbmRzLiBVUkw6ICcgKyB1cmwpO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5kZWxheShkZWxheSAqIDEwMDApLnRoZW4oKCkgPT4ge1xuICAgICAgICAgIHJldHVybiB0aGlzLmZpbGVSZXF1ZXN0KHVybCwgZGVzdGluYXRpb24pO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRocm93IGVycm9yO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhdXRoZW50aWNhdGUoKTogUHJvbWlzZTxhbnk+IHtcbiAgICB2YXIgb3B0aW9ucyA9IHtcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgdXJsOiB0aGlzLnNlcnZlclVybCArICcvc2VydmljZXMvbG9naW4/dXNlcm5hbWU9JyArIHRoaXMudXNlcm5hbWUgKyAnJnBhc3N3b3JkPScgKyB0aGlzLnBhc3N3b3JkXG4gICAgfVxuICAgIGNvbnNvbGUuaW5mbygnTm90IGxvZ2dlZCBpbiwgbG9nZ2luZyBpbi4uLicpO1xuICAgIHJldHVybiB0aGlzLmFwaVJlcXVlc3Qob3B0aW9ucykudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBpZiAoIXJlc3BvbnNlLmxvZ2luU3VjY2Vzcykge1xuICAgICAgICB0aHJvdyBuZXcgSHR0cEVycm9yKHJlc3BvbnNlLmxvZ2luRmF1bHRNZXNzYWdlLCA0MDEsIG9wdGlvbnMpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5pbmZvKCdMb2dpbiBzdWNjZXNzZnVsIScpO1xuICAgICAgICBpZiAocmVzcG9uc2UuY3NyZlRva2VuKSB7XG4gICAgICAgICAgLy8gRWx2aXMgNisgbG9naW5cbiAgICAgICAgICB0aGlzLmNzcmZUb2tlbiA9IHJlc3BvbnNlLmNzcmZUb2tlbjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGFkZENzcmZUb2tlbihvcHRpb25zKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuY3NyZlRva2VuKSB7XG4gICAgICAvLyBFbHZpcyA2K1xuICAgICAgaWYgKCFvcHRpb25zLmhlYWRlcnMpIHtcbiAgICAgICAgb3B0aW9ucy5oZWFkZXJzID0ge307XG4gICAgICB9XG4gICAgICBvcHRpb25zLmhlYWRlcnNbJ1gtQ1NSRi1UT0tFTiddID0gdGhpcy5jc3JmVG9rZW47XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhcGlSZXF1ZXN0KG9wdGlvbnM6IChVcmlPcHRpb25zICYgQ29yZU9wdGlvbnMpIHwgKFVybE9wdGlvbnMgJiBDb3JlT3B0aW9ucykpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBvcHRpb25zLmpzb24gPSB0cnVlO1xuICAgICAgdGhpcy5hZGRDc3JmVG9rZW4ob3B0aW9ucyk7XG4gICAgICByZXF1ZXN0KG9wdGlvbnMsIChlcnJvciwgcmVzcG9uc2UsIGJvZHkpID0+IHtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgLy8gSGFuZGxlIGdlbmVyaWMgZXJyb3JzLCBmb3IgZXhhbXBsZSB1bmtub3duIGhvc3RcbiAgICAgICAgICByZWplY3QobmV3IEh0dHBFcnJvcignRWx2aXMgcmVxdWVzdCBmYWlsZWQ6ICcgKyBlcnJvciwgMCwgb3B0aW9ucykpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGJvZHkuZXJyb3Jjb2RlKSB7XG4gICAgICAgICAgcmVzcG9uc2Uuc3RhdHVzQ29kZSA9IGJvZHkuZXJyb3Jjb2RlO1xuICAgICAgICAgIHJlc3BvbnNlLnN0YXR1c01lc3NhZ2UgPSBib2R5Lm1lc3NhZ2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzQ29kZSA8IDIwMCB8fCByZXNwb25zZS5zdGF0dXNDb2RlID4gMjk5KSB7XG4gICAgICAgICAgLy8gSGFuZGxlIEVsdmlzIEhUVFAgZXJyb3JzOiA0MDQsIDQwMSwgNTAwLCBldGNcbiAgICAgICAgICByZWplY3QobmV3IEh0dHBFcnJvcignRWx2aXMgcmVxdWVzdCBmYWlsZWQ6ICcgKyByZXNwb25zZS5zdGF0dXNNZXNzYWdlLCByZXNwb25zZS5zdGF0dXNDb2RlLCBvcHRpb25zKSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgLy8gQWxsIGdvb2QsIHJldHVybiBBUEkgcmVzcG9uc2VcbiAgICAgICAgICByZXNvbHZlKGJvZHkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgZmlsZVJlcXVlc3QodXJsOiBzdHJpbmcsIGRlc3RpbmF0aW9uOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxzdHJpbmc+KChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBlcnJvck1zZzogc3RyaW5nID0gJ0Rvd25sb2FkIG9mICcgKyB1cmwgKyAnIHRvOiAnICsgZGVzdGluYXRpb24gKyAnIGZhaWxlZCc7XG4gICAgICBsZXQgZmlsZTogZnMuV3JpdGVTdHJlYW0gPSBmcy5jcmVhdGVXcml0ZVN0cmVhbShkZXN0aW5hdGlvbik7XG4gICAgICBsZXQgb3B0aW9uczogYW55ID0ge1xuICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICB1cmw6IHVybFxuICAgICAgfTtcbiAgICAgIHRoaXMuYWRkQ3NyZlRva2VuKG9wdGlvbnMpO1xuXG4gICAgICBsZXQgcmVxID0gcmVxdWVzdChvcHRpb25zKVxuICAgICAgICAub24oJ2Vycm9yJywgKGVycm9yKSA9PiB7XG4gICAgICAgICAgLy8gSGFuZGxlIGdlbmVyaWMgZXJyb3JzIHdoZW4gZ2V0dGluZyB0aGUgZmlsZSwgZm9yIGV4YW1wbGUgdW5rbm93biBob3N0XG4gICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihlcnJvck1zZyArICc6ICcgKyBlcnJvcikpO1xuICAgICAgICB9KVxuICAgICAgICAub24oJ3Jlc3BvbnNlJywgcmVzcG9uc2UgPT4ge1xuXG4gICAgICAgICAgLy8gSGFuZGxlIEVsdmlzIEhUVFAgZXJyb3JzOiA0MDQsIDQwMSwgNTAwLCBldGNcbiAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzQ29kZSA8IDIwMCB8fCByZXNwb25zZS5zdGF0dXNDb2RlID4gMjk5KSB7XG4gICAgICAgICAgICByZWplY3QobmV3IEh0dHBFcnJvcihlcnJvck1zZywgcmVzcG9uc2Uuc3RhdHVzQ29kZSwgb3B0aW9ucykpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIFJlcXVlc3Qgd2VudCB3ZWxsLCBsZXQncyBzdGFydCBwaXBpbmcgdGhlIGRhdGEuLi5cbiAgICAgICAgICByZXEucGlwZShmaWxlKVxuICAgICAgICAgICAgLm9uKCdlcnJvcicsIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAvLyBIYW5kbGUgcGlwaW5nIGVycm9yczogdW5hYmxlIHRvIHdyaXRlIGZpbGUsIHN0cmVhbSBjbG9zZWQsIC4uLlxuICAgICAgICAgICAgICByZWplY3QobmV3IEVycm9yKGVycm9yTXNnICsgJzogJyArIGVycm9yKSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKCdmaW5pc2gnLCAoKSA9PiB7XG4gICAgICAgICAgICAgIC8vIFBpcGluZyBjb21wbGV0ZSwgd2UndmUgZ290IHRoZSBmaWxlIVxuICAgICAgICAgICAgICByZXNvbHZlKGRlc3RpbmF0aW9uKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9KTtcbiAgfVxufSJdfQ==