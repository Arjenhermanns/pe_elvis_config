"use strict";
/**
 * Start web server and entry point for API requests.
 */
Object.defineProperty(exports, "__esModule", { value: true });
require("console-stamp")(console, { pattern: "dd-mm-yyyy HH:MM:ss.l" });
const express = require("express");
const http = require("http");
const https = require("https");
const fs = require("fs");
const bodyParser = require("body-parser");
const config_1 = require("./config");
const webhook_endpoint_1 = require("./app/webhook-endpoint");
const recognize_api_1 = require("./app/recognize-api");
/**
 * Singleton server class
 */
class Server {
    constructor() {
        this.allowCrossDomain = function (req, res, next) {
            // Keep the compiler happy
            req = req;
            res.header('Access-Control-Allow-Origin', config_1.Config.elvisUrl);
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept');
            next();
        };
        if (config_1.Config.httpEnabled) {
            this.httpApp = express();
        }
        if (config_1.Config.httpsEnabled) {
            this.httpsApp = express();
        }
        this.app = config_1.Config.httpsEnabled ? this.httpsApp : this.httpApp;
        if (config_1.Config.recognizeOnImport) {
            this.webhookEndPoint = new webhook_endpoint_1.WebhookEndpoint(this.app);
        }
        if (config_1.Config.restAPIEnabled) {
            this.recognizeApi = new recognize_api_1.RecognizeApi(this.app);
        }
    }
    static getInstance() {
        return this.instance || (this.instance = new this());
    }
    /**
     * Start the server
     */
    start() {
        // Configure bodyParser
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        // Allow cross domain requests from Elvis plugins
        this.app.use(this.allowCrossDomain);
        if (config_1.Config.httpEnabled && config_1.Config.httpsEnabled) {
            // Redirect all HTTP traffic to HTTPS
            this.httpApp.get('*', (req, res) => {
                res.redirect('https://' + req.headers.host + ':' + config_1.Config.httpsPort + '/' + req.path);
            });
        }
        if (config_1.Config.httpEnabled) {
            // Start HTTP server
            http.createServer(this.httpApp).listen(config_1.Config.httpPort, () => {
                this.logStartupMessage('HTTP Server started at port: ' + config_1.Config.httpPort);
            });
        }
        if (config_1.Config.httpsEnabled) {
            // Start HTTPS server
            let httpsOptions = {
                key: fs.readFileSync(config_1.Config.httpsKeyFile),
                cert: fs.readFileSync(config_1.Config.httpsCertFile)
            };
            https.createServer(httpsOptions, this.httpsApp).listen(config_1.Config.httpsPort, () => {
                this.logStartupMessage('HTTPS Server started at port: ' + config_1.Config.httpsPort);
            });
        }
        if (config_1.Config.recognizeOnImport) {
            // Start listening for webhook events
            this.webhookEndPoint.addRoutes();
        }
        if (config_1.Config.restAPIEnabled) {
            // Start REST API
            this.recognizeApi.addRoutes();
        }
    }
    logStartupMessage(serverMsg) {
        console.info(serverMsg);
        console.info('Recognize imported files on import: ' + config_1.Config.recognizeOnImport);
        console.info('REST API enabled: ' + config_1.Config.restAPIEnabled);
    }
}
let server = Server.getInstance();
server.start();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL3NlcnZlci50cyIsInNvdXJjZXMiOlsiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL3NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7O0dBRUc7O0FBRUgsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxDQUFDLENBQUM7QUFDeEUsbUNBQW9DO0FBQ3BDLDZCQUE4QjtBQUM5QiwrQkFBZ0M7QUFDaEMseUJBQTBCO0FBRTFCLDBDQUEyQztBQUMzQyxxQ0FBa0M7QUFDbEMsNkRBQXlEO0FBQ3pELHVEQUFrRDtBQUVsRDs7R0FFRztBQUNIO0lBY0U7UUFxRVEscUJBQWdCLEdBQUcsVUFBVSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUk7WUFDakQsMEJBQTBCO1lBQzFCLEdBQUcsR0FBRyxHQUFHLENBQUM7WUFFVixHQUFHLENBQUMsTUFBTSxDQUFDLDZCQUE2QixFQUFFLGVBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzRCxHQUFHLENBQUMsTUFBTSxDQUFDLDhCQUE4QixFQUFFLDZCQUE2QixDQUFDLENBQUM7WUFDMUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyw4QkFBOEIsRUFBRSw2Q0FBNkMsQ0FBQyxDQUFDO1lBRTFGLElBQUksRUFBRSxDQUFDO1FBQ1QsQ0FBQyxDQUFBO1FBN0VDLEVBQUUsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUFFLENBQUM7UUFDM0IsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxFQUFFLENBQUM7UUFDNUIsQ0FBQztRQUNELElBQUksQ0FBQyxHQUFHLEdBQUcsZUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM5RCxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxrQ0FBZSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2RCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsZUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLDRCQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELENBQUM7SUFDSCxDQUFDO0lBeEJNLE1BQU0sQ0FBQyxXQUFXO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQXdCRDs7T0FFRztJQUNJLEtBQUs7UUFDVix1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFFaEMsaURBQWlEO1FBQ2pELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBRXBDLEVBQUUsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxXQUFXLElBQUksZUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDOUMscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtnQkFDakMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLGVBQU0sQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUN2QixvQkFBb0I7WUFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLGVBQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO2dCQUMzRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsK0JBQStCLEdBQUcsZUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVFLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLHFCQUFxQjtZQUNyQixJQUFJLFlBQVksR0FBRztnQkFDakIsR0FBRyxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsZUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDekMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsZUFBTSxDQUFDLGFBQWEsQ0FBQzthQUM1QyxDQUFDO1lBQ0YsS0FBSyxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdDQUFnQyxHQUFHLGVBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM5RSxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQzdCLHFDQUFxQztZQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25DLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxlQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMxQixpQkFBaUI7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNoQyxDQUFDO0lBQ0gsQ0FBQztJQUVPLGlCQUFpQixDQUFDLFNBQWlCO1FBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEIsT0FBTyxDQUFDLElBQUksQ0FBQyxzQ0FBc0MsR0FBRyxlQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNoRixPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGVBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM3RCxDQUFDO0NBWUY7QUFFRCxJQUFJLE1BQU0sR0FBVyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDMUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBTdGFydCB3ZWIgc2VydmVyIGFuZCBlbnRyeSBwb2ludCBmb3IgQVBJIHJlcXVlc3RzLlxuICovXG5cbnJlcXVpcmUoXCJjb25zb2xlLXN0YW1wXCIpKGNvbnNvbGUsIHsgcGF0dGVybjogXCJkZC1tbS15eXl5IEhIOk1NOnNzLmxcIiB9KTtcbmltcG9ydCBleHByZXNzID0gcmVxdWlyZSgnZXhwcmVzcycpO1xuaW1wb3J0IGh0dHAgPSByZXF1aXJlKCdodHRwJyk7XG5pbXBvcnQgaHR0cHMgPSByZXF1aXJlKCdodHRwcycpO1xuaW1wb3J0IGZzID0gcmVxdWlyZSgnZnMnKTtcbmltcG9ydCB7IEFwcGxpY2F0aW9uIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgYm9keVBhcnNlciA9IHJlcXVpcmUoJ2JvZHktcGFyc2VyJyk7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5pbXBvcnQgeyBXZWJob29rRW5kcG9pbnQgfSBmcm9tICcuL2FwcC93ZWJob29rLWVuZHBvaW50JztcbmltcG9ydCB7IFJlY29nbml6ZUFwaSB9IGZyb20gJy4vYXBwL3JlY29nbml6ZS1hcGknXG5cbi8qKlxuICogU2luZ2xldG9uIHNlcnZlciBjbGFzc1xuICovXG5jbGFzcyBTZXJ2ZXIge1xuXG4gIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBTZXJ2ZXI7XG5cbiAgcHVibGljIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBTZXJ2ZXIge1xuICAgIHJldHVybiB0aGlzLmluc3RhbmNlIHx8ICh0aGlzLmluc3RhbmNlID0gbmV3IHRoaXMoKSk7XG4gIH1cblxuICBwcml2YXRlIGFwcDogQXBwbGljYXRpb247XG4gIHByaXZhdGUgaHR0cEFwcDogQXBwbGljYXRpb247XG4gIHByaXZhdGUgaHR0cHNBcHA6IEFwcGxpY2F0aW9uO1xuICBwcml2YXRlIHdlYmhvb2tFbmRQb2ludDogV2ViaG9va0VuZHBvaW50O1xuICBwcml2YXRlIHJlY29nbml6ZUFwaTogUmVjb2duaXplQXBpO1xuXG4gIHByaXZhdGUgY29uc3RydWN0b3IoKSB7XG4gICAgaWYgKENvbmZpZy5odHRwRW5hYmxlZCkge1xuICAgICAgdGhpcy5odHRwQXBwID0gZXhwcmVzcygpO1xuICAgIH1cbiAgICBpZiAoQ29uZmlnLmh0dHBzRW5hYmxlZCkge1xuICAgICAgdGhpcy5odHRwc0FwcCA9IGV4cHJlc3MoKTtcbiAgICB9XG4gICAgdGhpcy5hcHAgPSBDb25maWcuaHR0cHNFbmFibGVkID8gdGhpcy5odHRwc0FwcCA6IHRoaXMuaHR0cEFwcDtcbiAgICBpZiAoQ29uZmlnLnJlY29nbml6ZU9uSW1wb3J0KSB7XG4gICAgICB0aGlzLndlYmhvb2tFbmRQb2ludCA9IG5ldyBXZWJob29rRW5kcG9pbnQodGhpcy5hcHApO1xuICAgIH1cbiAgICBpZiAoQ29uZmlnLnJlc3RBUElFbmFibGVkKSB7XG4gICAgICB0aGlzLnJlY29nbml6ZUFwaSA9IG5ldyBSZWNvZ25pemVBcGkodGhpcy5hcHApO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBTdGFydCB0aGUgc2VydmVyXG4gICAqL1xuICBwdWJsaWMgc3RhcnQoKTogdm9pZCB7XG4gICAgLy8gQ29uZmlndXJlIGJvZHlQYXJzZXJcbiAgICB0aGlzLmFwcC51c2UoYm9keVBhcnNlci51cmxlbmNvZGVkKHsgZXh0ZW5kZWQ6IHRydWUgfSkpO1xuICAgIHRoaXMuYXBwLnVzZShib2R5UGFyc2VyLmpzb24oKSk7XG5cbiAgICAvLyBBbGxvdyBjcm9zcyBkb21haW4gcmVxdWVzdHMgZnJvbSBFbHZpcyBwbHVnaW5zXG4gICAgdGhpcy5hcHAudXNlKHRoaXMuYWxsb3dDcm9zc0RvbWFpbik7XG5cbiAgICBpZiAoQ29uZmlnLmh0dHBFbmFibGVkICYmIENvbmZpZy5odHRwc0VuYWJsZWQpIHtcbiAgICAgIC8vIFJlZGlyZWN0IGFsbCBIVFRQIHRyYWZmaWMgdG8gSFRUUFNcbiAgICAgIHRoaXMuaHR0cEFwcC5nZXQoJyonLCAocmVxLCByZXMpID0+IHtcbiAgICAgICAgcmVzLnJlZGlyZWN0KCdodHRwczovLycgKyByZXEuaGVhZGVycy5ob3N0ICsgJzonICsgQ29uZmlnLmh0dHBzUG9ydCArICcvJyArIHJlcS5wYXRoKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChDb25maWcuaHR0cEVuYWJsZWQpIHtcbiAgICAgIC8vIFN0YXJ0IEhUVFAgc2VydmVyXG4gICAgICBodHRwLmNyZWF0ZVNlcnZlcih0aGlzLmh0dHBBcHApLmxpc3RlbihDb25maWcuaHR0cFBvcnQsICgpID0+IHtcbiAgICAgICAgdGhpcy5sb2dTdGFydHVwTWVzc2FnZSgnSFRUUCBTZXJ2ZXIgc3RhcnRlZCBhdCBwb3J0OiAnICsgQ29uZmlnLmh0dHBQb3J0KTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChDb25maWcuaHR0cHNFbmFibGVkKSB7XG4gICAgICAvLyBTdGFydCBIVFRQUyBzZXJ2ZXJcbiAgICAgIGxldCBodHRwc09wdGlvbnMgPSB7XG4gICAgICAgIGtleTogZnMucmVhZEZpbGVTeW5jKENvbmZpZy5odHRwc0tleUZpbGUpLFxuICAgICAgICBjZXJ0OiBmcy5yZWFkRmlsZVN5bmMoQ29uZmlnLmh0dHBzQ2VydEZpbGUpXG4gICAgICB9O1xuICAgICAgaHR0cHMuY3JlYXRlU2VydmVyKGh0dHBzT3B0aW9ucywgdGhpcy5odHRwc0FwcCkubGlzdGVuKENvbmZpZy5odHRwc1BvcnQsICgpID0+IHtcbiAgICAgICAgdGhpcy5sb2dTdGFydHVwTWVzc2FnZSgnSFRUUFMgU2VydmVyIHN0YXJ0ZWQgYXQgcG9ydDogJyArIENvbmZpZy5odHRwc1BvcnQpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKENvbmZpZy5yZWNvZ25pemVPbkltcG9ydCkge1xuICAgICAgLy8gU3RhcnQgbGlzdGVuaW5nIGZvciB3ZWJob29rIGV2ZW50c1xuICAgICAgdGhpcy53ZWJob29rRW5kUG9pbnQuYWRkUm91dGVzKCk7XG4gICAgfVxuXG4gICAgaWYgKENvbmZpZy5yZXN0QVBJRW5hYmxlZCkge1xuICAgICAgLy8gU3RhcnQgUkVTVCBBUElcbiAgICAgIHRoaXMucmVjb2duaXplQXBpLmFkZFJvdXRlcygpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgbG9nU3RhcnR1cE1lc3NhZ2Uoc2VydmVyTXNnOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBjb25zb2xlLmluZm8oc2VydmVyTXNnKTtcbiAgICBjb25zb2xlLmluZm8oJ1JlY29nbml6ZSBpbXBvcnRlZCBmaWxlcyBvbiBpbXBvcnQ6ICcgKyBDb25maWcucmVjb2duaXplT25JbXBvcnQpO1xuICAgIGNvbnNvbGUuaW5mbygnUkVTVCBBUEkgZW5hYmxlZDogJyArIENvbmZpZy5yZXN0QVBJRW5hYmxlZCk7XG4gIH1cblxuICBwcml2YXRlIGFsbG93Q3Jvc3NEb21haW4gPSBmdW5jdGlvbiAocmVxLCByZXMsIG5leHQpIHtcbiAgICAvLyBLZWVwIHRoZSBjb21waWxlciBoYXBweVxuICAgIHJlcSA9IHJlcTtcblxuICAgIHJlcy5oZWFkZXIoJ0FjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpbicsIENvbmZpZy5lbHZpc1VybCk7XG4gICAgcmVzLmhlYWRlcignQWNjZXNzLUNvbnRyb2wtQWxsb3ctTWV0aG9kcycsICdHRVQsUFVULFBPU1QsREVMRVRFLE9QVElPTlMnKTtcbiAgICByZXMuaGVhZGVyKCdBY2Nlc3MtQ29udHJvbC1BbGxvdy1IZWFkZXJzJywgJ09yaWdpbixYLVJlcXVlc3RlZC1XaXRoLENvbnRlbnQtVHlwZSxBY2NlcHQnKTtcblxuICAgIG5leHQoKTtcbiAgfVxufVxuXG5sZXQgc2VydmVyOiBTZXJ2ZXIgPSBTZXJ2ZXIuZ2V0SW5zdGFuY2UoKTtcbnNlcnZlci5zdGFydCgpOyJdfQ==