"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jayson = require("jayson");
const Promise = require("bluebird");
class EnterpriseApi {
    constructor(url, username, password) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.isObject = function (a) {
            return (!!a) && (a.constructor === Object);
        };
        let endPoint = url += '?protocol=JSON';
        this.rpcClient = url.startsWith('https') ? jayson.client.https(endPoint) : jayson.client.http(endPoint);
        this.logOn();
    }
    getObjects(params) {
        return this.runRequest('GetObjects', params);
    }
    runRequest(operation, params) {
        return new Promise((resolve, reject) => {
            // Insert ticket
            params.Ticket = this.ticket;
            // Run request
            this.rpcClient.request(operation, [params], (err, response) => {
                let error = this.getError(err, response);
                if (error) {
                    // Request failed
                    if (error.message && error.message.search('S1043')) {
                        if (!this.auth) {
                            // Ticket is expired, log on again
                            this.auth = this.logOn().then(() => {
                                // Re-run original request
                                this.auth = null;
                                return this.runRequest(operation, params);
                            });
                            resolve(this.auth);
                        }
                        else {
                            console.log('Already logging on to Enterprise, waiting for log-on to finish...');
                            resolve(this.auth.then(() => {
                                // Retry initial call
                                return this.runRequest(operation, params);
                            }));
                        }
                    }
                    else {
                        reject(new Error('Operation ' + operation + ' to Enterprise failed, with error:\n' + this.getErrorString(error)));
                    }
                }
                else {
                    // Request was successful, return results
                    resolve(response.result);
                }
            });
        });
    }
    logOn() {
        let logOnRequest = {
            User: this.username,
            Password: this.password,
            ClientName: 'localhost',
            ClientAppName: 'web',
            ClientAppVersion: '10.0.0 build 0',
            RequestTicket: true
        };
        return new Promise((resolve, reject) => {
            this.rpcClient.request('LogOn', [logOnRequest], (err, response) => {
                let error = this.getError(err, response);
                if (error) {
                    return reject(new Error('LogOn to Enterprise failed:\n' + this.getErrorString(error)));
                }
                console.info('Logged in to Enterprise with ticket: ' + response.result.Ticket);
                this.ticket = response.result.Ticket;
                resolve();
            });
        });
    }
    getError(error, response) {
        if (error) {
            return error;
        }
        else if (response && response.error) {
            return response.error;
        }
        else {
            return null;
        }
    }
    getErrorString(error, response) {
        let err = this.getError(error, response);
        if (err.message) {
            return err.message;
        }
        else if (this.isObject(err)) {
            return JSON.stringify(err, null, 2);
        }
        else {
            return err;
        }
    }
}
exports.EnterpriseApi = EnterpriseApi;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9lbnRlcnByaXNlLWFwaS9lbnRlcnByaXNlLWFwaS50cyIsInNvdXJjZXMiOlsiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9lbnRlcnByaXNlLWFwaS9lbnRlcnByaXNlLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFrQztBQUNsQyxvQ0FBcUM7QUFFckM7SUFNRSxZQUFtQixHQUFXLEVBQVMsUUFBZ0IsRUFBUyxRQUFnQjtRQUE3RCxRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQVMsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7UUFrR3hFLGFBQVEsR0FBRyxVQUFVLENBQUM7WUFDNUIsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSyxNQUFNLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUM7UUFuR0EsSUFBSSxRQUFRLEdBQVcsR0FBRyxJQUFJLGdCQUFnQixDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN4RyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDO0lBRU0sVUFBVSxDQUFDLE1BQVc7UUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFTSxVQUFVLENBQUMsU0FBaUIsRUFBRSxNQUFXO1FBQzlDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBTSxDQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3RDLGdCQUFnQjtZQUNoQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7WUFFNUIsY0FBYztZQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLFFBQVE7Z0JBQ3hELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN6QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNWLGlCQUFpQjtvQkFDakIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ25ELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ2Ysa0NBQWtDOzRCQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0NBQzVCLDBCQUEwQjtnQ0FDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Z0NBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQzs0QkFDNUMsQ0FBQyxDQUFDLENBQUM7NEJBQ0gsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDckIsQ0FBQzt3QkFDRCxJQUFJLENBQUMsQ0FBQzs0QkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG1FQUFtRSxDQUFDLENBQUM7NEJBQ2pGLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQ0FDckIscUJBQXFCO2dDQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7NEJBQzVDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ04sQ0FBQztvQkFDSCxDQUFDO29CQUNELElBQUksQ0FBQyxDQUFDO3dCQUNKLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxZQUFZLEdBQUcsU0FBUyxHQUFHLHNDQUFzQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwSCxDQUFDO2dCQUNILENBQUM7Z0JBQ0QsSUFBSSxDQUFDLENBQUM7b0JBQ0oseUNBQXlDO29CQUN6QyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMzQixDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxLQUFLO1FBQ1gsSUFBSSxZQUFZLEdBQVE7WUFDdEIsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ25CLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixVQUFVLEVBQUUsV0FBVztZQUN2QixhQUFhLEVBQUUsS0FBSztZQUNwQixnQkFBZ0IsRUFBRSxnQkFBZ0I7WUFDbEMsYUFBYSxFQUFFLElBQUk7U0FDcEIsQ0FBQTtRQUVELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBTSxDQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLFFBQVE7Z0JBQzVELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN6QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNWLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsK0JBQStCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pGLENBQUM7Z0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyx1Q0FBdUMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMvRSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUNyQyxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRU8sUUFBUSxDQUFDLEtBQU0sRUFBRSxRQUFTO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDVixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2YsQ0FBQztRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDeEIsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLElBQUksQ0FBQztRQUNkLENBQUM7SUFDSCxDQUFDO0lBRU8sY0FBYyxDQUFDLEtBQU0sRUFBRSxRQUFTO1FBQ3RDLElBQUksR0FBRyxHQUFRLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzlDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ2IsQ0FBQztJQUNILENBQUM7Q0FLRjtBQTNHRCxzQ0EyR0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgamF5c29uID0gcmVxdWlyZSgnamF5c29uJyk7XG5pbXBvcnQgUHJvbWlzZSA9IHJlcXVpcmUoJ2JsdWViaXJkJyk7XG5cbmV4cG9ydCBjbGFzcyBFbnRlcnByaXNlQXBpIHtcblxuICBwcml2YXRlIHJwY0NsaWVudDogYW55O1xuICBwcml2YXRlIHRpY2tldDogc3RyaW5nO1xuICBwcml2YXRlIGF1dGg6IFByb21pc2U8YW55PjtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgdXJsOiBzdHJpbmcsIHB1YmxpYyB1c2VybmFtZTogc3RyaW5nLCBwdWJsaWMgcGFzc3dvcmQ6IHN0cmluZykge1xuICAgIGxldCBlbmRQb2ludDogc3RyaW5nID0gdXJsICs9ICc/cHJvdG9jb2w9SlNPTic7XG4gICAgdGhpcy5ycGNDbGllbnQgPSB1cmwuc3RhcnRzV2l0aCgnaHR0cHMnKSA/IGpheXNvbi5jbGllbnQuaHR0cHMoZW5kUG9pbnQpIDogamF5c29uLmNsaWVudC5odHRwKGVuZFBvaW50KTtcbiAgICB0aGlzLmxvZ09uKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0T2JqZWN0cyhwYXJhbXM6IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMucnVuUmVxdWVzdCgnR2V0T2JqZWN0cycsIHBhcmFtcyk7XG4gIH1cblxuICBwdWJsaWMgcnVuUmVxdWVzdChvcGVyYXRpb246IHN0cmluZywgcGFyYW1zOiBhbnkpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIC8vIEluc2VydCB0aWNrZXRcbiAgICAgIHBhcmFtcy5UaWNrZXQgPSB0aGlzLnRpY2tldDtcblxuICAgICAgLy8gUnVuIHJlcXVlc3RcbiAgICAgIHRoaXMucnBjQ2xpZW50LnJlcXVlc3Qob3BlcmF0aW9uLCBbcGFyYW1zXSwgKGVyciwgcmVzcG9uc2UpID0+IHtcbiAgICAgICAgbGV0IGVycm9yID0gdGhpcy5nZXRFcnJvcihlcnIsIHJlc3BvbnNlKTtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgLy8gUmVxdWVzdCBmYWlsZWRcbiAgICAgICAgICBpZiAoZXJyb3IubWVzc2FnZSAmJiBlcnJvci5tZXNzYWdlLnNlYXJjaCgnUzEwNDMnKSkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLmF1dGgpIHtcbiAgICAgICAgICAgICAgLy8gVGlja2V0IGlzIGV4cGlyZWQsIGxvZyBvbiBhZ2FpblxuICAgICAgICAgICAgICB0aGlzLmF1dGggPSB0aGlzLmxvZ09uKCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gUmUtcnVuIG9yaWdpbmFsIHJlcXVlc3RcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGggPSBudWxsO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnJ1blJlcXVlc3Qob3BlcmF0aW9uLCBwYXJhbXMpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLmF1dGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdBbHJlYWR5IGxvZ2dpbmcgb24gdG8gRW50ZXJwcmlzZSwgd2FpdGluZyBmb3IgbG9nLW9uIHRvIGZpbmlzaC4uLicpO1xuICAgICAgICAgICAgICByZXNvbHZlKHRoaXMuYXV0aC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAvLyBSZXRyeSBpbml0aWFsIGNhbGxcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ydW5SZXF1ZXN0KG9wZXJhdGlvbiwgcGFyYW1zKTtcbiAgICAgICAgICAgICAgfSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoJ09wZXJhdGlvbiAnICsgb3BlcmF0aW9uICsgJyB0byBFbnRlcnByaXNlIGZhaWxlZCwgd2l0aCBlcnJvcjpcXG4nICsgdGhpcy5nZXRFcnJvclN0cmluZyhlcnJvcikpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgLy8gUmVxdWVzdCB3YXMgc3VjY2Vzc2Z1bCwgcmV0dXJuIHJlc3VsdHNcbiAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlLnJlc3VsdCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBsb2dPbigpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGxldCBsb2dPblJlcXVlc3Q6IGFueSA9IHtcbiAgICAgIFVzZXI6IHRoaXMudXNlcm5hbWUsXG4gICAgICBQYXNzd29yZDogdGhpcy5wYXNzd29yZCxcbiAgICAgIENsaWVudE5hbWU6ICdsb2NhbGhvc3QnLFxuICAgICAgQ2xpZW50QXBwTmFtZTogJ3dlYicsXG4gICAgICBDbGllbnRBcHBWZXJzaW9uOiAnMTAuMC4wIGJ1aWxkIDAnLFxuICAgICAgUmVxdWVzdFRpY2tldDogdHJ1ZVxuICAgIH1cblxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMucnBjQ2xpZW50LnJlcXVlc3QoJ0xvZ09uJywgW2xvZ09uUmVxdWVzdF0sIChlcnIsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGxldCBlcnJvciA9IHRoaXMuZ2V0RXJyb3IoZXJyLCByZXNwb25zZSk7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIHJldHVybiByZWplY3QobmV3IEVycm9yKCdMb2dPbiB0byBFbnRlcnByaXNlIGZhaWxlZDpcXG4nICsgdGhpcy5nZXRFcnJvclN0cmluZyhlcnJvcikpKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmluZm8oJ0xvZ2dlZCBpbiB0byBFbnRlcnByaXNlIHdpdGggdGlja2V0OiAnICsgcmVzcG9uc2UucmVzdWx0LlRpY2tldCk7XG4gICAgICAgIHRoaXMudGlja2V0ID0gcmVzcG9uc2UucmVzdWx0LlRpY2tldDtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfSk7XG4gICAgfSlcbiAgfVxuXG4gIHByaXZhdGUgZ2V0RXJyb3IoZXJyb3I/LCByZXNwb25zZT8pOiBhbnkge1xuICAgIGlmIChlcnJvcikge1xuICAgICAgcmV0dXJuIGVycm9yO1xuICAgIH1cbiAgICBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5lcnJvcikge1xuICAgICAgcmV0dXJuIHJlc3BvbnNlLmVycm9yO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgZ2V0RXJyb3JTdHJpbmcoZXJyb3I/LCByZXNwb25zZT8pOiBzdHJpbmcge1xuICAgIGxldCBlcnI6IGFueSA9IHRoaXMuZ2V0RXJyb3IoZXJyb3IsIHJlc3BvbnNlKTtcbiAgICBpZiAoZXJyLm1lc3NhZ2UpIHtcbiAgICAgIHJldHVybiBlcnIubWVzc2FnZTtcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5pc09iamVjdChlcnIpKSB7XG4gICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkoZXJyLCBudWxsLCAyKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICByZXR1cm4gZXJyO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgaXNPYmplY3QgPSBmdW5jdGlvbiAoYSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAoISFhKSAmJiAoYS5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0KTtcbiAgfTtcbn0iXX0=