"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Config {
}
/**
 * Port where the app runs.
 */
Config.port = process.env.AF_PORT || '9092';
/**
 * Temporary directory used for downloading assets.
 */
Config.tempDir = process.env.AF_TEMP_DIR || './temp';
/**
 * Elvis server url.
 */
Config.elvisUrl = process.env.AF_ELVIS_URL || 'http://localhost:80';
/**
 * Elvis username.
 *
 * The user should be able to move the original in Elvis.
 */
Config.elvisUsername = process.env.AF_ELVIS_USER || 'webhooks';
/**
 * Elvis password.
 */
Config.elvisPassword = process.env.AF_ELVIS_PASSWORD || 'W@BH00K5';
/**
 * Elvis webhook token. Create a webhook that listens for "asset_metadata_update" events.
 *
 * More info creating a webhook: https://helpcenter.woodwing.com/hc/en-us/articles/115001884346
 */
// static elvisToken: string = process.env.AF_ELVIS_TOKEN || ' xu5AjGU2S+mWMI3vZZAz2g=='; //QLD-T
// static elvisToken: string = process.env.AF_ELVIS_TOKEN || 'l2pR8WvE1v1w6heW1v6V5A=='; //QLD-A
Config.elvisToken = process.env.AF_ELVIS_TOKEN || 'yEyiKitOtH2QeoOHXv9BoQ=='; //PROD
/**
 * Enterprise server url.
 */
//  static enterpriseServerUrl: string = process.env.AF_ENTERPRISE_SERVER_URL || 'http://qld-enterprise.portoeditora.pt/enterpriseqadebug/index.php';
Config.enterpriseServerUrl = process.env.AF_ENTERPRISE_SERVER_URL || 'http://enterprise.portoeditora.pt/enterprisep01/index.php';
/**
 * Enterprise username.
 *
 * The user should be able to access all brands that have shadow object in Elvis
 */
Config.enterpriseUsername = process.env.AF_ENTERPRISE_USERNAME || 'pq_ww_elvis';
/**
 * Enterprise password.
 */
Config.enterprisePassword = process.env.AF_ENTERPRISE_PASSWORD || '@ww_3lv1s#';
/**
 * List of production root folders used by Enterprise, separated by pipeline (|).
 * Assets copied into these production folders will be moved to the specific issue subfolder
 */
Config.productionFolders = process.env.AF_PRODUCTION_FOLDERS || '/Production Zone/PE|/Production Zone/AE|/Production Zone/RE';
/**
 * GetObject timeout to ensure Enterprise is finshed creating the object and it's relation(s).
 *
 * Enterprise first creates an object and then an object relarion to the dossier when a new shadow object is created inside a dossier.
 * This integration triggers right after the object is created. When we retrieve the object directly, the relation info might not be
 * inserted in Enterprise yet, which results in missing issue info. This timeout (in milliseconds) is used to ensure that Enterprise
 * is finished with creating the dossier object relation.
 */
Config.enterpriseGetObjectTimeout = process.env.AF_ENTERPRISE_GETOBJECT_TIMEOUT || 3000;
exports.Config = Config;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9jb25maWcudHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7O0FBQ0U7O0dBRUc7QUFDSSxXQUFJLEdBQVcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDO0FBRXBEOztHQUVHO0FBQ0ksY0FBTyxHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQztBQUU3RDs7R0FFRztBQUNJLGVBQVEsR0FBVyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksSUFBSSxxQkFBcUIsQ0FBQztBQUU1RTs7OztHQUlHO0FBQ0ksb0JBQWEsR0FBVyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsSUFBSSxVQUFVLENBQUM7QUFFdkU7O0dBRUc7QUFDSSxvQkFBYSxHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLElBQUksVUFBVSxDQUFDO0FBRTNFOzs7O0dBSUc7QUFDTCxpR0FBaUc7QUFDakcsZ0dBQWdHO0FBQ3hGLGlCQUFVLEdBQVcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLElBQUksMEJBQTBCLENBQUMsQ0FBQyxNQUFNO0FBRzNGOztHQUVHO0FBQ0wscUpBQXFKO0FBQzVJLDBCQUFtQixHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLElBQUksMkRBQTJELENBQUM7QUFFekk7Ozs7R0FJRztBQUNJLHlCQUFrQixHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLElBQUksYUFBYSxDQUFDO0FBRXhGOztHQUVHO0FBQ0kseUJBQWtCLEdBQVcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsSUFBSSxZQUFZLENBQUM7QUFFdkY7OztHQUdHO0FBQ0ksd0JBQWlCLEdBQVcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsSUFBSSw2REFBNkQsQ0FBQztBQUV0STs7Ozs7OztHQU9HO0FBQ0ksaUNBQTBCLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsSUFBSSxJQUFJLENBQUM7QUF0RTFGLHdCQXVFQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBDb25maWcge1xuICAvKipcbiAgICogUG9ydCB3aGVyZSB0aGUgYXBwIHJ1bnMuXG4gICAqL1xuICBzdGF0aWMgcG9ydDogc3RyaW5nID0gcHJvY2Vzcy5lbnYuQUZfUE9SVCB8fCAnOTA5Mic7XG5cbiAgLyoqXG4gICAqIFRlbXBvcmFyeSBkaXJlY3RvcnkgdXNlZCBmb3IgZG93bmxvYWRpbmcgYXNzZXRzLlxuICAgKi9cbiAgc3RhdGljIHRlbXBEaXI6IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX1RFTVBfRElSIHx8ICcuL3RlbXAnO1xuXG4gIC8qKlxuICAgKiBFbHZpcyBzZXJ2ZXIgdXJsLlxuICAgKi9cbiAgc3RhdGljIGVsdmlzVXJsOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9FTFZJU19VUkwgfHwgJ2h0dHA6Ly9sb2NhbGhvc3Q6ODAnO1xuXG4gIC8qKlxuICAgKiBFbHZpcyB1c2VybmFtZS4gXG4gICAqIFxuICAgKiBUaGUgdXNlciBzaG91bGQgYmUgYWJsZSB0byBtb3ZlIHRoZSBvcmlnaW5hbCBpbiBFbHZpcy5cbiAgICovXG4gIHN0YXRpYyBlbHZpc1VzZXJuYW1lOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9FTFZJU19VU0VSIHx8ICd3ZWJob29rcyc7XG5cbiAgLyoqXG4gICAqIEVsdmlzIHBhc3N3b3JkLlxuICAgKi9cbiAgc3RhdGljIGVsdmlzUGFzc3dvcmQ6IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX0VMVklTX1BBU1NXT1JEIHx8ICdXQEJIMDBLNSc7XG5cbiAgLyoqXG4gICAqIEVsdmlzIHdlYmhvb2sgdG9rZW4uIENyZWF0ZSBhIHdlYmhvb2sgdGhhdCBsaXN0ZW5zIGZvciBcImFzc2V0X21ldGFkYXRhX3VwZGF0ZVwiIGV2ZW50cy5cbiAgICogXG4gICAqIE1vcmUgaW5mbyBjcmVhdGluZyBhIHdlYmhvb2s6IGh0dHBzOi8vaGVscGNlbnRlci53b29kd2luZy5jb20vaGMvZW4tdXMvYXJ0aWNsZXMvMTE1MDAxODg0MzQ2XG4gICAqL1xuLy8gc3RhdGljIGVsdmlzVG9rZW46IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX0VMVklTX1RPS0VOIHx8ICcgeHU1QWpHVTJTK21XTUkzdlpaQXoyZz09JzsgLy9RTEQtVFxuLy8gc3RhdGljIGVsdmlzVG9rZW46IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX0VMVklTX1RPS0VOIHx8ICdsMnBSOFd2RTF2MXc2aGVXMXY2VjVBPT0nOyAvL1FMRC1BXG4gc3RhdGljIGVsdmlzVG9rZW46IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX0VMVklTX1RPS0VOIHx8ICd5RXlpS2l0T3RIMlFlb09IWHY5Qm9RPT0nOyAvL1BST0RcblxuXG4gIC8qKlxuICAgKiBFbnRlcnByaXNlIHNlcnZlciB1cmwuXG4gICAqL1xuLy8gIHN0YXRpYyBlbnRlcnByaXNlU2VydmVyVXJsOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9FTlRFUlBSSVNFX1NFUlZFUl9VUkwgfHwgJ2h0dHA6Ly9xbGQtZW50ZXJwcmlzZS5wb3J0b2VkaXRvcmEucHQvZW50ZXJwcmlzZXFhZGVidWcvaW5kZXgucGhwJztcbiAgc3RhdGljIGVudGVycHJpc2VTZXJ2ZXJVcmw6IHN0cmluZyA9IHByb2Nlc3MuZW52LkFGX0VOVEVSUFJJU0VfU0VSVkVSX1VSTCB8fCAnaHR0cDovL2VudGVycHJpc2UucG9ydG9lZGl0b3JhLnB0L2VudGVycHJpc2VwMDEvaW5kZXgucGhwJztcblxuICAvKipcbiAgICogRW50ZXJwcmlzZSB1c2VybmFtZS5cbiAgICogXG4gICAqIFRoZSB1c2VyIHNob3VsZCBiZSBhYmxlIHRvIGFjY2VzcyBhbGwgYnJhbmRzIHRoYXQgaGF2ZSBzaGFkb3cgb2JqZWN0IGluIEVsdmlzXG4gICAqL1xuICBzdGF0aWMgZW50ZXJwcmlzZVVzZXJuYW1lOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9FTlRFUlBSSVNFX1VTRVJOQU1FIHx8ICdwcV93d19lbHZpcyc7XG5cbiAgLyoqXG4gICAqIEVudGVycHJpc2UgcGFzc3dvcmQuXG4gICAqL1xuICBzdGF0aWMgZW50ZXJwcmlzZVBhc3N3b3JkOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9FTlRFUlBSSVNFX1BBU1NXT1JEIHx8ICdAd3dfM2x2MXMjJztcblxuICAvKipcbiAgICogTGlzdCBvZiBwcm9kdWN0aW9uIHJvb3QgZm9sZGVycyB1c2VkIGJ5IEVudGVycHJpc2UsIHNlcGFyYXRlZCBieSBwaXBlbGluZSAofCkuIFxuICAgKiBBc3NldHMgY29waWVkIGludG8gdGhlc2UgcHJvZHVjdGlvbiBmb2xkZXJzIHdpbGwgYmUgbW92ZWQgdG8gdGhlIHNwZWNpZmljIGlzc3VlIHN1YmZvbGRlclxuICAgKi9cbiAgc3RhdGljIHByb2R1Y3Rpb25Gb2xkZXJzOiBzdHJpbmcgPSBwcm9jZXNzLmVudi5BRl9QUk9EVUNUSU9OX0ZPTERFUlMgfHwgJy9Qcm9kdWN0aW9uIFpvbmUvUEV8L1Byb2R1Y3Rpb24gWm9uZS9BRXwvUHJvZHVjdGlvbiBab25lL1JFJztcblxuICAvKipcbiAgICogR2V0T2JqZWN0IHRpbWVvdXQgdG8gZW5zdXJlIEVudGVycHJpc2UgaXMgZmluc2hlZCBjcmVhdGluZyB0aGUgb2JqZWN0IGFuZCBpdCdzIHJlbGF0aW9uKHMpLlxuICAgKiBcbiAgICogRW50ZXJwcmlzZSBmaXJzdCBjcmVhdGVzIGFuIG9iamVjdCBhbmQgdGhlbiBhbiBvYmplY3QgcmVsYXJpb24gdG8gdGhlIGRvc3NpZXIgd2hlbiBhIG5ldyBzaGFkb3cgb2JqZWN0IGlzIGNyZWF0ZWQgaW5zaWRlIGEgZG9zc2llci5cbiAgICogVGhpcyBpbnRlZ3JhdGlvbiB0cmlnZ2VycyByaWdodCBhZnRlciB0aGUgb2JqZWN0IGlzIGNyZWF0ZWQuIFdoZW4gd2UgcmV0cmlldmUgdGhlIG9iamVjdCBkaXJlY3RseSwgdGhlIHJlbGF0aW9uIGluZm8gbWlnaHQgbm90IGJlIFxuICAgKiBpbnNlcnRlZCBpbiBFbnRlcnByaXNlIHlldCwgd2hpY2ggcmVzdWx0cyBpbiBtaXNzaW5nIGlzc3VlIGluZm8uIFRoaXMgdGltZW91dCAoaW4gbWlsbGlzZWNvbmRzKSBpcyB1c2VkIHRvIGVuc3VyZSB0aGF0IEVudGVycHJpc2VcbiAgICogaXMgZmluaXNoZWQgd2l0aCBjcmVhdGluZyB0aGUgZG9zc2llciBvYmplY3QgcmVsYXRpb24uXG4gICAqL1xuICBzdGF0aWMgZW50ZXJwcmlzZUdldE9iamVjdFRpbWVvdXQgPSBwcm9jZXNzLmVudi5BRl9FTlRFUlBSSVNFX0dFVE9CSkVDVF9USU1FT1VUIHx8IDMwMDA7XG59Il19