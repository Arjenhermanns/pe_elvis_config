"use strict";
/**
 * Start web server and entry point for API requests.
 */
Object.defineProperty(exports, "__esModule", { value: true });
require("console-stamp")(console, { pattern: "dd-mm-yyyy HH:MM:ss.l" });
const express = require("express");
const bodyParser = require("body-parser");
const config_1 = require("./config");
const webhook_endpoint_1 = require("./app/webhook-endpoint");
/**
 * Singleton server class
 */
class Server {
    static getInstance() {
        return this.instance || (this.instance = new this());
    }
    constructor() {
        this.app = express();
        this.webhookEndPoint = new webhook_endpoint_1.WebhookEndpoint(this.app);
    }
    /**
     * Start the server
     *
     * @param port Server HTTP port.
     */
    start(port) {
        // Configure bodyParser
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        // Start server
        this.app.listen(port);
        console.info('Move to issue folder server started at port: ' + port);
        // Start listening for webhook events
        this.webhookEndPoint.addRoutes();
    }
}
let server = Server.getInstance();
server.start(config_1.Config.port);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9zZXJ2ZXIudHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7R0FFRzs7QUFFSCxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLENBQUMsQ0FBQztBQUN4RSxtQ0FBb0M7QUFFcEMsMENBQTJDO0FBQzNDLHFDQUFrQztBQUNsQyw2REFBeUQ7QUFFekQ7O0dBRUc7QUFDSDtJQUlTLE1BQU0sQ0FBQyxXQUFXO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUtEO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksa0NBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxLQUFLLENBQUMsSUFBWTtRQUN2Qix1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFFaEMsZUFBZTtRQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsK0NBQStDLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFFckUscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkMsQ0FBQztDQUNGO0FBRUQsSUFBSSxNQUFNLEdBQVcsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBQzFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBTdGFydCB3ZWIgc2VydmVyIGFuZCBlbnRyeSBwb2ludCBmb3IgQVBJIHJlcXVlc3RzLlxuICovXG5cbnJlcXVpcmUoXCJjb25zb2xlLXN0YW1wXCIpKGNvbnNvbGUsIHsgcGF0dGVybjogXCJkZC1tbS15eXl5IEhIOk1NOnNzLmxcIiB9KTtcbmltcG9ydCBleHByZXNzID0gcmVxdWlyZSgnZXhwcmVzcycpO1xuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdleHByZXNzJztcbmltcG9ydCBib2R5UGFyc2VyID0gcmVxdWlyZSgnYm9keS1wYXJzZXInKTtcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IFdlYmhvb2tFbmRwb2ludCB9IGZyb20gJy4vYXBwL3dlYmhvb2stZW5kcG9pbnQnO1xuXG4vKipcbiAqIFNpbmdsZXRvbiBzZXJ2ZXIgY2xhc3NcbiAqL1xuY2xhc3MgU2VydmVyIHtcblxuICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogU2VydmVyO1xuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogU2VydmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbnN0YW5jZSB8fCAodGhpcy5pbnN0YW5jZSA9IG5ldyB0aGlzKCkpO1xuICB9XG5cbiAgcHJpdmF0ZSBhcHA6IEFwcGxpY2F0aW9uO1xuICBwcml2YXRlIHdlYmhvb2tFbmRQb2ludDogV2ViaG9va0VuZHBvaW50O1xuXG4gIHByaXZhdGUgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5hcHAgPSBleHByZXNzKCk7XG4gICAgdGhpcy53ZWJob29rRW5kUG9pbnQgPSBuZXcgV2ViaG9va0VuZHBvaW50KHRoaXMuYXBwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTdGFydCB0aGUgc2VydmVyXG4gICAqIFxuICAgKiBAcGFyYW0gcG9ydCBTZXJ2ZXIgSFRUUCBwb3J0LlxuICAgKi9cbiAgcHVibGljIHN0YXJ0KHBvcnQ6IHN0cmluZyk6IHZvaWQge1xuICAgIC8vIENvbmZpZ3VyZSBib2R5UGFyc2VyXG4gICAgdGhpcy5hcHAudXNlKGJvZHlQYXJzZXIudXJsZW5jb2RlZCh7IGV4dGVuZGVkOiB0cnVlIH0pKTtcbiAgICB0aGlzLmFwcC51c2UoYm9keVBhcnNlci5qc29uKCkpO1xuXG4gICAgLy8gU3RhcnQgc2VydmVyXG4gICAgdGhpcy5hcHAubGlzdGVuKHBvcnQpO1xuICAgIGNvbnNvbGUuaW5mbygnTW92ZSB0byBpc3N1ZSBmb2xkZXIgc2VydmVyIHN0YXJ0ZWQgYXQgcG9ydDogJyArIHBvcnQpO1xuXG4gICAgLy8gU3RhcnQgbGlzdGVuaW5nIGZvciB3ZWJob29rIGV2ZW50c1xuICAgIHRoaXMud2ViaG9va0VuZFBvaW50LmFkZFJvdXRlcygpO1xuICB9XG59XG5cbmxldCBzZXJ2ZXI6IFNlcnZlciA9IFNlcnZlci5nZXRJbnN0YW5jZSgpO1xuc2VydmVyLnN0YXJ0KENvbmZpZy5wb3J0KTsiXX0=