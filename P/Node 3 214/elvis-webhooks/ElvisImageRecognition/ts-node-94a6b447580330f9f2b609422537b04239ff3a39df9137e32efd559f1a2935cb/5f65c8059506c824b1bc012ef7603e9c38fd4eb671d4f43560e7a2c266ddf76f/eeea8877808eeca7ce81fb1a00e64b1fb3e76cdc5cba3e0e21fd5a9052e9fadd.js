"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const url = require("url");
const fs = require("fs");
const path = require("path");
const api_manager_1 = require("../elvis-api/api-manager");
const Promise = require("bluebird");
const uuidV4 = require("uuid/v4");
class FileUtils {
    /**
     * Download a thumbnail from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadThumbnail(hit, destinationFolder) {
        return this.download(DownloadKind.Thumbnail, hit, destinationFolder);
    }
    /**
     * Download a preview from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadPreview(hit, destinationFolder) {
        return this.download(DownloadKind.Preview, hit, destinationFolder);
    }
    /**
     * Download the original file from Elvis.
     * On success, returns a Promise with the destination file path.
     *
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static downloadOriginal(hit, destinationFolder) {
        return this.download(DownloadKind.Original, hit, destinationFolder);
    }
    /**
     * Download a thumbnail, preview or original file from Elvis to a specified destination folder.
     * On success, returns a Promise with the destination file path.
     *
     * @param kind What to download: thumbnail, preview or original
     * @param hit Hit to download
     * @param destinationFolder Target folder where the file is downloaded
     */
    static download(kind, hit, destinationFolder) {
        if (!hit)
            throw new Error('hit is a required parameter');
        if (!destinationFolder)
            throw new Error('destinationFolder is a required parameter');
        let fileUrl;
        switch (kind) {
            case DownloadKind.Thumbnail:
                fileUrl = hit.thumbnailUrl;
                break;
            case DownloadKind.Preview:
                fileUrl = hit.previewUrl;
                break;
            case DownloadKind.Original:
                fileUrl = hit.originalUrl;
                break;
            default:
                throw new Error('Unsupported kind specified: ' + kind);
        }
        if (!fileUrl) {
            throw new Error('No ' + DownloadKind[kind] + ' URL available for hit: ' + hit.id);
        }
        let filename = path.basename(url.parse(fileUrl).pathname);
        let ext = path.extname(filename);
        let baseFilename = path.basename(filename, ext);
        let tempFilename = baseFilename + '_' + uuidV4() + ext;
        let destination = path.join(destinationFolder, tempFilename);
        return this.downloadFile(hit.previewUrl, destination);
    }
    /**
     * Download an Elvis URL.
     * On success, returns a Promise with the destination file path.
     *
     *  @param url URL to download
     * @param destination Destination folder
     */
    static downloadFile(url, destination) {
        return this.createDestinationDirectory(destination).then(() => {
            return api_manager_1.ApiManager.getApi().elvisRequest.requestFile(url, destination);
        });
    }
    /**
     * Create a folder for a given file if it doesn't exists
     *
     * @param file
     */
    static createDestinationDirectory(file) {
        let dir = require('path').dirname(file);
        return new Promise((resolve, reject) => {
            fs.mkdir(dir, error => {
                if (!error || (error && error.code === 'EEXIST')) {
                    resolve(dir);
                }
                else {
                    reject(error);
                }
            });
        });
    }
}
exports.FileUtils = FileUtils;
var DownloadKind;
(function (DownloadKind) {
    DownloadKind[DownloadKind["Thumbnail"] = 0] = "Thumbnail";
    DownloadKind[DownloadKind["Preview"] = 1] = "Preview";
    DownloadKind[DownloadKind["Original"] = 2] = "Original";
})(DownloadKind = exports.DownloadKind || (exports.DownloadKind = {}));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9maWxlLXV0aWxzLnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL2ZpbGUtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyQkFBNEI7QUFDNUIseUJBQTBCO0FBQzFCLDZCQUE4QjtBQUM5QiwwREFBc0Q7QUFDdEQsb0NBQXFDO0FBRXJDLGtDQUFtQztBQUVuQztJQUVFOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFtQixFQUFFLGlCQUF5QjtRQUM1RSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQW1CLEVBQUUsaUJBQXlCO1FBQzFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFtQixFQUFFLGlCQUF5QjtRQUMzRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0ksTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFrQixFQUFFLEdBQW1CLEVBQUUsaUJBQXlCO1FBQ3ZGLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1lBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBQ3pELEVBQUUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUM7WUFBQyxNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7UUFFckYsSUFBSSxPQUFlLENBQUM7UUFDcEIsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNiLEtBQUssWUFBWSxDQUFDLFNBQVM7Z0JBQ3pCLE9BQU8sR0FBRyxHQUFHLENBQUMsWUFBWSxDQUFDO2dCQUMzQixLQUFLLENBQUM7WUFDUixLQUFLLFlBQVksQ0FBQyxPQUFPO2dCQUN2QixPQUFPLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQztnQkFDekIsS0FBSyxDQUFDO1lBQ1IsS0FBSyxZQUFZLENBQUMsUUFBUTtnQkFDeEIsT0FBTyxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUM7Z0JBQzFCLEtBQUssQ0FBQztZQUNSO2dCQUNFLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDM0QsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNiLE1BQU0sSUFBSSxLQUFLLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRywwQkFBMEIsR0FBRyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDcEYsQ0FBQztRQUVELElBQUksUUFBUSxHQUFXLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsRSxJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLElBQUksWUFBWSxHQUFXLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3hELElBQUksWUFBWSxHQUFXLFlBQVksR0FBRyxHQUFHLEdBQUcsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBQy9ELElBQUksV0FBVyxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFFckUsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0ssTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFXLEVBQUUsV0FBbUI7UUFDMUQsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQzVELE1BQU0sQ0FBQyx3QkFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSyxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBWTtRQUNwRCxJQUFJLEdBQUcsR0FBVyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUM3QyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDcEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDZixDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEIsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBRUY7QUExR0QsOEJBMEdDO0FBRUQsSUFBWSxZQUlYO0FBSkQsV0FBWSxZQUFZO0lBQ3RCLHlEQUFTLENBQUE7SUFDVCxxREFBTyxDQUFBO0lBQ1AsdURBQVEsQ0FBQTtBQUNWLENBQUMsRUFKVyxZQUFZLEdBQVosb0JBQVksS0FBWixvQkFBWSxRQUl2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB1cmwgPSByZXF1aXJlKCd1cmwnKTtcbmltcG9ydCBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG5pbXBvcnQgcGF0aCA9IHJlcXVpcmUoJ3BhdGgnKTtcbmltcG9ydCB7IEFwaU1hbmFnZXIgfSBmcm9tICcuLi9lbHZpcy1hcGkvYXBpLW1hbmFnZXInO1xuaW1wb3J0IFByb21pc2UgPSByZXF1aXJlKCdibHVlYmlyZCcpO1xuaW1wb3J0IGx2cyA9IHJlcXVpcmUoJy4uL2VsdmlzLWFwaS9hcGknKTtcbmltcG9ydCB1dWlkVjQgPSByZXF1aXJlKCd1dWlkL3Y0Jyk7XG5cbmV4cG9ydCBjbGFzcyBGaWxlVXRpbHMge1xuXG4gIC8qKlxuICAgKiBEb3dubG9hZCBhIHRodW1ibmFpbCBmcm9tIEVsdmlzLlxuICAgKiBPbiBzdWNjZXNzLCByZXR1cm5zIGEgUHJvbWlzZSB3aXRoIHRoZSBkZXN0aW5hdGlvbiBmaWxlIHBhdGguXG4gICAqIFxuICAgKiBAcGFyYW0gaGl0IEhpdCB0byBkb3dubG9hZFxuICAgKiBAcGFyYW0gZGVzdGluYXRpb25Gb2xkZXIgVGFyZ2V0IGZvbGRlciB3aGVyZSB0aGUgZmlsZSBpcyBkb3dubG9hZGVkXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGRvd25sb2FkVGh1bWJuYWlsKGhpdDogbHZzLkhpdEVsZW1lbnQsIGRlc3RpbmF0aW9uRm9sZGVyOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmRvd25sb2FkKERvd25sb2FkS2luZC5UaHVtYm5haWwsIGhpdCwgZGVzdGluYXRpb25Gb2xkZXIpO1xuICB9XG5cbiAgLyoqXG4gICAqIERvd25sb2FkIGEgcHJldmlldyBmcm9tIEVsdmlzLlxuICAgKiBPbiBzdWNjZXNzLCByZXR1cm5zIGEgUHJvbWlzZSB3aXRoIHRoZSBkZXN0aW5hdGlvbiBmaWxlIHBhdGguXG4gICAqIFxuICAgKiBAcGFyYW0gaGl0IEhpdCB0byBkb3dubG9hZFxuICAgKiBAcGFyYW0gZGVzdGluYXRpb25Gb2xkZXIgVGFyZ2V0IGZvbGRlciB3aGVyZSB0aGUgZmlsZSBpcyBkb3dubG9hZGVkXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGRvd25sb2FkUHJldmlldyhoaXQ6IGx2cy5IaXRFbGVtZW50LCBkZXN0aW5hdGlvbkZvbGRlcjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5kb3dubG9hZChEb3dubG9hZEtpbmQuUHJldmlldywgaGl0LCBkZXN0aW5hdGlvbkZvbGRlcik7XG4gIH1cblxuICAvKipcbiAgICogRG93bmxvYWQgdGhlIG9yaWdpbmFsIGZpbGUgZnJvbSBFbHZpcy5cbiAgICogT24gc3VjY2VzcywgcmV0dXJucyBhIFByb21pc2Ugd2l0aCB0aGUgZGVzdGluYXRpb24gZmlsZSBwYXRoLlxuICAgKiBcbiAgICogQHBhcmFtIGhpdCBIaXQgdG8gZG93bmxvYWRcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uRm9sZGVyIFRhcmdldCBmb2xkZXIgd2hlcmUgdGhlIGZpbGUgaXMgZG93bmxvYWRlZFxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBkb3dubG9hZE9yaWdpbmFsKGhpdDogbHZzLkhpdEVsZW1lbnQsIGRlc3RpbmF0aW9uRm9sZGVyOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmRvd25sb2FkKERvd25sb2FkS2luZC5PcmlnaW5hbCwgaGl0LCBkZXN0aW5hdGlvbkZvbGRlcik7XG4gIH1cblxuICAvKipcbiAgICogRG93bmxvYWQgYSB0aHVtYm5haWwsIHByZXZpZXcgb3Igb3JpZ2luYWwgZmlsZSBmcm9tIEVsdmlzIHRvIGEgc3BlY2lmaWVkIGRlc3RpbmF0aW9uIGZvbGRlci4gXG4gICAqIE9uIHN1Y2Nlc3MsIHJldHVybnMgYSBQcm9taXNlIHdpdGggdGhlIGRlc3RpbmF0aW9uIGZpbGUgcGF0aC5cbiAgICogXG4gICAqIEBwYXJhbSBraW5kIFdoYXQgdG8gZG93bmxvYWQ6IHRodW1ibmFpbCwgcHJldmlldyBvciBvcmlnaW5hbFxuICAgKiBAcGFyYW0gaGl0IEhpdCB0byBkb3dubG9hZFxuICAgKiBAcGFyYW0gZGVzdGluYXRpb25Gb2xkZXIgVGFyZ2V0IGZvbGRlciB3aGVyZSB0aGUgZmlsZSBpcyBkb3dubG9hZGVkXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGRvd25sb2FkKGtpbmQ6IERvd25sb2FkS2luZCwgaGl0OiBsdnMuSGl0RWxlbWVudCwgZGVzdGluYXRpb25Gb2xkZXI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgaWYgKCFoaXQpIHRocm93IG5ldyBFcnJvcignaGl0IGlzIGEgcmVxdWlyZWQgcGFyYW1ldGVyJyk7XG4gICAgaWYgKCFkZXN0aW5hdGlvbkZvbGRlcikgdGhyb3cgbmV3IEVycm9yKCdkZXN0aW5hdGlvbkZvbGRlciBpcyBhIHJlcXVpcmVkIHBhcmFtZXRlcicpO1xuXG4gICAgbGV0IGZpbGVVcmw6IHN0cmluZztcbiAgICBzd2l0Y2ggKGtpbmQpIHtcbiAgICAgIGNhc2UgRG93bmxvYWRLaW5kLlRodW1ibmFpbDpcbiAgICAgICAgZmlsZVVybCA9IGhpdC50aHVtYm5haWxVcmw7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBEb3dubG9hZEtpbmQuUHJldmlldzpcbiAgICAgICAgZmlsZVVybCA9IGhpdC5wcmV2aWV3VXJsO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRG93bmxvYWRLaW5kLk9yaWdpbmFsOlxuICAgICAgICBmaWxlVXJsID0gaGl0Lm9yaWdpbmFsVXJsO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQga2luZCBzcGVjaWZpZWQ6ICcgKyBraW5kKTtcbiAgICB9XG4gICAgaWYgKCFmaWxlVXJsKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vICcgKyBEb3dubG9hZEtpbmRba2luZF0gKyAnIFVSTCBhdmFpbGFibGUgZm9yIGhpdDogJyArIGhpdC5pZCk7XG4gICAgfVxuXG4gICAgbGV0IGZpbGVuYW1lOiBzdHJpbmcgPSBwYXRoLmJhc2VuYW1lKHVybC5wYXJzZShmaWxlVXJsKS5wYXRobmFtZSk7XG4gICAgbGV0IGV4dDogc3RyaW5nID0gcGF0aC5leHRuYW1lKGZpbGVuYW1lKTtcbiAgICBsZXQgYmFzZUZpbGVuYW1lOiBzdHJpbmcgPSBwYXRoLmJhc2VuYW1lKGZpbGVuYW1lLCBleHQpO1xuICAgIGxldCB0ZW1wRmlsZW5hbWU6IHN0cmluZyA9IGJhc2VGaWxlbmFtZSArICdfJyArIHV1aWRWNCgpICsgZXh0O1xuICAgIGxldCBkZXN0aW5hdGlvbjogc3RyaW5nID0gcGF0aC5qb2luKGRlc3RpbmF0aW9uRm9sZGVyLCB0ZW1wRmlsZW5hbWUpO1xuXG4gICAgcmV0dXJuIHRoaXMuZG93bmxvYWRGaWxlKGhpdC5wcmV2aWV3VXJsLCBkZXN0aW5hdGlvbik7XG4gIH1cblxuICAvKipcbiAgICogRG93bmxvYWQgYW4gRWx2aXMgVVJMLlxuICAgKiBPbiBzdWNjZXNzLCByZXR1cm5zIGEgUHJvbWlzZSB3aXRoIHRoZSBkZXN0aW5hdGlvbiBmaWxlIHBhdGguXG4gICAqIFxuICAgKiAgQHBhcmFtIHVybCBVUkwgdG8gZG93bmxvYWRcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uIERlc3RpbmF0aW9uIGZvbGRlclxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgZG93bmxvYWRGaWxlKHVybDogc3RyaW5nLCBkZXN0aW5hdGlvbjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy5jcmVhdGVEZXN0aW5hdGlvbkRpcmVjdG9yeShkZXN0aW5hdGlvbikudGhlbigoKSA9PiB7XG4gICAgICByZXR1cm4gQXBpTWFuYWdlci5nZXRBcGkoKS5lbHZpc1JlcXVlc3QucmVxdWVzdEZpbGUodXJsLCBkZXN0aW5hdGlvbik7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQ3JlYXRlIGEgZm9sZGVyIGZvciBhIGdpdmVuIGZpbGUgaWYgaXQgZG9lc24ndCBleGlzdHNcbiAgICogXG4gICAqIEBwYXJhbSBmaWxlIFxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgY3JlYXRlRGVzdGluYXRpb25EaXJlY3RvcnkoZmlsZTogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgZGlyOiBzdHJpbmcgPSByZXF1aXJlKCdwYXRoJykuZGlybmFtZShmaWxlKTtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBmcy5ta2RpcihkaXIsIGVycm9yID0+IHtcbiAgICAgICAgaWYgKCFlcnJvciB8fCAoZXJyb3IgJiYgZXJyb3IuY29kZSA9PT0gJ0VFWElTVCcpKSB7XG4gICAgICAgICAgcmVzb2x2ZShkaXIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHJlamVjdChlcnJvcik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbn1cblxuZXhwb3J0IGVudW0gRG93bmxvYWRLaW5kIHtcbiAgVGh1bWJuYWlsLFxuICBQcmV2aWV3LFxuICBPcmlnaW5hbFxufSJdfQ==