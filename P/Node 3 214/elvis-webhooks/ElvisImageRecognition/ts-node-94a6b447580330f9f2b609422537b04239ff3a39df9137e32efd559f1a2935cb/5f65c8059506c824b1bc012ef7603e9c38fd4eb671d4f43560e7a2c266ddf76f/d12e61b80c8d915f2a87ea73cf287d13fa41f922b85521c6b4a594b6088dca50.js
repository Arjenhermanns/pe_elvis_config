"use strict";
/**
 * Elvis REST API
 * Elvis RESTful API
 *
 * OpenAPI spec version: 1.1.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const elvis_request_1 = require("./elvis-request");
let defaultBasePath = 'http://localhost:8080';
// ===============================================
// This file is autogenerated - Please do not edit
// ===============================================
/* tslint:disable:no-unused-variable */
class AssetSearch {
}
exports.AssetSearch = AssetSearch;
class ExpandType {
}
exports.ExpandType = ExpandType;
class Facet {
}
exports.Facet = Facet;
class FacetOrder {
}
exports.FacetOrder = FacetOrder;
class FacetSelection {
}
exports.FacetSelection = FacetSelection;
class HitElement {
}
exports.HitElement = HitElement;
class Query {
}
exports.Query = Query;
class QueryStringQuery {
}
exports.QueryStringQuery = QueryStringQuery;
class RelationElement {
}
exports.RelationElement = RelationElement;
class SearchResponse {
}
exports.SearchResponse = SearchResponse;
class Sort {
}
exports.Sort = Sort;
class HttpBasicAuth {
    applyToRequest(requestOptions) {
        requestOptions.auth = {
            username: this.username, password: this.password
        };
    }
}
exports.HttpBasicAuth = HttpBasicAuth;
class ApiKeyAuth {
    constructor(location, paramName) {
        this.location = location;
        this.paramName = paramName;
    }
    applyToRequest(requestOptions) {
        if (this.location == "query") {
            requestOptions.qs[this.paramName] = this.apiKey;
        }
        else if (this.location == "header" && requestOptions && requestOptions.headers) {
            requestOptions.headers[this.paramName] = this.apiKey;
        }
    }
}
exports.ApiKeyAuth = ApiKeyAuth;
class OAuth {
    applyToRequest(requestOptions) {
        if (requestOptions && requestOptions.headers) {
            requestOptions.headers["Authorization"] = "Bearer " + this.accessToken;
        }
    }
}
exports.OAuth = OAuth;
class VoidAuth {
    applyToRequest(_) {
        // Do nothing
    }
}
exports.VoidAuth = VoidAuth;
var ElvisApiApiKeys;
(function (ElvisApiApiKeys) {
})(ElvisApiApiKeys = exports.ElvisApiApiKeys || (exports.ElvisApiApiKeys = {}));
class ElvisApi {
    constructor(username, password, basePath) {
        this.basePath = defaultBasePath;
        this.defaultHeaders = {};
        this._useQuerystring = false;
        this.authentications = {
            'default': new VoidAuth(),
        };
        this.basePath = basePath;
        this.elvisRequest = new elvis_request_1.ElvisRequest(this.basePath, username, password);
    }
    set useQuerystring(value) {
        this._useQuerystring = value;
    }
    setApiKey(key, value) {
        this.authentications[ElvisApiApiKeys[key]].apiKey = value;
    }
    /**
     * Search in Elvis
     *
     * @param q The query to search for.
     * @param start First hit to be returned. Starting at 0 for the first hit. Used to skip hits to return &#39;paged&#39; results.  Optional. Default value is 0.
     * @param num Number of hits to return.  Optional. Default value is 50.
     * @param metadataToReturn Comma-delimited list of metadata fields to return. Specify &#39;all&#39; to return all available metadata. Omit to return a minimal list of fields.  Optional. If omitted, only the file will be updated.
     * @param appendRequestSecret When set to true will append an encrypted code to the thumbnail, preview and original URLs. This is useful when the search is transformed to HTML by an intermediary and is then served to a web browser that is not authenticated against the server. The request secret is valid for a limited time (30 minutes).  Optional. Default value is false.
     */
    searchGet(q, start, num, metadataToReturn, appendRequestSecret) {
        const localVarPath = this.basePath + '/services/search';
        let queryParameters = {};
        let headerParams = Object.assign({}, this.defaultHeaders);
        let formParams = {};
        // verify required parameter 'q' is not null or undefined
        if (q === null || q === undefined) {
            throw new Error('Required parameter q was null or undefined when calling searchGet.');
        }
        if (q !== undefined) {
            queryParameters['q'] = q;
        }
        if (start !== undefined) {
            queryParameters['start'] = start;
        }
        if (num !== undefined) {
            queryParameters['num'] = num;
        }
        if (metadataToReturn !== undefined) {
            queryParameters['metadataToReturn'] = metadataToReturn;
        }
        if (appendRequestSecret !== undefined) {
            queryParameters['appendRequestSecret'] = appendRequestSecret;
        }
        let useFormData = false;
        let requestOptions = {
            method: 'GET',
            qs: queryParameters,
            headers: headerParams,
            uri: localVarPath,
            useQuerystring: this._useQuerystring,
            json: true,
        };
        this.authentications.default.applyToRequest(requestOptions);
        if (Object.keys(formParams).length) {
            if (useFormData) {
                requestOptions.formData = formParams;
            }
            else {
                requestOptions.form = formParams;
            }
        }
        return this.elvisRequest.request(requestOptions);
    }
    /**
     * Search in Elvis
     *
     * @param body AssetSearch object
     */
    searchPost(body) {
        const localVarPath = this.basePath + '/services/search';
        let queryParameters = {};
        let headerParams = Object.assign({}, this.defaultHeaders);
        let formParams = {};
        // verify required parameter 'body' is not null or undefined
        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling searchPost.');
        }
        let useFormData = false;
        let requestOptions = {
            method: 'POST',
            qs: queryParameters,
            headers: headerParams,
            uri: localVarPath,
            useQuerystring: this._useQuerystring,
            json: true,
            body: body,
        };
        this.authentications.default.applyToRequest(requestOptions);
        if (Object.keys(formParams).length) {
            if (useFormData) {
                requestOptions.formData = formParams;
            }
            else {
                requestOptions.form = formParams;
            }
        }
        return this.elvisRequest.request(requestOptions);
    }
    /**
     * Updates the file and/or the metadata for a given asset. It&#39;s required to either specify the Fildata or metadata parameter.
     *
     * @param id Id of the asset to be updated.
     * @param metadata A JSON encoded object with properties that match Elvis metadata field names. This metadata will be set on the asset in Elvis.   &#x60;&#x60;&#x60;       {           \&quot;tags\&quot;: [\&quot;foo\&quot;, \&quot;bar\&quot;],           \&quot;description\&quot;: \&quot;foo bar\&quot;       }   &#x60;&#x60;&#x60;
     * @param filedata The file to be updated in Elvis. Note that it&#39;s not mandatory to checkout a file before updating it.  Optional. If omitted, only metadata will be updated.
     * @param metadataToReturn Comma-delimited list of metadata fields to return. Specify &#39;all&#39; to return all available metadata. Omit to return a minimal list of fields.  Optional. If omitted, only the file will be updated.
     * @param nextUrl When specified, the service will send a 301 redirect to this URL when it completes successfully. If you place &#39;${id}&#39; in the URL, it will be replaced with the Elvis asset id of the updated asset. If omitted, a 200 OK status code will be returned.  Optional. If omitted, the updated HitElement will be returned.
     * @param parseMetadataModifications When updating metadata, Elvis parses plus(+) and minus(-) symbols as special characters to add or remove values for multi-value fields. For example, this removes the tag \&quot;foo\&quot; and adds the tag \&quot;bar\&quot;;    &#x60;&#x60;&#x60;       {           \&quot;tags\&quot;: [\&quot;-foo\&quot;, \&quot;+bar\&quot;]       }   &#x60;&#x60;&#x60;      Set to false if you want to disable the parsing and index the exact specified metadata value including the + or - symbol.  Optional. Default value is true
     * @param clearCheckoutState When the file is checked-out, check in the file as part of updating it.  Optional. Default value is false.
     * @param createVersion Create a file version when the file is checked in.  Optional. Default value is true.
     * @param autoRename When set to true, the file is automatically renamed when a file already exists on the destination location. When set to false, an excpetion is thrown when a file already exists.  A destination change can happen when for example the newly updated file has a different extension or when a new filename is specified in the metadata.  Optional. Default value is true.
     * @param authcred User credentials specifified as a base64 encoded string. Use this alternative type of authentication only for testing purposes or when the user can publicly access assets. Usage example; assume the &#39;admin&#39; user with default password &#39;changemenow&#39;, the authcred is the base64 encoded equivalent of admin:changemenow &#x3D;&gt; YWRtaW46Y2hhbmdlbWVub3c&#x3D;
     */
    update(id, metadata, filedata, metadataToReturn, nextUrl, parseMetadataModifications, clearCheckoutState, createVersion, autoRename, authcred) {
        const localVarPath = this.basePath + '/services/asset/update';
        let queryParameters = {};
        let headerParams = Object.assign({}, this.defaultHeaders);
        let formParams = {};
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling update.');
        }
        if (id !== undefined) {
            queryParameters['id'] = id;
        }
        if (metadata !== undefined) {
            queryParameters['metadata'] = metadata;
        }
        if (metadataToReturn !== undefined) {
            queryParameters['metadataToReturn'] = metadataToReturn;
        }
        if (nextUrl !== undefined) {
            queryParameters['nextUrl'] = nextUrl;
        }
        if (parseMetadataModifications !== undefined) {
            queryParameters['parseMetadataModifications'] = parseMetadataModifications;
        }
        if (clearCheckoutState !== undefined) {
            queryParameters['clearCheckoutState'] = clearCheckoutState;
        }
        if (createVersion !== undefined) {
            queryParameters['createVersion'] = createVersion;
        }
        if (autoRename !== undefined) {
            queryParameters['autoRename'] = autoRename;
        }
        if (authcred !== undefined) {
            queryParameters['authcred'] = authcred;
        }
        let useFormData = false;
        if (filedata !== undefined) {
            formParams['Filedata'] = filedata;
        }
        useFormData = true;
        let requestOptions = {
            method: 'POST',
            qs: queryParameters,
            headers: headerParams,
            uri: localVarPath,
            useQuerystring: this._useQuerystring,
            json: true,
        };
        this.authentications.default.applyToRequest(requestOptions);
        if (Object.keys(formParams).length) {
            if (useFormData) {
                requestOptions.formData = formParams;
            }
            else {
                requestOptions.form = formParams;
            }
        }
        return this.elvisRequest.request(requestOptions);
    }
}
exports.ElvisApi = ElvisApi;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2VsdmlzLWFwaS9hcGkudHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNJbWFnZVJlY29nbml0aW9uL3NyYy9lbHZpcy1hcGkvYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7Ozs7Ozs7OztHQVVHOztBQUVILG1EQUErQztBQUkvQyxJQUFJLGVBQWUsR0FBRyx1QkFBdUIsQ0FBQztBQUU5QyxrREFBa0Q7QUFDbEQsa0RBQWtEO0FBQ2xELGtEQUFrRDtBQUVsRCx1Q0FBdUM7QUFFdkM7Q0FZQztBQVpELGtDQVlDO0FBRUQ7Q0FDQztBQURELGdDQUNDO0FBRUQ7Q0FRQztBQVJELHNCQVFDO0FBRUQ7Q0FDQztBQURELGdDQUNDO0FBRUQ7Q0FJQztBQUpELHdDQUlDO0FBRUQ7Q0FXQztBQVhELGdDQVdDO0FBRUQ7Q0FFQztBQUZELHNCQUVDO0FBRUQ7Q0FFQztBQUZELDRDQUVDO0FBRUQ7Q0FPQztBQVBELDBDQU9DO0FBRUQ7Q0FLQztBQUxELHdDQUtDO0FBRUQ7Q0FHQztBQUhELG9CQUdDO0FBVUQ7SUFHSSxjQUFjLENBQUMsY0FBK0I7UUFDMUMsY0FBYyxDQUFDLElBQUksR0FBRztZQUNsQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDbkQsQ0FBQTtJQUNMLENBQUM7Q0FDSjtBQVJELHNDQVFDO0FBRUQ7SUFHSSxZQUFvQixRQUFnQixFQUFVLFNBQWlCO1FBQTNDLGFBQVEsR0FBUixRQUFRLENBQVE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFRO0lBQy9ELENBQUM7SUFFRCxjQUFjLENBQUMsY0FBK0I7UUFDMUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLGNBQWMsQ0FBQyxFQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDM0QsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsSUFBSSxjQUFjLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDL0UsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN6RCxDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBYkQsZ0NBYUM7QUFFRDtJQUdJLGNBQWMsQ0FBQyxjQUErQjtRQUMxQyxFQUFFLENBQUMsQ0FBQyxjQUFjLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDM0MsY0FBYyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMzRSxDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBUkQsc0JBUUM7QUFFRDtJQUdJLGNBQWMsQ0FBQyxDQUFrQjtRQUM3QixhQUFhO0lBQ2pCLENBQUM7Q0FDSjtBQU5ELDRCQU1DO0FBRUQsSUFBWSxlQUNYO0FBREQsV0FBWSxlQUFlO0FBQzNCLENBQUMsRUFEVyxlQUFlLEdBQWYsdUJBQWUsS0FBZix1QkFBZSxRQUMxQjtBQUVEO0lBVUksWUFBWSxRQUFnQixFQUFFLFFBQWdCLEVBQUUsUUFBZ0I7UUFUdEQsYUFBUSxHQUFHLGVBQWUsQ0FBQztRQUMzQixtQkFBYyxHQUFRLEVBQUUsQ0FBQztRQUN6QixvQkFBZSxHQUFZLEtBQUssQ0FBQztRQUdqQyxvQkFBZSxHQUFHO1lBQ3hCLFNBQVMsRUFBa0IsSUFBSSxRQUFRLEVBQUU7U0FDNUMsQ0FBQTtRQUdHLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSw0QkFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFRCxJQUFJLGNBQWMsQ0FBQyxLQUFjO1FBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxTQUFTLENBQUMsR0FBb0IsRUFBRSxLQUFhO1FBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM5RCxDQUFDO0lBQ0Q7Ozs7Ozs7O09BUUc7SUFDSSxTQUFTLENBQUMsQ0FBUyxFQUFFLEtBQWMsRUFBRSxHQUFZLEVBQUUsZ0JBQXlCLEVBQUUsbUJBQTZCO1FBQzlHLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsa0JBQWtCLENBQUM7UUFDeEQsSUFBSSxlQUFlLEdBQVEsRUFBRSxDQUFDO1FBQzlCLElBQUksWUFBWSxHQUFjLE1BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN0RSxJQUFJLFVBQVUsR0FBUSxFQUFFLENBQUM7UUFHekIseURBQXlEO1FBQ3pELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDaEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxvRUFBb0UsQ0FBQyxDQUFDO1FBQzFGLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNsQixlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN0QixlQUFlLENBQUMsT0FBTyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ3JDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNwQixlQUFlLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQ2pDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLGdCQUFnQixDQUFDO1FBQzNELENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxtQkFBbUIsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLG1CQUFtQixDQUFDO1FBQ2pFLENBQUM7UUFFRCxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFFeEIsSUFBSSxjQUFjLEdBQW9CO1lBQ2xDLE1BQU0sRUFBRSxLQUFLO1lBQ2IsRUFBRSxFQUFFLGVBQWU7WUFDbkIsT0FBTyxFQUFFLFlBQVk7WUFDckIsR0FBRyxFQUFFLFlBQVk7WUFDakIsY0FBYyxFQUFFLElBQUksQ0FBQyxlQUFlO1lBQ3BDLElBQUksRUFBRSxJQUFJO1NBQ2IsQ0FBQztRQUVGLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUU1RCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDakMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDUixjQUFlLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztZQUNoRCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osY0FBYyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7WUFDckMsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUNEOzs7O09BSUc7SUFDSSxVQUFVLENBQUMsSUFBaUI7UUFDL0IsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQztRQUN4RCxJQUFJLGVBQWUsR0FBUSxFQUFFLENBQUM7UUFDOUIsSUFBSSxZQUFZLEdBQWMsTUFBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksVUFBVSxHQUFRLEVBQUUsQ0FBQztRQUd6Qiw0REFBNEQ7UUFDNUQsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN0QyxNQUFNLElBQUksS0FBSyxDQUFDLHdFQUF3RSxDQUFDLENBQUM7UUFDOUYsQ0FBQztRQUVELElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUV4QixJQUFJLGNBQWMsR0FBb0I7WUFDbEMsTUFBTSxFQUFFLE1BQU07WUFDZCxFQUFFLEVBQUUsZUFBZTtZQUNuQixPQUFPLEVBQUUsWUFBWTtZQUNyQixHQUFHLEVBQUUsWUFBWTtZQUNqQixjQUFjLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDcEMsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFFRixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFNUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ1IsY0FBZSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7WUFDaEQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLGNBQWMsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1lBQ3JDLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFDRDs7Ozs7Ozs7Ozs7OztPQWFHO0lBQ0ksTUFBTSxDQUFDLEVBQVUsRUFBRSxRQUFpQixFQUFFLFFBQWlCLEVBQUUsZ0JBQXlCLEVBQUUsT0FBZ0IsRUFBRSwwQkFBb0MsRUFBRSxrQkFBNEIsRUFBRSxhQUF1QixFQUFFLFVBQW9CLEVBQUUsUUFBaUI7UUFDN08sTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyx3QkFBd0IsQ0FBQztRQUM5RCxJQUFJLGVBQWUsR0FBUSxFQUFFLENBQUM7UUFDOUIsSUFBSSxZQUFZLEdBQWMsTUFBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksVUFBVSxHQUFRLEVBQUUsQ0FBQztRQUd6QiwwREFBMEQ7UUFDMUQsRUFBRSxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksSUFBSSxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNsQyxNQUFNLElBQUksS0FBSyxDQUFDLGtFQUFrRSxDQUFDLENBQUM7UUFDeEYsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ25CLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDL0IsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUM7UUFDM0MsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDakMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsZ0JBQWdCLENBQUM7UUFDM0QsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLGVBQWUsQ0FBQyxTQUFTLENBQUMsR0FBRyxPQUFPLENBQUM7UUFDekMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLDBCQUEwQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsZUFBZSxDQUFDLDRCQUE0QixDQUFDLEdBQUcsMEJBQTBCLENBQUM7UUFDL0UsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLGtCQUFrQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDbkMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsa0JBQWtCLENBQUM7UUFDL0QsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLGFBQWEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzlCLGVBQWUsQ0FBQyxlQUFlLENBQUMsR0FBRyxhQUFhLENBQUM7UUFDckQsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzNCLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUM7UUFDL0MsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUM7UUFDM0MsQ0FBQztRQUVELElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUV4QixFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN6QixVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDO1FBQ3RDLENBQUM7UUFDRCxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBRW5CLElBQUksY0FBYyxHQUFvQjtZQUNsQyxNQUFNLEVBQUUsTUFBTTtZQUNkLEVBQUUsRUFBRSxlQUFlO1lBQ25CLE9BQU8sRUFBRSxZQUFZO1lBQ3JCLEdBQUcsRUFBRSxZQUFZO1lBQ2pCLGNBQWMsRUFBRSxJQUFJLENBQUMsZUFBZTtZQUNwQyxJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFFRixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFNUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ1IsY0FBZSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7WUFDaEQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLGNBQWMsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1lBQ3JDLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3JELENBQUM7Q0FDSjtBQXRORCw0QkFzTkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEVsdmlzIFJFU1QgQVBJXG4gKiBFbHZpcyBSRVNUZnVsIEFQSVxuICpcbiAqIE9wZW5BUEkgc3BlYyB2ZXJzaW9uOiAxLjEuMFxuICogXG4gKlxuICogTk9URTogVGhpcyBjbGFzcyBpcyBhdXRvIGdlbmVyYXRlZCBieSB0aGUgc3dhZ2dlciBjb2RlIGdlbmVyYXRvciBwcm9ncmFtLlxuICogaHR0cHM6Ly9naXRodWIuY29tL3N3YWdnZXItYXBpL3N3YWdnZXItY29kZWdlbi5naXRcbiAqIERvIG5vdCBlZGl0IHRoZSBjbGFzcyBtYW51YWxseS5cbiAqL1xuXG5pbXBvcnQgeyBFbHZpc1JlcXVlc3QgfSBmcm9tICcuL2VsdmlzLXJlcXVlc3QnO1xuaW1wb3J0IHJlcXVlc3QgPSByZXF1aXJlKCdyZXF1ZXN0Jyk7XG5pbXBvcnQgUHJvbWlzZSA9IHJlcXVpcmUoJ2JsdWViaXJkJyk7XG5cbmxldCBkZWZhdWx0QmFzZVBhdGggPSAnaHR0cDovL2xvY2FsaG9zdDo4MDgwJztcblxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbi8vIFRoaXMgZmlsZSBpcyBhdXRvZ2VuZXJhdGVkIC0gUGxlYXNlIGRvIG5vdCBlZGl0XG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4vKiB0c2xpbnQ6ZGlzYWJsZTpuby11bnVzZWQtdmFyaWFibGUgKi9cblxuZXhwb3J0IGNsYXNzIEFzc2V0U2VhcmNoIHtcbiAgICBmaXJzdFJlc3VsdD86IG51bWJlcjtcbiAgICBtYXhSZXN1bHRIaXRzPzogbnVtYmVyO1xuICAgIHNvcnRpbmc/OiBBcnJheTxTb3J0PjtcbiAgICBxdWVyeTogUXVlcnk7XG4gICAgZmFjZXRzPzogeyBba2V5OiBzdHJpbmddOiBGYWNldDsgfTtcbiAgICBtZXRhZGF0YVRvUmV0dXJuPzogQXJyYXk8c3RyaW5nPjtcbiAgICBleHBhbmQ/OiBBcnJheTxFeHBhbmRUeXBlPjtcbiAgICByZXR1cm5IaXRzPzogYm9vbGVhbjtcbiAgICByZXR1cm5GYWNldHM/OiBib29sZWFuO1xuICAgIHJldHVyblBlbmRpbmdJbXBvcnRzPzogYm9vbGVhbjtcbiAgICByZXR1cm5IaWdobGlnaHRlZFRleHQ/OiBib29sZWFuO1xufVxuXG5leHBvcnQgY2xhc3MgRXhwYW5kVHlwZSB7XG59XG5cbmV4cG9ydCBjbGFzcyBGYWNldCB7XG4gICAgZmllbGQ6IHN0cmluZztcbiAgICBtaW5PY2N1cnM/OiBudW1iZXI7XG4gICAgbWF4T2NjdXJzPzogbnVtYmVyO1xuICAgIGV4cGFuZFNlbGVjdGlvbj86IGJvb2xlYW47XG4gICAgc2VhcmNoT3JkZXI/OiBGYWNldE9yZGVyO1xuICAgIHJlc3VsdE9yZGVyPzogRmFjZXRPcmRlcjtcbiAgICBzZWxlY3Rpb246IEZhY2V0U2VsZWN0aW9uO1xufVxuXG5leHBvcnQgY2xhc3MgRmFjZXRPcmRlciB7XG59XG5cbmV4cG9ydCBjbGFzcyBGYWNldFNlbGVjdGlvbiB7XG4gICAgdmFsdWVzOiBBcnJheTxzdHJpbmc+O1xuICAgIG5vdFZhbHVlcz86IEFycmF5PHN0cmluZz47XG4gICAgb3BlcmF0aW9uPzogc3RyaW5nO1xufVxuXG5leHBvcnQgY2xhc3MgSGl0RWxlbWVudCB7XG4gICAgaWQ/OiBzdHJpbmc7XG4gICAgdGh1bWJuYWlsVXJsPzogc3RyaW5nO1xuICAgIHByZXZpZXdVcmw/OiBzdHJpbmc7XG4gICAgb3JpZ2luYWxVcmw/OiBzdHJpbmc7XG4gICAgaGlnaGxpZ2h0ZWRUZXh0Pzogc3RyaW5nO1xuICAgIHRodW1ibmFpbEhpdHM/OiBhbnk7XG4gICAgbWV0YWRhdGE/OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfTtcbiAgICBvcmlnaW5hbFN0b3JhZ2VQYXRoPzogc3RyaW5nO1xuICAgIHJlbGF0aW9uPzogUmVsYXRpb25FbGVtZW50O1xuICAgIHBlcm1pc3Npb25zPzogc3RyaW5nO1xufVxuXG5leHBvcnQgY2xhc3MgUXVlcnkge1xuICAgIFF1ZXJ5U3RyaW5nUXVlcnk/OiBRdWVyeVN0cmluZ1F1ZXJ5O1xufVxuXG5leHBvcnQgY2xhc3MgUXVlcnlTdHJpbmdRdWVyeSB7XG4gICAgcXVlcnlTdHJpbmc/OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBjbGFzcyBSZWxhdGlvbkVsZW1lbnQge1xuICAgIHJlbGF0aW9uSWQ/OiBzdHJpbmc7XG4gICAgcmVsYXRpb25UeXBlPzogc3RyaW5nO1xuICAgIGRpcmVjdGlvbmFsUmVsYXRpb25UeXBlPzogc3RyaW5nO1xuICAgIHRhcmdldDFJZD86IHN0cmluZztcbiAgICB0YXJnZXQySWQ/OiBzdHJpbmc7XG4gICAgbWV0YWRhdGE/OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfTtcbn1cblxuZXhwb3J0IGNsYXNzIFNlYXJjaFJlc3BvbnNlIHtcbiAgICBmaXJzdFJlc3VsdD86IG51bWJlcjtcbiAgICBtYXhSZXN1bHRIaXRzPzogbnVtYmVyO1xuICAgIHRvdGFsSGl0cz86IG51bWJlcjtcbiAgICBoaXRzPzogQXJyYXk8SGl0RWxlbWVudD47XG59XG5cbmV4cG9ydCBjbGFzcyBTb3J0IHtcbiAgICBmaWVsZD86IHN0cmluZztcbiAgICBkZXNjZW5kaW5nPzogYm9vbGVhbjtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIEF1dGhlbnRpY2F0aW9uIHtcbiAgICAvKipcbiAgICAqIEFwcGx5IGF1dGhlbnRpY2F0aW9uIHNldHRpbmdzIHRvIGhlYWRlciBhbmQgcXVlcnkgcGFyYW1zLlxuICAgICovXG4gICAgYXBwbHlUb1JlcXVlc3QocmVxdWVzdE9wdGlvbnM6IHJlcXVlc3QuT3B0aW9ucyk6IHZvaWQ7XG59XG5cbmV4cG9ydCBjbGFzcyBIdHRwQmFzaWNBdXRoIGltcGxlbWVudHMgQXV0aGVudGljYXRpb24ge1xuICAgIHB1YmxpYyB1c2VybmFtZTogc3RyaW5nO1xuICAgIHB1YmxpYyBwYXNzd29yZDogc3RyaW5nO1xuICAgIGFwcGx5VG9SZXF1ZXN0KHJlcXVlc3RPcHRpb25zOiByZXF1ZXN0Lk9wdGlvbnMpOiB2b2lkIHtcbiAgICAgICAgcmVxdWVzdE9wdGlvbnMuYXV0aCA9IHtcbiAgICAgICAgICAgIHVzZXJuYW1lOiB0aGlzLnVzZXJuYW1lLCBwYXNzd29yZDogdGhpcy5wYXNzd29yZFxuICAgICAgICB9XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgQXBpS2V5QXV0aCBpbXBsZW1lbnRzIEF1dGhlbnRpY2F0aW9uIHtcbiAgICBwdWJsaWMgYXBpS2V5OiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvY2F0aW9uOiBzdHJpbmcsIHByaXZhdGUgcGFyYW1OYW1lOiBzdHJpbmcpIHtcbiAgICB9XG5cbiAgICBhcHBseVRvUmVxdWVzdChyZXF1ZXN0T3B0aW9uczogcmVxdWVzdC5PcHRpb25zKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmxvY2F0aW9uID09IFwicXVlcnlcIikge1xuICAgICAgICAgICAgKDxhbnk+cmVxdWVzdE9wdGlvbnMucXMpW3RoaXMucGFyYW1OYW1lXSA9IHRoaXMuYXBpS2V5O1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMubG9jYXRpb24gPT0gXCJoZWFkZXJcIiAmJiByZXF1ZXN0T3B0aW9ucyAmJiByZXF1ZXN0T3B0aW9ucy5oZWFkZXJzKSB7XG4gICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5oZWFkZXJzW3RoaXMucGFyYW1OYW1lXSA9IHRoaXMuYXBpS2V5O1xuICAgICAgICB9XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgT0F1dGggaW1wbGVtZW50cyBBdXRoZW50aWNhdGlvbiB7XG4gICAgcHVibGljIGFjY2Vzc1Rva2VuOiBzdHJpbmc7XG5cbiAgICBhcHBseVRvUmVxdWVzdChyZXF1ZXN0T3B0aW9uczogcmVxdWVzdC5PcHRpb25zKTogdm9pZCB7XG4gICAgICAgIGlmIChyZXF1ZXN0T3B0aW9ucyAmJiByZXF1ZXN0T3B0aW9ucy5oZWFkZXJzKSB7XG4gICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5oZWFkZXJzW1wiQXV0aG9yaXphdGlvblwiXSA9IFwiQmVhcmVyIFwiICsgdGhpcy5hY2Nlc3NUb2tlbjtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFZvaWRBdXRoIGltcGxlbWVudHMgQXV0aGVudGljYXRpb24ge1xuICAgIHB1YmxpYyB1c2VybmFtZTogc3RyaW5nO1xuICAgIHB1YmxpYyBwYXNzd29yZDogc3RyaW5nO1xuICAgIGFwcGx5VG9SZXF1ZXN0KF86IHJlcXVlc3QuT3B0aW9ucyk6IHZvaWQge1xuICAgICAgICAvLyBEbyBub3RoaW5nXG4gICAgfVxufVxuXG5leHBvcnQgZW51bSBFbHZpc0FwaUFwaUtleXMge1xufVxuXG5leHBvcnQgY2xhc3MgRWx2aXNBcGkge1xuICAgIHByb3RlY3RlZCBiYXNlUGF0aCA9IGRlZmF1bHRCYXNlUGF0aDtcbiAgICBwcm90ZWN0ZWQgZGVmYXVsdEhlYWRlcnM6IGFueSA9IHt9O1xuICAgIHByb3RlY3RlZCBfdXNlUXVlcnlzdHJpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwdWJsaWMgZWx2aXNSZXF1ZXN0OiBFbHZpc1JlcXVlc3Q7XG5cbiAgICBwcm90ZWN0ZWQgYXV0aGVudGljYXRpb25zID0ge1xuICAgICAgICAnZGVmYXVsdCc6IDxBdXRoZW50aWNhdGlvbj5uZXcgVm9pZEF1dGgoKSxcbiAgICB9XG5cbiAgICBjb25zdHJ1Y3Rvcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nLCBiYXNlUGF0aDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuYmFzZVBhdGggPSBiYXNlUGF0aDtcbiAgICAgICAgdGhpcy5lbHZpc1JlcXVlc3QgPSBuZXcgRWx2aXNSZXF1ZXN0KHRoaXMuYmFzZVBhdGgsIHVzZXJuYW1lLCBwYXNzd29yZCk7XG4gICAgfVxuXG4gICAgc2V0IHVzZVF1ZXJ5c3RyaW5nKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX3VzZVF1ZXJ5c3RyaW5nID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHVibGljIHNldEFwaUtleShrZXk6IEVsdmlzQXBpQXBpS2V5cywgdmFsdWU6IHN0cmluZykge1xuICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uc1tFbHZpc0FwaUFwaUtleXNba2V5XV0uYXBpS2V5ID0gdmFsdWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFNlYXJjaCBpbiBFbHZpc1xuICAgICAqIFxuICAgICAqIEBwYXJhbSBxIFRoZSBxdWVyeSB0byBzZWFyY2ggZm9yLlxuICAgICAqIEBwYXJhbSBzdGFydCBGaXJzdCBoaXQgdG8gYmUgcmV0dXJuZWQuIFN0YXJ0aW5nIGF0IDAgZm9yIHRoZSBmaXJzdCBoaXQuIFVzZWQgdG8gc2tpcCBoaXRzIHRvIHJldHVybiAmIzM5O3BhZ2VkJiMzOTsgcmVzdWx0cy4gIE9wdGlvbmFsLiBEZWZhdWx0IHZhbHVlIGlzIDAuIFxuICAgICAqIEBwYXJhbSBudW0gTnVtYmVyIG9mIGhpdHMgdG8gcmV0dXJuLiAgT3B0aW9uYWwuIERlZmF1bHQgdmFsdWUgaXMgNTAuIFxuICAgICAqIEBwYXJhbSBtZXRhZGF0YVRvUmV0dXJuIENvbW1hLWRlbGltaXRlZCBsaXN0IG9mIG1ldGFkYXRhIGZpZWxkcyB0byByZXR1cm4uIFNwZWNpZnkgJiMzOTthbGwmIzM5OyB0byByZXR1cm4gYWxsIGF2YWlsYWJsZSBtZXRhZGF0YS4gT21pdCB0byByZXR1cm4gYSBtaW5pbWFsIGxpc3Qgb2YgZmllbGRzLiAgT3B0aW9uYWwuIElmIG9taXR0ZWQsIG9ubHkgdGhlIGZpbGUgd2lsbCBiZSB1cGRhdGVkLiBcbiAgICAgKiBAcGFyYW0gYXBwZW5kUmVxdWVzdFNlY3JldCBXaGVuIHNldCB0byB0cnVlIHdpbGwgYXBwZW5kIGFuIGVuY3J5cHRlZCBjb2RlIHRvIHRoZSB0aHVtYm5haWwsIHByZXZpZXcgYW5kIG9yaWdpbmFsIFVSTHMuIFRoaXMgaXMgdXNlZnVsIHdoZW4gdGhlIHNlYXJjaCBpcyB0cmFuc2Zvcm1lZCB0byBIVE1MIGJ5IGFuIGludGVybWVkaWFyeSBhbmQgaXMgdGhlbiBzZXJ2ZWQgdG8gYSB3ZWIgYnJvd3NlciB0aGF0IGlzIG5vdCBhdXRoZW50aWNhdGVkIGFnYWluc3QgdGhlIHNlcnZlci4gVGhlIHJlcXVlc3Qgc2VjcmV0IGlzIHZhbGlkIGZvciBhIGxpbWl0ZWQgdGltZSAoMzAgbWludXRlcykuICBPcHRpb25hbC4gRGVmYXVsdCB2YWx1ZSBpcyBmYWxzZS4gXG4gICAgICovXG4gICAgcHVibGljIHNlYXJjaEdldChxOiBzdHJpbmcsIHN0YXJ0PzogbnVtYmVyLCBudW0/OiBudW1iZXIsIG1ldGFkYXRhVG9SZXR1cm4/OiBzdHJpbmcsIGFwcGVuZFJlcXVlc3RTZWNyZXQ/OiBib29sZWFuKTogUHJvbWlzZTxTZWFyY2hSZXNwb25zZT4ge1xuICAgICAgICBjb25zdCBsb2NhbFZhclBhdGggPSB0aGlzLmJhc2VQYXRoICsgJy9zZXJ2aWNlcy9zZWFyY2gnO1xuICAgICAgICBsZXQgcXVlcnlQYXJhbWV0ZXJzOiBhbnkgPSB7fTtcbiAgICAgICAgbGV0IGhlYWRlclBhcmFtczogYW55ID0gKDxhbnk+T2JqZWN0KS5hc3NpZ24oe30sIHRoaXMuZGVmYXVsdEhlYWRlcnMpO1xuICAgICAgICBsZXQgZm9ybVBhcmFtczogYW55ID0ge307XG5cblxuICAgICAgICAvLyB2ZXJpZnkgcmVxdWlyZWQgcGFyYW1ldGVyICdxJyBpcyBub3QgbnVsbCBvciB1bmRlZmluZWRcbiAgICAgICAgaWYgKHEgPT09IG51bGwgfHwgcSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlcXVpcmVkIHBhcmFtZXRlciBxIHdhcyBudWxsIG9yIHVuZGVmaW5lZCB3aGVuIGNhbGxpbmcgc2VhcmNoR2V0LicpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHEgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydxJ10gPSBxO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHN0YXJ0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1ldGVyc1snc3RhcnQnXSA9IHN0YXJ0O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG51bSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeVBhcmFtZXRlcnNbJ251bSddID0gbnVtO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG1ldGFkYXRhVG9SZXR1cm4gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydtZXRhZGF0YVRvUmV0dXJuJ10gPSBtZXRhZGF0YVRvUmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGFwcGVuZFJlcXVlc3RTZWNyZXQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydhcHBlbmRSZXF1ZXN0U2VjcmV0J10gPSBhcHBlbmRSZXF1ZXN0U2VjcmV0O1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IHVzZUZvcm1EYXRhID0gZmFsc2U7XG5cbiAgICAgICAgbGV0IHJlcXVlc3RPcHRpb25zOiByZXF1ZXN0Lk9wdGlvbnMgPSB7XG4gICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgICAgcXM6IHF1ZXJ5UGFyYW1ldGVycyxcbiAgICAgICAgICAgIGhlYWRlcnM6IGhlYWRlclBhcmFtcyxcbiAgICAgICAgICAgIHVyaTogbG9jYWxWYXJQYXRoLFxuICAgICAgICAgICAgdXNlUXVlcnlzdHJpbmc6IHRoaXMuX3VzZVF1ZXJ5c3RyaW5nLFxuICAgICAgICAgICAganNvbjogdHJ1ZSxcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9ucy5kZWZhdWx0LmFwcGx5VG9SZXF1ZXN0KHJlcXVlc3RPcHRpb25zKTtcblxuICAgICAgICBpZiAoT2JqZWN0LmtleXMoZm9ybVBhcmFtcykubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAodXNlRm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICAoPGFueT5yZXF1ZXN0T3B0aW9ucykuZm9ybURhdGEgPSBmb3JtUGFyYW1zO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5mb3JtID0gZm9ybVBhcmFtcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5lbHZpc1JlcXVlc3QucmVxdWVzdChyZXF1ZXN0T3B0aW9ucyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFNlYXJjaCBpbiBFbHZpc1xuICAgICAqIFxuICAgICAqIEBwYXJhbSBib2R5IEFzc2V0U2VhcmNoIG9iamVjdFxuICAgICAqL1xuICAgIHB1YmxpYyBzZWFyY2hQb3N0KGJvZHk6IEFzc2V0U2VhcmNoKTogUHJvbWlzZTxTZWFyY2hSZXNwb25zZT4ge1xuICAgICAgICBjb25zdCBsb2NhbFZhclBhdGggPSB0aGlzLmJhc2VQYXRoICsgJy9zZXJ2aWNlcy9zZWFyY2gnO1xuICAgICAgICBsZXQgcXVlcnlQYXJhbWV0ZXJzOiBhbnkgPSB7fTtcbiAgICAgICAgbGV0IGhlYWRlclBhcmFtczogYW55ID0gKDxhbnk+T2JqZWN0KS5hc3NpZ24oe30sIHRoaXMuZGVmYXVsdEhlYWRlcnMpO1xuICAgICAgICBsZXQgZm9ybVBhcmFtczogYW55ID0ge307XG5cblxuICAgICAgICAvLyB2ZXJpZnkgcmVxdWlyZWQgcGFyYW1ldGVyICdib2R5JyBpcyBub3QgbnVsbCBvciB1bmRlZmluZWRcbiAgICAgICAgaWYgKGJvZHkgPT09IG51bGwgfHwgYm9keSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlcXVpcmVkIHBhcmFtZXRlciBib2R5IHdhcyBudWxsIG9yIHVuZGVmaW5lZCB3aGVuIGNhbGxpbmcgc2VhcmNoUG9zdC4nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCB1c2VGb3JtRGF0YSA9IGZhbHNlO1xuXG4gICAgICAgIGxldCByZXF1ZXN0T3B0aW9uczogcmVxdWVzdC5PcHRpb25zID0ge1xuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICBxczogcXVlcnlQYXJhbWV0ZXJzLFxuICAgICAgICAgICAgaGVhZGVyczogaGVhZGVyUGFyYW1zLFxuICAgICAgICAgICAgdXJpOiBsb2NhbFZhclBhdGgsXG4gICAgICAgICAgICB1c2VRdWVyeXN0cmluZzogdGhpcy5fdXNlUXVlcnlzdHJpbmcsXG4gICAgICAgICAgICBqc29uOiB0cnVlLFxuICAgICAgICAgICAgYm9keTogYm9keSxcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9ucy5kZWZhdWx0LmFwcGx5VG9SZXF1ZXN0KHJlcXVlc3RPcHRpb25zKTtcblxuICAgICAgICBpZiAoT2JqZWN0LmtleXMoZm9ybVBhcmFtcykubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAodXNlRm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICAoPGFueT5yZXF1ZXN0T3B0aW9ucykuZm9ybURhdGEgPSBmb3JtUGFyYW1zO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5mb3JtID0gZm9ybVBhcmFtcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5lbHZpc1JlcXVlc3QucmVxdWVzdChyZXF1ZXN0T3B0aW9ucyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFVwZGF0ZXMgdGhlIGZpbGUgYW5kL29yIHRoZSBtZXRhZGF0YSBmb3IgYSBnaXZlbiBhc3NldC4gSXQmIzM5O3MgcmVxdWlyZWQgdG8gZWl0aGVyIHNwZWNpZnkgdGhlIEZpbGRhdGEgb3IgbWV0YWRhdGEgcGFyYW1ldGVyLlxuICAgICAqIFxuICAgICAqIEBwYXJhbSBpZCBJZCBvZiB0aGUgYXNzZXQgdG8gYmUgdXBkYXRlZC5cbiAgICAgKiBAcGFyYW0gbWV0YWRhdGEgQSBKU09OIGVuY29kZWQgb2JqZWN0IHdpdGggcHJvcGVydGllcyB0aGF0IG1hdGNoIEVsdmlzIG1ldGFkYXRhIGZpZWxkIG5hbWVzLiBUaGlzIG1ldGFkYXRhIHdpbGwgYmUgc2V0IG9uIHRoZSBhc3NldCBpbiBFbHZpcy4gICAmI3g2MDsmI3g2MDsmI3g2MDsgICAgICAgeyAgICAgICAgICAgXFwmcXVvdDt0YWdzXFwmcXVvdDs6IFtcXCZxdW90O2Zvb1xcJnF1b3Q7LCBcXCZxdW90O2JhclxcJnF1b3Q7XSwgICAgICAgICAgIFxcJnF1b3Q7ZGVzY3JpcHRpb25cXCZxdW90OzogXFwmcXVvdDtmb28gYmFyXFwmcXVvdDsgICAgICAgfSAgICYjeDYwOyYjeDYwOyYjeDYwOyBcbiAgICAgKiBAcGFyYW0gZmlsZWRhdGEgVGhlIGZpbGUgdG8gYmUgdXBkYXRlZCBpbiBFbHZpcy4gTm90ZSB0aGF0IGl0JiMzOTtzIG5vdCBtYW5kYXRvcnkgdG8gY2hlY2tvdXQgYSBmaWxlIGJlZm9yZSB1cGRhdGluZyBpdC4gIE9wdGlvbmFsLiBJZiBvbWl0dGVkLCBvbmx5IG1ldGFkYXRhIHdpbGwgYmUgdXBkYXRlZC4gXG4gICAgICogQHBhcmFtIG1ldGFkYXRhVG9SZXR1cm4gQ29tbWEtZGVsaW1pdGVkIGxpc3Qgb2YgbWV0YWRhdGEgZmllbGRzIHRvIHJldHVybi4gU3BlY2lmeSAmIzM5O2FsbCYjMzk7IHRvIHJldHVybiBhbGwgYXZhaWxhYmxlIG1ldGFkYXRhLiBPbWl0IHRvIHJldHVybiBhIG1pbmltYWwgbGlzdCBvZiBmaWVsZHMuICBPcHRpb25hbC4gSWYgb21pdHRlZCwgb25seSB0aGUgZmlsZSB3aWxsIGJlIHVwZGF0ZWQuIFxuICAgICAqIEBwYXJhbSBuZXh0VXJsIFdoZW4gc3BlY2lmaWVkLCB0aGUgc2VydmljZSB3aWxsIHNlbmQgYSAzMDEgcmVkaXJlY3QgdG8gdGhpcyBVUkwgd2hlbiBpdCBjb21wbGV0ZXMgc3VjY2Vzc2Z1bGx5LiBJZiB5b3UgcGxhY2UgJiMzOTske2lkfSYjMzk7IGluIHRoZSBVUkwsIGl0IHdpbGwgYmUgcmVwbGFjZWQgd2l0aCB0aGUgRWx2aXMgYXNzZXQgaWQgb2YgdGhlIHVwZGF0ZWQgYXNzZXQuIElmIG9taXR0ZWQsIGEgMjAwIE9LIHN0YXR1cyBjb2RlIHdpbGwgYmUgcmV0dXJuZWQuICBPcHRpb25hbC4gSWYgb21pdHRlZCwgdGhlIHVwZGF0ZWQgSGl0RWxlbWVudCB3aWxsIGJlIHJldHVybmVkLiBcbiAgICAgKiBAcGFyYW0gcGFyc2VNZXRhZGF0YU1vZGlmaWNhdGlvbnMgV2hlbiB1cGRhdGluZyBtZXRhZGF0YSwgRWx2aXMgcGFyc2VzIHBsdXMoKykgYW5kIG1pbnVzKC0pIHN5bWJvbHMgYXMgc3BlY2lhbCBjaGFyYWN0ZXJzIHRvIGFkZCBvciByZW1vdmUgdmFsdWVzIGZvciBtdWx0aS12YWx1ZSBmaWVsZHMuIEZvciBleGFtcGxlLCB0aGlzIHJlbW92ZXMgdGhlIHRhZyBcXCZxdW90O2Zvb1xcJnF1b3Q7IGFuZCBhZGRzIHRoZSB0YWcgXFwmcXVvdDtiYXJcXCZxdW90OzsgICAgJiN4NjA7JiN4NjA7JiN4NjA7ICAgICAgIHsgICAgICAgICAgIFxcJnF1b3Q7dGFnc1xcJnF1b3Q7OiBbXFwmcXVvdDstZm9vXFwmcXVvdDssIFxcJnF1b3Q7K2JhclxcJnF1b3Q7XSAgICAgICB9ICAgJiN4NjA7JiN4NjA7JiN4NjA7ICAgICAgU2V0IHRvIGZhbHNlIGlmIHlvdSB3YW50IHRvIGRpc2FibGUgdGhlIHBhcnNpbmcgYW5kIGluZGV4IHRoZSBleGFjdCBzcGVjaWZpZWQgbWV0YWRhdGEgdmFsdWUgaW5jbHVkaW5nIHRoZSArIG9yIC0gc3ltYm9sLiAgT3B0aW9uYWwuIERlZmF1bHQgdmFsdWUgaXMgdHJ1ZSBcbiAgICAgKiBAcGFyYW0gY2xlYXJDaGVja291dFN0YXRlIFdoZW4gdGhlIGZpbGUgaXMgY2hlY2tlZC1vdXQsIGNoZWNrIGluIHRoZSBmaWxlIGFzIHBhcnQgb2YgdXBkYXRpbmcgaXQuICBPcHRpb25hbC4gRGVmYXVsdCB2YWx1ZSBpcyBmYWxzZS4gXG4gICAgICogQHBhcmFtIGNyZWF0ZVZlcnNpb24gQ3JlYXRlIGEgZmlsZSB2ZXJzaW9uIHdoZW4gdGhlIGZpbGUgaXMgY2hlY2tlZCBpbi4gIE9wdGlvbmFsLiBEZWZhdWx0IHZhbHVlIGlzIHRydWUuIFxuICAgICAqIEBwYXJhbSBhdXRvUmVuYW1lIFdoZW4gc2V0IHRvIHRydWUsIHRoZSBmaWxlIGlzIGF1dG9tYXRpY2FsbHkgcmVuYW1lZCB3aGVuIGEgZmlsZSBhbHJlYWR5IGV4aXN0cyBvbiB0aGUgZGVzdGluYXRpb24gbG9jYXRpb24uIFdoZW4gc2V0IHRvIGZhbHNlLCBhbiBleGNwZXRpb24gaXMgdGhyb3duIHdoZW4gYSBmaWxlIGFscmVhZHkgZXhpc3RzLiAgQSBkZXN0aW5hdGlvbiBjaGFuZ2UgY2FuIGhhcHBlbiB3aGVuIGZvciBleGFtcGxlIHRoZSBuZXdseSB1cGRhdGVkIGZpbGUgaGFzIGEgZGlmZmVyZW50IGV4dGVuc2lvbiBvciB3aGVuIGEgbmV3IGZpbGVuYW1lIGlzIHNwZWNpZmllZCBpbiB0aGUgbWV0YWRhdGEuICBPcHRpb25hbC4gRGVmYXVsdCB2YWx1ZSBpcyB0cnVlLiBcbiAgICAgKiBAcGFyYW0gYXV0aGNyZWQgVXNlciBjcmVkZW50aWFscyBzcGVjaWZpZmllZCBhcyBhIGJhc2U2NCBlbmNvZGVkIHN0cmluZy4gVXNlIHRoaXMgYWx0ZXJuYXRpdmUgdHlwZSBvZiBhdXRoZW50aWNhdGlvbiBvbmx5IGZvciB0ZXN0aW5nIHB1cnBvc2VzIG9yIHdoZW4gdGhlIHVzZXIgY2FuIHB1YmxpY2x5IGFjY2VzcyBhc3NldHMuIFVzYWdlIGV4YW1wbGU7IGFzc3VtZSB0aGUgJiMzOTthZG1pbiYjMzk7IHVzZXIgd2l0aCBkZWZhdWx0IHBhc3N3b3JkICYjMzk7Y2hhbmdlbWVub3cmIzM5OywgdGhlIGF1dGhjcmVkIGlzIHRoZSBiYXNlNjQgZW5jb2RlZCBlcXVpdmFsZW50IG9mIGFkbWluOmNoYW5nZW1lbm93ICYjeDNEOyZndDsgWVdSdGFXNDZZMmhoYm1kbGJXVnViM2MmI3gzRDtcbiAgICAgKi9cbiAgICBwdWJsaWMgdXBkYXRlKGlkOiBzdHJpbmcsIG1ldGFkYXRhPzogc3RyaW5nLCBmaWxlZGF0YT86IEJ1ZmZlciwgbWV0YWRhdGFUb1JldHVybj86IHN0cmluZywgbmV4dFVybD86IHN0cmluZywgcGFyc2VNZXRhZGF0YU1vZGlmaWNhdGlvbnM/OiBib29sZWFuLCBjbGVhckNoZWNrb3V0U3RhdGU/OiBib29sZWFuLCBjcmVhdGVWZXJzaW9uPzogYm9vbGVhbiwgYXV0b1JlbmFtZT86IGJvb2xlYW4sIGF1dGhjcmVkPzogc3RyaW5nKTogUHJvbWlzZTxIaXRFbGVtZW50PiB7XG4gICAgICAgIGNvbnN0IGxvY2FsVmFyUGF0aCA9IHRoaXMuYmFzZVBhdGggKyAnL3NlcnZpY2VzL2Fzc2V0L3VwZGF0ZSc7XG4gICAgICAgIGxldCBxdWVyeVBhcmFtZXRlcnM6IGFueSA9IHt9O1xuICAgICAgICBsZXQgaGVhZGVyUGFyYW1zOiBhbnkgPSAoPGFueT5PYmplY3QpLmFzc2lnbih7fSwgdGhpcy5kZWZhdWx0SGVhZGVycyk7XG4gICAgICAgIGxldCBmb3JtUGFyYW1zOiBhbnkgPSB7fTtcblxuXG4gICAgICAgIC8vIHZlcmlmeSByZXF1aXJlZCBwYXJhbWV0ZXIgJ2lkJyBpcyBub3QgbnVsbCBvciB1bmRlZmluZWRcbiAgICAgICAgaWYgKGlkID09PSBudWxsIHx8IGlkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUmVxdWlyZWQgcGFyYW1ldGVyIGlkIHdhcyBudWxsIG9yIHVuZGVmaW5lZCB3aGVuIGNhbGxpbmcgdXBkYXRlLicpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1ldGVyc1snaWQnXSA9IGlkO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG1ldGFkYXRhICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1ldGVyc1snbWV0YWRhdGEnXSA9IG1ldGFkYXRhO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG1ldGFkYXRhVG9SZXR1cm4gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydtZXRhZGF0YVRvUmV0dXJuJ10gPSBtZXRhZGF0YVRvUmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG5leHRVcmwgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWyduZXh0VXJsJ10gPSBuZXh0VXJsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBhcnNlTWV0YWRhdGFNb2RpZmljYXRpb25zICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1ldGVyc1sncGFyc2VNZXRhZGF0YU1vZGlmaWNhdGlvbnMnXSA9IHBhcnNlTWV0YWRhdGFNb2RpZmljYXRpb25zO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNsZWFyQ2hlY2tvdXRTdGF0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeVBhcmFtZXRlcnNbJ2NsZWFyQ2hlY2tvdXRTdGF0ZSddID0gY2xlYXJDaGVja291dFN0YXRlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNyZWF0ZVZlcnNpb24gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydjcmVhdGVWZXJzaW9uJ10gPSBjcmVhdGVWZXJzaW9uO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGF1dG9SZW5hbWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXJzWydhdXRvUmVuYW1lJ10gPSBhdXRvUmVuYW1lO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGF1dGhjcmVkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1ldGVyc1snYXV0aGNyZWQnXSA9IGF1dGhjcmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IHVzZUZvcm1EYXRhID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKGZpbGVkYXRhICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGZvcm1QYXJhbXNbJ0ZpbGVkYXRhJ10gPSBmaWxlZGF0YTtcbiAgICAgICAgfVxuICAgICAgICB1c2VGb3JtRGF0YSA9IHRydWU7XG5cbiAgICAgICAgbGV0IHJlcXVlc3RPcHRpb25zOiByZXF1ZXN0Lk9wdGlvbnMgPSB7XG4gICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgIHFzOiBxdWVyeVBhcmFtZXRlcnMsXG4gICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJQYXJhbXMsXG4gICAgICAgICAgICB1cmk6IGxvY2FsVmFyUGF0aCxcbiAgICAgICAgICAgIHVzZVF1ZXJ5c3RyaW5nOiB0aGlzLl91c2VRdWVyeXN0cmluZyxcbiAgICAgICAgICAgIGpzb246IHRydWUsXG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvbnMuZGVmYXVsdC5hcHBseVRvUmVxdWVzdChyZXF1ZXN0T3B0aW9ucyk7XG5cbiAgICAgICAgaWYgKE9iamVjdC5rZXlzKGZvcm1QYXJhbXMpLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKHVzZUZvcm1EYXRhKSB7XG4gICAgICAgICAgICAgICAgKDxhbnk+cmVxdWVzdE9wdGlvbnMpLmZvcm1EYXRhID0gZm9ybVBhcmFtcztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVxdWVzdE9wdGlvbnMuZm9ybSA9IGZvcm1QYXJhbXM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuZWx2aXNSZXF1ZXN0LnJlcXVlc3QocmVxdWVzdE9wdGlvbnMpO1xuICAgIH1cbn1cbiJdfQ==