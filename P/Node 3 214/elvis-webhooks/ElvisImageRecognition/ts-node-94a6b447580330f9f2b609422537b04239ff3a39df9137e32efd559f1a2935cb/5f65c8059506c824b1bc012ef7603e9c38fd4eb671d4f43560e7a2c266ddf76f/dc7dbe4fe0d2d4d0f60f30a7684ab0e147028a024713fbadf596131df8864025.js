"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const translate = require("@google-cloud/translate");
const google_base_1 = require("./google-base");
const config_1 = require("../../config");
/**
 * Uses the Google Translate API to translate tags.
 */
class GoogleTranslate extends google_base_1.Google {
    constructor() {
        super();
        this.validateConfig().then(() => {
            if (!config_1.Config.languages) {
                throw new Error('The languages config property cannot be empty.');
            }
            if (!config_1.Config.sourceLanguage) {
                throw new Error('The sourceLanguage property cannot be empty.');
            }
            if (!config_1.Config.languageTagFields) {
                throw new Error('The languageTagFields property cannot be empty.');
            }
            this.languages = config_1.Config.languages.split(',');
            this.languageTagFields = config_1.Config.languageTagFields.split(',');
            if (this.languages.length != this.languageTagFields.length) {
                throw new Error('Invalid language translation configuration, the number of languages (' + this.languages.length + ') must be equal to the number of languageTagFields (' + this.languageTagFields.length + ').');
            }
            this.gt = translate({
                keyFilename: config_1.Config.googleKeyFilename
            });
        });
    }
    /**
     * Translate text into the configured languages
     *
     * @param text Text string to translate
     */
    translate(text) {
        let translations = [];
        let metadata = {};
        for (let i = 0; i < this.languages.length; i++) {
            if (this.languages[i] !== config_1.Config.sourceLanguage) {
                translations.push(this.translateTo(text, config_1.Config.sourceLanguage, this.languages[i]));
            }
        }
        return Promise.all(translations).then((responses) => {
            responses.forEach(response => {
                let metadataField = this.languageTagFields[this.languages.indexOf(response.language)];
                metadata[metadataField] = response.translation;
            });
            return metadata;
        });
    }
    translateTo(text, sourceLanguage, destinationLanguage) {
        let options = {
            from: sourceLanguage,
            to: destinationLanguage
        };
        return this.gt.translate(text, options).then((response) => {
            return {
                language: destinationLanguage,
                translation: response[0]
            };
        }).catch((error) => {
            throw new Error('An error occurred while translating tags: ' + error.message);
        });
    }
}
exports.GoogleTranslate = GoogleTranslate;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9zZXJ2aWNlL2dvb2dsZS10cmFuc2xhdGUudHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNJbWFnZVJlY29nbml0aW9uL3NyYy9hcHAvc2VydmljZS9nb29nbGUtdHJhbnNsYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscURBQXNEO0FBQ3RELCtDQUF1QztBQUN2Qyx5Q0FBc0M7QUFFdEM7O0dBRUc7QUFDSCxxQkFBNkIsU0FBUSxvQkFBTTtJQU96QztRQUNFLEtBQUssRUFBRSxDQUFDO1FBQ1IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDOUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO1lBQ3BFLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNLElBQUksS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDbEUsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDOUIsTUFBTSxJQUFJLEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO1lBQ3JFLENBQUM7WUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLGVBQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxlQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxNQUFNLElBQUksS0FBSyxDQUFDLHVFQUF1RSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLHNEQUFzRCxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDbk4sQ0FBQztZQUVELElBQUksQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUNsQixXQUFXLEVBQUUsZUFBTSxDQUFDLGlCQUFpQjthQUN0QyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksU0FBUyxDQUFDLElBQVk7UUFDM0IsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksUUFBUSxHQUFRLEVBQUUsQ0FBQztRQUV2QixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBVyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDdkQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxlQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxlQUFNLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLENBQUM7UUFDSCxDQUFDO1FBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBZ0IsRUFBRSxFQUFFO1lBQ3pELFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzNCLElBQUksYUFBYSxHQUFXLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDOUYsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDakQsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFdBQVcsQ0FBQyxJQUFZLEVBQUUsY0FBYyxFQUFFLG1CQUEyQjtRQUMzRSxJQUFJLE9BQU8sR0FBUTtZQUNqQixJQUFJLEVBQUUsY0FBYztZQUNwQixFQUFFLEVBQUUsbUJBQW1CO1NBQ3hCLENBQUM7UUFFRixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzdELE1BQU0sQ0FBQztnQkFDTCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUN6QixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDakIsTUFBTSxJQUFJLEtBQUssQ0FBQyw0Q0FBNEMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUF2RUQsMENBdUVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHRyYW5zbGF0ZSA9IHJlcXVpcmUoJ0Bnb29nbGUtY2xvdWQvdHJhbnNsYXRlJyk7XG5pbXBvcnQgeyBHb29nbGUgfSBmcm9tICcuL2dvb2dsZS1iYXNlJztcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICogVXNlcyB0aGUgR29vZ2xlIFRyYW5zbGF0ZSBBUEkgdG8gdHJhbnNsYXRlIHRhZ3MuXG4gKi9cbmV4cG9ydCBjbGFzcyBHb29nbGVUcmFuc2xhdGUgZXh0ZW5kcyBHb29nbGUge1xuXG4gIHByaXZhdGUgZ3Q7XG5cbiAgcHJpdmF0ZSBsYW5ndWFnZXM6IHN0cmluZ1tdO1xuICBwcml2YXRlIGxhbmd1YWdlVGFnRmllbGRzOiBzdHJpbmdbXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMudmFsaWRhdGVDb25maWcoKS50aGVuKCgpID0+IHtcbiAgICAgIGlmICghQ29uZmlnLmxhbmd1YWdlcykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBsYW5ndWFnZXMgY29uZmlnIHByb3BlcnR5IGNhbm5vdCBiZSBlbXB0eS4nKTtcbiAgICAgIH1cbiAgICAgIGlmICghQ29uZmlnLnNvdXJjZUxhbmd1YWdlKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHNvdXJjZUxhbmd1YWdlIHByb3BlcnR5IGNhbm5vdCBiZSBlbXB0eS4nKTtcbiAgICAgIH1cbiAgICAgIGlmICghQ29uZmlnLmxhbmd1YWdlVGFnRmllbGRzKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIGxhbmd1YWdlVGFnRmllbGRzIHByb3BlcnR5IGNhbm5vdCBiZSBlbXB0eS4nKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5sYW5ndWFnZXMgPSBDb25maWcubGFuZ3VhZ2VzLnNwbGl0KCcsJyk7XG4gICAgICB0aGlzLmxhbmd1YWdlVGFnRmllbGRzID0gQ29uZmlnLmxhbmd1YWdlVGFnRmllbGRzLnNwbGl0KCcsJyk7XG5cbiAgICAgIGlmICh0aGlzLmxhbmd1YWdlcy5sZW5ndGggIT0gdGhpcy5sYW5ndWFnZVRhZ0ZpZWxkcy5sZW5ndGgpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGxhbmd1YWdlIHRyYW5zbGF0aW9uIGNvbmZpZ3VyYXRpb24sIHRoZSBudW1iZXIgb2YgbGFuZ3VhZ2VzICgnICsgdGhpcy5sYW5ndWFnZXMubGVuZ3RoICsgJykgbXVzdCBiZSBlcXVhbCB0byB0aGUgbnVtYmVyIG9mIGxhbmd1YWdlVGFnRmllbGRzICgnICsgdGhpcy5sYW5ndWFnZVRhZ0ZpZWxkcy5sZW5ndGggKyAnKS4nKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5ndCA9IHRyYW5zbGF0ZSh7XG4gICAgICAgIGtleUZpbGVuYW1lOiBDb25maWcuZ29vZ2xlS2V5RmlsZW5hbWVcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFRyYW5zbGF0ZSB0ZXh0IGludG8gdGhlIGNvbmZpZ3VyZWQgbGFuZ3VhZ2VzXG4gICAqIFxuICAgKiBAcGFyYW0gdGV4dCBUZXh0IHN0cmluZyB0byB0cmFuc2xhdGVcbiAgICovXG4gIHB1YmxpYyB0cmFuc2xhdGUodGV4dDogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgdHJhbnNsYXRpb25zID0gW107XG4gICAgbGV0IG1ldGFkYXRhOiBhbnkgPSB7fTtcblxuICAgIGZvciAobGV0IGk6IG51bWJlciA9IDA7IGkgPCB0aGlzLmxhbmd1YWdlcy5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKHRoaXMubGFuZ3VhZ2VzW2ldICE9PSBDb25maWcuc291cmNlTGFuZ3VhZ2UpIHtcbiAgICAgICAgdHJhbnNsYXRpb25zLnB1c2godGhpcy50cmFuc2xhdGVUbyh0ZXh0LCBDb25maWcuc291cmNlTGFuZ3VhZ2UsIHRoaXMubGFuZ3VhZ2VzW2ldKSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBQcm9taXNlLmFsbCh0cmFuc2xhdGlvbnMpLnRoZW4oKHJlc3BvbnNlczogYW55W10pID0+IHtcbiAgICAgIHJlc3BvbnNlcy5mb3JFYWNoKHJlc3BvbnNlID0+IHtcbiAgICAgICAgbGV0IG1ldGFkYXRhRmllbGQ6IHN0cmluZyA9IHRoaXMubGFuZ3VhZ2VUYWdGaWVsZHNbdGhpcy5sYW5ndWFnZXMuaW5kZXhPZihyZXNwb25zZS5sYW5ndWFnZSldO1xuICAgICAgICBtZXRhZGF0YVttZXRhZGF0YUZpZWxkXSA9IHJlc3BvbnNlLnRyYW5zbGF0aW9uO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gbWV0YWRhdGE7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHRyYW5zbGF0ZVRvKHRleHQ6IHN0cmluZywgc291cmNlTGFuZ3VhZ2UsIGRlc3RpbmF0aW9uTGFuZ3VhZ2U6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IG9wdGlvbnM6IGFueSA9IHtcbiAgICAgIGZyb206IHNvdXJjZUxhbmd1YWdlLFxuICAgICAgdG86IGRlc3RpbmF0aW9uTGFuZ3VhZ2VcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuZ3QudHJhbnNsYXRlKHRleHQsIG9wdGlvbnMpLnRoZW4oKHJlc3BvbnNlOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxhbmd1YWdlOiBkZXN0aW5hdGlvbkxhbmd1YWdlLFxuICAgICAgICB0cmFuc2xhdGlvbjogcmVzcG9uc2VbMF1cbiAgICAgIH07XG4gICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIHRyYW5zbGF0aW5nIHRhZ3M6ICcgKyBlcnJvci5tZXNzYWdlKTtcbiAgICB9KTtcbiAgfVxufVxuIl19