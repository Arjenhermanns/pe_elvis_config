"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const config_1 = require("../../config");
class Google {
    validateConfig() {
        return new Promise((resolve, reject) => {
            fs.exists(config_1.Config.googleKeyFilename, exists => {
                // We have to check ourselves as Google throws weird errors when there's no valid path specified.
                // For example when people configure an API key instead of a path to a key file.
                if (!exists) {
                    return reject(Error('The file specified in googleKeyFilename doesn\'t exists: "' + config_1.Config.googleKeyFilename + '". Please configure the correct full file path to the Google Service account keyfile.'));
                }
                resolve('');
            });
        });
    }
}
exports.Google = Google;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9zZXJ2aWNlL2dvb2dsZS1iYXNlLnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3NlcnZpY2UvZ29vZ2xlLWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5QkFBMEI7QUFDMUIseUNBQXNDO0FBRXRDO0lBRVksY0FBYztRQUN0QixNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDckMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxlQUFNLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLEVBQUU7Z0JBQzNDLGlHQUFpRztnQkFDakcsZ0ZBQWdGO2dCQUNoRixFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ1osTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsNERBQTRELEdBQUcsZUFBTSxDQUFDLGlCQUFpQixHQUFHLHVGQUF1RixDQUFDLENBQUMsQ0FBQztnQkFDMU0sQ0FBQztnQkFDRCxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUVGO0FBZkQsd0JBZUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZnMgPSByZXF1aXJlKCdmcycpO1xuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEdvb2dsZSB7XG5cbiAgcHJvdGVjdGVkIHZhbGlkYXRlQ29uZmlnKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGZzLmV4aXN0cyhDb25maWcuZ29vZ2xlS2V5RmlsZW5hbWUsIGV4aXN0cyA9PiB7XG4gICAgICAgIC8vIFdlIGhhdmUgdG8gY2hlY2sgb3Vyc2VsdmVzIGFzIEdvb2dsZSB0aHJvd3Mgd2VpcmQgZXJyb3JzIHdoZW4gdGhlcmUncyBubyB2YWxpZCBwYXRoIHNwZWNpZmllZC5cbiAgICAgICAgLy8gRm9yIGV4YW1wbGUgd2hlbiBwZW9wbGUgY29uZmlndXJlIGFuIEFQSSBrZXkgaW5zdGVhZCBvZiBhIHBhdGggdG8gYSBrZXkgZmlsZS5cbiAgICAgICAgaWYgKCFleGlzdHMpIHtcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KEVycm9yKCdUaGUgZmlsZSBzcGVjaWZpZWQgaW4gZ29vZ2xlS2V5RmlsZW5hbWUgZG9lc25cXCd0IGV4aXN0czogXCInICsgQ29uZmlnLmdvb2dsZUtleUZpbGVuYW1lICsgJ1wiLiBQbGVhc2UgY29uZmlndXJlIHRoZSBjb3JyZWN0IGZ1bGwgZmlsZSBwYXRoIHRvIHRoZSBHb29nbGUgU2VydmljZSBhY2NvdW50IGtleWZpbGUuJykpO1xuICAgICAgICB9XG4gICAgICAgIHJlc29sdmUoJycpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxufVxuXG4iXX0=