"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const api_manager_1 = require("../elvis-api/api-manager");
const recognizer_1 = require("./recognizer");
const config_1 = require("../config");
const uuidV4 = require("uuid/v4");
class RecognizeApi {
    constructor(app) {
        this.app = app;
        this.api = api_manager_1.ApiManager.getApi();
        this.processes = [];
        let translateEnabled = config_1.Config.languages !== '';
        this.recognizer = new recognizer_1.Recognizer(config_1.Config.clarifaiEnabled, config_1.Config.googleEnabled, config_1.Config.awsEnabled, translateEnabled);
    }
    /**
     * Add API routes for recognition API's
     */
    addRoutes() {
        let router = express_1.Router();
        // API logging
        router.use((req, res, next) => {
            // Keep the compiler happy
            res = res;
            console.info('API call received: ' + req.method + ' "' + req.originalUrl + '" with body: ' + JSON.stringify(req.body));
            next();
        });
        // Recognize API
        router.post('/recognize', (req, res) => {
            // Validate parameters
            if (!req.body) {
                return this.handleError('Invalid request, no parameters specified.', req, res);
            }
            let rr = req.body;
            if (!rr.q) {
                return this.handleError('Invalid request, parameter "q" is required.', req, res);
            }
            let query = '(' + rr.q + ') AND assetDomain:image';
            let search = {
                sorting: [
                    {
                        field: 'assetCreated',
                        descending: true
                    }
                ],
                query: {
                    QueryStringQuery: {
                        queryString: query
                    }
                }
            };
            let pi = new ProcessInfo(rr, search);
            this.processes[pi.id] = pi;
            // Return 202 "ACCEPTED" status, client needs to monitor progress using the process id.
            res.status(202).json({ processId: pi.id });
            // Start recognition process
            this.recognizeBatch(pi);
        });
        // Get recognize process info by id
        router.get('/recognize/:id', (req, res) => {
            let pi = this.retrieveProcessInfo(req, res);
            if (!pi) {
                res.status(404);
            }
            else {
                // Return process info
                res.status(200).json(pi);
            }
        });
        // Cancel recognize process for a specified id
        router.delete('/recognize/:id', (req, res) => {
            let pi = this.retrieveProcessInfo(req, res);
            if (!pi) {
                res.status(404);
            }
            else {
                pi.cancelled = true;
                // Return process info
                res.status(200).send('Process with id "' + pi.id + '" is being cancelled.');
            }
        });
        // Prefix all API's with /api
        this.app.use('/api', router);
    }
    retrieveProcessInfo(req, res) {
        // Validate parameters
        let id = req.params.id;
        if (!req.params.id) {
            this.handleError('Invalid request, parameter "id" is required.', req, res);
            return null;
        }
        // Get process from list
        let pi = this.processes[id];
        if (!pi) {
            this.handleError('Process with id "' + id + '" doesn\'t exists.', req, res, 404);
            return null;
        }
        return pi;
    }
    /**
     * Recognize images in batches
     */
    recognizeBatch(pi, startIndex = 0, batchSize = 5) {
        pi.search.firstResult = startIndex;
        pi.search.maxResultHits = batchSize;
        let processedInBatch = 0;
        let lastBatch = false;
        // Search batch
        this.api.searchPost(pi.search).then((sr) => {
            lastBatch = sr.hits.length < batchSize;
            console.info('Processing in-progress: ' + pi.id + ', total hits: ' + sr.totalHits + ', startIndex: ' + startIndex + ', batchSize: ' + batchSize + ', hits found: ' + sr.hits.length + ', query: ' + JSON.stringify(pi.search.query));
            sr.hits.forEach((hit) => {
                // Start image recognition
                this.recognizer.recognize(hit.id, pi.recognizeRequest.models, hit.metadata['assetPath']).then(() => {
                    // Recognition successful
                    pi.successCount++;
                    processedInBatch++;
                    this.nextBatchHandler(pi, startIndex, batchSize, processedInBatch, sr.hits.length, lastBatch);
                }).catch(() => {
                    // Recognition failed
                    pi.failedCount++;
                    processedInBatch++;
                    this.nextBatchHandler(pi, startIndex, batchSize, processedInBatch, sr.hits.length, lastBatch);
                });
            });
        }).catch((error) => {
            // Search failed, we're done here...
            // TODO: set correct failed count here or do the first search before sending the ACCEPTED response
            console.error('Search failed for query: ' + pi.search + '. Error details: ' + error.stack);
        });
    }
    /**
     * Determine if we're done or if a new batch needs to be processed
     */
    nextBatchHandler(pi, startIndex, batchSize, processedInBatch, hitsFound, lastBatch) {
        if (pi.cancelled && processedInBatch == hitsFound) {
            // We're cancelled :-(
            console.info('Processing cancelled: ' + pi.id + ', successCount: ' + pi.successCount + ', failedCount: ' + pi.failedCount);
        }
        else if (lastBatch && processedInBatch == hitsFound) {
            // We're done!
            console.info('Processing completed: ' + pi.id + ', successCount: ' + pi.successCount + ', failedCount: ' + pi.failedCount);
        }
        else if (!lastBatch && processedInBatch == batchSize) {
            // There's more...
            this.recognizeBatch(pi, startIndex + batchSize, batchSize);
        }
    }
    handleError(message, req, res, statusCode = 500) {
        console.error('API call failed: ' + req.method + ' "' + req.originalUrl + '" with body: ' + JSON.stringify(req.body) + ' Error: ' + message);
        res.status(statusCode).send(message);
    }
}
exports.RecognizeApi = RecognizeApi;
class ProcessInfo {
    constructor(recognizeRequest, search) {
        this.cancelled = false;
        this.failedCount = 0;
        this.successCount = 0;
        this.id = uuidV4();
        this.search = search;
        this.recognizeRequest = recognizeRequest;
    }
}
class RecognizeRequest {
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC9yZWNvZ25pemUtYXBpLnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3JlY29nbml6ZS1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBOEM7QUFHOUMsMERBQXNEO0FBQ3RELDZDQUEwQztBQUMxQyxzQ0FBbUM7QUFDbkMsa0NBQW1DO0FBRW5DO0lBTUUsWUFBbUIsR0FBZ0I7UUFBaEIsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUozQixRQUFHLEdBQWEsd0JBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVwQyxjQUFTLEdBQWtCLEVBQUUsQ0FBQztRQUdwQyxJQUFJLGdCQUFnQixHQUFZLGVBQU0sQ0FBQyxTQUFTLEtBQUssRUFBRSxDQUFDO1FBQ3hELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSx1QkFBVSxDQUFDLGVBQU0sQ0FBQyxlQUFlLEVBQUUsZUFBTSxDQUFDLGFBQWEsRUFBRSxlQUFNLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDdEgsQ0FBQztJQUVEOztPQUVHO0lBQ0ksU0FBUztRQUVkLElBQUksTUFBTSxHQUFXLGdCQUFNLEVBQUUsQ0FBQztRQUU5QixjQUFjO1FBQ2QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUU7WUFDNUIsMEJBQTBCO1lBQzFCLEdBQUcsR0FBRyxHQUFHLENBQUM7WUFDVixPQUFPLENBQUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxXQUFXLEdBQUcsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdkgsSUFBSSxFQUFFLENBQUM7UUFDVCxDQUFDLENBQUMsQ0FBQztRQUVILGdCQUFnQjtRQUNoQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUNyQyxzQkFBc0I7WUFDdEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQywyQ0FBMkMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDakYsQ0FBQztZQUNELElBQUksRUFBRSxHQUFxQixHQUFHLENBQUMsSUFBSSxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsNkNBQTZDLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ25GLENBQUM7WUFFRCxJQUFJLEtBQUssR0FBVyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsR0FBRyx5QkFBeUIsQ0FBQztZQUUzRCxJQUFJLE1BQU0sR0FBZ0I7Z0JBQ3hCLE9BQU8sRUFBRTtvQkFDUDt3QkFDRSxLQUFLLEVBQUUsY0FBYzt3QkFDckIsVUFBVSxFQUFFLElBQUk7cUJBQ2pCO2lCQUNGO2dCQUNELEtBQUssRUFBRTtvQkFDTCxnQkFBZ0IsRUFBRTt3QkFDaEIsV0FBVyxFQUFFLEtBQUs7cUJBQ25CO2lCQUNGO2FBQ0YsQ0FBQztZQUVGLElBQUksRUFBRSxHQUFnQixJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBRTNCLHVGQUF1RjtZQUN2RixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUUzQyw0QkFBNEI7WUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztRQUVILG1DQUFtQztRQUNuQyxNQUFNLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQ3hDLElBQUksRUFBRSxHQUFnQixJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3pELEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDUixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDSixzQkFBc0I7Z0JBQ3RCLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILDhDQUE4QztRQUM5QyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQzNDLElBQUksRUFBRSxHQUFnQixJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3pELEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDUixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDSixFQUFFLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFFcEIsc0JBQXNCO2dCQUN0QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLHVCQUF1QixDQUFDLENBQUM7WUFDOUUsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsNkJBQTZCO1FBQzdCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRU8sbUJBQW1CLENBQUMsR0FBRyxFQUFFLEdBQUc7UUFDbEMsc0JBQXNCO1FBQ3RCLElBQUksRUFBRSxHQUFXLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxXQUFXLENBQUMsOENBQThDLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzNFLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBRUQsd0JBQXdCO1FBQ3hCLElBQUksRUFBRSxHQUFnQixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRXpDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNSLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxHQUFHLG9CQUFvQixFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDakYsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNkLENBQUM7UUFFRCxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVEOztPQUVHO0lBQ0ssY0FBYyxDQUFDLEVBQWUsRUFBRSxhQUFxQixDQUFDLEVBQUUsWUFBb0IsQ0FBQztRQUNuRixFQUFFLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFDbkMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1FBRXBDLElBQUksZ0JBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBQ2pDLElBQUksU0FBUyxHQUFZLEtBQUssQ0FBQztRQUUvQixlQUFlO1FBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQWtCLEVBQUUsRUFBRTtZQUN6RCxTQUFTLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxnQkFBZ0IsR0FBRyxFQUFFLENBQUMsU0FBUyxHQUFHLGdCQUFnQixHQUFHLFVBQVUsR0FBRyxlQUFlLEdBQUcsU0FBUyxHQUFHLGdCQUFnQixHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyTyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUV0QiwwQkFBMEI7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtvQkFDakcseUJBQXlCO29CQUN6QixFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ2xCLGdCQUFnQixFQUFFLENBQUM7b0JBQ25CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDaEcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRTtvQkFDWixxQkFBcUI7b0JBQ3JCLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDakIsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUNoRyxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDdEIsb0NBQW9DO1lBQ3BDLGtHQUFrRztZQUNsRyxPQUFPLENBQUMsS0FBSyxDQUFDLDJCQUEyQixHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsbUJBQW1CLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ssZ0JBQWdCLENBQUMsRUFBZSxFQUFFLFVBQWtCLEVBQUUsU0FBaUIsRUFBRSxnQkFBd0IsRUFBRSxTQUFpQixFQUFFLFNBQWtCO1FBQzlJLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLElBQUksZ0JBQWdCLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNsRCxzQkFBc0I7WUFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLGtCQUFrQixHQUFHLEVBQUUsQ0FBQyxZQUFZLEdBQUcsaUJBQWlCLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzdILENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDcEQsY0FBYztZQUNkLE9BQU8sQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxrQkFBa0IsR0FBRyxFQUFFLENBQUMsWUFBWSxHQUFHLGlCQUFpQixHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM3SCxDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDckQsa0JBQWtCO1lBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLFVBQVUsR0FBRyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDN0QsQ0FBQztJQUNILENBQUM7SUFFTyxXQUFXLENBQUMsT0FBZSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsYUFBcUIsR0FBRztRQUNyRSxPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxXQUFXLEdBQUcsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLFVBQVUsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUM3SSxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QyxDQUFDO0NBQ0Y7QUExS0Qsb0NBMEtDO0FBRUQ7SUFVRSxZQUFZLGdCQUFrQyxFQUFFLE1BQW1CO1FBTDVELGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFDeEIsaUJBQVksR0FBVyxDQUFDLENBQUM7UUFJOUIsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDM0MsQ0FBQztDQUNGO0FBRUQ7Q0FJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlciwgQXBwbGljYXRpb24gfSBmcm9tICdleHByZXNzJztcblxuaW1wb3J0IHsgRWx2aXNBcGksIEFzc2V0U2VhcmNoLCBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uL2VsdmlzLWFwaS9hcGknO1xuaW1wb3J0IHsgQXBpTWFuYWdlciB9IGZyb20gJy4uL2VsdmlzLWFwaS9hcGktbWFuYWdlcic7XG5pbXBvcnQgeyBSZWNvZ25pemVyIH0gZnJvbSAnLi9yZWNvZ25pemVyJztcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uL2NvbmZpZyc7XG5pbXBvcnQgdXVpZFY0ID0gcmVxdWlyZSgndXVpZC92NCcpO1xuXG5leHBvcnQgY2xhc3MgUmVjb2duaXplQXBpIHtcblxuICBwcml2YXRlIGFwaTogRWx2aXNBcGkgPSBBcGlNYW5hZ2VyLmdldEFwaSgpO1xuICBwcml2YXRlIHJlY29nbml6ZXI6IFJlY29nbml6ZXI7XG4gIHByaXZhdGUgcHJvY2Vzc2VzOiBQcm9jZXNzSW5mb1tdID0gW107XG5cbiAgY29uc3RydWN0b3IocHVibGljIGFwcDogQXBwbGljYXRpb24pIHtcbiAgICBsZXQgdHJhbnNsYXRlRW5hYmxlZDogYm9vbGVhbiA9IENvbmZpZy5sYW5ndWFnZXMgIT09ICcnO1xuICAgIHRoaXMucmVjb2duaXplciA9IG5ldyBSZWNvZ25pemVyKENvbmZpZy5jbGFyaWZhaUVuYWJsZWQsIENvbmZpZy5nb29nbGVFbmFibGVkLCBDb25maWcuYXdzRW5hYmxlZCwgdHJhbnNsYXRlRW5hYmxlZCk7XG4gIH1cblxuICAvKipcbiAgICogQWRkIEFQSSByb3V0ZXMgZm9yIHJlY29nbml0aW9uIEFQSSdzXG4gICAqL1xuICBwdWJsaWMgYWRkUm91dGVzKCk6IHZvaWQge1xuXG4gICAgbGV0IHJvdXRlcjogUm91dGVyID0gUm91dGVyKCk7XG5cbiAgICAvLyBBUEkgbG9nZ2luZ1xuICAgIHJvdXRlci51c2UoKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG4gICAgICAvLyBLZWVwIHRoZSBjb21waWxlciBoYXBweVxuICAgICAgcmVzID0gcmVzO1xuICAgICAgY29uc29sZS5pbmZvKCdBUEkgY2FsbCByZWNlaXZlZDogJyArIHJlcS5tZXRob2QgKyAnIFwiJyArIHJlcS5vcmlnaW5hbFVybCArICdcIiB3aXRoIGJvZHk6ICcgKyBKU09OLnN0cmluZ2lmeShyZXEuYm9keSkpO1xuICAgICAgbmV4dCgpO1xuICAgIH0pO1xuXG4gICAgLy8gUmVjb2duaXplIEFQSVxuICAgIHJvdXRlci5wb3N0KCcvcmVjb2duaXplJywgKHJlcSwgcmVzKSA9PiB7XG4gICAgICAvLyBWYWxpZGF0ZSBwYXJhbWV0ZXJzXG4gICAgICBpZiAoIXJlcS5ib2R5KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmRsZUVycm9yKCdJbnZhbGlkIHJlcXVlc3QsIG5vIHBhcmFtZXRlcnMgc3BlY2lmaWVkLicsIHJlcSwgcmVzKTtcbiAgICAgIH1cbiAgICAgIGxldCBycjogUmVjb2duaXplUmVxdWVzdCA9IHJlcS5ib2R5O1xuICAgICAgaWYgKCFyci5xKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhbmRsZUVycm9yKCdJbnZhbGlkIHJlcXVlc3QsIHBhcmFtZXRlciBcInFcIiBpcyByZXF1aXJlZC4nLCByZXEsIHJlcyk7XG4gICAgICB9XG5cbiAgICAgIGxldCBxdWVyeTogc3RyaW5nID0gJygnICsgcnIucSArICcpIEFORCBhc3NldERvbWFpbjppbWFnZSc7XG5cbiAgICAgIGxldCBzZWFyY2g6IEFzc2V0U2VhcmNoID0ge1xuICAgICAgICBzb3J0aW5nOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgZmllbGQ6ICdhc3NldENyZWF0ZWQnLFxuICAgICAgICAgICAgZGVzY2VuZGluZzogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgcXVlcnk6IHtcbiAgICAgICAgICBRdWVyeVN0cmluZ1F1ZXJ5OiB7XG4gICAgICAgICAgICBxdWVyeVN0cmluZzogcXVlcnlcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGxldCBwaTogUHJvY2Vzc0luZm8gPSBuZXcgUHJvY2Vzc0luZm8ocnIsIHNlYXJjaCk7XG4gICAgICB0aGlzLnByb2Nlc3Nlc1twaS5pZF0gPSBwaTtcblxuICAgICAgLy8gUmV0dXJuIDIwMiBcIkFDQ0VQVEVEXCIgc3RhdHVzLCBjbGllbnQgbmVlZHMgdG8gbW9uaXRvciBwcm9ncmVzcyB1c2luZyB0aGUgcHJvY2VzcyBpZC5cbiAgICAgIHJlcy5zdGF0dXMoMjAyKS5qc29uKHsgcHJvY2Vzc0lkOiBwaS5pZCB9KTtcblxuICAgICAgLy8gU3RhcnQgcmVjb2duaXRpb24gcHJvY2Vzc1xuICAgICAgdGhpcy5yZWNvZ25pemVCYXRjaChwaSk7XG4gICAgfSk7XG5cbiAgICAvLyBHZXQgcmVjb2duaXplIHByb2Nlc3MgaW5mbyBieSBpZFxuICAgIHJvdXRlci5nZXQoJy9yZWNvZ25pemUvOmlkJywgKHJlcSwgcmVzKSA9PiB7XG4gICAgICBsZXQgcGk6IFByb2Nlc3NJbmZvID0gdGhpcy5yZXRyaWV2ZVByb2Nlc3NJbmZvKHJlcSwgcmVzKTtcbiAgICAgIGlmICghcGkpIHtcbiAgICAgICAgcmVzLnN0YXR1cyg0MDQpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIC8vIFJldHVybiBwcm9jZXNzIGluZm9cbiAgICAgICAgcmVzLnN0YXR1cygyMDApLmpzb24ocGkpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy8gQ2FuY2VsIHJlY29nbml6ZSBwcm9jZXNzIGZvciBhIHNwZWNpZmllZCBpZFxuICAgIHJvdXRlci5kZWxldGUoJy9yZWNvZ25pemUvOmlkJywgKHJlcSwgcmVzKSA9PiB7XG4gICAgICBsZXQgcGk6IFByb2Nlc3NJbmZvID0gdGhpcy5yZXRyaWV2ZVByb2Nlc3NJbmZvKHJlcSwgcmVzKTtcbiAgICAgIGlmICghcGkpIHtcbiAgICAgICAgcmVzLnN0YXR1cyg0MDQpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHBpLmNhbmNlbGxlZCA9IHRydWU7XG5cbiAgICAgICAgLy8gUmV0dXJuIHByb2Nlc3MgaW5mb1xuICAgICAgICByZXMuc3RhdHVzKDIwMCkuc2VuZCgnUHJvY2VzcyB3aXRoIGlkIFwiJyArIHBpLmlkICsgJ1wiIGlzIGJlaW5nIGNhbmNlbGxlZC4nKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIFByZWZpeCBhbGwgQVBJJ3Mgd2l0aCAvYXBpXG4gICAgdGhpcy5hcHAudXNlKCcvYXBpJywgcm91dGVyKTtcbiAgfVxuXG4gIHByaXZhdGUgcmV0cmlldmVQcm9jZXNzSW5mbyhyZXEsIHJlcyk6IFByb2Nlc3NJbmZvIHtcbiAgICAvLyBWYWxpZGF0ZSBwYXJhbWV0ZXJzXG4gICAgbGV0IGlkOiBudW1iZXIgPSByZXEucGFyYW1zLmlkO1xuICAgIGlmICghcmVxLnBhcmFtcy5pZCkge1xuICAgICAgdGhpcy5oYW5kbGVFcnJvcignSW52YWxpZCByZXF1ZXN0LCBwYXJhbWV0ZXIgXCJpZFwiIGlzIHJlcXVpcmVkLicsIHJlcSwgcmVzKTtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIC8vIEdldCBwcm9jZXNzIGZyb20gbGlzdFxuICAgIGxldCBwaTogUHJvY2Vzc0luZm8gPSB0aGlzLnByb2Nlc3Nlc1tpZF07XG5cbiAgICBpZiAoIXBpKSB7XG4gICAgICB0aGlzLmhhbmRsZUVycm9yKCdQcm9jZXNzIHdpdGggaWQgXCInICsgaWQgKyAnXCIgZG9lc25cXCd0IGV4aXN0cy4nLCByZXEsIHJlcywgNDA0KTtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBwaTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZWNvZ25pemUgaW1hZ2VzIGluIGJhdGNoZXNcbiAgICovXG4gIHByaXZhdGUgcmVjb2duaXplQmF0Y2gocGk6IFByb2Nlc3NJbmZvLCBzdGFydEluZGV4OiBudW1iZXIgPSAwLCBiYXRjaFNpemU6IG51bWJlciA9IDUpOiB2b2lkIHtcbiAgICBwaS5zZWFyY2guZmlyc3RSZXN1bHQgPSBzdGFydEluZGV4O1xuICAgIHBpLnNlYXJjaC5tYXhSZXN1bHRIaXRzID0gYmF0Y2hTaXplO1xuXG4gICAgbGV0IHByb2Nlc3NlZEluQmF0Y2g6IG51bWJlciA9IDA7XG4gICAgbGV0IGxhc3RCYXRjaDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLy8gU2VhcmNoIGJhdGNoXG4gICAgdGhpcy5hcGkuc2VhcmNoUG9zdChwaS5zZWFyY2gpLnRoZW4oKHNyOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xuICAgICAgbGFzdEJhdGNoID0gc3IuaGl0cy5sZW5ndGggPCBiYXRjaFNpemU7XG4gICAgICBjb25zb2xlLmluZm8oJ1Byb2Nlc3NpbmcgaW4tcHJvZ3Jlc3M6ICcgKyBwaS5pZCArICcsIHRvdGFsIGhpdHM6ICcgKyBzci50b3RhbEhpdHMgKyAnLCBzdGFydEluZGV4OiAnICsgc3RhcnRJbmRleCArICcsIGJhdGNoU2l6ZTogJyArIGJhdGNoU2l6ZSArICcsIGhpdHMgZm91bmQ6ICcgKyBzci5oaXRzLmxlbmd0aCArICcsIHF1ZXJ5OiAnICsgSlNPTi5zdHJpbmdpZnkocGkuc2VhcmNoLnF1ZXJ5KSk7XG4gICAgICBzci5oaXRzLmZvckVhY2goKGhpdCkgPT4ge1xuXG4gICAgICAgIC8vIFN0YXJ0IGltYWdlIHJlY29nbml0aW9uXG4gICAgICAgIHRoaXMucmVjb2duaXplci5yZWNvZ25pemUoaGl0LmlkLCBwaS5yZWNvZ25pemVSZXF1ZXN0Lm1vZGVscywgaGl0Lm1ldGFkYXRhWydhc3NldFBhdGgnXSkudGhlbigoKSA9PiB7XG4gICAgICAgICAgLy8gUmVjb2duaXRpb24gc3VjY2Vzc2Z1bFxuICAgICAgICAgIHBpLnN1Y2Nlc3NDb3VudCsrO1xuICAgICAgICAgIHByb2Nlc3NlZEluQmF0Y2grKztcbiAgICAgICAgICB0aGlzLm5leHRCYXRjaEhhbmRsZXIocGksIHN0YXJ0SW5kZXgsIGJhdGNoU2l6ZSwgcHJvY2Vzc2VkSW5CYXRjaCwgc3IuaGl0cy5sZW5ndGgsIGxhc3RCYXRjaCk7XG4gICAgICAgIH0pLmNhdGNoKCgpID0+IHtcbiAgICAgICAgICAvLyBSZWNvZ25pdGlvbiBmYWlsZWRcbiAgICAgICAgICBwaS5mYWlsZWRDb3VudCsrO1xuICAgICAgICAgIHByb2Nlc3NlZEluQmF0Y2grKztcbiAgICAgICAgICB0aGlzLm5leHRCYXRjaEhhbmRsZXIocGksIHN0YXJ0SW5kZXgsIGJhdGNoU2l6ZSwgcHJvY2Vzc2VkSW5CYXRjaCwgc3IuaGl0cy5sZW5ndGgsIGxhc3RCYXRjaCk7XG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICB9KS5jYXRjaCgoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgLy8gU2VhcmNoIGZhaWxlZCwgd2UncmUgZG9uZSBoZXJlLi4uXG4gICAgICAvLyBUT0RPOiBzZXQgY29ycmVjdCBmYWlsZWQgY291bnQgaGVyZSBvciBkbyB0aGUgZmlyc3Qgc2VhcmNoIGJlZm9yZSBzZW5kaW5nIHRoZSBBQ0NFUFRFRCByZXNwb25zZVxuICAgICAgY29uc29sZS5lcnJvcignU2VhcmNoIGZhaWxlZCBmb3IgcXVlcnk6ICcgKyBwaS5zZWFyY2ggKyAnLiBFcnJvciBkZXRhaWxzOiAnICsgZXJyb3Iuc3RhY2spO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZSBpZiB3ZSdyZSBkb25lIG9yIGlmIGEgbmV3IGJhdGNoIG5lZWRzIHRvIGJlIHByb2Nlc3NlZFxuICAgKi9cbiAgcHJpdmF0ZSBuZXh0QmF0Y2hIYW5kbGVyKHBpOiBQcm9jZXNzSW5mbywgc3RhcnRJbmRleDogbnVtYmVyLCBiYXRjaFNpemU6IG51bWJlciwgcHJvY2Vzc2VkSW5CYXRjaDogbnVtYmVyLCBoaXRzRm91bmQ6IG51bWJlciwgbGFzdEJhdGNoOiBib29sZWFuKTogdm9pZCB7XG4gICAgaWYgKHBpLmNhbmNlbGxlZCAmJiBwcm9jZXNzZWRJbkJhdGNoID09IGhpdHNGb3VuZCkge1xuICAgICAgLy8gV2UncmUgY2FuY2VsbGVkIDotKFxuICAgICAgY29uc29sZS5pbmZvKCdQcm9jZXNzaW5nIGNhbmNlbGxlZDogJyArIHBpLmlkICsgJywgc3VjY2Vzc0NvdW50OiAnICsgcGkuc3VjY2Vzc0NvdW50ICsgJywgZmFpbGVkQ291bnQ6ICcgKyBwaS5mYWlsZWRDb3VudCk7XG4gICAgfVxuICAgIGVsc2UgaWYgKGxhc3RCYXRjaCAmJiBwcm9jZXNzZWRJbkJhdGNoID09IGhpdHNGb3VuZCkge1xuICAgICAgLy8gV2UncmUgZG9uZSFcbiAgICAgIGNvbnNvbGUuaW5mbygnUHJvY2Vzc2luZyBjb21wbGV0ZWQ6ICcgKyBwaS5pZCArICcsIHN1Y2Nlc3NDb3VudDogJyArIHBpLnN1Y2Nlc3NDb3VudCArICcsIGZhaWxlZENvdW50OiAnICsgcGkuZmFpbGVkQ291bnQpO1xuICAgIH1cbiAgICBlbHNlIGlmICghbGFzdEJhdGNoICYmIHByb2Nlc3NlZEluQmF0Y2ggPT0gYmF0Y2hTaXplKSB7XG4gICAgICAvLyBUaGVyZSdzIG1vcmUuLi5cbiAgICAgIHRoaXMucmVjb2duaXplQmF0Y2gocGksIHN0YXJ0SW5kZXggKyBiYXRjaFNpemUsIGJhdGNoU2l6ZSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBoYW5kbGVFcnJvcihtZXNzYWdlOiBzdHJpbmcsIHJlcSwgcmVzLCBzdGF0dXNDb2RlOiBudW1iZXIgPSA1MDApOiB2b2lkIHtcbiAgICBjb25zb2xlLmVycm9yKCdBUEkgY2FsbCBmYWlsZWQ6ICcgKyByZXEubWV0aG9kICsgJyBcIicgKyByZXEub3JpZ2luYWxVcmwgKyAnXCIgd2l0aCBib2R5OiAnICsgSlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpICsgJyBFcnJvcjogJyArIG1lc3NhZ2UpO1xuICAgIHJlcy5zdGF0dXMoc3RhdHVzQ29kZSkuc2VuZChtZXNzYWdlKTtcbiAgfVxufVxuXG5jbGFzcyBQcm9jZXNzSW5mbyB7XG4gIHB1YmxpYyBpZDogc3RyaW5nO1xuICBwdWJsaWMgc3RhcnQ6IERhdGU7IC8vIFRPRE86IGltcGxlbWVudCBvciByZW1vdmVcbiAgcHVibGljIGZpbmlzaDogRGF0ZTsgLy8gVE9ETzogaW1wbGVtZW50IG9yIHJlbW92ZVxuICBwdWJsaWMgc2VhcmNoOiBBc3NldFNlYXJjaDtcbiAgcHVibGljIGNhbmNlbGxlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBwdWJsaWMgZmFpbGVkQ291bnQ6IG51bWJlciA9IDA7XG4gIHB1YmxpYyBzdWNjZXNzQ291bnQ6IG51bWJlciA9IDA7XG4gIHB1YmxpYyByZWNvZ25pemVSZXF1ZXN0OiBSZWNvZ25pemVSZXF1ZXN0O1xuXG4gIGNvbnN0cnVjdG9yKHJlY29nbml6ZVJlcXVlc3Q6IFJlY29nbml6ZVJlcXVlc3QsIHNlYXJjaDogQXNzZXRTZWFyY2gpIHtcbiAgICB0aGlzLmlkID0gdXVpZFY0KCk7XG4gICAgdGhpcy5zZWFyY2ggPSBzZWFyY2g7XG4gICAgdGhpcy5yZWNvZ25pemVSZXF1ZXN0ID0gcmVjb2duaXplUmVxdWVzdDtcbiAgfVxufVxuXG5jbGFzcyBSZWNvZ25pemVSZXF1ZXN0IHtcbiAgcHVibGljIHE6IHN0cmluZztcbiAgcHVibGljIG1vZGVsczogc3RyaW5nW107XG4gIHB1YmxpYyBudW06IG51bWJlcjsgLy8gVE9ETzogaW1wbGVtZW50IG9yIHJlbW92ZVxufSJdfQ==