"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const fs = require("fs");
var request = require('request').defaults({ jar: true });
class HttpError extends Error {
    constructor(message, statusCode, options) {
        super(message);
        this.message = message;
        this.statusCode = statusCode;
        this.options = options;
    }
}
exports.HttpError = HttpError;
class ElvisRequest {
    constructor(serverUrl, username, password) {
        this.serverUrl = serverUrl;
        this.username = username;
        this.password = password;
    }
    // TODO: The request and requestFile code is long and redundant. not sure yet how to minimize code without losing functionality
    request(options) {
        return this.apiRequest(options).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.apiRequest(options);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.apiRequest(options);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    requestFile(url, destination) {
        return this.fileRequest(url, destination).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.fileRequest(url, destination);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.fileRequest(url, destination);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    authenticate() {
        var options = {
            method: 'POST',
            url: this.serverUrl + '/services/login?username=' + this.username + '&password=' + this.password
        };
        console.info('Not logged in, logging in...');
        return this.apiRequest(options).then(response => {
            if (!response.loginSuccess) {
                throw new HttpError(response.loginFaultMessage, 401, options);
            }
            else {
                console.info('Login successful!');
                if (response.csrfToken) {
                    // Elvis 6+ login
                    this.csrfToken = response.csrfToken;
                }
                return response;
            }
        });
    }
    addCsrfToken(options) {
        if (this.csrfToken) {
            // Elvis 6+
            if (!options.headers) {
                options.headers = {};
            }
            options.headers['X-CSRF-TOKEN'] = this.csrfToken;
        }
    }
    apiRequest(options) {
        return new Promise((resolve, reject) => {
            options.json = true;
            this.addCsrfToken(options);
            request(options, (error, response, body) => {
                if (error) {
                    // Handle generic errors, for example unknown host
                    reject(new HttpError('Elvis request failed: ' + error, 0, options));
                }
                if (body.errorcode) {
                    response.statusCode = body.errorcode;
                    response.statusMessage = body.message;
                }
                if (response.statusCode < 200 || response.statusCode > 299) {
                    // Handle Elvis HTTP errors: 404, 401, 500, etc
                    reject(new HttpError('Elvis request failed: ' + response.statusMessage, response.statusCode, options));
                }
                else {
                    // All good, return API response
                    resolve(body);
                }
            });
        });
    }
    fileRequest(url, destination) {
        return new Promise((resolve, reject) => {
            let errorMsg = 'Download of ' + url + ' to: ' + destination + ' failed';
            let file = fs.createWriteStream(destination);
            let options = {
                method: 'GET',
                url: url
            };
            this.addCsrfToken(options);
            let req = request(options)
                .on('error', (error) => {
                // Handle generic errors when getting the file, for example unknown host
                reject(new Error(errorMsg + ': ' + error));
            })
                .on('response', response => {
                // Handle Elvis HTTP errors: 404, 401, 500, etc
                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new HttpError(errorMsg, response.statusCode, options));
                }
                // Request went well, let's start piping the data...
                req.pipe(file)
                    .on('error', (error) => {
                    // Handle piping errors: unable to write file, stream closed, ...
                    reject(new Error(errorMsg + ': ' + error));
                })
                    .on('finish', () => {
                    // Piping complete, we've got the file!
                    resolve(destination);
                });
            });
        });
    }
}
exports.ElvisRequest = ElvisRequest;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9lbHZpcy1hcGkvZWx2aXMtcmVxdWVzdC50cyIsInNvdXJjZXMiOlsiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9lbHZpcy1hcGkvZWx2aXMtcmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG9DQUFxQztBQUVyQyx5QkFBMEI7QUFFMUIsSUFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBRXpELGVBQXVCLFNBQVEsS0FBSztJQUNsQyxZQUFtQixPQUFlLEVBQVMsVUFBa0IsRUFBUyxPQUFnRTtRQUNwSSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFERSxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQVMsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUFTLFlBQU8sR0FBUCxPQUFPLENBQXlEO0lBRXRJLENBQUM7Q0FDRjtBQUpELDhCQUlDO0FBRUQ7SUFLRSxZQUFvQixTQUFpQixFQUFVLFFBQWdCLEVBQVUsUUFBZ0I7UUFBckUsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVE7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFRO0lBQ3pGLENBQUM7SUFFRCwrSEFBK0g7SUFFeEgsT0FBTyxDQUFDLE9BQWdFO1FBQzdFLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDZiw2QkFBNkI7b0JBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQzt3QkFDbkMscUJBQXFCO3dCQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzt3QkFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO29CQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuQixDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0RBQW9ELENBQUMsQ0FBQztvQkFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUNwQixxQkFBcUI7d0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNsQyxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE1BQU0sS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLFdBQVcsQ0FBQyxHQUFXLEVBQUUsV0FBbUI7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ25ELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDZiw2QkFBNkI7b0JBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQzt3QkFDbkMscUJBQXFCO3dCQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzt3QkFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7b0JBQ2xFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDcEIscUJBQXFCO3dCQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBQzVDLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUM7WUFDSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sTUFBTSxLQUFLLENBQUM7WUFDZCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sWUFBWTtRQUNsQixJQUFJLE9BQU8sR0FBRztZQUNaLE1BQU0sRUFBRSxNQUFNO1lBQ2QsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVE7U0FDakcsQ0FBQTtRQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUMzQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDaEUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbEMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLGlCQUFpQjtvQkFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUN0QyxDQUFDO2dCQUNELE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDbEIsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFlBQVksQ0FBQyxPQUFPO1FBQzFCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ25CLFdBQVc7WUFDWCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixPQUFPLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUN2QixDQUFDO1lBQ0QsT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ25ELENBQUM7SUFDSCxDQUFDO0lBRU8sVUFBVSxDQUFDLE9BQWdFO1FBQ2pGLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0IsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsSUFBSTtnQkFDckMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixrREFBa0Q7b0JBQ2xELE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBQyx3QkFBd0IsR0FBRyxLQUFLLEVBQUUsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3RFLENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ25CLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDckMsUUFBUSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUN4QyxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxJQUFJLFFBQVEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDM0QsK0NBQStDO29CQUMvQyxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsd0JBQXdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pHLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLENBQUM7b0JBQ0osZ0NBQWdDO29CQUNoQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hCLENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFdBQVcsQ0FBQyxHQUFXLEVBQUUsV0FBbUI7UUFDbEQsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFTLENBQUMsT0FBTyxFQUFFLE1BQU07WUFDekMsSUFBSSxRQUFRLEdBQVcsY0FBYyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLFNBQVMsQ0FBQztZQUNoRixJQUFJLElBQUksR0FBbUIsRUFBRSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzdELElBQUksT0FBTyxHQUFRO2dCQUNqQixNQUFNLEVBQUUsS0FBSztnQkFDYixHQUFHLEVBQUUsR0FBRzthQUNULENBQUM7WUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRTNCLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7aUJBQ3ZCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLO2dCQUNqQix3RUFBd0U7Z0JBQ3hFLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDO2lCQUNELEVBQUUsQ0FBQyxVQUFVLEVBQUUsUUFBUTtnQkFFdEIsK0NBQStDO2dCQUMvQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzNELE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxDQUFDO2dCQUVELG9EQUFvRDtnQkFDcEQsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7cUJBQ1gsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUs7b0JBQ2pCLGlFQUFpRTtvQkFDakUsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQyxDQUFDO3FCQUNELEVBQUUsQ0FBQyxRQUFRLEVBQUU7b0JBQ1osdUNBQXVDO29CQUN2QyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FDRjtBQXhKRCxvQ0F3SkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvbWlzZSA9IHJlcXVpcmUoJ2JsdWViaXJkJyk7XG5pbXBvcnQgeyBVcmlPcHRpb25zLCBVcmxPcHRpb25zLCBDb3JlT3B0aW9ucyB9IGZyb20gJ3JlcXVlc3QnO1xuaW1wb3J0IGZzID0gcmVxdWlyZSgnZnMnKTtcblxudmFyIHJlcXVlc3QgPSByZXF1aXJlKCdyZXF1ZXN0JykuZGVmYXVsdHMoeyBqYXI6IHRydWUgfSk7XG5cbmV4cG9ydCBjbGFzcyBIdHRwRXJyb3IgZXh0ZW5kcyBFcnJvciB7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBtZXNzYWdlOiBzdHJpbmcsIHB1YmxpYyBzdGF0dXNDb2RlOiBudW1iZXIsIHB1YmxpYyBvcHRpb25zOiAoVXJpT3B0aW9ucyAmIENvcmVPcHRpb25zKSB8IChVcmxPcHRpb25zICYgQ29yZU9wdGlvbnMpKSB7XG4gICAgc3VwZXIobWVzc2FnZSk7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIEVsdmlzUmVxdWVzdCB7XG5cbiAgcHJpdmF0ZSBjc3JmVG9rZW46IHN0cmluZztcbiAgcHJpdmF0ZSBhdXRoOiBQcm9taXNlPGFueT5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZlclVybDogc3RyaW5nLCBwcml2YXRlIHVzZXJuYW1lOiBzdHJpbmcsIHByaXZhdGUgcGFzc3dvcmQ6IHN0cmluZykge1xuICB9XG5cbiAgLy8gVE9ETzogVGhlIHJlcXVlc3QgYW5kIHJlcXVlc3RGaWxlIGNvZGUgaXMgbG9uZyBhbmQgcmVkdW5kYW50LiBub3Qgc3VyZSB5ZXQgaG93IHRvIG1pbmltaXplIGNvZGUgd2l0aG91dCBsb3NpbmcgZnVuY3Rpb25hbGl0eVxuXG4gIHB1YmxpYyByZXF1ZXN0KG9wdGlvbnM6IChVcmlPcHRpb25zICYgQ29yZU9wdGlvbnMpIHwgKFVybE9wdGlvbnMgJiBDb3JlT3B0aW9ucykpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmFwaVJlcXVlc3Qob3B0aW9ucykuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgaWYgKGVycm9yLnN0YXR1c0NvZGUgPT0gNDAxKSB7XG4gICAgICAgIGlmICghdGhpcy5hdXRoKSB7XG4gICAgICAgICAgLy8gTm90IGxvZ2dlZCBpbiwgbG9naW4gZmlyc3RcbiAgICAgICAgICB0aGlzLmF1dGggPSB0aGlzLmF1dGhlbnRpY2F0ZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICB0aGlzLmF1dGggPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXBpUmVxdWVzdChvcHRpb25zKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdBbHJlYWR5IGxvZ2dpbmcgaW4sIHdhaXRpbmcgZm9yIGxvZ2luIHRvIGZpbmlzaC4uLicpO1xuICAgICAgICAgIHJldHVybiB0aGlzLmF1dGgudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAvLyBSZXRyeSBpbml0aWFsIGNhbGxcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaVJlcXVlc3Qob3B0aW9ucyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRocm93IGVycm9yO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHJlcXVlc3RGaWxlKHVybDogc3RyaW5nLCBkZXN0aW5hdGlvbjogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5maWxlUmVxdWVzdCh1cmwsIGRlc3RpbmF0aW9uKS5jYXRjaChlcnJvciA9PiB7XG4gICAgICBpZiAoZXJyb3Iuc3RhdHVzQ29kZSA9PSA0MDEpIHtcbiAgICAgICAgaWYgKCF0aGlzLmF1dGgpIHtcbiAgICAgICAgICAvLyBOb3QgbG9nZ2VkIGluLCBsb2dpbiBmaXJzdFxuICAgICAgICAgIHRoaXMuYXV0aCA9IHRoaXMuYXV0aGVudGljYXRlKCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAvLyBSZXRyeSBpbml0aWFsIGNhbGxcbiAgICAgICAgICAgIHRoaXMuYXV0aCA9IG51bGw7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5maWxlUmVxdWVzdCh1cmwsIGRlc3RpbmF0aW9uKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdBbHJlYWR5IGxvZ2dpbmcgaW4sIHdhaXRpbmcgZm9yIGxvZ2luIHRvIGZpbmlzaC4uLicpO1xuICAgICAgICAgIHJldHVybiB0aGlzLmF1dGgudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAvLyBSZXRyeSBpbml0aWFsIGNhbGxcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbGVSZXF1ZXN0KHVybCwgZGVzdGluYXRpb24pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYXV0aGVudGljYXRlKCk6IFByb21pc2U8YW55PiB7XG4gICAgdmFyIG9wdGlvbnMgPSB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIHVybDogdGhpcy5zZXJ2ZXJVcmwgKyAnL3NlcnZpY2VzL2xvZ2luP3VzZXJuYW1lPScgKyB0aGlzLnVzZXJuYW1lICsgJyZwYXNzd29yZD0nICsgdGhpcy5wYXNzd29yZFxuICAgIH1cbiAgICBjb25zb2xlLmluZm8oJ05vdCBsb2dnZWQgaW4sIGxvZ2dpbmcgaW4uLi4nKTtcbiAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgaWYgKCFyZXNwb25zZS5sb2dpblN1Y2Nlc3MpIHtcbiAgICAgICAgdGhyb3cgbmV3IEh0dHBFcnJvcihyZXNwb25zZS5sb2dpbkZhdWx0TWVzc2FnZSwgNDAxLCBvcHRpb25zKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnTG9naW4gc3VjY2Vzc2Z1bCEnKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmNzcmZUb2tlbikge1xuICAgICAgICAgIC8vIEVsdmlzIDYrIGxvZ2luXG4gICAgICAgICAgdGhpcy5jc3JmVG9rZW4gPSByZXNwb25zZS5jc3JmVG9rZW47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRDc3JmVG9rZW4ob3B0aW9ucyk6IHZvaWQge1xuICAgIGlmICh0aGlzLmNzcmZUb2tlbikge1xuICAgICAgLy8gRWx2aXMgNitcbiAgICAgIGlmICghb3B0aW9ucy5oZWFkZXJzKSB7XG4gICAgICAgIG9wdGlvbnMuaGVhZGVycyA9IHt9O1xuICAgICAgfVxuICAgICAgb3B0aW9ucy5oZWFkZXJzWydYLUNTUkYtVE9LRU4nXSA9IHRoaXMuY3NyZlRva2VuO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYXBpUmVxdWVzdChvcHRpb25zOiAoVXJpT3B0aW9ucyAmIENvcmVPcHRpb25zKSB8IChVcmxPcHRpb25zICYgQ29yZU9wdGlvbnMpKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgb3B0aW9ucy5qc29uID0gdHJ1ZTtcbiAgICAgIHRoaXMuYWRkQ3NyZlRva2VuKG9wdGlvbnMpO1xuICAgICAgcmVxdWVzdChvcHRpb25zLCAoZXJyb3IsIHJlc3BvbnNlLCBib2R5KSA9PiB7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIC8vIEhhbmRsZSBnZW5lcmljIGVycm9ycywgZm9yIGV4YW1wbGUgdW5rbm93biBob3N0XG4gICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoJ0VsdmlzIHJlcXVlc3QgZmFpbGVkOiAnICsgZXJyb3IsIDAsIG9wdGlvbnMpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChib2R5LmVycm9yY29kZSkge1xuICAgICAgICAgIHJlc3BvbnNlLnN0YXR1c0NvZGUgPSBib2R5LmVycm9yY29kZTtcbiAgICAgICAgICByZXNwb25zZS5zdGF0dXNNZXNzYWdlID0gYm9keS5tZXNzYWdlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1c0NvZGUgPCAyMDAgfHwgcmVzcG9uc2Uuc3RhdHVzQ29kZSA+IDI5OSkge1xuICAgICAgICAgIC8vIEhhbmRsZSBFbHZpcyBIVFRQIGVycm9yczogNDA0LCA0MDEsIDUwMCwgZXRjXG4gICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoJ0VsdmlzIHJlcXVlc3QgZmFpbGVkOiAnICsgcmVzcG9uc2Uuc3RhdHVzTWVzc2FnZSwgcmVzcG9uc2Uuc3RhdHVzQ29kZSwgb3B0aW9ucykpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIC8vIEFsbCBnb29kLCByZXR1cm4gQVBJIHJlc3BvbnNlXG4gICAgICAgICAgcmVzb2x2ZShib2R5KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGZpbGVSZXF1ZXN0KHVybDogc3RyaW5nLCBkZXN0aW5hdGlvbjogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgZXJyb3JNc2c6IHN0cmluZyA9ICdEb3dubG9hZCBvZiAnICsgdXJsICsgJyB0bzogJyArIGRlc3RpbmF0aW9uICsgJyBmYWlsZWQnO1xuICAgICAgbGV0IGZpbGU6IGZzLldyaXRlU3RyZWFtID0gZnMuY3JlYXRlV3JpdGVTdHJlYW0oZGVzdGluYXRpb24pO1xuICAgICAgbGV0IG9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgdXJsOiB1cmxcbiAgICAgIH07XG4gICAgICB0aGlzLmFkZENzcmZUb2tlbihvcHRpb25zKTtcblxuICAgICAgbGV0IHJlcSA9IHJlcXVlc3Qob3B0aW9ucylcbiAgICAgICAgLm9uKCdlcnJvcicsIChlcnJvcikgPT4ge1xuICAgICAgICAgIC8vIEhhbmRsZSBnZW5lcmljIGVycm9ycyB3aGVuIGdldHRpbmcgdGhlIGZpbGUsIGZvciBleGFtcGxlIHVua25vd24gaG9zdFxuICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoZXJyb3JNc2cgKyAnOiAnICsgZXJyb3IpKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm9uKCdyZXNwb25zZScsIHJlc3BvbnNlID0+IHtcblxuICAgICAgICAgIC8vIEhhbmRsZSBFbHZpcyBIVFRQIGVycm9yczogNDA0LCA0MDEsIDUwMCwgZXRjXG4gICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1c0NvZGUgPCAyMDAgfHwgcmVzcG9uc2Uuc3RhdHVzQ29kZSA+IDI5OSkge1xuICAgICAgICAgICAgcmVqZWN0KG5ldyBIdHRwRXJyb3IoZXJyb3JNc2csIHJlc3BvbnNlLnN0YXR1c0NvZGUsIG9wdGlvbnMpKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBSZXF1ZXN0IHdlbnQgd2VsbCwgbGV0J3Mgc3RhcnQgcGlwaW5nIHRoZSBkYXRhLi4uXG4gICAgICAgICAgcmVxLnBpcGUoZmlsZSlcbiAgICAgICAgICAgIC5vbignZXJyb3InLCAoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgLy8gSGFuZGxlIHBpcGluZyBlcnJvcnM6IHVuYWJsZSB0byB3cml0ZSBmaWxlLCBzdHJlYW0gY2xvc2VkLCAuLi5cbiAgICAgICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihlcnJvck1zZyArICc6ICcgKyBlcnJvcikpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vbignZmluaXNoJywgKCkgPT4ge1xuICAgICAgICAgICAgICAvLyBQaXBpbmcgY29tcGxldGUsIHdlJ3ZlIGdvdCB0aGUgZmlsZSFcbiAgICAgICAgICAgICAgcmVzb2x2ZShkZXN0aW5hdGlvbik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn0iXX0=