"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const api_1 = require("../elvis-api/api");
/**
 * Singleton class that ensures only one Elvis API session is used.
 */
class ApiManager {
    /**
     * Create an Elvis API instance
     */
    static getApi() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance.api;
    }
    constructor() {
        // Log into Elvis
        this.api = new api_1.ElvisApi(config_1.Config.elvisUsername, config_1.Config.elvisPassword, config_1.Config.elvisUrl);
    }
}
exports.ApiManager = ApiManager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9lbHZpcy1hcGkvYXBpLW1hbmFnZXIudHMiLCJzb3VyY2VzIjpbIi9zcnYvZWx2aXMtd2ViaG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvZWx2aXMtYXBpL2FwaS1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQW1DO0FBQ25DLDBDQUE0QztBQUU1Qzs7R0FFRztBQUNIO0lBTUU7O09BRUc7SUFDSSxNQUFNLENBQUMsTUFBTTtRQUNsQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUM3QixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO0lBQzNCLENBQUM7SUFFRDtRQUNFLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksY0FBUSxDQUFDLGVBQU0sQ0FBQyxhQUFhLEVBQUUsZUFBTSxDQUFDLGFBQWEsRUFBRSxlQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdkYsQ0FBQztDQUVGO0FBckJELGdDQXFCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uL2NvbmZpZyc7XG5pbXBvcnQgeyBFbHZpc0FwaSB9IGZyb20gJy4uL2VsdmlzLWFwaS9hcGknO1xuXG4vKipcbiAqIFNpbmdsZXRvbiBjbGFzcyB0aGF0IGVuc3VyZXMgb25seSBvbmUgRWx2aXMgQVBJIHNlc3Npb24gaXMgdXNlZC5cbiAqL1xuZXhwb3J0IGNsYXNzIEFwaU1hbmFnZXIge1xuXG4gIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBBcGlNYW5hZ2VyO1xuXG4gIHB1YmxpYyBhcGk6IEVsdmlzQXBpO1xuXG4gIC8qKlxuICAgKiBDcmVhdGUgYW4gRWx2aXMgQVBJIGluc3RhbmNlXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGdldEFwaSgpOiBFbHZpc0FwaSB7XG4gICAgaWYgKCF0aGlzLmluc3RhbmNlKSB7XG4gICAgICB0aGlzLmluc3RhbmNlID0gbmV3IHRoaXMoKTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMuaW5zdGFuY2UuYXBpO1xuICB9XG5cbiAgcHJpdmF0ZSBjb25zdHJ1Y3RvcigpIHtcbiAgICAvLyBMb2cgaW50byBFbHZpc1xuICAgIHRoaXMuYXBpID0gbmV3IEVsdmlzQXBpKENvbmZpZy5lbHZpc1VzZXJuYW1lLCBDb25maWcuZWx2aXNQYXNzd29yZCwgQ29uZmlnLmVsdmlzVXJsKTtcbiAgfVxuXG59Il19