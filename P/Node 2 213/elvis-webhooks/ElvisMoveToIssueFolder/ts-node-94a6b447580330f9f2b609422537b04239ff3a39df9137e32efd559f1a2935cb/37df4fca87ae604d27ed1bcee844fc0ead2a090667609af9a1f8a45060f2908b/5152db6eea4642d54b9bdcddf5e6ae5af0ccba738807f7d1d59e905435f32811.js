"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const compare = require("secure-compare");
const path = require("path");
const JSONPath = require("JSONPath");
const config_1 = require("../config");
const api_manager_1 = require("../elvis-api/api-manager");
const enterprise_api_1 = require("../enterprise-api/enterprise-api");
/**
 * Handle Elvis webhook events
 */
class WebhookEndpoint {
    constructor(app) {
        this.app = app;
        this.productionFolders = config_1.Config.productionFolders.split('|');
        this.enterpriseApi = new enterprise_api_1.EnterpriseApi(config_1.Config.enterpriseServerUrl, config_1.Config.enterpriseUsername, config_1.Config.enterprisePassword);
    }
    /**
     * Register HTTP Post route on '/' and listen for Elvis webhook events
     */
    addRoutes() {
        // Recognize API
        this.app.post('/', (req, res) => {
            // Send a response back to Elvis before handling the event, so the app won't keep 
            // the connection open while it's handling the event. This results in better performance.
            res.status(200).send();
            // Validate the webhook signature
            let signature = req.header('x-hook-signature');
            if (!this.validateSignature(signature, JSON.stringify(req.body))) {
                return console.error('Invalid WebHook signature: ' + signature + '. Make sure the elvisToken is configured correctly.');
            }
            // Handle the event. 
            this.handle(req.body);
        });
    }
    /**
     * Validate Elvis webhook signature
     *
     * @param signature Request header signature
     * @param data Request data to validate
     */
    validateSignature(signature, data) {
        try {
            let hmac = crypto.createHmac('sha256', config_1.Config.elvisToken);
            hmac.update(data);
            let digest = hmac.digest('hex');
            return compare(digest, signature);
        }
        catch (error) {
            console.warn('Signature validation failed for signature: ' + signature + ' and data: ' + data + '; details: ' + error);
        }
        return false;
    }
    /**
     * Handle Elvis webhook events.
     *
     * @param event The Elvis webhook event to handle
     */
    handle(event) {
        let isMetadataUpdateEvent = (event.type === 'asset_update_metadata');
        if (!isMetadataUpdateEvent) {
            console.warn('IGNORING EVENT, expecting "asset_update_metadata", received: "' + event.type + '"');
            return;
        }
        if (!event.metadata || !event.metadata.assetPath) {
            console.warn('IGNORING EVENT, expecting assetPath metadata field for: "' + event.assetId + '"');
            return;
        }
        let cm = event.changedMetadata;
        let sourceFolder = path.dirname(event.metadata.assetPath);
        if (!this.productionFolders.includes(sourceFolder) || !cm.sceId || !cm.sceId.newValue) {
            // We can ignore this event, since either:
            // A. The file is not in the root of a production zone; Or
            // B. The sceId isn't set to a new value
            return;
        }
        if (cm.sceIssue && !cm.sceIssue.oldValue && cm.sceIssue.newValue) {
            // sceIssue is set for the first time, let's use that as folder value
            this.moveAsset(event.assetId, event.metadata.assetPath, cm.sceIssue.newValue[0]);
        }
        else {
            // Wait here in order to make sure the object relations are also created in Enterprise (link to dossier)
            setTimeout(() => {
                // Get issue from Enterprise
                let request = {
                    IDs: [cm.sceId.newValue],
                    Lock: false,
                    Rendition: 'none'
                };
                this.enterpriseApi.getObjects(request).then((response) => {
                    // Get issue from dossier relation target
                    let issue = this.selectSingleString({ json: response, path: "$.Objects[0].Relations[?(@.ParentInfo.Type === 'Dossier')].Targets[0].Issue.Name" });
                    if (!issue) {
                        // Try to get the issue from the Object target (fallback)
                        issue = this.selectSingleString({ json: response, path: "$.Objects[0].Targets[0].Issue.Name" });
                    }
                    if (issue) {
                        // Move asset, issue name was found in Enterprise Target info
                        this.moveAsset(event.assetId, event.metadata.assetPath, issue);
                    }
                    else {
                        console.error('MOVE ASSET FAILED: "' + event.assetId + '". No issue name found in Enterprise for sceId: ' + cm.sceId.newValue + '. GetObjects response:\n' + JSON.stringify(response, null, 2));
                    }
                }).catch((error) => {
                    console.error('MOVE ASSET FAILED: "' + event.assetId + '". Unable to retrieve issue information from Enterprise for sceId: ' + cm.sceId.newValue + '. Cause: ' + error.message);
                });
            }, config_1.Config.enterpriseGetObjectTimeout);
        }
    }
    selectSingleString(params) {
        let result = JSONPath(params);
        if (result.length == 1) {
            return result[0];
        }
        return null;
    }
    moveAsset(assetId, sourceAssetPath, issueName) {
        let sourceFolder = path.dirname(sourceAssetPath);
        let filename = path.basename(sourceAssetPath);
        let destinationAssetPath = path.join(sourceFolder, issueName, filename);
        api_manager_1.ApiManager.getApi().move(sourceAssetPath, destinationAssetPath).then((response) => {
            if (response.errorCount == 0) {
                console.info('MOVED ASSET: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '"');
            }
            else {
                console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Check the Elvis Server log files for details.');
            }
        }).catch((error) => {
            console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Cause: ' + error.message);
        });
    }
}
exports.WebhookEndpoint = WebhookEndpoint;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9hcHAvd2ViaG9vay1lbmRwb2ludC50cyIsInNvdXJjZXMiOlsiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc01vdmVUb0lzc3VlRm9sZGVyL3NyYy9hcHAvd2ViaG9vay1lbmRwb2ludC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFrQztBQUNsQywwQ0FBMkM7QUFDM0MsNkJBQThCO0FBQzlCLHFDQUFzQztBQUV0QyxzQ0FBbUM7QUFDbkMsMERBQXNEO0FBQ3RELHFFQUFnRTtBQUdoRTs7R0FFRztBQUNIO0lBS0UsWUFBbUIsR0FBZ0I7UUFBaEIsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUNqQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZUFBTSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksOEJBQWEsQ0FBQyxlQUFNLENBQUMsbUJBQW1CLEVBQUUsZUFBTSxDQUFDLGtCQUFrQixFQUFFLGVBQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQzNILENBQUM7SUFFRDs7T0FFRztJQUNJLFNBQVM7UUFDZCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBWSxFQUFFLEdBQWE7WUFFN0Msa0ZBQWtGO1lBQ2xGLHlGQUF5RjtZQUN6RixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBRXZCLGlDQUFpQztZQUNqQyxJQUFJLFNBQVMsR0FBVyxHQUFHLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDdkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqRSxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsR0FBRyxTQUFTLEdBQUcscURBQXFELENBQUMsQ0FBQztZQUMxSCxDQUFDO1lBRUQscUJBQXFCO1lBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ssaUJBQWlCLENBQUMsU0FBaUIsRUFBRSxJQUFZO1FBQ3ZELElBQUksQ0FBQztZQUNILElBQUksSUFBSSxHQUFnQixNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxlQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixJQUFJLE1BQU0sR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFDRCxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2IsT0FBTyxDQUFDLElBQUksQ0FBQyw2Q0FBNkMsR0FBRyxTQUFTLEdBQUcsYUFBYSxHQUFHLElBQUksR0FBRyxhQUFhLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDekgsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLE1BQU0sQ0FBQyxLQUFVO1FBQ3ZCLElBQUkscUJBQXFCLEdBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLHVCQUF1QixDQUFDLENBQUM7UUFFOUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDM0IsT0FBTyxDQUFDLElBQUksQ0FBQyxnRUFBZ0UsR0FBRyxLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ2xHLE1BQU0sQ0FBQztRQUNULENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDakQsT0FBTyxDQUFDLElBQUksQ0FBQywyREFBMkQsR0FBRyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ2hHLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLEVBQUUsR0FBRyxLQUFLLENBQUMsZUFBZSxDQUFDO1FBQy9CLElBQUksWUFBWSxHQUFXLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLDBDQUEwQztZQUMxQywwREFBMEQ7WUFDMUQsd0NBQXdDO1lBQ3hDLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLHFFQUFxRTtZQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRixDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDSix3R0FBd0c7WUFDeEcsVUFBVSxDQUFDO2dCQUNULDRCQUE0QjtnQkFDNUIsSUFBSSxPQUFPLEdBQVE7b0JBQ2pCLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUN4QixJQUFJLEVBQUUsS0FBSztvQkFDWCxTQUFTLEVBQUUsTUFBTTtpQkFDbEIsQ0FBQTtnQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFhO29CQUN4RCx5Q0FBeUM7b0JBQ3pDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGtGQUFrRixFQUFFLENBQUMsQ0FBQztvQkFDbEosRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNYLHlEQUF5RDt3QkFDekQsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLG9DQUFvQyxFQUFFLENBQUMsQ0FBQztvQkFDbEcsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNWLDZEQUE2RDt3QkFDN0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNqRSxDQUFDO29CQUNELElBQUksQ0FBQyxDQUFDO3dCQUNKLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxrREFBa0QsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRywwQkFBMEIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbE0sQ0FBQztnQkFDSCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLO29CQUNiLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxxRUFBcUUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxXQUFXLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNsTCxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsRUFBRSxlQUFNLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUN4QyxDQUFDO0lBQ0gsQ0FBQztJQUVPLGtCQUFrQixDQUFDLE1BQVc7UUFDcEMsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVPLFNBQVMsQ0FBQyxPQUFlLEVBQUUsZUFBdUIsRUFBRSxTQUFpQjtRQUMzRSxJQUFJLFlBQVksR0FBVyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3pELElBQUksUUFBUSxHQUFXLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdEQsSUFBSSxvQkFBb0IsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFaEYsd0JBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUTtZQUM1RSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxlQUFlLEdBQUcsU0FBUyxHQUFHLG9CQUFvQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ3BILENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsZUFBZSxHQUFHLFNBQVMsR0FBRyxvQkFBb0IsR0FBRyxrREFBa0QsQ0FBQyxDQUFDO1lBQzFLLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFVO1lBQ2xCLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxlQUFlLEdBQUcsU0FBUyxHQUFHLG9CQUFvQixHQUFHLFlBQVksR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDcEosQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUF4SUQsMENBd0lDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGNyeXB0byA9IHJlcXVpcmUoJ2NyeXB0bycpO1xuaW1wb3J0IGNvbXBhcmUgPSByZXF1aXJlKCdzZWN1cmUtY29tcGFyZScpO1xuaW1wb3J0IHBhdGggPSByZXF1aXJlKCdwYXRoJyk7XG5pbXBvcnQgSlNPTlBhdGggPSByZXF1aXJlKCdKU09OUGF0aCcpO1xuXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi9jb25maWcnO1xuaW1wb3J0IHsgQXBpTWFuYWdlciB9IGZyb20gJy4uL2VsdmlzLWFwaS9hcGktbWFuYWdlcic7XG5pbXBvcnQgeyBFbnRlcnByaXNlQXBpIH0gZnJvbSAnLi4vZW50ZXJwcmlzZS1hcGkvZW50ZXJwcmlzZS1hcGknXG5pbXBvcnQgeyBBcHBsaWNhdGlvbiwgUmVxdWVzdCwgUmVzcG9uc2UgfSBmcm9tICdleHByZXNzJztcblxuLyoqXG4gKiBIYW5kbGUgRWx2aXMgd2ViaG9vayBldmVudHNcbiAqL1xuZXhwb3J0IGNsYXNzIFdlYmhvb2tFbmRwb2ludCB7XG5cbiAgcHJpdmF0ZSBwcm9kdWN0aW9uRm9sZGVyczogc3RyaW5nW107XG4gIHByaXZhdGUgZW50ZXJwcmlzZUFwaTogRW50ZXJwcmlzZUFwaTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgYXBwOiBBcHBsaWNhdGlvbikge1xuICAgIHRoaXMucHJvZHVjdGlvbkZvbGRlcnMgPSBDb25maWcucHJvZHVjdGlvbkZvbGRlcnMuc3BsaXQoJ3wnKTtcbiAgICB0aGlzLmVudGVycHJpc2VBcGkgPSBuZXcgRW50ZXJwcmlzZUFwaShDb25maWcuZW50ZXJwcmlzZVNlcnZlclVybCwgQ29uZmlnLmVudGVycHJpc2VVc2VybmFtZSwgQ29uZmlnLmVudGVycHJpc2VQYXNzd29yZCk7XG4gIH1cblxuICAvKipcbiAgICogUmVnaXN0ZXIgSFRUUCBQb3N0IHJvdXRlIG9uICcvJyBhbmQgbGlzdGVuIGZvciBFbHZpcyB3ZWJob29rIGV2ZW50c1xuICAgKi9cbiAgcHVibGljIGFkZFJvdXRlcygpOiB2b2lkIHtcbiAgICAvLyBSZWNvZ25pemUgQVBJXG4gICAgdGhpcy5hcHAucG9zdCgnLycsIChyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpID0+IHtcblxuICAgICAgLy8gU2VuZCBhIHJlc3BvbnNlIGJhY2sgdG8gRWx2aXMgYmVmb3JlIGhhbmRsaW5nIHRoZSBldmVudCwgc28gdGhlIGFwcCB3b24ndCBrZWVwIFxuICAgICAgLy8gdGhlIGNvbm5lY3Rpb24gb3BlbiB3aGlsZSBpdCdzIGhhbmRsaW5nIHRoZSBldmVudC4gVGhpcyByZXN1bHRzIGluIGJldHRlciBwZXJmb3JtYW5jZS5cbiAgICAgIHJlcy5zdGF0dXMoMjAwKS5zZW5kKCk7XG5cbiAgICAgIC8vIFZhbGlkYXRlIHRoZSB3ZWJob29rIHNpZ25hdHVyZVxuICAgICAgbGV0IHNpZ25hdHVyZTogc3RyaW5nID0gcmVxLmhlYWRlcigneC1ob29rLXNpZ25hdHVyZScpO1xuICAgICAgaWYgKCF0aGlzLnZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZSwgSlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpKSkge1xuICAgICAgICByZXR1cm4gY29uc29sZS5lcnJvcignSW52YWxpZCBXZWJIb29rIHNpZ25hdHVyZTogJyArIHNpZ25hdHVyZSArICcuIE1ha2Ugc3VyZSB0aGUgZWx2aXNUb2tlbiBpcyBjb25maWd1cmVkIGNvcnJlY3RseS4nKTtcbiAgICAgIH1cblxuICAgICAgLy8gSGFuZGxlIHRoZSBldmVudC4gXG4gICAgICB0aGlzLmhhbmRsZShyZXEuYm9keSk7XG4gICAgfSk7XG5cbiAgfVxuXG4gIC8qKlxuICAgKiBWYWxpZGF0ZSBFbHZpcyB3ZWJob29rIHNpZ25hdHVyZVxuICAgKiBcbiAgICogQHBhcmFtIHNpZ25hdHVyZSBSZXF1ZXN0IGhlYWRlciBzaWduYXR1cmVcbiAgICogQHBhcmFtIGRhdGEgUmVxdWVzdCBkYXRhIHRvIHZhbGlkYXRlXG4gICAqL1xuICBwcml2YXRlIHZhbGlkYXRlU2lnbmF0dXJlKHNpZ25hdHVyZTogc3RyaW5nLCBkYXRhOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICB0cnkge1xuICAgICAgbGV0IGhtYWM6IGNyeXB0by5IbWFjID0gY3J5cHRvLmNyZWF0ZUhtYWMoJ3NoYTI1NicsIENvbmZpZy5lbHZpc1Rva2VuKTtcbiAgICAgIGhtYWMudXBkYXRlKGRhdGEpO1xuICAgICAgbGV0IGRpZ2VzdDogc3RyaW5nID0gaG1hYy5kaWdlc3QoJ2hleCcpO1xuICAgICAgcmV0dXJuIGNvbXBhcmUoZGlnZXN0LCBzaWduYXR1cmUpO1xuICAgIH1cbiAgICBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUud2FybignU2lnbmF0dXJlIHZhbGlkYXRpb24gZmFpbGVkIGZvciBzaWduYXR1cmU6ICcgKyBzaWduYXR1cmUgKyAnIGFuZCBkYXRhOiAnICsgZGF0YSArICc7IGRldGFpbHM6ICcgKyBlcnJvcik7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIYW5kbGUgRWx2aXMgd2ViaG9vayBldmVudHMuXG4gICAqIFxuICAgKiBAcGFyYW0gZXZlbnQgVGhlIEVsdmlzIHdlYmhvb2sgZXZlbnQgdG8gaGFuZGxlXG4gICAqL1xuICBwcml2YXRlIGhhbmRsZShldmVudDogYW55KTogdm9pZCB7XG4gICAgbGV0IGlzTWV0YWRhdGFVcGRhdGVFdmVudDogYm9vbGVhbiA9IChldmVudC50eXBlID09PSAnYXNzZXRfdXBkYXRlX21ldGFkYXRhJyk7XG5cbiAgICBpZiAoIWlzTWV0YWRhdGFVcGRhdGVFdmVudCkge1xuICAgICAgY29uc29sZS53YXJuKCdJR05PUklORyBFVkVOVCwgZXhwZWN0aW5nIFwiYXNzZXRfdXBkYXRlX21ldGFkYXRhXCIsIHJlY2VpdmVkOiBcIicgKyBldmVudC50eXBlICsgJ1wiJyk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICghZXZlbnQubWV0YWRhdGEgfHwgIWV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCkge1xuICAgICAgY29uc29sZS53YXJuKCdJR05PUklORyBFVkVOVCwgZXhwZWN0aW5nIGFzc2V0UGF0aCBtZXRhZGF0YSBmaWVsZCBmb3I6IFwiJyArIGV2ZW50LmFzc2V0SWQgKyAnXCInKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsZXQgY20gPSBldmVudC5jaGFuZ2VkTWV0YWRhdGE7XG4gICAgbGV0IHNvdXJjZUZvbGRlcjogc3RyaW5nID0gcGF0aC5kaXJuYW1lKGV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCk7XG4gICAgaWYgKCF0aGlzLnByb2R1Y3Rpb25Gb2xkZXJzLmluY2x1ZGVzKHNvdXJjZUZvbGRlcikgfHwgIWNtLnNjZUlkIHx8ICFjbS5zY2VJZC5uZXdWYWx1ZSkge1xuICAgICAgLy8gV2UgY2FuIGlnbm9yZSB0aGlzIGV2ZW50LCBzaW5jZSBlaXRoZXI6XG4gICAgICAvLyBBLiBUaGUgZmlsZSBpcyBub3QgaW4gdGhlIHJvb3Qgb2YgYSBwcm9kdWN0aW9uIHpvbmU7IE9yXG4gICAgICAvLyBCLiBUaGUgc2NlSWQgaXNuJ3Qgc2V0IHRvIGEgbmV3IHZhbHVlXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGNtLnNjZUlzc3VlICYmICFjbS5zY2VJc3N1ZS5vbGRWYWx1ZSAmJiBjbS5zY2VJc3N1ZS5uZXdWYWx1ZSkge1xuICAgICAgLy8gc2NlSXNzdWUgaXMgc2V0IGZvciB0aGUgZmlyc3QgdGltZSwgbGV0J3MgdXNlIHRoYXQgYXMgZm9sZGVyIHZhbHVlXG4gICAgICB0aGlzLm1vdmVBc3NldChldmVudC5hc3NldElkLCBldmVudC5tZXRhZGF0YS5hc3NldFBhdGgsIGNtLnNjZUlzc3VlLm5ld1ZhbHVlWzBdKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAvLyBXYWl0IGhlcmUgaW4gb3JkZXIgdG8gbWFrZSBzdXJlIHRoZSBvYmplY3QgcmVsYXRpb25zIGFyZSBhbHNvIGNyZWF0ZWQgaW4gRW50ZXJwcmlzZSAobGluayB0byBkb3NzaWVyKVxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIC8vIEdldCBpc3N1ZSBmcm9tIEVudGVycHJpc2VcbiAgICAgICAgbGV0IHJlcXVlc3Q6IGFueSA9IHtcbiAgICAgICAgICBJRHM6IFtjbS5zY2VJZC5uZXdWYWx1ZV0sXG4gICAgICAgICAgTG9jazogZmFsc2UsXG4gICAgICAgICAgUmVuZGl0aW9uOiAnbm9uZSdcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmVudGVycHJpc2VBcGkuZ2V0T2JqZWN0cyhyZXF1ZXN0KS50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgLy8gR2V0IGlzc3VlIGZyb20gZG9zc2llciByZWxhdGlvbiB0YXJnZXRcbiAgICAgICAgICBsZXQgaXNzdWUgPSB0aGlzLnNlbGVjdFNpbmdsZVN0cmluZyh7IGpzb246IHJlc3BvbnNlLCBwYXRoOiBcIiQuT2JqZWN0c1swXS5SZWxhdGlvbnNbPyhALlBhcmVudEluZm8uVHlwZSA9PT0gJ0Rvc3NpZXInKV0uVGFyZ2V0c1swXS5Jc3N1ZS5OYW1lXCIgfSk7XG4gICAgICAgICAgaWYgKCFpc3N1ZSkge1xuICAgICAgICAgICAgLy8gVHJ5IHRvIGdldCB0aGUgaXNzdWUgZnJvbSB0aGUgT2JqZWN0IHRhcmdldCAoZmFsbGJhY2spXG4gICAgICAgICAgICBpc3N1ZSA9IHRoaXMuc2VsZWN0U2luZ2xlU3RyaW5nKHsganNvbjogcmVzcG9uc2UsIHBhdGg6IFwiJC5PYmplY3RzWzBdLlRhcmdldHNbMF0uSXNzdWUuTmFtZVwiIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoaXNzdWUpIHtcbiAgICAgICAgICAgIC8vIE1vdmUgYXNzZXQsIGlzc3VlIG5hbWUgd2FzIGZvdW5kIGluIEVudGVycHJpc2UgVGFyZ2V0IGluZm9cbiAgICAgICAgICAgIHRoaXMubW92ZUFzc2V0KGV2ZW50LmFzc2V0SWQsIGV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCwgaXNzdWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ01PVkUgQVNTRVQgRkFJTEVEOiBcIicgKyBldmVudC5hc3NldElkICsgJ1wiLiBObyBpc3N1ZSBuYW1lIGZvdW5kIGluIEVudGVycHJpc2UgZm9yIHNjZUlkOiAnICsgY20uc2NlSWQubmV3VmFsdWUgKyAnLiBHZXRPYmplY3RzIHJlc3BvbnNlOlxcbicgKyBKU09OLnN0cmluZ2lmeShyZXNwb25zZSwgbnVsbCwgMikpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgY29uc29sZS5lcnJvcignTU9WRSBBU1NFVCBGQUlMRUQ6IFwiJyArIGV2ZW50LmFzc2V0SWQgKyAnXCIuIFVuYWJsZSB0byByZXRyaWV2ZSBpc3N1ZSBpbmZvcm1hdGlvbiBmcm9tIEVudGVycHJpc2UgZm9yIHNjZUlkOiAnICsgY20uc2NlSWQubmV3VmFsdWUgKyAnLiBDYXVzZTogJyArIGVycm9yLm1lc3NhZ2UpO1xuICAgICAgICB9KTtcbiAgICAgIH0sIENvbmZpZy5lbnRlcnByaXNlR2V0T2JqZWN0VGltZW91dCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzZWxlY3RTaW5nbGVTdHJpbmcocGFyYW1zOiBhbnkpOiBzdHJpbmcge1xuICAgIGxldCByZXN1bHQgPSBKU09OUGF0aChwYXJhbXMpO1xuICAgIGlmIChyZXN1bHQubGVuZ3RoID09IDEpIHtcbiAgICAgIHJldHVybiByZXN1bHRbMF07XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBtb3ZlQXNzZXQoYXNzZXRJZDogc3RyaW5nLCBzb3VyY2VBc3NldFBhdGg6IHN0cmluZywgaXNzdWVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBsZXQgc291cmNlRm9sZGVyOiBzdHJpbmcgPSBwYXRoLmRpcm5hbWUoc291cmNlQXNzZXRQYXRoKTtcbiAgICBsZXQgZmlsZW5hbWU6IHN0cmluZyA9IHBhdGguYmFzZW5hbWUoc291cmNlQXNzZXRQYXRoKTtcbiAgICBsZXQgZGVzdGluYXRpb25Bc3NldFBhdGg6IHN0cmluZyA9IHBhdGguam9pbihzb3VyY2VGb2xkZXIsIGlzc3VlTmFtZSwgZmlsZW5hbWUpO1xuXG4gICAgQXBpTWFuYWdlci5nZXRBcGkoKS5tb3ZlKHNvdXJjZUFzc2V0UGF0aCwgZGVzdGluYXRpb25Bc3NldFBhdGgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICBpZiAocmVzcG9uc2UuZXJyb3JDb3VudCA9PSAwKSB7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnTU9WRUQgQVNTRVQ6IFwiJyArIGFzc2V0SWQgKyAnXCIgRlJPTTogXCInICsgc291cmNlQXNzZXRQYXRoICsgJ1wiIFRPOiBcIicgKyBkZXN0aW5hdGlvbkFzc2V0UGF0aCArICdcIicpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ01PVkUgQVNTRVQgRkFJTEVEOiBcIicgKyBhc3NldElkICsgJ1wiIEZST006IFwiJyArIHNvdXJjZUFzc2V0UGF0aCArICdcIiBUTzogXCInICsgZGVzdGluYXRpb25Bc3NldFBhdGggKyAnXCIuIENoZWNrIHRoZSBFbHZpcyBTZXJ2ZXIgbG9nIGZpbGVzIGZvciBkZXRhaWxzLicpO1xuICAgICAgfVxuICAgIH0pLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG4gICAgICBjb25zb2xlLmVycm9yKCdNT1ZFIEFTU0VUIEZBSUxFRDogXCInICsgYXNzZXRJZCArICdcIiBGUk9NOiBcIicgKyBzb3VyY2VBc3NldFBhdGggKyAnXCIgVE86IFwiJyArIGRlc3RpbmF0aW9uQXNzZXRQYXRoICsgJ1wiLiBDYXVzZTogJyArIGVycm9yLm1lc3NhZ2UpO1xuICAgIH0pO1xuICB9XG59XG4iXX0=