export class Config {
  /**
   * Port where the app runs.
   */
  static port: string = '9092';

  /**
   * Temporary directory used for downloading assets.
   */
  static tempDir: string = process.env.AF_TEMP_DIR || './temp';

  /**
   * Elvis server url.
   */
  static elvisUrl: string = process.env.AF_ELVIS_URL || 'http://localhost:80';

  /**
   * Elvis username. 
   * 
   * The user should be able to move the original in Elvis.
   */
  static elvisUsername: string = process.env.AF_ELVIS_USER || 'webhooks';

  /**
   * Elvis password.
   */
  static elvisPassword: string = process.env.AF_ELVIS_PASSWORD || 'W@BH00K5';

  /**
   * Elvis webhook token. Create a webhook that listens for "asset_metadata_update" events.
   * 
   * More info creating a webhook: https://helpcenter.woodwing.com/hc/en-us/articles/115001884346
   */
// static elvisToken: string = process.env.AF_ELVIS_TOKEN || ' xu5AjGU2S+mWMI3vZZAz2g=='; //QLD-T
// static elvisToken: string = process.env.AF_ELVIS_TOKEN || 'l2pR8WvE1v1w6heW1v6V5A=='; //QLD-A
 static elvisToken: string = process.env.AF_ELVIS_TOKEN || 'yEyiKitOtH2QeoOHXv9BoQ=='; //PROD


  /**
   * Enterprise server url.
   */
//  static enterpriseServerUrl: string = process.env.AF_ENTERPRISE_SERVER_URL || 'http://qld-enterprise.portoeditora.pt/enterpriseqadebug/index.php';
  static enterpriseServerUrl: string = process.env.AF_ENTERPRISE_SERVER_URL || 'http://enterprise.portoeditora.pt/enterprisep01/index.php';

  /**
   * Enterprise username.
   * 
   * The user should be able to access all brands that have shadow object in Elvis
   */
  static enterpriseUsername: string = process.env.AF_ENTERPRISE_USERNAME || 'pq_ww_elvis';

  /**
   * Enterprise password.
   */
  static enterprisePassword: string = process.env.AF_ENTERPRISE_PASSWORD || '@ww_3lv1s#';

  /**
   * List of production root folders used by Enterprise, separated by pipeline (|). 
   * Assets copied into these production folders will be moved to the specific issue subfolder
   */
  static productionFolders: string = process.env.AF_PRODUCTION_FOLDERS || '/Production Zone/PE|/Production Zone/AE|/Production Zone/RE';

  /**
   * GetObject timeout to ensure Enterprise is finshed creating the object and it's relation(s).
   * 
   * Enterprise first creates an object and then an object relarion to the dossier when a new shadow object is created inside a dossier.
   * This integration triggers right after the object is created. When we retrieve the object directly, the relation info might not be 
   * inserted in Enterprise yet, which results in missing issue info. This timeout (in milliseconds) is used to ensure that Enterprise
   * is finished with creating the dossier object relation.
   */
  static enterpriseGetObjectTimeout = process.env.AF_ENTERPRISE_GETOBJECT_TIMEOUT || 5000;
}