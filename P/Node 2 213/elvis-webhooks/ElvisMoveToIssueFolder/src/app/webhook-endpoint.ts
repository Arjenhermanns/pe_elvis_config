import crypto = require('crypto');
import compare = require('secure-compare');
import path = require('path');
import JSONPath = require('JSONPath');

import { Config } from '../config';
import { ApiManager } from '../elvis-api/api-manager';
import { EnterpriseApi } from '../enterprise-api/enterprise-api'
import { Application, Request, Response } from 'express';
import {SearchResponse} from '../elvis-api/api';

/**
 * Handle Elvis webhook events
 */
export class WebhookEndpoint {

  private productionFolders: string[];
  private enterpriseApi: EnterpriseApi;

  constructor(public app: Application) {
    this.productionFolders = Config.productionFolders.split('|');
    this.enterpriseApi = new EnterpriseApi(Config.enterpriseServerUrl, Config.enterpriseUsername, Config.enterprisePassword);
  }

  /**
   * Register HTTP Post route on '/' and listen for Elvis webhook events
   */
  public addRoutes(): void {
    // Recognize API
    this.app.post('/', (req: Request, res: Response) => {

      // Send a response back to Elvis before handling the event, so the app won't keep 
      // the connection open while it's handling the event. This results in better performance.
      res.status(200).send();

      // Validate the webhook signature
      let signature: string = req.header('x-hook-signature');
      if (!this.validateSignature(signature, JSON.stringify(req.body))) {
        return console.error('Invalid WebHook signature: ' + signature + '. Make sure the elvisToken is configured correctly.');
      }

      // Handle the event. 
      this.handle(req.body);
    });

  }

  /**
   * Validate Elvis webhook signature
   * 
   * @param signature Request header signature
   * @param data Request data to validate
   */
  private validateSignature(signature: string, data: string): boolean {
    try {
      let hmac: crypto.Hmac = crypto.createHmac('sha256', Config.elvisToken);
      hmac.update(data);
      let digest: string = hmac.digest('hex');
      return compare(digest, signature);
    }
    catch (error) {
      console.warn('Signature validation failed for signature: ' + signature + ' and data: ' + data + '; details: ' + error);
    }
    return false;
  }

  /**
   * Handle Elvis webhook events.
   * 
   * @param event The Elvis webhook event to handle
   */
  private handle(event: any): void {
    let isMetadataUpdateEvent: boolean = (event.type === 'asset_update_metadata');

    if (!isMetadataUpdateEvent) {
      console.warn('IGNORING EVENT, expecting "asset_update_metadata", received: "' + event.type + '"');
      return;
    }
    if (!event.metadata || !event.metadata.assetPath) {
      console.warn('IGNORING EVENT, expecting assetPath metadata field for: "' + event.assetId + '"');
      return;
    }

    let cm = event.changedMetadata;
    let sourceFolder: string = path.dirname(event.metadata.assetPath);
    if (!this.productionFolders.includes(sourceFolder) || !cm.sceId || !cm.sceId.newValue) {
      // We can ignore this event, since either:
      // A. The file is not in the root of a production zone; Or
      // B. The sceId isn't set to a new value
      return;
    }

    if (cm.sceIssue && !cm.sceIssue.oldValue && cm.sceIssue.newValue) {
      // sceIssue is set for the first time, let's use that as folder value
      this.moveAsset(event.assetId, event.metadata.assetPath, cm.sceIssue.newValue[0]);
    }
    else {
      // Wait here in order to make sure the object relations are also created in Enterprise (link to dossier)
      setTimeout(() => {
        // Get issue from Enterprise
        let request: any = {
          IDs: [cm.sceId.newValue],
          Lock: false,
          Rendition: 'none'
        }
        this.enterpriseApi.getObjects(request).then((response: any) => {
          // Get issue from dossier relation target
          let issue = this.selectSingleString({ json: response, path: "$.Objects[0].Relations[?(@.ParentInfo.Type === 'Dossier')].Targets[0].Issue.Name" });
          if (!issue) {
            // Try to get the issue from the Object target (fallback)
            issue = this.selectSingleString({ json: response, path: "$.Objects[0].Targets[0].Issue.Name" });
          }
          if (issue) {
            // Search assetPath in Elvis, it might have changed since this call is delayed
            ApiManager.getApi().searchGet("id:" + event.assetId, 0, 1, "assetPath").then((sr:SearchResponse) => {
              if (!sr.hits || sr.hits.length == 0) {
                console.error('MOVE ASSET FAILED: "' + event.assetId + '". The asset is no longer available in Elvis.');
              }
              else{
                // Move asset, issue name was found in Enterprise Target info
                this.moveAsset(event.assetId, sr.hits[0].metadata.assetPath, issue);
              } 
            });
          }
          else {
            console.error('MOVE ASSET FAILED: "' + event.assetId + '". No issue name found in Enterprise for sceId: ' + cm.sceId.newValue + '. GetObjects response:\n' + JSON.stringify(response, null, 2));
          }
        }).catch((error) => {
          console.error('MOVE ASSET FAILED: "' + event.assetId + '". Unable to retrieve issue information from Enterprise for sceId: ' + cm.sceId.newValue + '. Cause: ' + error.message);
        });
      }, Config.enterpriseGetObjectTimeout);
    }
  }

  private selectSingleString(params: any): string {
    let result = JSONPath(params);
    if (result.length == 1) {
      return result[0];
    }
    return null;
  }

  private moveAsset(assetId: string, sourceAssetPath: string, issueName: string): void {
    let sourceFolder: string = path.dirname(sourceAssetPath);
    let filename: string = path.basename(sourceAssetPath);
    let destinationAssetPath: string = path.join(sourceFolder, issueName, filename);

    ApiManager.getApi().move(sourceAssetPath, destinationAssetPath).then((response) => {
      if (response.errorCount == 0) {
        console.info('MOVED ASSET: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '"');
      }
      else {
        console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Check the Elvis Server log files for details.');
      }
    }).catch((error: any) => {
      console.error('MOVE ASSET FAILED: "' + assetId + '" FROM: "' + sourceAssetPath + '" TO: "' + destinationAssetPath + '". Cause: ' + error.message);
    });
  }
}
