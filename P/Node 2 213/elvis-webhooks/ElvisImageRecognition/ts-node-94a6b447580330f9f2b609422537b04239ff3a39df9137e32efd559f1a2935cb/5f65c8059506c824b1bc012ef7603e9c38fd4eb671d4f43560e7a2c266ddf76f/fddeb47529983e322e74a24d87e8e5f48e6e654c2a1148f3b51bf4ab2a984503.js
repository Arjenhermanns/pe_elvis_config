"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const compare = require("secure-compare");
const recognizer_1 = require("./recognizer");
const config_1 = require("../config");
/**
 * Handle Elvis webhook events
 */
class WebhookEndpoint {
    constructor(app) {
        this.app = app;
        let translateEnabled = config_1.Config.languages !== '';
        this.recognizer = new recognizer_1.Recognizer(config_1.Config.clarifaiEnabled, config_1.Config.googleEnabled, config_1.Config.awsEnabled, translateEnabled);
    }
    /**
     * Register HTTP Post route on '/' and listen for Elvis webhook events
     */
    addRoutes() {
        // Recognize API
        this.app.post('/', (req, res) => {
            // Send a response back to Elvis before handling the event, so the app won't keep 
            // the connection open while it's handling the event. This results in better performance.
            res.status(200).send();
            // Validate the webhook signature
            let signature = req.header('x-hook-signature');
            if (!this.validateSignature(signature, JSON.stringify(req.body))) {
                return console.error('Invalid WebHook signature: ' + signature + '. Make sure the elvisToken is configured correctly.');
            }
            // Handle the event. 
            this.handle(req.body);
        });
    }
    /**
     * Validate Elvis webhook signature
     *
     * @param signature Request header signature
     * @param data Request data to validate
     */
    validateSignature(signature, data) {
        try {
            let hmac = crypto.createHmac('sha256', config_1.Config.elvisToken);
            hmac.update(data);
            let digest = hmac.digest('hex');
            return compare(digest, signature);
        }
        catch (error) {
            console.warn('Signature validation failed for signature: ' + signature + ' and data: ' + data + '; details: ' + error);
        }
        return false;
    }
    /**
     * Handles the asset_update_metadata Elvis Server event and
     * starts the recognition process if the correct metadata is specified
     *
     * @param event The Elvis webhook event to handle
     */
    handle(event) {
        let isUpdateMetadataEvent = (event.type === 'asset_update_metadata');
        let hasAssetDomainMetadata = (event.metadata && event.metadata.assetDomain);
        let hasAssetPathMetadata = (event.metadata && event.metadata.assetPath);
        if (!isUpdateMetadataEvent || !hasAssetDomainMetadata || !hasAssetPathMetadata) {
            console.warn('Received useless event, we\'re only interested in metadata_update events where the assetDomain and assetPath metadata fields are specified. ' +
                'Make sure to configure the Elvis webhook correctly. Event: '
                + JSON.stringify(event));
            return;
        }
        let previewIsReady = (event.changedMetadata && event.changedMetadata.previewState && event.changedMetadata.previewState.newValue === 'yes');
        if (event.metadata.assetPath.startsWith('/Users/') || !previewIsReady || event.metadata.assetDomain !== 'image') {
            // Simply ignore any metadata update that:
            // - Is in the Users folder, the configured API user doesn't have access here
            // - When we don't have a preview
            // - When it's not an image
            return;
        }
        this.recognizer.recognize(event.assetId, null, event.metadata.assetPath);
    }
}
exports.WebhookEndpoint = WebhookEndpoint;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9lbHZpcy13ZWJob29rcy9FbHZpc0ltYWdlUmVjb2duaXRpb24vc3JjL2FwcC93ZWJob29rLWVuZHBvaW50LnRzIiwic291cmNlcyI6WyIvc3J2L2VsdmlzLXdlYmhvb2tzL0VsdmlzSW1hZ2VSZWNvZ25pdGlvbi9zcmMvYXBwL3dlYmhvb2stZW5kcG9pbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQ0FBa0M7QUFDbEMsMENBQTJDO0FBRTNDLDZDQUEwQztBQUMxQyxzQ0FBbUM7QUFHbkM7O0dBRUc7QUFDSDtJQUlFLFlBQW1CLEdBQWdCO1FBQWhCLFFBQUcsR0FBSCxHQUFHLENBQWE7UUFDakMsSUFBSSxnQkFBZ0IsR0FBWSxlQUFNLENBQUMsU0FBUyxLQUFLLEVBQUUsQ0FBQztRQUN4RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksdUJBQVUsQ0FBQyxlQUFNLENBQUMsZUFBZSxFQUFFLGVBQU0sQ0FBQyxhQUFhLEVBQUUsZUFBTSxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3RILENBQUM7SUFFRDs7T0FFRztJQUNJLFNBQVM7UUFDZCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBWSxFQUFFLEdBQWEsRUFBRSxFQUFFO1lBRWpELGtGQUFrRjtZQUNsRix5RkFBeUY7WUFDekYsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV2QixpQ0FBaUM7WUFDakMsSUFBSSxTQUFTLEdBQVcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEdBQUcsU0FBUyxHQUFHLHFEQUFxRCxDQUFDLENBQUM7WUFDMUgsQ0FBQztZQUVELHFCQUFxQjtZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLGlCQUFpQixDQUFDLFNBQWlCLEVBQUUsSUFBWTtRQUN2RCxJQUFJLENBQUM7WUFDSCxJQUFJLElBQUksR0FBZ0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsZUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBQ0QsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNiLE9BQU8sQ0FBQyxJQUFJLENBQUMsNkNBQTZDLEdBQUcsU0FBUyxHQUFHLGFBQWEsR0FBRyxJQUFJLEdBQUcsYUFBYSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ3pILENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ssTUFBTSxDQUFDLEtBQVU7UUFDdkIsSUFBSSxxQkFBcUIsR0FBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssdUJBQXVCLENBQUMsQ0FBQztRQUM5RSxJQUFJLHNCQUFzQixHQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JGLElBQUksb0JBQW9CLEdBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakYsRUFBRSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsSUFBSSxDQUFDLHNCQUFzQixJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQy9FLE9BQU8sQ0FBQyxJQUFJLENBQUMsOElBQThJO2dCQUN6Siw2REFBNkQ7a0JBQzNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUM7UUFDVCxDQUFDO1FBQ0QsSUFBSSxjQUFjLEdBQVksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsWUFBWSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsQ0FBQztRQUNySixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNoSCwwQ0FBMEM7WUFDMUMsNkVBQTZFO1lBQzdFLGlDQUFpQztZQUNqQywyQkFBMkI7WUFDM0IsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDM0UsQ0FBQztDQUNGO0FBN0VELDBDQTZFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjcnlwdG8gPSByZXF1aXJlKCdjcnlwdG8nKTtcbmltcG9ydCBjb21wYXJlID0gcmVxdWlyZSgnc2VjdXJlLWNvbXBhcmUnKTtcblxuaW1wb3J0IHsgUmVjb2duaXplciB9IGZyb20gJy4vcmVjb2duaXplcic7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi9jb25maWcnO1xuaW1wb3J0IHsgQXBwbGljYXRpb24sIFJlcXVlc3QsIFJlc3BvbnNlIH0gZnJvbSAnZXhwcmVzcyc7XG5cbi8qKlxuICogSGFuZGxlIEVsdmlzIHdlYmhvb2sgZXZlbnRzXG4gKi9cbmV4cG9ydCBjbGFzcyBXZWJob29rRW5kcG9pbnQge1xuXG4gIHByaXZhdGUgcmVjb2duaXplcjogUmVjb2duaXplcjtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgYXBwOiBBcHBsaWNhdGlvbikge1xuICAgIGxldCB0cmFuc2xhdGVFbmFibGVkOiBib29sZWFuID0gQ29uZmlnLmxhbmd1YWdlcyAhPT0gJyc7XG4gICAgdGhpcy5yZWNvZ25pemVyID0gbmV3IFJlY29nbml6ZXIoQ29uZmlnLmNsYXJpZmFpRW5hYmxlZCwgQ29uZmlnLmdvb2dsZUVuYWJsZWQsIENvbmZpZy5hd3NFbmFibGVkLCB0cmFuc2xhdGVFbmFibGVkKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZWdpc3RlciBIVFRQIFBvc3Qgcm91dGUgb24gJy8nIGFuZCBsaXN0ZW4gZm9yIEVsdmlzIHdlYmhvb2sgZXZlbnRzXG4gICAqL1xuICBwdWJsaWMgYWRkUm91dGVzKCk6IHZvaWQge1xuICAgIC8vIFJlY29nbml6ZSBBUElcbiAgICB0aGlzLmFwcC5wb3N0KCcvJywgKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkgPT4ge1xuXG4gICAgICAvLyBTZW5kIGEgcmVzcG9uc2UgYmFjayB0byBFbHZpcyBiZWZvcmUgaGFuZGxpbmcgdGhlIGV2ZW50LCBzbyB0aGUgYXBwIHdvbid0IGtlZXAgXG4gICAgICAvLyB0aGUgY29ubmVjdGlvbiBvcGVuIHdoaWxlIGl0J3MgaGFuZGxpbmcgdGhlIGV2ZW50LiBUaGlzIHJlc3VsdHMgaW4gYmV0dGVyIHBlcmZvcm1hbmNlLlxuICAgICAgcmVzLnN0YXR1cygyMDApLnNlbmQoKTtcblxuICAgICAgLy8gVmFsaWRhdGUgdGhlIHdlYmhvb2sgc2lnbmF0dXJlXG4gICAgICBsZXQgc2lnbmF0dXJlOiBzdHJpbmcgPSByZXEuaGVhZGVyKCd4LWhvb2stc2lnbmF0dXJlJyk7XG4gICAgICBpZiAoIXRoaXMudmFsaWRhdGVTaWduYXR1cmUoc2lnbmF0dXJlLCBKU09OLnN0cmluZ2lmeShyZXEuYm9keSkpKSB7XG4gICAgICAgIHJldHVybiBjb25zb2xlLmVycm9yKCdJbnZhbGlkIFdlYkhvb2sgc2lnbmF0dXJlOiAnICsgc2lnbmF0dXJlICsgJy4gTWFrZSBzdXJlIHRoZSBlbHZpc1Rva2VuIGlzIGNvbmZpZ3VyZWQgY29ycmVjdGx5LicpO1xuICAgICAgfVxuXG4gICAgICAvLyBIYW5kbGUgdGhlIGV2ZW50LiBcbiAgICAgIHRoaXMuaGFuZGxlKHJlcS5ib2R5KTtcbiAgICB9KTtcblxuICB9XG5cbiAgLyoqXG4gICAqIFZhbGlkYXRlIEVsdmlzIHdlYmhvb2sgc2lnbmF0dXJlXG4gICAqIFxuICAgKiBAcGFyYW0gc2lnbmF0dXJlIFJlcXVlc3QgaGVhZGVyIHNpZ25hdHVyZVxuICAgKiBAcGFyYW0gZGF0YSBSZXF1ZXN0IGRhdGEgdG8gdmFsaWRhdGVcbiAgICovXG4gIHByaXZhdGUgdmFsaWRhdGVTaWduYXR1cmUoc2lnbmF0dXJlOiBzdHJpbmcsIGRhdGE6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHRyeSB7XG4gICAgICBsZXQgaG1hYzogY3J5cHRvLkhtYWMgPSBjcnlwdG8uY3JlYXRlSG1hYygnc2hhMjU2JywgQ29uZmlnLmVsdmlzVG9rZW4pO1xuICAgICAgaG1hYy51cGRhdGUoZGF0YSk7XG4gICAgICBsZXQgZGlnZXN0OiBzdHJpbmcgPSBobWFjLmRpZ2VzdCgnaGV4Jyk7XG4gICAgICByZXR1cm4gY29tcGFyZShkaWdlc3QsIHNpZ25hdHVyZSk7XG4gICAgfVxuICAgIGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS53YXJuKCdTaWduYXR1cmUgdmFsaWRhdGlvbiBmYWlsZWQgZm9yIHNpZ25hdHVyZTogJyArIHNpZ25hdHVyZSArICcgYW5kIGRhdGE6ICcgKyBkYXRhICsgJzsgZGV0YWlsczogJyArIGVycm9yKTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZXMgdGhlIGFzc2V0X3VwZGF0ZV9tZXRhZGF0YSBFbHZpcyBTZXJ2ZXIgZXZlbnQgYW5kIFxuICAgKiBzdGFydHMgdGhlIHJlY29nbml0aW9uIHByb2Nlc3MgaWYgdGhlIGNvcnJlY3QgbWV0YWRhdGEgaXMgc3BlY2lmaWVkXG4gICAqIFxuICAgKiBAcGFyYW0gZXZlbnQgVGhlIEVsdmlzIHdlYmhvb2sgZXZlbnQgdG8gaGFuZGxlXG4gICAqL1xuICBwcml2YXRlIGhhbmRsZShldmVudDogYW55KTogdm9pZCB7XG4gICAgbGV0IGlzVXBkYXRlTWV0YWRhdGFFdmVudDogYm9vbGVhbiA9IChldmVudC50eXBlID09PSAnYXNzZXRfdXBkYXRlX21ldGFkYXRhJyk7XG4gICAgbGV0IGhhc0Fzc2V0RG9tYWluTWV0YWRhdGE6IGJvb2xlYW4gPSAoZXZlbnQubWV0YWRhdGEgJiYgZXZlbnQubWV0YWRhdGEuYXNzZXREb21haW4pO1xuICAgIGxldCBoYXNBc3NldFBhdGhNZXRhZGF0YTogYm9vbGVhbiA9IChldmVudC5tZXRhZGF0YSAmJiBldmVudC5tZXRhZGF0YS5hc3NldFBhdGgpO1xuICAgIGlmICghaXNVcGRhdGVNZXRhZGF0YUV2ZW50IHx8ICFoYXNBc3NldERvbWFpbk1ldGFkYXRhIHx8ICFoYXNBc3NldFBhdGhNZXRhZGF0YSkge1xuICAgICAgY29uc29sZS53YXJuKCdSZWNlaXZlZCB1c2VsZXNzIGV2ZW50LCB3ZVxcJ3JlIG9ubHkgaW50ZXJlc3RlZCBpbiBtZXRhZGF0YV91cGRhdGUgZXZlbnRzIHdoZXJlIHRoZSBhc3NldERvbWFpbiBhbmQgYXNzZXRQYXRoIG1ldGFkYXRhIGZpZWxkcyBhcmUgc3BlY2lmaWVkLiAnICtcbiAgICAgICAgJ01ha2Ugc3VyZSB0byBjb25maWd1cmUgdGhlIEVsdmlzIHdlYmhvb2sgY29ycmVjdGx5LiBFdmVudDogJ1xuICAgICAgICArIEpTT04uc3RyaW5naWZ5KGV2ZW50KSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBwcmV2aWV3SXNSZWFkeTogYm9vbGVhbiA9IChldmVudC5jaGFuZ2VkTWV0YWRhdGEgJiYgZXZlbnQuY2hhbmdlZE1ldGFkYXRhLnByZXZpZXdTdGF0ZSAmJiBldmVudC5jaGFuZ2VkTWV0YWRhdGEucHJldmlld1N0YXRlLm5ld1ZhbHVlID09PSAneWVzJyk7XG4gICAgaWYgKGV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aC5zdGFydHNXaXRoKCcvVXNlcnMvJykgfHwgIXByZXZpZXdJc1JlYWR5IHx8IGV2ZW50Lm1ldGFkYXRhLmFzc2V0RG9tYWluICE9PSAnaW1hZ2UnKSB7XG4gICAgICAvLyBTaW1wbHkgaWdub3JlIGFueSBtZXRhZGF0YSB1cGRhdGUgdGhhdDpcbiAgICAgIC8vIC0gSXMgaW4gdGhlIFVzZXJzIGZvbGRlciwgdGhlIGNvbmZpZ3VyZWQgQVBJIHVzZXIgZG9lc24ndCBoYXZlIGFjY2VzcyBoZXJlXG4gICAgICAvLyAtIFdoZW4gd2UgZG9uJ3QgaGF2ZSBhIHByZXZpZXdcbiAgICAgIC8vIC0gV2hlbiBpdCdzIG5vdCBhbiBpbWFnZVxuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnJlY29nbml6ZXIucmVjb2duaXplKGV2ZW50LmFzc2V0SWQsIG51bGwsIGV2ZW50Lm1ldGFkYXRhLmFzc2V0UGF0aCk7XG4gIH1cbn1cbiJdfQ==