/**
* eventhandler.js
* ----------
* Module containing event handling logic.
*/

// DEPENDENCIES
// =============================================================================
const file = require('./file.js')();

const serverUrl = process.env.ELVIS_SERVER_URL;

const request = require('./elvis-request.js')({
	serverUrl: serverUrl,
	useBaseUrl: true,
	username: process.env.ELVIS_USERNAME,
	password: process.env.ELVIS_PASSWORD,
	parseJSON: true
});

var exports = {}

exports.handle = (event) => {
	//Log event
	console.log('Event ' + JSON.stringify(event, null, 2));

	//Using a switch to make it easy to add additional event types in the future.
	switch (event.type) {
		case "asset_create":
			handleAssetCreate(event);
			break;
		case "asset_update_metadata":
			handleMetadataUpdate(event);
			break;
		default:
			break;
	}
}

function handleAssetCreate(event) {
	console.log("Handling event: handleAssetCreate") ;

	//
	if (event.metadata.assetModifier == process.env.ELVIS_USERNAME) {
		console.log("Detected possible recursive WebHook, assetModifier = ["+event.metadata.assetModifier+"], acting user = ["+process.env.ELVIS_USERNAME+"]; bail out") ; 
		return ;	
	} ;

} 

function handleMetadataUpdate(event) {

	//
	if (event.metadata.assetModifier == process.env.ELVIS_USERNAME) {
		console.log("Detected possible recursive WebHook, assetModifier = ["+event.metadata.assetModifier+"], acting user = ["+process.env.ELVIS_USERNAME+"]; bail out") ; 
		return ;	
	} ;

	if (event.metadata.assetDomain != "image" ) {
		console.log("Not an image, it's a ["+event.metadata.assetDomain+"]; bail out") ; 
		return ;	
	} ;

	if (event.changedMetadata.cf_MaxAllowedDimensions) {
		console.log("Detected cf_MaxAllowedDimensions change; bail out") ; 
		return ;	
	} ;

	if (!event.metadata.width || !event.metadata.height) {
		console.log("No width/height supplied; bail out") ; 
		return ;	
	}
	
	var expectedRootFolder="BANCOS" ;
	var fields = event.metadata.folderPath.split('/');
	if (fields[1].toLowerCase() != expectedRootFolder.toLowerCase() ) {
		console.log("Not in the expected root folder ["+expectedRootFolder+"], but in ["+fields[1]+"]; bail out") ; 
		return ;	
	} ;

	//
	// Calculate cf_MaxAllowedDimensions
	var width = event.metadata.width ;
	var height = event.metadata.height ;
	var cf_MaxAllowedDimensions = event.metadata.cf_MaxAllowedDimensions
	var myWidth=(parseInt(width)/300)*25.4 ;
	var myHeight=(parseInt(height)/300)*25.4 ;
	var newcf_MaxAllowedDimensions= "Largura: " + myWidth.toFixed(2) + " mm " + "Altura: " + myHeight.toFixed(2)  + " mm" ; 

	//
	console.log("Current cf_MaxAllowedDimensions:  [" + cf_MaxAllowedDimensions +"]") ;
	console.log("New cf_MaxAllowedDimensions    :  [" + newcf_MaxAllowedDimensions +"]") ;

	if (cf_MaxAllowedDimensions == newcf_MaxAllowedDimensions) {
		console.log("cf_MaxAllowedDimensions NOT to be updated; bail out") ;
		return ;
	} ;

	//Update the cf_MaxAllowedDimensions
	var options = {
		url: '/services/update?id=' + event.assetId + '&metadata=' + JSON.stringify({ cf_MaxAllowedDimensions: newcf_MaxAllowedDimensions }),
		method: 'PUT'
	}
	request(options).then(result => {
		console.log("cf_MaxAllowedDimensions updated") ;
	}).catch(err => {
		console.log(err);
	}) 

	
}

module.exports = exports;
