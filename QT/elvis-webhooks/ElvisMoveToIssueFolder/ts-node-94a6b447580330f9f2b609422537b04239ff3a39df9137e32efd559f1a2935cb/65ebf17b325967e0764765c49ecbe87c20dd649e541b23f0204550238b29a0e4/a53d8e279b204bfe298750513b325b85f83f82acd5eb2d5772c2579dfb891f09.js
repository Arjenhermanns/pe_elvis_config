"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const api_1 = require("../elvis-api/api");
/**
 * Singleton class that ensures only one Elvis API session is used.
 */
class ApiManager {
    /**
     * Create an Elvis API instance
     */
    static getApi() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance.api;
    }
    constructor() {
        // Log into Elvis
        this.api = new api_1.ElvisApi(config_1.Config.elvisUsername, config_1.Config.elvisPassword, config_1.Config.elvisUrl);
    }
}
exports.ApiManager = ApiManager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL2VsdmlzLWFwaS9hcGktbWFuYWdlci50cyIsInNvdXJjZXMiOlsiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL2VsdmlzLWFwaS9hcGktbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFtQztBQUNuQywwQ0FBNEM7QUFFNUM7O0dBRUc7QUFDSDtJQU1FOztPQUVHO0lBQ0ksTUFBTSxDQUFDLE1BQU07UUFDbEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDN0IsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztJQUMzQixDQUFDO0lBRUQ7UUFDRSxpQkFBaUI7UUFDakIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLGNBQVEsQ0FBQyxlQUFNLENBQUMsYUFBYSxFQUFFLGVBQU0sQ0FBQyxhQUFhLEVBQUUsZUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7Q0FFRjtBQXJCRCxnQ0FxQkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi9jb25maWcnO1xuaW1wb3J0IHsgRWx2aXNBcGkgfSBmcm9tICcuLi9lbHZpcy1hcGkvYXBpJztcblxuLyoqXG4gKiBTaW5nbGV0b24gY2xhc3MgdGhhdCBlbnN1cmVzIG9ubHkgb25lIEVsdmlzIEFQSSBzZXNzaW9uIGlzIHVzZWQuXG4gKi9cbmV4cG9ydCBjbGFzcyBBcGlNYW5hZ2VyIHtcblxuICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogQXBpTWFuYWdlcjtcblxuICBwdWJsaWMgYXBpOiBFbHZpc0FwaTtcblxuICAvKipcbiAgICogQ3JlYXRlIGFuIEVsdmlzIEFQSSBpbnN0YW5jZVxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRBcGkoKTogRWx2aXNBcGkge1xuICAgIGlmICghdGhpcy5pbnN0YW5jZSkge1xuICAgICAgdGhpcy5pbnN0YW5jZSA9IG5ldyB0aGlzKCk7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmluc3RhbmNlLmFwaTtcbiAgfVxuXG4gIHByaXZhdGUgY29uc3RydWN0b3IoKSB7XG4gICAgLy8gTG9nIGludG8gRWx2aXNcbiAgICB0aGlzLmFwaSA9IG5ldyBFbHZpc0FwaShDb25maWcuZWx2aXNVc2VybmFtZSwgQ29uZmlnLmVsdmlzUGFzc3dvcmQsIENvbmZpZy5lbHZpc1VybCk7XG4gIH1cblxufSJdfQ==