"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const fs = require("fs");
var request = require('request').defaults({ jar: true });
class HttpError extends Error {
    constructor(message, statusCode, options) {
        super(message);
        this.message = message;
        this.statusCode = statusCode;
        this.options = options;
    }
}
exports.HttpError = HttpError;
class ElvisRequest {
    constructor(serverUrl, username, password) {
        this.serverUrl = serverUrl;
        this.username = username;
        this.password = password;
    }
    // TODO: The request and requestFile code is long and redundant. not sure yet how to minimize code without losing functionality
    request(options) {
        return this.apiRequest(options).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.apiRequest(options);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.apiRequest(options);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    requestFile(url, destination) {
        return this.fileRequest(url, destination).catch(error => {
            if (error.statusCode == 401) {
                if (!this.auth) {
                    // Not logged in, login first
                    this.auth = this.authenticate().then(() => {
                        // Retry initial call
                        this.auth = null;
                        return this.fileRequest(url, destination);
                    });
                    return this.auth;
                }
                else {
                    console.log('Already logging in, waiting for login to finish...');
                    return this.auth.then(() => {
                        // Retry initial call
                        return this.fileRequest(url, destination);
                    });
                }
            }
            else {
                throw error;
            }
        });
    }
    authenticate() {
        var options = {
            method: 'POST',
            url: this.serverUrl + '/services/login?username=' + this.username + '&password=' + this.password
        };
        console.info('Not logged in, logging in...');
        return this.apiRequest(options).then(response => {
            if (!response.loginSuccess) {
                throw new HttpError(response.loginFaultMessage, 401, options);
            }
            else {
                console.info('Login successful!');
                if (response.csrfToken) {
                    // Elvis 6+ login
                    this.csrfToken = response.csrfToken;
                }
                return response;
            }
        });
    }
    addCsrfToken(options) {
        if (this.csrfToken) {
            // Elvis 6+
            if (!options.headers) {
                options.headers = {};
            }
            options.headers['X-CSRF-TOKEN'] = this.csrfToken;
        }
    }
    apiRequest(options) {
        return new Promise((resolve, reject) => {
            options.json = true;
            this.addCsrfToken(options);
            request(options, (error, response, body) => {
                if (error) {
                    // Handle generic errors, for example unknown host
                    reject(new HttpError('Elvis request failed: ' + error, 0, options));
                }
                if (body.errorcode) {
                    response.statusCode = body.errorcode;
                    response.statusMessage = body.message;
                }
                if (response.statusCode < 200 || response.statusCode > 299) {
                    // Handle Elvis HTTP errors: 404, 401, 500, etc
                    reject(new HttpError('Elvis request failed: ' + response.statusMessage, response.statusCode, options));
                }
                else {
                    // All good, return API response
                    resolve(body);
                }
            });
        });
    }
    fileRequest(url, destination) {
        return new Promise((resolve, reject) => {
            let errorMsg = 'Download of ' + url + ' to: ' + destination + ' failed';
            let file = fs.createWriteStream(destination);
            let options = {
                method: 'GET',
                url: url
            };
            this.addCsrfToken(options);
            let req = request(options)
                .on('error', (error) => {
                // Handle generic errors when getting the file, for example unknown host
                reject(new Error(errorMsg + ': ' + error));
            })
                .on('response', response => {
                // Handle Elvis HTTP errors: 404, 401, 500, etc
                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new HttpError(errorMsg, response.statusCode, options));
                }
                // Request went well, let's start piping the data...
                req.pipe(file)
                    .on('error', (error) => {
                    // Handle piping errors: unable to write file, stream closed, ...
                    reject(new Error(errorMsg + ': ' + error));
                })
                    .on('finish', () => {
                    // Piping complete, we've got the file!
                    resolve(destination);
                });
            });
        });
    }
}
exports.ElvisRequest = ElvisRequest;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Nydi9FbHZpc1dlYkhvb2tzL0VsdmlzTW92ZVRvSXNzdWVGb2xkZXIvc3JjL2VsdmlzLWFwaS9lbHZpcy1yZXF1ZXN0LnRzIiwic291cmNlcyI6WyIvc3J2L0VsdmlzV2ViSG9va3MvRWx2aXNNb3ZlVG9Jc3N1ZUZvbGRlci9zcmMvZWx2aXMtYXBpL2VsdmlzLXJlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvQ0FBcUM7QUFFckMseUJBQTBCO0FBRTFCLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztBQUV6RCxlQUF1QixTQUFRLEtBQUs7SUFDbEMsWUFBbUIsT0FBZSxFQUFTLFVBQWtCLEVBQVMsT0FBZ0U7UUFDcEksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBREUsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUFTLGVBQVUsR0FBVixVQUFVLENBQVE7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUF5RDtJQUV0SSxDQUFDO0NBQ0Y7QUFKRCw4QkFJQztBQUVEO0lBS0UsWUFBb0IsU0FBaUIsRUFBVSxRQUFnQixFQUFVLFFBQWdCO1FBQXJFLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBUTtJQUN6RixDQUFDO0lBRUQsK0hBQStIO0lBRXhILE9BQU8sQ0FBQyxPQUFnRTtRQUM3RSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSztZQUN6QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2YsNkJBQTZCO29CQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQ25DLHFCQUFxQjt3QkFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7d0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNsQyxDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7b0JBQ2xFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDcEIscUJBQXFCO3dCQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLEtBQUssQ0FBQztZQUNkLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxXQUFXLENBQUMsR0FBVyxFQUFFLFdBQW1CO1FBQ2pELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSztZQUNuRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2YsNkJBQTZCO29CQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQ25DLHFCQUFxQjt3QkFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7d0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDNUMsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO29CQUNsRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ3BCLHFCQUFxQjt3QkFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE1BQU0sS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFlBQVk7UUFDbEIsSUFBSSxPQUFPLEdBQUc7WUFDWixNQUFNLEVBQUUsTUFBTTtZQUNkLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLDJCQUEyQixHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRO1NBQ2pHLENBQUE7UUFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDM0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDM0IsTUFBTSxJQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2hFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ2xDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUN2QixpQkFBaUI7b0JBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDdEMsQ0FBQztnQkFDRCxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ2xCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxZQUFZLENBQUMsT0FBTztRQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNuQixXQUFXO1lBQ1gsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDckIsT0FBTyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDdkIsQ0FBQztZQUNELE9BQU8sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNuRCxDQUFDO0lBQ0gsQ0FBQztJQUVPLFVBQVUsQ0FBQyxPQUFnRTtRQUNqRixNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUNqQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUk7Z0JBQ3JDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ1Ysa0RBQWtEO29CQUNsRCxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsd0JBQXdCLEdBQUcsS0FBSyxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN0RSxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNuQixRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3JDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDeEMsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzNELCtDQUErQztvQkFDL0MsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDLHdCQUF3QixHQUFHLFFBQVEsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN6RyxDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNKLGdDQUFnQztvQkFDaEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxXQUFXLENBQUMsR0FBVyxFQUFFLFdBQW1CO1FBQ2xELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3pDLElBQUksUUFBUSxHQUFXLGNBQWMsR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxTQUFTLENBQUM7WUFDaEYsSUFBSSxJQUFJLEdBQW1CLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM3RCxJQUFJLE9BQU8sR0FBUTtnQkFDakIsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsR0FBRyxFQUFFLEdBQUc7YUFDVCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUUzQixJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO2lCQUN2QixFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSztnQkFDakIsd0VBQXdFO2dCQUN4RSxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQztpQkFDRCxFQUFFLENBQUMsVUFBVSxFQUFFLFFBQVE7Z0JBRXRCLCtDQUErQztnQkFDL0MsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksUUFBUSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEUsQ0FBQztnQkFFRCxvREFBb0Q7Z0JBQ3BELEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3FCQUNYLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLO29CQUNqQixpRUFBaUU7b0JBQ2pFLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLENBQUMsQ0FBQztxQkFDRCxFQUFFLENBQUMsUUFBUSxFQUFFO29CQUNaLHVDQUF1QztvQkFDdkMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN2QixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUF4SkQsb0NBd0pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb21pc2UgPSByZXF1aXJlKCdibHVlYmlyZCcpO1xuaW1wb3J0IHsgVXJpT3B0aW9ucywgVXJsT3B0aW9ucywgQ29yZU9wdGlvbnMgfSBmcm9tICdyZXF1ZXN0JztcbmltcG9ydCBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG5cbnZhciByZXF1ZXN0ID0gcmVxdWlyZSgncmVxdWVzdCcpLmRlZmF1bHRzKHsgamFyOiB0cnVlIH0pO1xuXG5leHBvcnQgY2xhc3MgSHR0cEVycm9yIGV4dGVuZHMgRXJyb3Ige1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgbWVzc2FnZTogc3RyaW5nLCBwdWJsaWMgc3RhdHVzQ29kZTogbnVtYmVyLCBwdWJsaWMgb3B0aW9uczogKFVyaU9wdGlvbnMgJiBDb3JlT3B0aW9ucykgfCAoVXJsT3B0aW9ucyAmIENvcmVPcHRpb25zKSkge1xuICAgIHN1cGVyKG1lc3NhZ2UpO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBFbHZpc1JlcXVlc3Qge1xuXG4gIHByaXZhdGUgY3NyZlRva2VuOiBzdHJpbmc7XG4gIHByaXZhdGUgYXV0aDogUHJvbWlzZTxhbnk+XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXJ2ZXJVcmw6IHN0cmluZywgcHJpdmF0ZSB1c2VybmFtZTogc3RyaW5nLCBwcml2YXRlIHBhc3N3b3JkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIC8vIFRPRE86IFRoZSByZXF1ZXN0IGFuZCByZXF1ZXN0RmlsZSBjb2RlIGlzIGxvbmcgYW5kIHJlZHVuZGFudC4gbm90IHN1cmUgeWV0IGhvdyB0byBtaW5pbWl6ZSBjb2RlIHdpdGhvdXQgbG9zaW5nIGZ1bmN0aW9uYWxpdHlcblxuICBwdWJsaWMgcmVxdWVzdChvcHRpb25zOiAoVXJpT3B0aW9ucyAmIENvcmVPcHRpb25zKSB8IChVcmxPcHRpb25zICYgQ29yZU9wdGlvbnMpKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIGlmIChlcnJvci5zdGF0dXNDb2RlID09IDQwMSkge1xuICAgICAgICBpZiAoIXRoaXMuYXV0aCkge1xuICAgICAgICAgIC8vIE5vdCBsb2dnZWQgaW4sIGxvZ2luIGZpcnN0XG4gICAgICAgICAgdGhpcy5hdXRoID0gdGhpcy5hdXRoZW50aWNhdGUoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIC8vIFJldHJ5IGluaXRpYWwgY2FsbFxuICAgICAgICAgICAgdGhpcy5hdXRoID0gbnVsbDtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaVJlcXVlc3Qob3B0aW9ucyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnQWxyZWFkeSBsb2dnaW5nIGluLCB3YWl0aW5nIGZvciBsb2dpbiB0byBmaW5pc2guLi4nKTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcGlSZXF1ZXN0KG9wdGlvbnMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyByZXF1ZXN0RmlsZSh1cmw6IHN0cmluZywgZGVzdGluYXRpb246IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZmlsZVJlcXVlc3QodXJsLCBkZXN0aW5hdGlvbikuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgaWYgKGVycm9yLnN0YXR1c0NvZGUgPT0gNDAxKSB7XG4gICAgICAgIGlmICghdGhpcy5hdXRoKSB7XG4gICAgICAgICAgLy8gTm90IGxvZ2dlZCBpbiwgbG9naW4gZmlyc3RcbiAgICAgICAgICB0aGlzLmF1dGggPSB0aGlzLmF1dGhlbnRpY2F0ZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICB0aGlzLmF1dGggPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmlsZVJlcXVlc3QodXJsLCBkZXN0aW5hdGlvbik7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuYXV0aDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnQWxyZWFkeSBsb2dnaW5nIGluLCB3YWl0aW5nIGZvciBsb2dpbiB0byBmaW5pc2guLi4nKTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5hdXRoLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gUmV0cnkgaW5pdGlhbCBjYWxsXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5maWxlUmVxdWVzdCh1cmwsIGRlc3RpbmF0aW9uKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGF1dGhlbnRpY2F0ZSgpOiBQcm9taXNlPGFueT4ge1xuICAgIHZhciBvcHRpb25zID0ge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICB1cmw6IHRoaXMuc2VydmVyVXJsICsgJy9zZXJ2aWNlcy9sb2dpbj91c2VybmFtZT0nICsgdGhpcy51c2VybmFtZSArICcmcGFzc3dvcmQ9JyArIHRoaXMucGFzc3dvcmRcbiAgICB9XG4gICAgY29uc29sZS5pbmZvKCdOb3QgbG9nZ2VkIGluLCBsb2dnaW5nIGluLi4uJyk7XG4gICAgcmV0dXJuIHRoaXMuYXBpUmVxdWVzdChvcHRpb25zKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIGlmICghcmVzcG9uc2UubG9naW5TdWNjZXNzKSB7XG4gICAgICAgIHRocm93IG5ldyBIdHRwRXJyb3IocmVzcG9uc2UubG9naW5GYXVsdE1lc3NhZ2UsIDQwMSwgb3B0aW9ucyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmluZm8oJ0xvZ2luIHN1Y2Nlc3NmdWwhJyk7XG4gICAgICAgIGlmIChyZXNwb25zZS5jc3JmVG9rZW4pIHtcbiAgICAgICAgICAvLyBFbHZpcyA2KyBsb2dpblxuICAgICAgICAgIHRoaXMuY3NyZlRva2VuID0gcmVzcG9uc2UuY3NyZlRva2VuO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYWRkQ3NyZlRva2VuKG9wdGlvbnMpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5jc3JmVG9rZW4pIHtcbiAgICAgIC8vIEVsdmlzIDYrXG4gICAgICBpZiAoIW9wdGlvbnMuaGVhZGVycykge1xuICAgICAgICBvcHRpb25zLmhlYWRlcnMgPSB7fTtcbiAgICAgIH1cbiAgICAgIG9wdGlvbnMuaGVhZGVyc1snWC1DU1JGLVRPS0VOJ10gPSB0aGlzLmNzcmZUb2tlbjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFwaVJlcXVlc3Qob3B0aW9uczogKFVyaU9wdGlvbnMgJiBDb3JlT3B0aW9ucykgfCAoVXJsT3B0aW9ucyAmIENvcmVPcHRpb25zKSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIG9wdGlvbnMuanNvbiA9IHRydWU7XG4gICAgICB0aGlzLmFkZENzcmZUb2tlbihvcHRpb25zKTtcbiAgICAgIHJlcXVlc3Qob3B0aW9ucywgKGVycm9yLCByZXNwb25zZSwgYm9keSkgPT4ge1xuICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAvLyBIYW5kbGUgZ2VuZXJpYyBlcnJvcnMsIGZvciBleGFtcGxlIHVua25vd24gaG9zdFxuICAgICAgICAgIHJlamVjdChuZXcgSHR0cEVycm9yKCdFbHZpcyByZXF1ZXN0IGZhaWxlZDogJyArIGVycm9yLCAwLCBvcHRpb25zKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYm9keS5lcnJvcmNvZGUpIHtcbiAgICAgICAgICByZXNwb25zZS5zdGF0dXNDb2RlID0gYm9keS5lcnJvcmNvZGU7XG4gICAgICAgICAgcmVzcG9uc2Uuc3RhdHVzTWVzc2FnZSA9IGJvZHkubWVzc2FnZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXNDb2RlIDwgMjAwIHx8IHJlc3BvbnNlLnN0YXR1c0NvZGUgPiAyOTkpIHtcbiAgICAgICAgICAvLyBIYW5kbGUgRWx2aXMgSFRUUCBlcnJvcnM6IDQwNCwgNDAxLCA1MDAsIGV0Y1xuICAgICAgICAgIHJlamVjdChuZXcgSHR0cEVycm9yKCdFbHZpcyByZXF1ZXN0IGZhaWxlZDogJyArIHJlc3BvbnNlLnN0YXR1c01lc3NhZ2UsIHJlc3BvbnNlLnN0YXR1c0NvZGUsIG9wdGlvbnMpKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAvLyBBbGwgZ29vZCwgcmV0dXJuIEFQSSByZXNwb25zZVxuICAgICAgICAgIHJlc29sdmUoYm9keSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBmaWxlUmVxdWVzdCh1cmw6IHN0cmluZywgZGVzdGluYXRpb246IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPHN0cmluZz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IGVycm9yTXNnOiBzdHJpbmcgPSAnRG93bmxvYWQgb2YgJyArIHVybCArICcgdG86ICcgKyBkZXN0aW5hdGlvbiArICcgZmFpbGVkJztcbiAgICAgIGxldCBmaWxlOiBmcy5Xcml0ZVN0cmVhbSA9IGZzLmNyZWF0ZVdyaXRlU3RyZWFtKGRlc3RpbmF0aW9uKTtcbiAgICAgIGxldCBvcHRpb25zOiBhbnkgPSB7XG4gICAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICAgIHVybDogdXJsXG4gICAgICB9O1xuICAgICAgdGhpcy5hZGRDc3JmVG9rZW4ob3B0aW9ucyk7XG5cbiAgICAgIGxldCByZXEgPSByZXF1ZXN0KG9wdGlvbnMpXG4gICAgICAgIC5vbignZXJyb3InLCAoZXJyb3IpID0+IHtcbiAgICAgICAgICAvLyBIYW5kbGUgZ2VuZXJpYyBlcnJvcnMgd2hlbiBnZXR0aW5nIHRoZSBmaWxlLCBmb3IgZXhhbXBsZSB1bmtub3duIGhvc3RcbiAgICAgICAgICByZWplY3QobmV3IEVycm9yKGVycm9yTXNnICsgJzogJyArIGVycm9yKSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5vbigncmVzcG9uc2UnLCByZXNwb25zZSA9PiB7XG5cbiAgICAgICAgICAvLyBIYW5kbGUgRWx2aXMgSFRUUCBlcnJvcnM6IDQwNCwgNDAxLCA1MDAsIGV0Y1xuICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXNDb2RlIDwgMjAwIHx8IHJlc3BvbnNlLnN0YXR1c0NvZGUgPiAyOTkpIHtcbiAgICAgICAgICAgIHJlamVjdChuZXcgSHR0cEVycm9yKGVycm9yTXNnLCByZXNwb25zZS5zdGF0dXNDb2RlLCBvcHRpb25zKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gUmVxdWVzdCB3ZW50IHdlbGwsIGxldCdzIHN0YXJ0IHBpcGluZyB0aGUgZGF0YS4uLlxuICAgICAgICAgIHJlcS5waXBlKGZpbGUpXG4gICAgICAgICAgICAub24oJ2Vycm9yJywgKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgIC8vIEhhbmRsZSBwaXBpbmcgZXJyb3JzOiB1bmFibGUgdG8gd3JpdGUgZmlsZSwgc3RyZWFtIGNsb3NlZCwgLi4uXG4gICAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoZXJyb3JNc2cgKyAnOiAnICsgZXJyb3IpKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2ZpbmlzaCcsICgpID0+IHtcbiAgICAgICAgICAgICAgLy8gUGlwaW5nIGNvbXBsZXRlLCB3ZSd2ZSBnb3QgdGhlIGZpbGUhXG4gICAgICAgICAgICAgIHJlc29sdmUoZGVzdGluYXRpb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59Il19